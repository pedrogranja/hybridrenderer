#include "DepthImage.h"

void DepthImage::init(VkPhysicalDevice physicalDevice,
                      VkDevice device,
                      uint32_t width,
                      uint32_t height,
                      VkSampleCountFlagBits sampleCount)
{
	this->physicalDevice = physicalDevice;
    this->device = device;

	this->depthFormat = utils::findSupportedFormat(
                                            physicalDevice,
                                            { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
                                            VK_IMAGE_TILING_OPTIMAL,
                                            VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
                                        );

    utils::createImage(device,
                       physicalDevice,
                       width,
                       height,
                       this->depthFormat,
                       VK_IMAGE_TILING_OPTIMAL,
                       VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                       VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                       depthImage,
                       depthImageMemory,
                       1,
                       0,
                       1,
                       sampleCount);

    utils::createImageView(device, depthImage, this->depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, depthImageView);
}

void DepthImage::cleanup()
{
    vkDestroyImageView(device, depthImageView, nullptr);
    vkDestroyImage(device, depthImage, nullptr);
    vkFreeMemory(device, depthImageMemory, nullptr);
}
