#include "SceneGraph.h"

static int idGeneratorSceneNodes = 1;
static int idGeneratorPrimitive = 0;

Material::Material() {
	inDeviceMemory = false;
}

Material::Material(std::string name, 
				   int albedoTextureIndex,
				   int normalTextureIndex, 
				   int aoTextureIndex,
				   int emissionTextureIndex,
				   int metalRoughnessTextureIndex,
				   glm::vec3 baseColor, 
				   float metallicFactor,
				   float roughnessFactor,
				   std::vector<TextureImage*>& textures) : 
														   name(name),
														   albedoTextureIndex(albedoTextureIndex),
														   normalTextureIndex(normalTextureIndex),
														   aoTextureIndex(aoTextureIndex),
														   emissionTextureIndex(emissionTextureIndex),
														   metalRoughnessTextureIndex(metalRoughnessTextureIndex),
														   baseColor(baseColor), 
														   metallicFactor(metallicFactor),
														   roughnessFactor(roughnessFactor),
														   textures(&textures)
{
	raytracingMaterials.albedoTextureIndex = albedoTextureIndex;
	raytracingMaterials.normalTextureIndex = normalTextureIndex;
	raytracingMaterials.aoTextureIndex = aoTextureIndex;
	raytracingMaterials.emissionTextureIndex = emissionTextureIndex;
	raytracingMaterials.metalRoughnessTextureIndex = metalRoughnessTextureIndex;
	raytracingMaterials.baseColor = baseColor;
	raytracingMaterials.metallicFactor = metallicFactor;
	raytracingMaterials.roughnessFactor = roughnessFactor;

	matParams.albedoTextureSet = albedoTextureIndex;
	matParams.normalTextureSet = normalTextureIndex;
	matParams.occlusionTextureSet = aoTextureIndex;
	matParams.emissiveTextureSet = emissionTextureIndex;
	matParams.metallicRoughnessTextureSet = metalRoughnessTextureIndex;
	matParams.baseColor = baseColor;
	matParams.metallicFactor = metallicFactor;
	matParams.roughnessFactor = roughnessFactor;

	imGuiDisplay.albedoTextureSet = albedoTextureIndex > -1 ? true : false;
	imGuiDisplay.metallicFactor = metallicFactor;
	imGuiDisplay.roughnessFactor = roughnessFactor;
	imGuiDisplay.baseColor = baseColor;

	inDeviceMemory = false;
}

void Material::cleanup()
{
	this->uniformBuffer.cleanup();
	this->descriptorSet.cleanup();
}

TextureImage* Material::getAlbedoTexture()
{
	if (matParams.albedoTextureSet <= -1)
		return nullptr;

	std::vector<TextureImage*>& textures = *this->textures;
	return textures[albedoTextureIndex];
}

TextureImage* Material::getNormalTexture()
{
	if (matParams.normalTextureSet <= -1)
		return nullptr;

	std::vector<TextureImage*>& textures = *this->textures;
	return textures[normalTextureIndex];
}

TextureImage* Material::getAoTexture()
{
	if (matParams.occlusionTextureSet <= -1)
		return nullptr;

	std::vector<TextureImage*>& textures = *this->textures;
	return textures[aoTextureIndex];
}

TextureImage* Material::getEmissionTexture()
{
	if (matParams.emissiveTextureSet <= -1)
		return nullptr;

	std::vector<TextureImage*>& textures = *this->textures;
	return textures[emissionTextureIndex];
}

TextureImage* Material::getMetalRoughnessTexture()
{
	if (matParams.metallicRoughnessTextureSet <= -1)
		return nullptr;

	std::vector<TextureImage*>& textures = *this->textures;
	return textures[metalRoughnessTextureIndex];
}

Primitive::Primitive(uint32_t firstIndex,
					 uint32_t indexCount,
					 uint32_t firstVertex,
					 uint32_t vertexCount, 
					 int materialIndex,
					 int customId,
					 std::vector<Material*>* materials) : firstIndex(firstIndex),
														  indexCount(indexCount),
														  firstVertex(firstVertex),
														  vertexCount(vertexCount),
														  materialIndex(materialIndex),
														  id(customId),
														  materials(materials), 
														  bottomLevelAs(nullptr)
{
}

Material* Primitive::getMaterial()
{
	std::vector<Material*>& materials = *this->materials;
	return materials[materialIndex];
}

void Primitive::cleanup() {
	bottomLevelAs->cleanup();
	if(bottomLevelAs != nullptr)
		delete bottomLevelAs;
	delete this;
}

void Mesh::cleanup() {
	for (int i = 0; i < primitives.size(); i++) {
		primitives[i]->cleanup();
	}

	delete this;
}

SceneNode::SceneNode(std::string name, SceneNode* parent, uint32_t nodeIndex) {
	this->name = name;
	this->mesh = nullptr;
	this->parent = nullptr;
	this->id = idGeneratorSceneNodes++;

	if (parent != nullptr) {
		this->parent = parent;
		this->parent->children.push_back(this);
	}

	this->nodeIndex = nodeIndex;
}

glm::mat4 SceneNode::getLocalMatrix()
{
	return glm::translate(glm::mat4(1.0f), this->translation)			//translate
						  * glm::mat4(this->rotation)								//rotate
						  * glm::scale(glm::mat4(1.0f), scale);						//scale
}

glm::mat4 SceneNode::getWorldMatrix()
{
	glm::mat4 localMat = getLocalMatrix();

	glm::mat4 parentMat = glm::mat4(1.0f);
	if (this->parent != nullptr)
		parentMat = this->parent->getWorldMatrix();
	
	return parentMat * localMat;
}

void SceneNode::addChild(SceneNode* child)
{
	children.push_back(child);
	child->parent = this;
}

void SceneNode::cleanup() {
	for (int i = 0; i < children.size(); i++) {
		children[i]->cleanup();
	}

	if(this->mesh)
		this->mesh->cleanup();

	uniformBuffer.cleanup();
	descriptorSet.cleanup();

	delete this;
}

Scene::Scene() {}

Scene::Scene(VulkanDevice& device,
			 VulkanInstance& instance,
			 VkCommandPool commandPool, 
			 VkQueue graphicsQueue, 
			 SwapChain* swapChain,
			 Camera* camera,
			 uint32_t maxLevelRoughnessMipMap,
			 bool storePrevMVP) :
												device(device),
												instance(instance),
												commandPool(commandPool),
												graphicsQueue(graphicsQueue),
												swapChain(swapChain),
												camera(camera),
												maxLevelRoughnessMipMap(maxLevelRoughnessMipMap), 
												storePrevMVP(storePrevMVP){}

void Scene::loadScene(std::string filename, GltfImporter* gltfLoader) {
	SceneNode* sceneNode = gltfLoader->loadFromFile(device,
													commandPool,
													filename,
													vertices,
													indices,
													indicesOffsets,
													textures,
													materials);

	sceneNodes.push_back(sceneNode);
}

void Scene::loadCubeMap(std::string filename)
{
	textureCubeMap = TextureCubeMap(device, commandPool, filename.data());
	textureCubeMap.submitImageToStageMemory();
	textureCubeMap.submitImageToDeviceMemory();
}

void Scene::ready()
{
	processScene();
	submitSceneData();
	createRaytracingStructures();
	createDescriptorSets();
}

void Scene::processScene()
{
	//Collect information about the scenes
	for (auto& node : this->sceneNodes) {
		processScene(node);
	}
}

void Scene::processScene(SceneNode* sceneNode)
{
	sceneNodesList.push_back(sceneNode);
	numNodes++;

	sceneNode->uniformBuffer = UniformBuffer("Scene Node " + std::to_string(sceneNode->id),
											 device,
											 sizeof(UniformBufferObject),
											 swapChain->getSize());
	
	if (sceneNode->mesh) {
		this->numMeshes++;

		for (Primitive* primitive : sceneNode->mesh->primitives) {
			this->numPrimitives++;
			if (primitive->materialIndex != -1 && primitive->getMaterial() && !primitive->getMaterial()->inDeviceMemory) {
				primitive->getMaterial()->inDeviceMemory = true;
				this->numMaterials++;
				this->numTexturesUsed+=5;
				Material* mat = primitive->getMaterial();
				if(mat->matParams.albedoTextureSet > -1 && !mat->getAlbedoTexture()->inDeviceMemory)
					mat->getAlbedoTexture()->submitImageToDeviceMemory();
				if (mat->matParams.normalTextureSet > -1 && !mat->getNormalTexture()->inDeviceMemory)
					mat->getNormalTexture()->submitImageToDeviceMemory();
				if (mat->matParams.occlusionTextureSet > -1 && !mat->getAoTexture()->inDeviceMemory)
					mat->getAoTexture()->submitImageToDeviceMemory();
				if (mat->matParams.emissiveTextureSet > -1 && !mat->getEmissionTexture()->inDeviceMemory)
					mat->getEmissionTexture()->submitImageToDeviceMemory();
				if (mat->matParams.metallicRoughnessTextureSet > -1 && !mat->getMetalRoughnessTexture()->inDeviceMemory)
					mat->getMetalRoughnessTexture()->submitImageToDeviceMemory();
				mat->uniformBuffer = UniformBuffer(mat->name, device, sizeof(Material::MaterialParams));
				mat->uniformBuffer.submitDataToMemory(&mat->matParams);
			}
		}
	}

	for (auto& node : sceneNode->children) {
		processScene(node);
	}
}

void Scene::createRaytracingStructures()
{
	sceneNodesRaytracingData.resize(numPrimitives);

	topLevelAs = new TopLevelAS(device, instance, commandPool);

	//Collect information about the scenes
	for (auto& node : this->sceneNodes) {
		createRaytracingStructures(node);
	}

	topLevelAs->createTopLevelAS();

	//Scene node data buffer
	int size = static_cast<int>(sceneNodesRaytracingData.size()) * sizeof(SceneNode::SceneNodeRaytracing);
	sceneNodeDataStagingBuffer = Buffer("Scene Node Staging Buffer",
										device,
										device.getQueues().graphicsQueue,
										size,
										VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
										VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	sceneNodeDataStagingBuffer.submitDataToMemory(sceneNodesRaytracingData.data());

	sceneNodeDataBuffer = Buffer("Scene Node Data Buffer",
								 device,
								 device.getQueues().graphicsQueue,
								 size,
								 VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
								 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	utils::copyBuffer(device.getLogicalDeviceHandle(),
					  device.getPhysicalDeviceHandle(),
					  device.getQueues().graphicsQueue,
					  commandPool,
					  sceneNodeDataStagingBuffer.buffer,
					  sceneNodeDataBuffer.buffer,
					  size);
	
}

void Scene::createRaytracingStructures(SceneNode* node)
{
	if (node->mesh) {

		for (Primitive* primitive : node->mesh->primitives) {
			primitive->bottomLevelAs = new BottomLevelAS(primitive->firstVertex,
														 primitive->vertexCount,
														 vertexBuffer,
														 primitive->firstIndex,
														 primitive->indexCount,
														 indexBuffer,
														 device,
														 instance,
														 commandPool);
			
			glm::mat4 transform = node->getWorldMatrix();
			primitive->bottomLevelAs->update(transform, primitive->id);

			SceneNode::SceneNodeRaytracing sceneNodeRaytracingData;
			sceneNodeRaytracingData.model = node->getWorldMatrix();
			sceneNodeRaytracingData.modelInverted = glm::inverse(node->getWorldMatrix());
			sceneNodesRaytracingData[primitive->id] = sceneNodeRaytracingData;

			topLevelAs->addBlas(*primitive->bottomLevelAs);
			
		}
	}

	for (auto& child : node->children) {
		createRaytracingStructures(child);
	}
}

void Scene::submitSceneData()
{
	vertexBuffer = VertexBuffer(device, commandPool, sizeof(vertices[0]) * static_cast<int>(vertices.size()));
	vertexBuffer.submitDataToStageBuffer(vertices);
	vertexBuffer.submitDataToDevice();

	indexBuffer = IndexBuffer(device, commandPool, sizeof(indices[0]) * static_cast<int>(indices.size()));
	indexBuffer.submitDataToStageBuffer(indices);
	indexBuffer.submitDataToDevice();

	//indices offset buffer
	int size = static_cast<int>(indicesOffsets.size()) * sizeof(uint32_t);
	indexOffsetStagingBuffer = Buffer("Index Offset Staging Buffer",
									  device,
									  device.getQueues().graphicsQueue,
								      size,
									  VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
									  VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	indexOffsetStagingBuffer.submitDataToMemory(indicesOffsets.data());

	indexOffsetBuffer = Buffer("Index Offset Buffer",
							   device,
							   device.getQueues().graphicsQueue,
							   size,
							   VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
							   VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	utils::copyBuffer(device.getLogicalDeviceHandle(), 
					  device.getPhysicalDeviceHandle(), 
					  device.getQueues().graphicsQueue, 
					  commandPool, 
					  indexOffsetStagingBuffer.buffer,
					  indexOffsetBuffer.buffer,
					  size);

	indexOffsetStagingBuffer.cleanup();


	size = static_cast<int>(materials.size()) * sizeof(Material::RaytracingMaterial);

	materialStagingBuffer = Buffer("Material Staging Buffer",
									device,
									device.getQueues().graphicsQueue,
									size,
									VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
									VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	materialBuffer = Buffer("Material Buffer",
							 device,
							 device.getQueues().graphicsQueue,
							 size,
							 VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
							 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
}

//create descriptor layouts
void Scene::createDescriptorSets()
{

	for (int i = 0; i < materials.size(); i++) {
		Material* mat = materials[i];
		mat->descriptorSet = DescriptorSet(mat->name,
											{ {0, &mat->uniformBuffer} },
											{ {1, mat->getAlbedoTexture()}, 
											  {2, mat->getNormalTexture()}, 
											  {3, mat->getAoTexture()}, 
											  {4, mat->getEmissionTexture()} ,
											  {5, mat->getMetalRoughnessTexture()} },
											{},
											{},
											VK_SHADER_STAGE_FRAGMENT_BIT,
											device,
											instance,
											commandPool,
											this->swapChain->getSize());
	}
	
	for (auto& node : this->sceneNodes) {
		createNodeDescriptorSets(node);
	}
	
	descriptorLayouts.push_back(this->sceneNodes[0]->descriptorSet.getDescriptorLayout());
	descriptorLayouts.push_back(this->materials[0]->descriptorSet.getDescriptorLayout());
}

void Scene::createNodeDescriptorSets(SceneNode* sceneNode)
{
	sceneNode->descriptorSet = DescriptorSet(sceneNode->name,
											 { {0,&sceneNode->uniformBuffer} },
											 {},
											 {},
											 {},
											 VK_SHADER_STAGE_ALL,
											 device,
											 instance,
											 commandPool,
											 this->swapChain->getSize());

	for (auto& node : sceneNode->children) {
		createNodeDescriptorSets(node);
	}
}

void Scene::update(int currentImage)
{
	//update raytracing materials

	//materials buffer
	std::vector<Material::RaytracingMaterial> raytracingMaterials;
	for (int i = 0; i < materials.size(); i++) {
		raytracingMaterials.push_back(materials[i]->raytracingMaterials);
	}

	int size = static_cast<int>(raytracingMaterials.size()) * sizeof(Material::RaytracingMaterial);

	materialStagingBuffer.submitDataToMemory(raytracingMaterials.data());

	utils::copyBuffer(device.getLogicalDeviceHandle(),
					  device.getPhysicalDeviceHandle(),
				   	  device.getQueues().graphicsQueue,
					  commandPool,
					  materialStagingBuffer.buffer,
					  materialBuffer.buffer,
					  size);

	size = static_cast<int>(sceneNodesRaytracingData.size()) * sizeof(SceneNode::SceneNodeRaytracing);

	sceneNodeDataStagingBuffer.submitDataToMemory(sceneNodesRaytracingData.data());

	utils::copyBuffer(device.getLogicalDeviceHandle(),
					  device.getPhysicalDeviceHandle(),
					  device.getQueues().graphicsQueue,
					  commandPool,
					  sceneNodeDataStagingBuffer.buffer,
					  sceneNodeDataBuffer.buffer,
					  size);


	for (auto node : sceneNodes) {
		update(node, currentImage);
	}
	
	topLevelAs->update();
}

void Scene::update(SceneNode* sceneNode, int currentImage)
{
	UniformBufferObject ubo{};
	ubo.model = sceneNode->getWorldMatrix();
	ubo.view = camera->GetViewMatrix();
	ubo.proj = camera->GetProjectionMatrix();
	ubo.prevModel = sceneNode->prevModel;
	ubo.prevView = sceneNode->prevView;
	ubo.prevProj = sceneNode->prevProj;
	ubo.id = sceneNode->id;
	ubo.jitter = camera->GetJitter();

	if(sceneNode->mesh != nullptr) {
		sceneNode->uniformBuffer.submitDataToMemory(&ubo, currentImage);

		for (Primitive* primitive : sceneNode->mesh->primitives) {
			primitive->bottomLevelAs->update(ubo.model, primitive->id);

			Material* mat = primitive->getMaterial();
			mat->uniformBuffer.submitDataToMemory(&mat->matParams);
		
			SceneNode::SceneNodeRaytracing sceneNodeRaytracingData;
			sceneNodeRaytracingData.model = sceneNode->getWorldMatrix();
			sceneNodeRaytracingData.modelInverted = glm::inverse(sceneNode->getWorldMatrix());
			sceneNodesRaytracingData[primitive->id] = sceneNodeRaytracingData;
		}
	}

	if(storePrevMVP) {
		sceneNode->prevModel = ubo.model;
		sceneNode->prevView = ubo.view;
		sceneNode->prevProj = camera->GetProjectionMatrixNoJitter();
	}

	for (auto node : sceneNode->children) {
		update(node, currentImage);
	}
}

//draw the scene
void Scene::drawScene(VkCommandBuffer drawCommandbuffer, VkPipelineLayout pipelineLayout, int currentImage)
{
	VkDeviceSize offsets[] = { 0 };
	vkCmdBindVertexBuffers(drawCommandbuffer, 0, 1, &this->vertexBuffer.vertexBuffer, offsets);
	vkCmdBindIndexBuffer(drawCommandbuffer, this->indexBuffer.indexBuffer, 0, VK_INDEX_TYPE_UINT32);

	for (auto& node : this->sceneNodes) {
		drawSceneNode(*node, drawCommandbuffer, pipelineLayout, currentImage);
	}
}

void Scene::drawSceneNode(SceneNode& sceneNode, 
						  VkCommandBuffer drawCommandbuffer, 
						  VkPipelineLayout pipelineLayout,
						  int currentImage)
{
	//bind the scene node descriptor set
	vkCmdBindDescriptorSets(drawCommandbuffer,
							VK_PIPELINE_BIND_POINT_GRAPHICS,
							pipelineLayout,
							0,
							1,
							&sceneNode.descriptorSet.getDescriptorSetsHandle()[currentImage],
							0,
							nullptr);

	if (sceneNode.mesh) {
		for (Primitive* primitive : sceneNode.mesh->primitives) {
			if (primitive->materialIndex == -1)
				continue;

			VkDescriptorSet* matDescriptor = &primitive->getMaterial()->descriptorSet.getDescriptorSetsHandle()[currentImage];
			vkCmdBindDescriptorSets(drawCommandbuffer,
									VK_PIPELINE_BIND_POINT_GRAPHICS,
									pipelineLayout,
									1,
									1,
									matDescriptor,
									0,
									nullptr);

			vkCmdDrawIndexed(drawCommandbuffer, primitive->indexCount, 1, primitive->firstIndex, 0, 0);
		}
	}
	for (auto& child : sceneNode.children) {
		drawSceneNode(*child, drawCommandbuffer, pipelineLayout, currentImage);
	}
}

void Scene::cleanup() {

	for (int i = 0; i < materials.size(); i++) {
		materials[i]->cleanup();
		delete materials[i];
	}

	indexOffsetBuffer.cleanup();
	materialBuffer.cleanup();
	materialStagingBuffer.cleanup();
	sceneNodeDataStagingBuffer.cleanup();
	sceneNodeDataBuffer.cleanup();

	textureCubeMap.cleanup();

	for (int i = 0; i < textures.size(); i++) {
		textures[i]->cleanup();
		delete textures[i];
	}

	for (int i = 0; i < sceneNodes.size(); i++) {
		sceneNodes[i]->cleanup();
	}
	
	topLevelAs->cleanup();
	delete topLevelAs;
	
	vertexBuffer.cleanup();
	indexBuffer.cleanup();
}