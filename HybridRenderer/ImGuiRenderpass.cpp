#include "ImGuiRenderpass.h"

ImGuiRenderpass::ImGuiRenderpass()
{
}

ImGuiRenderpass::ImGuiRenderpass(VkDevice device,
                                 SwapChain& swapChain) :
                                                        device(device),
                                                        swapChain(swapChain)
{
}

void ImGuiRenderpass::createRenderPasses(VkImageLayout finalFormat)
{
    //Color attachment
    VkAttachmentDescription colorAttachmentDescription =
        initializers::colorAttachmentDescription(VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                                                 finalFormat,  //we want to display to screen
                                                 swapChain.getSwapChainImageFormat(),
                                                 VK_SAMPLE_COUNT_1_BIT,
                                                 VK_ATTACHMENT_LOAD_OP_LOAD);

    VkAttachmentReference colorAttachmentRef = initializers::colorAttachmentRef(0);

    VkSubpassDescription subpass{};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;

    VkSubpassDependency dependency = initializers::subpassDependency(VK_SUBPASS_EXTERNAL, 0);

    std::array<VkAttachmentDescription, 1> attachments = { colorAttachmentDescription };
    VkRenderPassCreateInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
    renderPassInfo.pAttachments = attachments.data();
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;

    VK_CHECK_RESULT(vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass));
}

void ImGuiRenderpass::createFramebuffers()
{
    swapChainFramebuffers.resize(swapChain.getSize());

    for (size_t i = 0; i < swapChain.getSize(); i++) {
        std::vector<VkImageView> attachments = {
            swapChain.getSwapChainImageViews()[i]
        };

        VkFramebufferCreateInfo framebufferInfo = initializers::framebufferCreateInfo(renderPass,
                                                                                      static_cast<int>(attachments.size()),
                                                                                      attachments.data(),
                                                                                      swapChain.getSwapChainExtent().width,
                                                                                      swapChain.getSwapChainExtent().height);

        VK_CHECK_RESULT(vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapChainFramebuffers[i]));
    }
}

void ImGuiRenderpass::cleanup()
{
    for (auto framebuffer : swapChainFramebuffers) {
        vkDestroyFramebuffer(device, framebuffer, nullptr);
    }

    vkDestroyRenderPass(device, renderPass, nullptr);
}