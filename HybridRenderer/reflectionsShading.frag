#version 450

#define PI 3.1415926535897932384626433832795

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;

layout(binding = 0) uniform Params {	
	vec4 lightDir;
	vec4 lightColor;
	float lightIntensity;
    bool checkRef;
    bool useAdjacentHitPoints;
    vec4 camPos;
} params;

layout(binding = 1) uniform sampler2D reflectionsImage;
layout(binding = 2) uniform sampler2D hitPointsImage;
layout(binding = 3) uniform sampler2D albedoImage;
layout(binding = 4) uniform sampler2D metalnessRoughnessImage;
layout(binding = 5) uniform sampler2D normalsImage;
layout(binding = 6) uniform sampler2D positionImage;

float luma(vec3 c){
    return dot(c, vec3(0.2126, 0.7152, 0.0722));
}

float DistributionGGX(vec3 n, vec3 h, float roughness) {
    float a2 = roughness * roughness;
    float nDotH = max(dot(n,h),0.0f);
    float nDotH2 = nDotH*nDotH;

    float numerator = a2;
    float denominator = (nDotH2 * (a2 - 1.0f) + 1.0f);
    denominator = PI * denominator * denominator;

    return numerator / denominator;
}

float smithLambda(vec3 w, vec3 n, float roughness) {
    float wDotN = dot(w,n);
    if (wDotN <= 0) 
        return 0;

    float wDotN2 = wDotN * wDotN;
    float tanSqrd = max((1-wDotN2),0.0f) / wDotN2;

    return 0.5f * (-1 + sqrt(1 + roughness * tanSqrd));
}

float SmithHeightCorrelated(vec3 w0, vec3 wi, vec3 n, float roughness) {
    float a = roughness * roughness;
    return 1/(1+smithLambda(w0,n,a)+smithLambda(wi,n,a));
}

vec3 FresnelSchlick(vec3 F0, float cosTheta) {
    float exponent = (-5.5547*cosTheta-6.98316) * cosTheta;
    return F0 + (1-F0) * pow(2, exponent);

    //return F0 + (1.0 - F0) * pow(1.0 - cosTheta,5.0);
}

vec3 FresnelSchlickRoughness(vec3 F0, float cosTheta, float roughness)
{
    float exponent = (-5.5547*cosTheta-6.98316) * cosTheta;
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(2, exponent);
    //return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}  

float calculateBrdfWeight(vec3 w0, vec3 normal, vec3 wi, vec3 lightColor, float lightIntensity, float roughness, float metal, vec3 albedo, out float pdf ) {
	vec3 res = vec3(0.0);

    float dist = length(wi);
    vec3 h = normalize(wi + w0);
    float nDotWi = max(dot(normal, wi), 0.0f);
	float w0DotN = max(dot(normal, w0), 0.0f);
	float nDotH = max(dot(normal, h), 0.0f);

    //Normal distribution
    float NDF = DistributionGGX(normal, h, roughness);
    //Geometry Function
    float G = SmithHeightCorrelated(w0, wi, normal, roughness);


	return NDF * G * (PI/ 4.0);
}

float vndfPdf(vec3 w0, vec3 wi, vec3 n, float r) {
/*
    vec3 h = normalize(wi + w0);
    float w0DotN = max(dot(w0, n), 0.0f);
    float w0DotZ = dot(w0, vec3(0.0f, 1.0f, 0.0f));

    //Normal distribution
    float NDF = DistributionGGX(n, h, r);
    //Geometry Function
    float G1 = smithLambda(w0, n, r);

    float numerator = NDF * G1 * w0DotN / w0DotZ;
    float denominator = 4 * w0DotN;

    return numerator/ denominator;
    */

    vec3 res = vec3(0.0);

    float dist = length(wi);
    vec3 h = normalize(wi + w0);
    float nDotWi = max(dot(n, wi), 0.0f);
	float w0DotN = max(dot(n, w0), 0.0f);
	float nDotH = max(dot(n, h), 0.0f);

    //Normal distribution
    float NDF = DistributionGGX(n, h, r);
    //Geometry Function
    float G = SmithHeightCorrelated(w0, wi, n, r);


	return NDF * G * (PI/ 4.0);
}

float getAdjacentPDF(vec2 uv) {
    
    vec3 reflectionPoint = texture(positionImage,uv).xyz;
    vec3 hitPoint = texture(hitPointsImage,uv).xyz;

    vec3 w0 = normalize(params.camPos.xyz - reflectionPoint);
    vec3 wi = normalize(reflectionPoint - hitPoint);
    vec3 normal = texture(normalsImage, uv).xyz;
    float roughness = texture(metalnessRoughnessImage, uv).g;

    return vndfPdf(w0, wi, normal, roughness);
}

void main()
{
    if(params.useAdjacentHitPoints) {
    /*
        float offsetx = 1.0 / textureSize(albedoImage,0).x;
        float offsety= 1.0 / textureSize(albedoImage,0).y;
        vec2[5] offsets = { vec2(0.0f,0.0f),
                            vec2(offsetx, 0.0f),  
                            vec2(-offsetx, 0.0f),  
                            vec2(0.0f, offsety), 
                            vec2(0.0f, -offsety) };
        int numSuccessHitPoints = 0;
        vec3 res = vec3(0.0f);
        
        vec3 albedo = texture(albedoImage, inUV).xyz;
        vec3 normal = texture(normalsImage, inUV).xyz;
        if(abs(normal.x) < 0.01f && abs(normal.y) < 0.01f && abs(normal.z) < 0.01f) {
            outFragColor = vec4(0.0f,0.0f,0.0f, 1.0f);
            return;
        }
        float roughness = texture(metalnessRoughnessImage, inUV).g;
        float metal = texture(metalnessRoughnessImage, inUV).b;
        vec3 lightColor = params.lightColor.rgb;
        vec3 wi = -normalize(params.lightDir.xyz);
        float lightIntensity = params.lightIntensity;
        vec3 reflectionPoint = texture(positionImage,inUV).xyz;

        float weightSum = 0.0f;
        for(int i = 0; i < 16; i++) {
            vec2 uv = inUV - offsets[i];
            vec3 hitPoint = texture(hitPointsImage,uv).xyz;

            if(abs(hitPoint.x) < 0.01f && abs(hitPoint.y) < 0.01f && abs(hitPoint.z) < 0.01f) {
                continue;
            }

            vec3 sampleColor = texture(reflectionsImage, uv).xyz;
            float pdf = max(texture(hitPointsImage, uv).w, 0.0001);

            vec3 unormalizedW0 = reflectionPoint - hitPoint;
            float w0Length = length(unormalizedW0);
            vec3 w0 = vec3(0.0f);
            if(w0Length < 0.001f) {
                outFragColor = vec4(0.0f);
                return;
            }
            else
                w0 = unormalizedW0 / w0Length;

            float pdfOrigin;
            float weight = calculateBrdfWeight(w0, normal, wi, lightColor, lightIntensity, roughness, metal, albedo, pdfOrigin) / pdf;
            weight = min(weight, 1.0f);
            weightSum += weight;
            res += sampleColor * weight;
        }
        res /= max(weightSum, 0.1);

	    outFragColor = vec4(res, 1.0f);
        */

        float offsetx = 1.0 / textureSize(albedoImage,0).x;
        float offsety = 1.0 / textureSize(albedoImage,0).y;
        vec2[17] offsets = { vec2(0.0f,0.0f),
                            vec2(offsetx, 0.0f),  
                            vec2(-offsetx, 0.0f),  
                            vec2(0.0f, offsety), 
                            vec2(0.0f, -offsety),
                            vec2(-offsetx, -offsety),
                            vec2(offsetx, -offsety),
                            vec2(offsetx, offsety),
                            vec2(-offsetx, offsety), 
                            
                            vec2(2*offsetx, 0.0f),  
                            vec2(2*-offsetx, 0.0f),  
                            vec2(0.0f, 2*offsety), 
                            vec2(0.0f, 2*-offsety),
                            vec2(2*-offsetx, 2*-offsety),
                            vec2(2*offsetx, 2*-offsety),
                            vec2(2*offsetx, 2*offsety),
                            vec2(2*-offsetx, 2*offsety) };

        int numSuccessHitPoints = 0;
        vec3 res = vec3(0.0f);
        
        vec3 albedo = texture(albedoImage, inUV).xyz;
        vec3 normal = texture(normalsImage, inUV).xyz;
        if(abs(normal.x) < 0.01f && abs(normal.y) < 0.01f && abs(normal.z) < 0.01f) {
            outFragColor = vec4(0.0f,0.0f,0.0f, 1.0f);
            return;
        }
        float roughness = texture(metalnessRoughnessImage, inUV).g;
        vec3 reflectionPoint = texture(positionImage,inUV).xyz;

        vec3 w0 = normalize(params.camPos.xyz - reflectionPoint);

        float weightSum = 0.0f;
        for(int i = 0; i < 17; i++) {
            vec2 uv = inUV - offsets[i];
            vec3 hitPoint = texture(hitPointsImage,uv).xyz;

            if(abs(hitPoint.x) < 0.01f && abs(hitPoint.y) < 0.01f && abs(hitPoint.z) < 0.01f) {
                weightSum += 1.0f;
                continue;
            }

            vec3 sampleColor = texture(reflectionsImage, uv).xyz;
            float pdf = max(texture(hitPointsImage, uv).w, 0.0001);
            vec3 wi = normalize(reflectionPoint - hitPoint);

            float pdfOrigin;
            float weight = max(vndfPdf(w0, wi, normal, roughness) / getAdjacentPDF(uv), 1e-5);
            res += sampleColor * weight;
            weightSum += weight;
            numSuccessHitPoints++;
        }
        
        if(res.r > 1e-5 && res.g > 1e-5 && res.r > 1e-5)
            res /= weightSum;
        //res /= numSuccessHitPoints;

        res /= (1+res);
	    outFragColor = vec4(res, 1.0f);
    }
    else if(params.checkRef) {         
	    outFragColor = texture(reflectionsImage, inUV);
    }
    else {
        vec3 sampleColor = texture(reflectionsImage, inUV).xyz;
        vec3 reflectionPoint = texture(positionImage,inUV).xyz;
        vec3 hitPoint = texture(hitPointsImage,inUV).xyz;
        vec3 normal = texture(normalsImage, inUV).xyz;
        float roughness = texture(metalnessRoughnessImage, inUV).g;
        vec3 w0 = normalize(params.camPos.xyz - reflectionPoint);
        vec3 wi = normalize(reflectionPoint - hitPoint);
        sampleColor = sampleColor / max(vndfPdf(w0, wi, normal, roughness), 0.0001f);
        sampleColor /= (1+luma(sampleColor));
        outFragColor = vec4(sampleColor, 1.0f);
    }
}