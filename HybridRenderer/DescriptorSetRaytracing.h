#pragma once
#include "vulkan/vulkan.h"
#include "UniformBuffer.h"
#include "TextureImage.h"
#include "TextureCubeMap.h"
#include <vector>
#include "SwapChain.h"
#include "FrameGraph.h"
#include <algorithm>
#include "VulkanInstance.h"
#include "SceneGraph.h"
#include "TopLevelAS.h"


class DescriptorSetRaytracing
{
public:
	struct TextureResource {
		int binding;
		TextureImage* texImage = nullptr;
		VkImageLayout imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	};

	struct TextureArrayResource {
		int binding;
		std::vector<TextureImage*> texImages;
		VkImageLayout imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	};

	struct TextureCubeResource {
		int binding;
		TextureCubeMap* texImage;
		VkImageLayout imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	};

private:
	VulkanDevice device;
	VulkanInstance instance;
	VkCommandPool commandPool;
	UniformBuffer uniformBuffer;
	std::vector<TextureResource> textureResources;
	std::vector<TextureArrayResource> textureArrayResources;
	std::vector<TextureResource> textureTargets;
	std::vector<TextureCubeResource> textureCubeResources;
	TextureImage target;
	Scene scene;
	TopLevelAS tlas;

	VkDescriptorSet descriptorSet;
	VkDescriptorSetLayout descriptorSetLayout;
	VkDescriptorPool descriptorPool;
	std::string name;

public:
	DescriptorSetRaytracing();
	DescriptorSetRaytracing(std::string name, 
							UniformBuffer& uniformBuffer,
							std::vector<TextureResource>&& textureResources,
							std::vector<TextureArrayResource>&& textureArrayResources,
							std::vector<TextureResource>&& textureTargets,
							std::vector<TextureCubeResource>&& textureCubeResources,
							Scene& scene,
							TopLevelAS& tlas,
							VulkanDevice& device,
							VulkanInstance& instance,
							VkCommandPool commandPool);

	void cleanup();
	VkDescriptorSet& getDescriptorSetsHandle();
	VkDescriptorSetLayout& getDescriptorSetLayout();

private:
	void createDescriptorPool();
	void createDescriptorSet();
};

