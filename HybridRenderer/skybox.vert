#version 450
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inUv;

layout (binding = 0) uniform UBO 
{
	mat4 projection;
	mat4 view;
	float skyboxIntensity;
} ubo;

layout (location = 0) out vec3 outUVW;

void main() 
{
	outUVW = vec3(inPosition.x,-inPosition.y,inPosition.z );
	vec4 pos = ubo.projection * ubo.view * vec4(inPosition.xyz, 1.0);
	gl_Position = pos.xyww;
}