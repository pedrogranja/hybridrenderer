#pragma once
#include "vulkan/vulkan.h"
#include <vector>
#include "utils.hpp"
#include "Initializers2.hpp"
#include "SwapChain.h"
#include "DepthImage.h"
#include "config.hpp"
#include "TextureImage.h"

class ImGuiRenderpass
{
public:
	VkRenderPass renderPass;
	std::vector<VkFramebuffer> swapChainFramebuffers;
	SwapChain swapChain;
	VkDevice device;

	ImGuiRenderpass();
	ImGuiRenderpass(VkDevice device, SwapChain& swapChain);
	void createRenderPasses(VkImageLayout finalImageLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
	void createFramebuffers();
	void cleanup();
};

