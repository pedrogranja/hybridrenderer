#include "TextureImage.h"

TextureImage::TextureImage()
{
}

TextureImage::TextureImage(std::string name, 
                           VulkanDevice& device,
                           VkCommandPool commandPool,
                           int texWidth,
                           int texHeight,
                           int texChannels,
                           VkFormat format,
                           int usage,
                           int numImages,
                           bool createSampler,
                           int mipLevels,
                           VkSampleCountFlagBits sampleCount,
                           VkImageLayout initialImageLayout) : name(name),
                                                               device(device),
                                                               commandPool(commandPool),
                                                               texWidth(texWidth),
                                                               texHeight(texHeight),
                                                               texChannels(texChannels),
                                                               format(format),
                                                               numImages(numImages)
{
    textureImages.resize(numImages);
    textureImageMemories.resize(numImages);
    textureImageViews.resize(numImages);
    if(createSampler)
        textureSamplers.resize(numImages);

    for(int i = 0; i < textureImages.size(); i++) {
        utils::createImage(device.getLogicalDeviceHandle(),
                           device.getPhysicalDeviceHandle(),
                           texWidth,
                           texHeight,
                           this->format,
                           VK_IMAGE_TILING_OPTIMAL,
                           usage,
                           VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                           textureImages[i],
                           textureImageMemories[i],
                           1,
                           0,
                           mipLevels,
                           sampleCount,
                           initialImageLayout);

        device.nameVulkanObject(VK_OBJECT_TYPE_IMAGE, (uint64_t) textureImages[i], name + " Image " + std::to_string(i));
        device.nameVulkanObject(VK_OBJECT_TYPE_DEVICE_MEMORY, (uint64_t)textureImageMemories[i], name + " Image Memory " + std::to_string(i));

        //if is depth...
        if (format == VK_FORMAT_D32_SFLOAT
            || format == VK_FORMAT_D32_SFLOAT_S8_UINT
            || format == VK_FORMAT_D24_UNORM_S8_UINT) {
             utils::createImageView(device.getLogicalDeviceHandle(), textureImages[i], format, VK_IMAGE_ASPECT_DEPTH_BIT, textureImageViews[i], VK_IMAGE_VIEW_TYPE_2D, 1, mipLevels);
        }
        else {
            utils::createImageView(device.getLogicalDeviceHandle(), textureImages[i], format, VK_IMAGE_ASPECT_COLOR_BIT, textureImageViews[i],  VK_IMAGE_VIEW_TYPE_2D, 1, mipLevels);
        }

        device.nameVulkanObject(VK_OBJECT_TYPE_IMAGE_VIEW, (uint64_t)textureImageViews[i], name + " Image View " + std::to_string(i));

        if(createSampler) {
            utils::createTextureSampler(device.getLogicalDeviceHandle(), VK_SAMPLER_ADDRESS_MODE_REPEAT, textureSamplers[i], mipLevels);
            device.nameVulkanObject(VK_OBJECT_TYPE_SAMPLER, (uint64_t)textureSamplers[i], name + " Image Sampler " + std::to_string(i));
        }
    }
}

TextureImage::TextureImage(std::string name, 
                           VulkanDevice& device,
                           VkCommandPool commandPool,
                           stbi_uc* pixels,
                           VkDeviceSize size,
                           int texWidth,
                           int texHeight,
                           int texChannels,
                           int numImages,
                           VkFormat format) : name(name),
                                              device(device),
                                              commandPool(commandPool),
                                              pixels(pixels),
                                              imageSize(size),
                                              texWidth(texWidth),
                                              texHeight(texHeight),
                                              texChannels(texChannels),
                                              format(format),
                                              stbiDelete(false),
                                              stageBuffersDeleted(false),
                                              inDeviceMemory(false)
                                            
{
    textureImages.resize(numImages);
    textureImageMemories.resize(numImages);
    textureImageViews.resize(numImages);
    textureSamplers.resize(numImages);

    utils::createBuffer(device.getLogicalDeviceHandle(),
                        device.getPhysicalDeviceHandle(),
                        imageSize,
                        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                        stagingBuffer,
                        stagingMemory);

    for(int i = 0; i < textureImages.size(); i++) {

        utils::createImage(device.getLogicalDeviceHandle(),
                           device.getPhysicalDeviceHandle(),
                           texWidth,
                           texHeight,
                           this->format,
                           VK_IMAGE_TILING_OPTIMAL,
                           VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                           VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                           textureImages[i],
                           textureImageMemories[i]);

        utils::createImageView(device.getLogicalDeviceHandle(), textureImages[i], format, VK_IMAGE_ASPECT_COLOR_BIT, textureImageViews[i]);
        utils::createTextureSampler(device.getLogicalDeviceHandle(), VK_SAMPLER_ADDRESS_MODE_REPEAT, textureSamplers[i]);


        device.nameVulkanObject(VK_OBJECT_TYPE_IMAGE, (uint64_t) textureImages[i], name + " Image " + std::to_string(i));
        device.nameVulkanObject(VK_OBJECT_TYPE_DEVICE_MEMORY, (uint64_t) textureImageMemories[i], name + " Image Memory " + std::to_string(i));
        device.nameVulkanObject(VK_OBJECT_TYPE_IMAGE_VIEW, (uint64_t) textureImageViews[i], name + " Image View " + std::to_string(i));
        device.nameVulkanObject(VK_OBJECT_TYPE_SAMPLER, (uint64_t)textureSamplers[i], name + " Image Sampler " + std::to_string(i));
    }
}

TextureImage::TextureImage(std::string name, 
                           VulkanDevice& device,
                           VkCommandPool commandPool,
                           const char* imageFile,
                           int numImages,
                           VkFormat format) : name(name), 
                                              device(device),
                                              commandPool(commandPool),
                                              format(format),
                                              stbiDelete(true),
                                              stageBuffersDeleted(false)
{
    textureImages.resize(numImages);
    textureImageMemories.resize(numImages);
    textureImageViews.resize(numImages);
    textureSamplers.resize(numImages);

    importImage(imageFile);

    utils::createBuffer(device.getLogicalDeviceHandle(),
                        device.getPhysicalDeviceHandle(),
                        imageSize,
                        VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
                        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                        stagingBuffer,
                        stagingMemory);

    for(int i = 0; i < textureImages.size(); i++) {
        utils::createImage(device.getLogicalDeviceHandle(),
                           device.getPhysicalDeviceHandle(),
                           texWidth,
                           texHeight,
                           this->format,
                           VK_IMAGE_TILING_OPTIMAL,
                           VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                           VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                           textureImages[i],
                           textureImageMemories[i]);

        utils::createImageView(device.getLogicalDeviceHandle(), textureImages[i], this->format, VK_IMAGE_ASPECT_COLOR_BIT, textureImageViews[i]);
        utils::createTextureSampler(device.getLogicalDeviceHandle(), VK_SAMPLER_ADDRESS_MODE_REPEAT, textureSamplers[i]);

        device.nameVulkanObject(VK_OBJECT_TYPE_IMAGE, (uint64_t)textureImages[i], name + " Image " + std::to_string(i));
        device.nameVulkanObject(VK_OBJECT_TYPE_DEVICE_MEMORY, (uint64_t)textureImageMemories[i], name + " Image Memory " + std::to_string(i));
        device.nameVulkanObject(VK_OBJECT_TYPE_IMAGE_VIEW, (uint64_t)textureImageViews[i], name + " Image View " + std::to_string(i));
        device.nameVulkanObject(VK_OBJECT_TYPE_SAMPLER, (uint64_t)textureSamplers[i], name + " Image Sampler " + std::to_string(i));
    }
}

void TextureImage::submitImageToStageMemory()
{
    void* data;
    vkMapMemory(device.getLogicalDeviceHandle(), stagingMemory, 0, imageSize, 0, &data);
    memcpy(data, pixels, static_cast<size_t>(imageSize));
    vkUnmapMemory(device.getLogicalDeviceHandle(), stagingMemory);

    if (this->stbiDelete)
        stbi_image_free(pixels);
}

void TextureImage::submitImageToDeviceMemory()
{
    for(int i = 0; i < textureImages.size(); i++) {
        utils::transitionImageLayout(device.getLogicalDeviceHandle(), 
                                     commandPool, 
                                     device.getQueues().graphicsQueue, 
                                     textureImages[i],
                                     VK_IMAGE_LAYOUT_UNDEFINED,
                                     VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

            utils::copyBufferToImage(device.getLogicalDeviceHandle(), 
                                     commandPool, 
                                     device.getQueues().graphicsQueue, 
                                     stagingBuffer, 
                                     textureImages[i],
                                     static_cast<uint32_t>(texWidth),
                                     static_cast<uint32_t>(texHeight));

        utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                     commandPool,
                                     device.getQueues().graphicsQueue,
                                     textureImages[i],
                                     VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    }

    vkDestroyBuffer(device.getLogicalDeviceHandle(), stagingBuffer, nullptr);
    vkFreeMemory(device.getLogicalDeviceHandle(), stagingMemory, nullptr);
    this->stageBuffersDeleted = true;
    this->inDeviceMemory = true;
}

void TextureImage::cleanup()
{
    for (int i = 0; i < textureImages.size(); i++) {
        if(textureSamplers.size() > 0)
            vkDestroySampler(device.getLogicalDeviceHandle(), textureSamplers[i], nullptr);
        vkDestroyImageView(device.getLogicalDeviceHandle(), textureImageViews[i], nullptr);
        vkDestroyImage(device.getLogicalDeviceHandle(), textureImages[i], nullptr);
        vkFreeMemory(device.getLogicalDeviceHandle(), textureImageMemories[i], nullptr);
    }

    if (!this->stageBuffersDeleted && this->stagingBuffer != VK_NULL_HANDLE) {
        vkDestroyBuffer(device.getLogicalDeviceHandle(), stagingBuffer, nullptr);
        vkFreeMemory(device.getLogicalDeviceHandle(), stagingMemory, nullptr);
    }
}

void TextureImage::importImage(const char* imageFile)
{
    pixels = stbi_load(imageFile, &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
    imageSize = texWidth * texHeight * 4;

    if (!pixels) {
        throw std::runtime_error("failed to load texture image!");
    }
}