#pragma once
#include "EngineBase.h"

class DeferredRenderer : public EngineBase
{
	RenderTarget positionRenderTarget;
	RenderTarget albedoRenderTarget;
	RenderTarget normalRenderTarget;
	RenderTarget aoRenderTarget;
	RenderTarget metalRoughnessRenderTarget;
	RenderTarget emissionRenderTarget;
	RenderTarget skyboxRenderTarget;
	Pass skyboxRenderPass;
	Pass gBufferPass;
	VertexFragPipeline gBufferPipeline;
	DescriptorSet gBufferDescriptorSet;

	virtual void createDrawCommandBuffers();
	virtual void defineRenderer();
	virtual void cleanupSwapChain();
};