#version 450

layout (location = 0) in vec2 outUV;

layout (location = 0) out vec4 outColor;

const float PI = 3.1415926536;
const uint SAMPLE_COUNT = 1024u;

// Based on http://byteblacksmith.com/improvements-to-the-canonical-one-liner-glsl-rand-for-opengl-es-2-0/
float random(vec2 co)
{
	float a = 12.9898;
	float b = 78.233;
	float c = 43758.5453;
	float dt= dot(co.xy ,vec2(a,b));
	float sn= mod(dt,3.14);
	return fract(sin(sn) * c);
}

vec2 hammersley2d(uint i, uint N) 
{
	// Radical inverse based on http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
	uint bits = (i << 16u) | (i >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	float rdi = float(bits) * 2.3283064365386963e-10;
	return vec2(float(i) /float(N), rdi);
}

vec3 importanceSample_GGX(vec2 Xi, float roughness, vec3 normal) 
{
	// Maps a 2D point to a hemisphere with spread based on roughness
	float alpha = roughness * roughness;
	float phi = 2.0 * PI * Xi.x + random(normal.xz) * 0.1;
	float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (alpha*alpha - 1.0) * Xi.y));
	float sinTheta = sqrt(1.0 - cosTheta * cosTheta);

	//from spherical coordinates to cartesian coordinates
	vec3 H;
	H.x = cos(phi) * sinTheta;
	H.y = sin(phi) * sinTheta;
	H.z = cosTheta;

	// Tangent space
	vec3 up = abs(normal.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
	vec3 tangentX = normalize(cross(up, normal));
	vec3 tangentY = normalize(cross(normal, tangentX));

	// Convert to world Space
	return normalize(tangentX * H.x + tangentY * H.y + normal * H.z);
}

float smithLambda(vec3 w, vec3 n, float roughness) {
    float wDotN = dot(w,n);
    if (wDotN <= 0) 
        return 0;

    float wDotN2 = wDotN * wDotN;
    float tanSqrd = max((1-wDotN2),0.0f) / wDotN2;

    return 0.5f * (-1 + sqrt(1 + roughness * tanSqrd));
}

float SmithHeightCorrelated(vec3 w0, vec3 wi, vec3 n, float roughness) {
    float a = roughness * roughness;
    return 1/(1+smithLambda(w0,n,a)+smithLambda(wi,n,a));
}

vec2 IntegrateBRDF(float NdotW0, float roughness) 
{
	const vec3 n = vec3(0.0f, 0.0f, 1.0f);

	vec3 w0;
	w0.x = sqrt(1-NdotW0*NdotW0);
	w0.y = 0.0f;
	w0.z = NdotW0;

	float A = 0.0f;
	float B = 0.0f;

	for(uint i = 0u; i < SAMPLE_COUNT; i++) {
		vec2 Xi = hammersley2d(i, SAMPLE_COUNT);
		vec3 H = importanceSample_GGX(Xi, roughness, n);
		vec3 wi = 2.0 * dot(w0,H) * H - w0;

		float NDotWi = max(dot(n,wi),0.0f);
		float W0DotH = max(dot(w0,H),0.0f);
		float HDotN = max(dot(H,n),0.0f);

		if(NDotWi > 0.0) {
			float G = SmithHeightCorrelated(w0, wi,  n, roughness);
			//Divide by the PDF used to importance sample
			float G_Vis = (G * W0DotH) / (HDotN * NdotW0);
			//Calculate Fresnel
			float a = pow(2, (-5.5547*W0DotH-6.98316) * W0DotH);
			A += (1.0 - a) * G_Vis;
			B += a * G_Vis;
		}
	}

	A /= float(SAMPLE_COUNT);
	B /= float(SAMPLE_COUNT);
	return vec2(A,B);
}

void main() 
{
	vec2 integratedBRDF = IntegrateBRDF(outUV.x, outUV.y);
	outColor = vec4(integratedBRDF.x,integratedBRDF.y,0.0f,1.0f);
}