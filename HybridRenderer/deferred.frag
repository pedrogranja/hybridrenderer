#version 450
#extension GL_ARB_separate_shader_objects : enable
struct Light {
	vec4 position;
	vec3 color;
	float radius;
};

layout(binding = 1) uniform sampler2D texAlbedo;
layout(binding = 2) uniform sampler2D texNormal;
layout(binding = 3) uniform sampler2D texWorldPos;
layout(binding = 4) uniform RenderProp {
    int mode;
} renderProp;

layout(binding = 5) uniform Lights {
	Light lights[6];
	vec4 viewPos;
} lights;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

vec3 computeLightContribution(int i, vec3 albedo, vec3 normal, vec3 fragPos) {
	// Vector to light
	vec3 L = lights.lights[i].position.xyz - fragPos;
	// Distance from light to fragment position
	float dist = length(L);

	// Viewer to fragment
	vec3 V = lights.viewPos.xyz - fragPos;
	V = normalize(V);
		
	// Light to fragment
	L = normalize(L);

	// Attenuation
	float atten = lights.lights[i].radius / (pow(dist, 2.0) + 1.0);

	// Diffuse part
	vec3 N = normalize(normal);
	float NdotL = max(0.0, dot(N, L));
	vec3 diff = lights.lights[i].color * albedo.rgb * NdotL * atten;

	// Specular part		
	vec3 R = reflect(-L, N);
	float NdotR = max(0.0, dot(R, V));
	vec3 spec = lights.lights[i].color * pow(NdotR, 128.0) * atten;
		
	//return spec;
	return diff + spec;
}

void main() {
    vec3 color = vec3(0.0,0.0,0.0);
    vec3 albedo = fragColor * texture(texAlbedo, fragTexCoord).rgb;
    vec3 normal = fragColor * texture(texNormal, fragTexCoord).rgb;
    vec3 fragPos = fragColor * texture(texWorldPos, fragTexCoord).rgb;

	#define lightCount 6
	#define ambient 0.0

    if(renderProp.mode == 0) {
        color = albedo;
    }
    else if(renderProp.mode == 1) {
        color = normal;
    }
    else if(renderProp.mode == 2) {
        color = fragPos;
    }
	else if(renderProp.mode == 3) {

        color = albedo * ambient;

		color += computeLightContribution(0, albedo, normal, fragPos);
    }
    else if(renderProp.mode == 4) {

        color = albedo * ambient;

        for(int i = 0; i < lightCount; ++i)
		{		
			color += computeLightContribution(i, albedo, normal, fragPos);
		}    	
    }
	else {
		color = vec3(1.0f,0.0f,0.0f);
	}
    outColor = vec4(color, 1.0);
}