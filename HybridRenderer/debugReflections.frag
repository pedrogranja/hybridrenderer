#version 450
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_nonuniform_qualifier : require

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;

layout(binding = 0) uniform Params {
	bool debugAlpha;
	bool debugShadowsVarianceInitial;
	bool debugShadowsVarianceFinal;
	bool debugTemporalHistory;
	bool spatialVarianceEstimation;
	bool debugShadows;
	int debugAtrousPasses;
	int debugAtrousPassesVariance;
	int debugAtrousPassesWeights;
	bool debugReflections;
	bool debugTemporalReflections;
	bool debugReflectionsVariance;
	bool debugReflectionsHistory;
	bool reflectionsSpatialVarianceEstimation;
	bool debugReflectionsHitPoints;
	bool debugReflectionsShading;
	int varianceCalculationMode;
	float varianceWeightMultiplier;
	bool debugReflectionsDist;
	bool debugKernelSize;
	bool debugKernelWeights;
	bool debugReflectionsNormals;
	bool debugReflectionsBilateral;
	bool debugRoughness;
	bool debugMetalness;
	bool debugNormals;
	bool debugAlbedo;
	bool debugAlbedoTexture;
	bool debugDepth;
	bool debugVelocity;
	int debugBilateralPasses;
	int debugReflectionVariancePasses;
	bool debugPositions;
} params;

layout(binding = 1) uniform sampler2D currentImage;
layout(binding = 3) uniform sampler2D shadowsImage;
layout(binding = 4) uniform sampler2D shadowsMomentsImage;

layout(binding = 5) uniform sampler2D[] atrousPassImages;

layout(binding = 15) uniform sampler2D[] atrousPassVariance;

layout(binding = 20) uniform sampler2D reflectionsRaytracer;
layout(binding = 21) uniform sampler2D temporalReflections;
layout(binding = 22) uniform sampler2D reflectionsMomentsImage;
layout(binding = 24) uniform sampler2D reflectionsHitPoints;
layout(binding = 25) uniform sampler2D reflectionsShading;
layout(binding = 26) uniform sampler2D reflectionsDist;
layout(binding = 27) uniform sampler2D bilateralFinalColor;
layout(binding = 29) uniform sampler2D reflectionsNormalsImage;
layout(binding = 30) uniform sampler2D reflectionsBilateralImage;
layout(binding = 31) uniform sampler2D roughnessMetalnessImage;
layout(binding = 32) uniform sampler2D normalsImage;
layout(binding = 33) uniform sampler2D albedoImage;
layout(binding = 34) uniform sampler2D albedoTextureImage;
layout(binding = 35) uniform sampler2D depthImage;
layout(binding = 36) uniform sampler2D velImage;
layout(binding = 37) uniform sampler2D positionImage;
layout(binding = 38) uniform sampler2D[] bilateralPasses;

void main() 
{
	if(params.debugShadowsVarianceInitial) {
		vec2 colMoments = texture(shadowsMomentsImage, inUV).xy;
		outFragColor = vec4(colMoments.y - colMoments.x * colMoments.x, 0.0f, 0.0f, 1.0f);
	}
	else if(params.debugShadowsVarianceFinal) {
		//outFragColor = vec4(texture(atrousPassImage5, inUV).w, 0.0f, 0.0f, 1.0f);
	}
	else if(params.debugTemporalHistory) {
		float temporalHistory = texture(shadowsMomentsImage, inUV).z;
		outFragColor = vec4(temporalHistory/4.0f,temporalHistory/4.0f,temporalHistory/4.0f, 1.0f);
	}
	else if(params.debugShadows) {
		outFragColor = texture(shadowsImage, inUV);
	}
	else if(params.debugAtrousPasses > 0) {
		outFragColor = vec4(texture(atrousPassImages[params.debugAtrousPasses-1], inUV).xyz, 1.0f);
	}
	else if(params.debugAtrousPassesVariance > 0) {
		outFragColor = vec4(texture(atrousPassImages[params.debugAtrousPassesVariance-1], inUV).w, 0.0f, 0.0f, 1.0f);
	}
	else if(params.debugReflections) {
		outFragColor = vec4(texture(reflectionsRaytracer, inUV).xyz, 1.0f);
	}
	else if(params.debugTemporalReflections) {
		outFragColor = vec4(texture(temporalReflections, inUV).xyz, 1.0f);
	}
	else if(params.debugReflectionsVariance) {
		vec2 colMoments = texture(reflectionsMomentsImage, inUV).xy;
		float var = (colMoments.y - colMoments.x * colMoments.x);
		outFragColor = vec4(var, 0.0f, 0.0f, 1.0f);
	}
	else if(params.debugReflectionsHistory) {
		float history = texture(reflectionsMomentsImage, inUV).z;
		outFragColor = vec4(history/4.0f, history/4.0f, history/4.0f, 1.0f);
	}
	else if(params.debugReflectionsHitPoints) {
		outFragColor = texture(reflectionsHitPoints, inUV);
	}
	else if(params.debugReflectionsShading) {
		outFragColor = texture(reflectionsShading, inUV);
	}
	else if(params.debugKernelSize) {
		outFragColor = vec4(texture(bilateralFinalColor, inUV).w / 5.0);
	}
	else if(params.debugReflectionsNormals) {
		outFragColor = texture(reflectionsNormalsImage, inUV);
	}
	else if(params.debugReflectionsBilateral) {
		outFragColor = texture(reflectionsBilateralImage, inUV);
	}
	else if(params.debugRoughness) {
		outFragColor = vec4(texture(roughnessMetalnessImage, inUV).g);
	}	
	else if(params.debugMetalness) {
		outFragColor = vec4(texture(roughnessMetalnessImage, inUV).b);
	}	
	else if(params.debugNormals) {
		outFragColor = vec4(texture(normalsImage, inUV).rgb, 1.0f);
	}	
	else if(params.debugAlbedo) {
		outFragColor = vec4(texture(albedoImage, inUV).rgb, 1.0f);
	}
	else if(params.debugAlbedoTexture) {
		outFragColor = vec4(texture(albedoTextureImage, inUV).rgb, 1.0f);
	}
	else if(params.debugDepth) {
		outFragColor = vec4(texture(depthImage, inUV).r, 0.0f, 0.0f, 1.0f);
	}
	else if(params.debugVelocity) {
		outFragColor = vec4(texture(velImage, inUV).rg, 0.0f, 1.0f);
	}
	else if(params.debugPositions) {
		outFragColor = vec4(texture(positionImage, inUV).rgb , 1.0f);
	}
	else if(params.debugBilateralPasses > 0) {
		outFragColor = vec4(texture(bilateralPasses[params.debugBilateralPasses-1], inUV).xyz, 1.0f);
	}
	else if(params.debugReflectionVariancePasses > 0) {
		outFragColor = vec4(texture(bilateralPasses[params.debugReflectionVariancePasses-1], inUV).w * 100.0f, 0.0f, 0.0f, 1.0f);
	}
	else {
		vec3 result = texture(currentImage, inUV).rgb;
		outFragColor = vec4(result, 1.0f);
	}
}