#pragma once
#include "VulkanInstance.h"
#include <set>

//Interface between the host and the device
class VulkanDevice
{
private:
	VulkanInstance instance;
	VkDevice logicalDeviceHandle;

	bool deviceFeatures2Initialized;
	VkPhysicalDeviceFeatures2 deviceFeatures2;
	bool deviceProperties2Initialized;
	VkPhysicalDeviceProperties2 deviceProperties2;
	VkPhysicalDeviceAccelerationStructurePropertiesKHR accelProps{};
	VkPhysicalDeviceRayTracingPipelinePropertiesKHR pipelineProps{};
	//features required by raytracing
	VkPhysicalDeviceBufferDeviceAddressFeatures enabledBufferDeviceAddresFeatures = {};
	VkPhysicalDeviceDescriptorIndexingFeatures indexingFeatures = {};
	VkPhysicalDeviceAccelerationStructureFeaturesKHR accelerationStructureFeatures = {};
	VkPhysicalDeviceRayTracingPipelineFeaturesKHR rayTracingFeatures = {};

public:
	struct Queues {
		VkQueue graphicsQueue;
		VkQueue presentationQueue;
	} queues;

	VulkanDevice();
	VulkanDevice(VulkanInstance instance);
	void cleanup();
	VkDevice& getLogicalDeviceHandle();
	VkPhysicalDevice& getPhysicalDeviceHandle();
	void nameVulkanObject(VkObjectType objectType, uint64_t objectHandle, std::string name);
	VkPhysicalDeviceAccelerationStructureFeaturesKHR& getAccelerationStructureFeatures();
	VkPhysicalDeviceProperties2& getDeviceProperties2();
	Queues& getQueues();

private:
	void setEnabledFeatures();
	void createLogicalDevice();
};