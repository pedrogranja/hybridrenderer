#include "VertexFragPipeline.h"

VertexFragPipeline::VertexFragPipeline()
{
}

VertexFragPipeline::VertexFragPipeline(std::string name,
                                       const char* vertexShaderFile,
                                       const char* fragShaderFile,
                                       VulkanDevice& vulkanDevice,
                                       SwapChain& swapChain,
                                       Pass& renderPass,
                                       std::vector<VkPipelineColorBlendAttachmentState>& colorBlendAttachments,
                                       std::vector<VkDescriptorSetLayout>& descriptorLayouts,
                                       std::vector<VkVertexInputBindingDescription>& vertexBindingDescription,
                                       std::vector<VkVertexInputAttributeDescription>& vertexAttributeDescription,
                                       std::vector<VkPushConstantRange>& pushConstantRanges,
                                       VkCullModeFlags cullMode,
                                       VkCompareOp depthCompareOp) : vertexShaderFile(vertexShaderFile),
                                                                     fragShaderFile(fragShaderFile),
                                                                     vulkanDevice(vulkanDevice),
                                                                     swapChain(swapChain),
                                                                     renderPass(renderPass)
{
    createPipeline(colorBlendAttachments, descriptorLayouts, vertexBindingDescription, vertexAttributeDescription, pushConstantRanges, cullMode, depthCompareOp);
}

VertexFragPipeline::VertexFragPipeline(std::string name, 
                                       const char* vertexShaderFile,
                                       const char* fragShaderFile,
                                       VulkanDevice& vulkanDevice,
                                       SwapChain& swapChain,
                                       Pass& renderPass,
                                       std::vector<VkPipelineColorBlendAttachmentState>&& colorBlendAttachments,
                                       std::vector<VkDescriptorSetLayout>&& descriptorLayouts,
                                       std::vector<VkVertexInputBindingDescription>&& vertexBindingDescription,
                                       std::vector<VkVertexInputAttributeDescription>&& vertexAttributeDescription,
                                       std::vector<VkPushConstantRange>&& pushConstantRanges,
                                       VkCullModeFlags cullMode,
                                       VkCompareOp depthCompareOp) : vertexShaderFile(vertexShaderFile),
                                                                     fragShaderFile(fragShaderFile),
                                                                     vulkanDevice(vulkanDevice),
                                                                     swapChain(swapChain),
                                                                     renderPass(renderPass)
{
    createPipeline(colorBlendAttachments, descriptorLayouts, vertexBindingDescription, vertexAttributeDescription, pushConstantRanges, cullMode, depthCompareOp);
}

VertexFragPipeline::VertexFragPipeline(std::string name, 
                                       const char* vertexShaderFile,
                                       const char* fragShaderFile,
                                       VulkanDevice& vulkanDevice,
                                       SwapChain& swapChain,
                                       Pass& renderPass,
                                       std::vector<VkPipelineColorBlendAttachmentState>&& colorBlendAttachments,
                                       std::vector<VkDescriptorSetLayout>& descriptorLayouts,
                                       std::vector<VkVertexInputBindingDescription>&& vertexBindingDescription,
                                       std::vector<VkVertexInputAttributeDescription>&& vertexAttributeDescription,
                                       std::vector<VkPushConstantRange>&& pushConstantRanges,
                                       VkCullModeFlags cullMode,
                                       VkCompareOp depthCompareOp) : name(name),
                                                                     vertexShaderFile(vertexShaderFile),
                                                                     fragShaderFile(fragShaderFile),
                                                                     vulkanDevice(vulkanDevice),
                                                                     swapChain(swapChain),
                                                                     renderPass(renderPass)
{
    createPipeline(colorBlendAttachments, descriptorLayouts, vertexBindingDescription, vertexAttributeDescription, pushConstantRanges, cullMode, depthCompareOp);
}


void VertexFragPipeline::cleanup()
{
    vkDestroyPipeline(vulkanDevice.getLogicalDeviceHandle(), pipeline, nullptr);
    vkDestroyPipelineLayout(vulkanDevice.getLogicalDeviceHandle(), pipelineLayout, nullptr);
}

VkPipelineLayout& VertexFragPipeline::getPipelineLayout()
{
    return pipelineLayout;
}

VkPipeline& VertexFragPipeline::getPipeline()
{
    return pipeline;
}

void VertexFragPipeline::createPipeline(std::vector<VkPipelineColorBlendAttachmentState>& colorBlendAttachments,
                                        std::vector<VkDescriptorSetLayout>& descriptorLayouts,
                                        std::vector<VkVertexInputBindingDescription>& vertexBindingDescription,
                                        std::vector<VkVertexInputAttributeDescription>& vertexAttributeDescription,
                                        std::vector<VkPushConstantRange>& pushConstantRanges,
                                        VkCullModeFlags cullMode,
                                        VkCompareOp depthCompareOp)
{
    VkShaderModule vertShaderModule = utils::loadShader(vertexShaderFile, vulkanDevice.getLogicalDeviceHandle());
    VkShaderModule fragShaderModule = utils::loadShader(fragShaderFile, vulkanDevice.getLogicalDeviceHandle());

    VkPipelineShaderStageCreateInfo vertShaderStageInfo = initializers::vertexShaderCreateInfo(vertShaderModule);
    VkPipelineShaderStageCreateInfo fragShaderStageInfo = initializers::fragShaderCreateInfo(fragShaderModule);

    std::vector<VkPipelineShaderStageCreateInfo> shaderStages = { vertShaderStageInfo, fragShaderStageInfo };

    auto vertexInputInfo = initializers::pipelineVertexInputStateCreateInfo(vertexBindingDescription, vertexAttributeDescription);
    auto inputAssembly = initializers::pipelineInputAssemblyStateCreateInfo(); // describes the way the vertexs are gonna be used
    auto viewport = initializers::viewport((float)swapChain.getSwapChainExtent().width, (float)swapChain.getSwapChainExtent().height);
    auto scissor = initializers::scissor(swapChain.getSwapChainExtent());
    auto viewportState = initializers::pipelineViewportStateCreateInfo(viewport, scissor);
    std::vector<VkDynamicState> dynamicStateEnables = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
    VkPipelineDynamicStateCreateInfo dynamicStateInfo{};
    dynamicStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicStateInfo.pDynamicStates = dynamicStateEnables.data();
    dynamicStateInfo.dynamicStateCount = static_cast<uint32_t>(dynamicStateEnables.size());

    auto rasterizer = initializers::pipelineRasterizationStateCreateInfo(VK_POLYGON_MODE_FILL, cullMode, VK_FRONT_FACE_COUNTER_CLOCKWISE);
    auto multisampling = initializers::pipelineMultisampleStateCreateInfo(sampleCount);

    auto colorBlending = initializers::pipelineColorBlendStateCreateInfo(colorBlendAttachments.data(), static_cast<uint32_t>(colorBlendAttachments.size()));
    //depth attachment
    auto depthStencil = initializers::pipelineDepthStencilStateCreateInfo(depthCompareOp);

    VkPipelineLayoutCreateInfo pipelineLayoutInfo =
        initializers::pipelineLayoutCreateInfo(static_cast<uint32_t>(descriptorLayouts.size()), descriptorLayouts.data());
    pipelineLayoutInfo.pushConstantRangeCount = static_cast<uint32_t>(pushConstantRanges.size());
    pipelineLayoutInfo.pPushConstantRanges = pushConstantRanges.data();
    VK_CHECK_RESULT(vkCreatePipelineLayout(vulkanDevice.getLogicalDeviceHandle(), &pipelineLayoutInfo, nullptr, &pipelineLayout));

    VkGraphicsPipelineCreateInfo pipelineInfo{};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = shaderStages.data();
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pDynamicState = &dynamicStateInfo;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDepthStencilState = &depthStencil;
    pipelineInfo.layout = pipelineLayout;
    pipelineInfo.renderPass = renderPass.getRenderPassHandle();
    pipelineInfo.subpass = 0;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

    VK_CHECK_RESULT(vkCreateGraphicsPipelines(vulkanDevice.getLogicalDeviceHandle(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &pipeline));

    vulkanDevice.nameVulkanObject(VK_OBJECT_TYPE_PIPELINE_LAYOUT, (uint64_t) pipelineLayout, name + " Pipeline Layout");
    vulkanDevice.nameVulkanObject(VK_OBJECT_TYPE_PIPELINE, (uint64_t) pipeline, name + " Pipeline");

    vkDestroyShaderModule(vulkanDevice.getLogicalDeviceHandle(), fragShaderModule, nullptr);
    vkDestroyShaderModule(vulkanDevice.getLogicalDeviceHandle(), vertShaderModule, nullptr);
}