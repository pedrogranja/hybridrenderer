#version 450

layout(set = 0, binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;

	mat4 prevModel;
	mat4 prevView;
	mat4 prevProj;

    int id;
    vec2 jitter;
} ubo;

layout(set = 1, binding = 0) uniform Material {
	int albedoTextureSet;
	int metallicRoughnessTextureSet;
	int normalTextureSet;
	int occlusionTextureSet;
	int emissiveTextureSet;
	float metallicFactor;
	float roughnessFactor;
	vec3 baseColor;
	vec3 baseEmissivity;
} mat;
layout(set = 1, binding = 1) uniform sampler2D texSampler;
layout(set = 1, binding = 2) uniform sampler2D normalSampler;
layout(set = 1, binding = 3) uniform sampler2D aoSampler;
layout(set = 1, binding = 4) uniform sampler2D emissionSampler;
layout(set = 1, binding = 5) uniform sampler2D metalRoughnessSampler;

layout(set = 2, binding = 0) uniform UiShaderInputs {
	float indirectLightingWeight;
	bool directDiffuse;
	bool directSpecular;
	bool indirectDiffuse;
	bool indirectSpecular;
	bool useEmissionTexture;
	bool useRoughnessTexture;
	bool useMetalnessTexture;
	bool useAoTexture;
	bool useAlbedoTexture;
	bool useNormalTexture;
    int lightToDebug;
	bool fresnelFunctionOnly;
	bool uniformDistributionFunctionOnly;
	bool geometryFunctionOnly;
	float baseRoughness;
	float baseMetalness;
    float exposure;
} globalShaderInputs;

layout(location = 0) in vec4 fragPos;
layout(location = 1) in vec3 fragColor;
layout(location = 2) in vec3 fragNormal;
layout(location = 3) in vec2 fragTexCoord;
layout(location = 4) in vec4 fragPosScreen;
layout(location = 5) in vec4 prevFragPosScreen;
layout(location = 6) in float depth;
layout(location = 7) flat in int id;


layout (location = 0) out vec4 outPosition;
layout (location = 1) out vec4 outAlbedoAo;
layout (location = 2) out vec4 outNormal;
layout (location = 3) out vec4 outMetalRoughness;
layout (location = 4) out vec4 outNormalsTextureless;
layout (location = 5) out vec4 outVelocity;
layout (location = 6) out vec4 outDepth;
layout (location = 7) out vec4 outTextureAlbedo;

//https://github.com/JoeyDeVries/LearnOpenGL/blob/bc41d2c0192220fb146c5eabf05f3d8317851200/src/6.pbr/1.2.lighting_textured/1.2.pbr.fs
mat3 getTBN() {

    vec3 q1 = dFdx(fragPos).xyz;
	vec3 q2 = dFdy(fragPos).xyz;
	vec2 st1 = dFdx(fragTexCoord);
	vec2 st2 = dFdy(fragTexCoord);

    vec3 N = normalize(fragNormal);
	vec3 T = normalize(q1 * st2.t - q2 * st1.t);
	vec3 B = -normalize(cross(N, T));
	mat3 TBN = mat3(T, B, N);

	return TBN;
}

void main()
{
	//World Space Positions
	//Small offset to remove artifacts on low roughness shading
	outPosition = fragPos + 0.00001f;

	//Texture albedo
	if(mat.albedoTextureSet > -1 && globalShaderInputs.useAlbedoTexture) {
        outTextureAlbedo = texture(texSampler,fragTexCoord);
    }
	else {
		outTextureAlbedo = vec4(1.0f);
	}
	if(outTextureAlbedo.w <= 0.01f)
		discard;
		
	//Albedo
	vec4 albedo = vec4(fragColor * mat.baseColor, 1.0);
	if(albedo.w <= 0.01f)
		discard;
	
	// AO
	if(mat.occlusionTextureSet > -1 && globalShaderInputs.useAoTexture) {
		albedo.w = texture(aoSampler,fragTexCoord).r;
	}
	outAlbedoAo = albedo;

	//World Space Normals
	//Small offset to remove artifacts on low roughness shading
	vec3 N = normalize(fragNormal) + 0.00001f;
	outNormalsTextureless = vec4(normalize(fragNormal), 1.0f);

	//World Space Texture Normals
	if(mat.normalTextureSet > -1 && globalShaderInputs.useNormalTexture) {
		mat3 TBN = getTBN();
	    vec3 tangentNormal = texture(normalSampler,fragTexCoord).rgb * 2.0 - 1.0;
		vec3 textureNormal = TBN * tangentNormal;

		if(!isnan(textureNormal.x) && !isnan(textureNormal.y) && !isnan(textureNormal.z))
			N = normalize(textureNormal) + 0.00001f; //Small offset to remove artifacts on low roughness shading
	}
	outNormal = vec4(N,id);

	//Roughness Metalness
	float metal = globalShaderInputs.baseMetalness * mat.metallicFactor;
	float roughness = globalShaderInputs.baseRoughness * mat.roughnessFactor;
    if(mat.metallicRoughnessTextureSet > -1) {
		vec4 metalRoughness = texture(metalRoughnessSampler,fragTexCoord);

		if(globalShaderInputs.useMetalnessTexture)
			metal *= texture(metalRoughnessSampler,fragTexCoord).b;

		if(globalShaderInputs.useRoughnessTexture)
			roughness *= texture(metalRoughnessSampler,fragTexCoord).g;
    }
	outMetalRoughness = vec4(0.0f, roughness, metal, 0.0f);

	//Velocity
    vec3 fragPosNDC = fragPosScreen.xyz / fragPosScreen.w; 
    vec3 prevFragPosNDC = prevFragPosScreen.xyz / prevFragPosScreen.w;
	fragPosNDC = fragPosNDC * 0.5f + 0.5f; // convert from [-1,1] to [0,1] range
	prevFragPosNDC = prevFragPosNDC * 0.5f + 0.5f; // convert from [-1,1] to [0,1]

    vec2 velocity = fragPosNDC.xy - prevFragPosNDC.xy - vec2(ubo.jitter.x, ubo.jitter.y);
	outVelocity = vec4(velocity, 0.0, 0.0);
	
	//Linear Camera Space Depth and Linear Camera Space Depth Derivatives
	float linearDepth = fragPosScreen.z * fragPos.w;
	float ddxDepth = abs(dFdx(linearDepth));
	float ddyDepth = abs(dFdy(linearDepth));
	outDepth = vec4(linearDepth, ddxDepth, ddyDepth, gl_FragCoord.z);
}