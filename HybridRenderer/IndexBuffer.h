#pragma once
#include "VulkanDevice.h"
#include "utils.hpp"
#include "RaytracingUtils.hpp"

class IndexBuffer
{
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingMemory;

public:
	int size;
	VkCommandPool commandPool;
	VulkanDevice device;

	VkBuffer indexBuffer;
	VkDeviceMemory indexMemory;
	uint64_t devicePntr;

	IndexBuffer();
	IndexBuffer(VulkanDevice& device, VkCommandPool commandPool, int size);
	void submitDataToStageBuffer(std::vector<uint32_t> indices);
	void submitDataToDevice();
	void cleanup();
};

