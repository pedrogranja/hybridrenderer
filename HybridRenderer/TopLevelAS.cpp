#include "TopLevelAS.h"

TopLevelAS::TopLevelAS()
{
}

TopLevelAS::TopLevelAS(VulkanDevice& device,
					   VulkanInstance& instance,
					   VkCommandPool& commandPool) : device(device),
													 instance(instance),
													 commandPool(commandPool)
{
}

void TopLevelAS::addBlas(BottomLevelAS& bottomLevelAS)
{
	bottomLevelASs.push_back(&bottomLevelAS);
}

void TopLevelAS::cleanup()
{
	utils::vkDestroyAccelerationStructureKHR(device.getLogicalDeviceHandle(), accelerationStructure, nullptr);

	tlasBuffer.cleanup();

	instancesBuffer.cleanup();
}

VkAccelerationStructureKHR& TopLevelAS::getAccelerationStructure()
{
	return accelerationStructure;
}

void TopLevelAS::createTopLevelAS()
{
	//Create the bottom level instances buffer
	std::vector<VkAccelerationStructureInstanceKHR> bottomLevelAsInstances;
	for (int i = 0; i < bottomLevelASs.size(); i++) {
		bottomLevelAsInstances.push_back(bottomLevelASs[i]->getAccelerationStructureInstanceKHR());
	}

	instancesBuffer = Buffer("Instances Buffer",
							 device,
							 device.getQueues().graphicsQueue,
							 sizeof(VkAccelerationStructureInstanceKHR) * static_cast<int>(bottomLevelAsInstances.size()),
							 VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
							 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	instancesBuffer.submitDataToMemory(bottomLevelAsInstances.data());

	this->instancesBufferHandle = utils::getBufferDeviceAddress(instancesBuffer.buffer, device.getLogicalDeviceHandle());

	//Wrap the device instances pointer
	VkAccelerationStructureGeometryInstancesDataKHR instances = {};
	instances.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
	instances.arrayOfPointers = VK_FALSE;
	instances.data.deviceAddress = this->instancesBufferHandle;

	//Define the geometries of this acceleration structure
	VkAccelerationStructureGeometryKHR topAsGeometry = {};
	topAsGeometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
	topAsGeometry.geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR;
	topAsGeometry.geometry.instances = instances;

	VkAccelerationStructureBuildGeometryInfoKHR buildGeometryInfo = {};
	buildGeometryInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
	buildGeometryInfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR;
	buildGeometryInfo.geometryCount = 1;
	buildGeometryInfo.pGeometries = &topAsGeometry;
	buildGeometryInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
	buildGeometryInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
	buildGeometryInfo.srcAccelerationStructure = nullptr;

	uint32_t size = static_cast<uint32_t>(bottomLevelASs.size());
	VkAccelerationStructureBuildSizesInfoKHR buildSizeInfo = utils::getBuildSizes(&size, &buildGeometryInfo, device.getLogicalDeviceHandle());
	
	//the buffer that will hold the acceleration structure
	tlasBuffer = Buffer("TLAS Buffer", 
						device,
						device.getQueues().graphicsQueue,
						static_cast<int>(buildSizeInfo.accelerationStructureSize),
						VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
						VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	//create the acceleration structure
	VkAccelerationStructureCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
	createInfo.pNext = nullptr;
	createInfo.type = buildGeometryInfo.type;
	createInfo.size = buildSizeInfo.accelerationStructureSize;
	createInfo.buffer = tlasBuffer.buffer;
	createInfo.offset = 0;

	VK_CHECK_RESULT(utils::vkCreateAccelerationStructureKHR(device.getLogicalDeviceHandle(), &createInfo, nullptr, &accelerationStructure));

	VkAccelerationStructureDeviceAddressInfoKHR accelerationDeviceAddressInfo = {};
	accelerationDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
	accelerationDeviceAddressInfo.accelerationStructure = accelerationStructure;
	this->topLevelAsDeviceHandle = utils::vkGetAccelerationStructureDeviceAddressKHR(device.getLogicalDeviceHandle(), &accelerationDeviceAddressInfo);

	//scratch buffer for the build
	scratchBuffer = RayTracingScratchBuffer(buildSizeInfo.accelerationStructureSize, device, instance);

	//the offsets for the geometry (in this case instances)
	VkAccelerationStructureBuildRangeInfoKHR buildOffsetInfo = {};
	buildOffsetInfo.primitiveCount = static_cast<uint32_t>(bottomLevelASs.size());
	std::vector<VkAccelerationStructureBuildRangeInfoKHR*> accelerationBuildStructureRangeInfos = { &buildOffsetInfo };

	buildGeometryInfo.dstAccelerationStructure = accelerationStructure;
	buildGeometryInfo.scratchData.deviceAddress = scratchBuffer.deviceAddress;

	if(device.getAccelerationStructureFeatures().accelerationStructureHostCommands) {
		utils::vkBuildAccelerationStructuresKHR(device.getLogicalDeviceHandle(),
												VK_NULL_HANDLE,
												1, 
												&buildGeometryInfo,
												accelerationBuildStructureRangeInfos.data());
	}
	else {
		VkCommandBuffer cmdBuf = utils::beginSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool);
		utils::vkCmdBuildAccelerationStructuresKHR(cmdBuf, 1, &buildGeometryInfo, accelerationBuildStructureRangeInfos.data());
		utils::endSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool, device.getQueues().graphicsQueue, cmdBuf);
	}
	scratchBuffer.cleanup();
}

void TopLevelAS::update()
{
	std::vector<VkAccelerationStructureInstanceKHR> bottomLevelAsInstances;
	for (int i = 0; i < bottomLevelASs.size(); i++) {
		bottomLevelAsInstances.push_back(bottomLevelASs[i]->getAccelerationStructureInstanceKHR());
	}

	instancesBuffer.submitDataToMemory(bottomLevelAsInstances.data());

	this->instancesBufferHandle = utils::getBufferDeviceAddress(instancesBuffer.buffer, device.getLogicalDeviceHandle());

	//Wrap the device instances pointer
	VkAccelerationStructureGeometryInstancesDataKHR instances = {};
	instances.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
	instances.arrayOfPointers = VK_FALSE;
	instances.data.deviceAddress = this->instancesBufferHandle;

	VkAccelerationStructureGeometryKHR topAsGeometry = {};
	topAsGeometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
	topAsGeometry.geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR;
	topAsGeometry.geometry.instances = instances;

	VkAccelerationStructureBuildGeometryInfoKHR buildGeometryInfo = {};
	buildGeometryInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
	buildGeometryInfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR;
	buildGeometryInfo.geometryCount = 1;
	buildGeometryInfo.pGeometries = &topAsGeometry;
	buildGeometryInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
	buildGeometryInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
	buildGeometryInfo.srcAccelerationStructure = accelerationStructure;
	buildGeometryInfo.dstAccelerationStructure = accelerationStructure;

	uint32_t size = static_cast<uint32_t>(bottomLevelASs.size());
	VkAccelerationStructureBuildSizesInfoKHR buildSizeInfo = utils::getBuildSizes(&size, &buildGeometryInfo, device.getLogicalDeviceHandle());

	//scratch buffer for the build
	Buffer scratchBufferUpdate = Buffer("Scratch Buffer",
										device,
										device.getQueues().graphicsQueue,
										static_cast<uint32_t>(buildSizeInfo.updateScratchSize),
									    VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR,
									    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	VkAccelerationStructureBuildRangeInfoKHR buildOffsetInfo = {};
	buildOffsetInfo.primitiveCount = static_cast<uint32_t>(bottomLevelASs.size());
	VkAccelerationStructureBuildRangeInfoKHR* accelerationBuildOffsetInfoPtr = &buildOffsetInfo;

	buildGeometryInfo.dstAccelerationStructure = accelerationStructure;
	buildGeometryInfo.scratchData.deviceAddress = scratchBufferUpdate.getDeviceAddress();

	VkCommandBuffer cmdBuf = utils::beginSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool);
	utils::vkCmdBuildAccelerationStructuresKHR(cmdBuf, 1, &buildGeometryInfo, &accelerationBuildOffsetInfoPtr);
	utils::endSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool, device.getQueues().graphicsQueue, cmdBuf);

	scratchBufferUpdate.cleanup();
}