#pragma once
#include "VulkanDevice.h"
#include "RaytracingUtils.hpp"
#include "Initializers2.hpp"

class RaytracingPipeline
{
public:

	struct ShaderDescription {
		int index;
		std::string filename;
	};

private:
	VulkanDevice device;
	VkDescriptorSetLayout descriptorLayout;
	std::vector<ShaderDescription> raygenShaderDescription;
	std::vector<ShaderDescription> missShaderDescription;
	std::vector<ShaderDescription> closestHitShaderDescription;
	std::vector<ShaderDescription> shaderDescriptions;
	std::vector<VkRayTracingShaderGroupCreateInfoKHR> shaderGroups;
	VkPipelineLayout pipelineLayout;
	VkPipeline pipeline;
	std::string raygenFileName;
	std::string name;

public:
	RaytracingPipeline();
	RaytracingPipeline(std::string name,
					   VulkanDevice& device,
					   VkDescriptorSetLayout descriptorLayout, 
					   std::vector<ShaderDescription>&& raygenShaderDescription,
					   std::vector<ShaderDescription>&& missShaderDescription,
					   std::vector<ShaderDescription>&& closestHitShaderDescription);
	std::vector<VkRayTracingShaderGroupCreateInfoKHR> getShaderGroups();
	VkPipeline getPipeline();
	VkPipelineLayout getPipelineLayout();
	void cleanup();
	std::vector<ShaderDescription>& getShaderDescriptions();
	std::vector<ShaderDescription>& getRaygenShaderDescription();
	std::vector<ShaderDescription>& getClosestHitShaderDescription();
	std::vector<ShaderDescription>& getMissShaderDescription();

private:
	void createPipeline();
};