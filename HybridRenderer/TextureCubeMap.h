#pragma once
#include "TextureImage.h"
#include <gli.hpp>
#include <vulkan/vulkan.h>
#include "initializers2.hpp"
#include "utils.hpp"

class TextureCubeMap
{
	VulkanDevice device;
	VkCommandPool commandPool;
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingMemory;

public:
	int texWidth;
	int texHeight;
	int texChannels;

	VkFormat format = format;
	bool stbiDelete;
	bool stageBuffersDeleted;
	bool inDeviceMemory;
	int mipLevels;
	gli::texture_cube texCube;

	VkImage textureImage;
	VkDeviceMemory textureImageMemory;
	VkImageView textureImageView;
	VkSampler textureSampler;
	VkDeviceSize imageSize;

	TextureCubeMap();

	TextureCubeMap(VulkanDevice& device,
				   VkCommandPool commandPool,
				   VkFormat format = VK_FORMAT_R16G16B16A16_SFLOAT,
				   int texWidth = 1024,
				   int texHeight = 1024,
				   int texChannels = 4,
				   int mipLevels = 0);

	TextureCubeMap(VulkanDevice& device,
				   VkCommandPool commandPool, 
				   const char* imageFile);

	void importCubeMap(const char* imageFile);
	VkDescriptorSetLayout createDescriptorLayout();
	void submitImageToStageMemory();
	void submitImageToDeviceMemory();
	void cleanup();

private:
	void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
};

