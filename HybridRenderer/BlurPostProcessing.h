#pragma once
#include "vulkan/vulkan.h"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_vulkan.h"
#include "utils.hpp"
#include "UniformBuffer.h"
#include "SceneGraph.h"
#include "VertexFragPipeline.h"
#include "FrameGraph.h"

class BlurPostProcessing
{
public:
	struct BlurParams {
		alignas(4) float blurScale = 1.0f;
		alignas(4) float blurStrength = 1.5f;
		alignas(4) int blurHorizontal = 1;
	} blurParams;

	utils::QueueFamilyIndices queueFamilyIndices;
	VkDescriptorPool descriptorPool;
	VkCommandPool commandPool;
	VkPhysicalDevice physicalDevice;
	VkDevice device;
	Pass renderpass;
	VkQueue graphicsQueue;
	SwapChain swapChain;
	VkDescriptorSetLayout descriptorLayout;
	std::vector<VkDescriptorSet> descriptorSets;
	std::vector<UniformBuffer> uniformBuffers;
	TextureImage previousImage;
	VertexFragPipeline* pipeline;

	BlurPostProcessing();
	BlurPostProcessing(VkPhysicalDevice physicalDevice,
					   VkDevice device,
					   VkQueue graphicsQueue,
					   utils::QueueFamilyIndices& queueFamilyIndices,
					   SwapChain& swapChain,
					   Pass& renderpass,
					   TextureImage& previousImage,
					   VertexFragPipeline* pipeline,
					   bool blurHor = true);
	void createCommandPool();
	void createDescriptorPool();
	void initUniformBuffers();
	void createDescriptorSetLayout();
	void createDescriptorSet();
	void update(uint32_t imageIndex);
	void draw(VkCommandBuffer commandBuffer, uint32_t imageIndex);
	void cleanup();
};