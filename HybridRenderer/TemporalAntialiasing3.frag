#version 450


//adapted from https://github.com/NVIDIAGameWorks/Falcor/blob/c0729e806045731d71cfaae9d31a992ac62070e7/Source/RenderPasses/Antialiasing/TAA/TAA.ps.slang


layout(set=0, binding = 0) uniform Params {
	float colorBoxSigma;
    float alpha;
    int width;
    int height;
    bool useTemporalAntialiasing;
} params;

layout (location = 0) in vec2 texC;
layout(location = 0) out vec4 FragColor;

layout(set=0, binding = 1) uniform sampler2D gTexColor;
layout(set=0, binding = 2) uniform sampler2D gTexMotionVec;
layout(set=0, binding = 3) uniform sampler2D gTexPrevColor;

/** Converts color from RGB to YCgCo space
    \param RGBColor linear HDR RGB color
*/
vec3 RGBToYCgCo(vec3 rgb)
{
    float Y = dot(rgb, vec3(0.25f, 0.50f, 0.25f));
    float Cg = dot(rgb, vec3(-0.25f, 0.50f, -0.25f));
    float Co = dot(rgb, vec3(0.50f, 0.00f, -0.50f));
    return vec3(Y, Cg, Co);
}

/** Converts color from YCgCo to RGB space
    \param YCgCoColor linear HDR YCgCo color
*/
vec3 YCgCoToRGB(vec3 YCgCo)
{
    float tmp = YCgCo.x - YCgCo.y;
    float r = tmp + YCgCo.z;
    float g = YCgCo.x + YCgCo.y;
    float b = tmp - YCgCo.z;
    return vec3(r, g, b);
}


// Catmull-Rom filtering code from http://vec3.ca/bicubic-filtering-in-fewer-taps/
vec3 bicubicSampleCatmullRom(sampler2D tex, vec2 samplePos, vec2 texDim)
{
    vec2 invTextureSize = 1.0 / texDim;
    vec2 tc = floor(samplePos - 0.5) + 0.5;
    vec2 f = samplePos - tc;
    vec2 f2 = f * f;
    vec2 f3 = f2 * f;

    vec2 w0 = f2 - 0.5 * (f3 + f);
    vec2 w1 = 1.5 * f3 - 2.5 * f2 + 1;
    vec2 w3 = 0.5 * (f3 - f2);
    vec2 w2 = 1 - w0 - w1 - w3;

    vec2 w12 = w1 + w2;
    
    vec2 tc0 = (tc - 1) * invTextureSize;
    vec2 tc12 = (tc + w2 / w12) * invTextureSize;
    vec2 tc3 = (tc + 2) * invTextureSize;

    vec3 result =
        texture(tex, vec2(tc0.x,  tc0.y), 0).rgb  * (w0.x  * w0.y) +
        texture(tex, vec2(tc0.x,  tc12.y), 0).rgb * (w0.x  * w12.y) +
        texture(tex, vec2(tc0.x,  tc3.y), 0).rgb  * (w0.x  * w3.y) +
        texture(tex, vec2(tc12.x, tc0.y), 0).rgb  * (w12.x * w0.y) +
        texture(tex, vec2(tc12.x, tc12.y), 0).rgb * (w12.x * w12.y) +
        texture(tex, vec2(tc12.x, tc3.y), 0).rgb  * (w12.x * w3.y) +
        texture(tex, vec2(tc3.x,  tc0.y), 0).rgb  * (w3.x  * w0.y) +
        texture(tex, vec2(tc3.x,  tc12.y), 0).rgb * (w3.x  * w12.y) +
        texture(tex, vec2(tc3.x,  tc3.y), 0).rgb  * (w3.x  * w3.y);

    return result;
}


void main()
{
    if(!params.useTemporalAntialiasing) {
        FragColor = texture(gTexColor, texC);
        return;
    }

    const ivec2 offset[8] = { ivec2(-1, -1), ivec2(-1,  1),
                              ivec2( 1, -1), ivec2( 1,  1), 
                              ivec2( 1,  0), ivec2( 0, -1), 
                              ivec2( 0,  1), ivec2(-1,  0), };
    
    vec2 texDim = vec2(params.width, params.height);

    vec2 pos = texC * texDim;
    ivec2 ipos = ivec2(pos);

    // Fetch the current pixel color and compute the color bounding box
    // Details here: http://www.gdcvault.com/play/1023521/From-the-Lab-Bench-Real
    // and here: http://cwyman.org/papers/siga16_gazeTrackedFoveatedRendering.pdf
    vec3 color = texelFetch(gTexColor, ipos, 0).rgb;
    color = RGBToYCgCo(color);
    vec3 colorAvg = color;
    vec3 colorVar = color * color;

    for (int k = 0; k < 8; k++) 
    {
        vec3 c = texelFetch(gTexColor, ipos + offset[k], 0).rgb;
        c = RGBToYCgCo(c);
        colorAvg += c;
        colorVar += c * c;
    }

    float oneOverNine = 1.0 / 9.0;
    colorAvg *= oneOverNine;
    colorVar *= oneOverNine;

    vec3 sigma = sqrt(max(vec3(0.0f), colorVar - colorAvg * colorAvg));
    vec3 colorMin = colorAvg - params.colorBoxSigma * sigma;
    vec3 colorMax = colorAvg + params.colorBoxSigma * sigma;    

    // Find the longest motion vector
    vec2 motion = texelFetch(gTexMotionVec, ipos, 0).xy;
    for (int a = 0; a < 8; a++) 
    {
        vec2 m = texelFetch(gTexMotionVec, ipos + offset[a], 0).xy;
        motion = dot(m, m) > dot(motion, motion) ? m : motion;   
    }

    // Use motion vector to fetch previous frame color (history)
    vec3 history = bicubicSampleCatmullRom(gTexPrevColor, (texC - motion) * texDim, texDim);

    history = RGBToYCgCo(history);

    // Anti-flickering, based on Brian Karis talk @Siggraph 2014
    // https://de45xmedrsdbp.cloudfront.net/Resources/files/TemporalAA_small-59732822.pdf
    // Reduce blend factor when history is near clamping
    float distToClamp = min(abs(colorMin.x - history.x), abs(colorMax.x - history.x));
    float alpha = clamp((params.alpha * distToClamp) / (distToClamp + colorMax.x - colorMin.x), 0.0f, 1.0f);

    history = clamp(history, colorMin, colorMax);
    vec3 result = YCgCoToRGB(mix(history, color, alpha));
    FragColor = vec4(result, 0);
}