#version 450

#include "rand.glsl"

const float EPSILON = 0.00001;
float SIGMA_D = 0.5f;

layout (location = 0) in vec2 inUV;

layout(set=0, binding = 0) uniform Params {
	bool useNormalWeight;
	float reflectionNormalSigma;
	bool useLumaWeight;
	float reflectionsLuminanceSigma;
	bool useDepthWeight;
	float depthSigma;
	int currBlurIteration;
	int maxBlurIteration;
	bool useRoughnessAdaptativeIterations;
	bool useSeparatedBilateral;
	bool useAtrous;
	int blurScale;
	int width;
	int height;
	bool useImplicitSamplingOfEnvironmentMap;
} params;

layout(set=0, binding = 1) uniform sampler2D currentImage;
layout(set=0, binding = 2) uniform sampler2D normalImage;
layout(set=0, binding = 3) uniform sampler2D roughnessMetalImage;
layout(set=0, binding = 4) uniform sampler2D depthDerivativesSampler;

const float gaussKernel[9] = float[9](1.0/16.0, 1.0/8.0, 1.0/16.0,
									  1.0/8.0, 1.0/4.0, 1.0/8.0, 
									  1.0/16.0, 1.0/8.0, 1.0/16.0);
float varGauss(vec2 uv) {
	int c = 0;
	float res = 0.0;
	vec2 texelSize = 1.0 / textureSize(currentImage,0).xy;
	for(int i=-1; i <= 1; i++) {
		for(int j=-1; j <= 1; j++) {
			float var = texture(currentImage, uv + vec2(texelSize.x*i, texelSize.y*j)).w;
			res += gaussKernel[c] * var;
			c++;
		}
	}
	return res;
}

const float kernelWeight[3] = { 1.0, 2.0 / 3.0, 1.0 / 6.0 };

layout (location = 0) out vec4 outFragColor;

float normalWeight(vec3 originNormal, vec3 normal, float sigma) {
	return pow(max(0.0f, dot(originNormal,normal)), sigma);
}

float luma(vec3 c){
    return dot(c, vec3(0.2126, 0.7152, 0.0722));
}

float luminanceWeight(vec3 originColor, float var, vec3 color) {
	float numerator = abs(luma(originColor) - luma(color));
	float varEps = 1e-10;
	float denominator = params.reflectionsLuminanceSigma * sqrt(max(var + varEps, 0.0f));
	return exp(-numerator / denominator);
}

float depthWeight(float depth, float originDepth, float phiDepth) {
	float weightZ = (phiDepth == 0.0) ? 0.0f : abs(originDepth - depth) / phiDepth;
	return exp(-max(weightZ * params.depthSigma, 0.0f));
}

void main() 
{
	if(!params.useAtrous) {
		 outFragColor = texture(currentImage, inUV);
		 return;
	}

	vec2 coords = inUV;
	float roughness = texture(roughnessMetalImage, coords).g;

	int maxBlurIteration = 0;
	int blurScale = 0;

	//Use roughness to define blur size and iterations
	if(params.useRoughnessAdaptativeIterations) {
		//change size of blur depending on surface roughness
		if(roughness < 0.2)
			blurScale =  1 << params.blurScale;
		//larger blur scale for high roughness surfaces
		else
			blurScale = 1 << min((params.blurScale + 1), 4);

		//change number of blur iterations depending on surface roughness
		//Separable bilateral filter has 2x the number of iterations
		if(params.useImplicitSamplingOfEnvironmentMap) {
			if(params.useSeparatedBilateral) {
				if(roughness <= 0.0) {
					maxBlurIteration = 0;
				}
				else if(roughness <= 0.05)
					maxBlurIteration = 2;
				else
					maxBlurIteration = 8;
			}
			else {
				if(roughness <= 0.0) {
					maxBlurIteration = 0;
				}
				else if(roughness <= 0.05)
					maxBlurIteration = 1;
				else
					maxBlurIteration = 4;
			}
		}
		else {
			if(params.useSeparatedBilateral)
				maxBlurIteration = 8;
			else
				maxBlurIteration = 4;
		}
	}
	else if(params.useSeparatedBilateral) {
		blurScale =  1 << params.blurScale;
		maxBlurIteration = 8;
	}
	else {
		blurScale =  1 << params.blurScale;
		maxBlurIteration = 4;
	}

	//make sure we are under our maximum number of iterations
	maxBlurIteration = min(maxBlurIteration, params.maxBlurIteration);

	//If we have reached the maximum number of iterations, pass through
	if(params.currBlurIteration >= maxBlurIteration) {
		outFragColor = texture(currentImage, inUV);
		return;
	}
	
	//Load the necessary data
	vec4 originColor = texture(currentImage, coords);
	vec3 originNormal = texture(normalImage, coords).xyz;
	float originDepth = texture(depthDerivativesSampler, coords).x;
	float phiDepth = max(max(texture(depthDerivativesSampler, coords).y, texture(depthDerivativesSampler, coords).z), 1e-8) * blurScale;
	float originVar = varGauss(inUV);

	//Define kernel size
	int maxKernelSize = 2;
	int kernelSizeX = maxKernelSize;
	int kernelSizeY = maxKernelSize;
	//if separable blur, each kernel side has 0 width alternatively
	if(params.useSeparatedBilateral) {
		if(params.currBlurIteration % 2 == 0)
			kernelSizeX = 0;
		else
			kernelSizeY = 0;
	}
	
	vec4 finalColor = vec4(0.0f);
	vec3 sumColor = vec3(0.0f);
	float weightSum = 0.0f;
	float varSum = 0;
	vec2 texelSize = 1.0 / textureSize(currentImage,0).xy;

	//visit around the center of the pixel
	for (int i = -kernelSizeX; i <= kernelSizeX; i++) {
		for (int j = -kernelSizeY; j <= kernelSizeY; j++) {


			//vec2 uv = inUV + vec2(texelSize.x * offsetX * blurScale, texelSize.y * offsetY * blurScale);
			vec2 uv = coords + vec2(texelSize.x * float(i) * blurScale, texelSize.y * float(j) * blurScale);

			//check if sample is outside of the screen
			bool outside = (uv.x < 0.0f || uv.y < 0.0f || uv.x > 1.0f || uv.y > 1.0f);
			if(!outside) {
				float weight = kernelWeight[abs(i)] * kernelWeight[abs(j)];
				vec4 color = texture(currentImage, uv);
				vec3 normal = texture(normalImage, uv).xyz;
				float depth =  texture(depthDerivativesSampler, uv).x;

				//calculate edge weights and multiply + store them in the weight variable
				if(params.useNormalWeight) {
					float w = normalWeight(originNormal, normal, params.reflectionNormalSigma);
					weight *= w;
				}

				if(params.useLumaWeight) {
					float w = luminanceWeight(originColor.xyz, originVar, color.xyz);
					weight *= w;
				}

				if(params.useDepthWeight) {
					float w = depthWeight(depth, originDepth, phiDepth * length(vec2(i, j)));
					weight *= w;
				}
										 
				sumColor += color.xyz * weight;
				varSum += color.w * weight * weight;
				weightSum += weight;
			}
		}
	}

	if(weightSum >= EPSILON) {
		sumColor /= weightSum;
		varSum /= (weightSum * weightSum);

		//If its a separable bilateral filter, change variance only at the second iteration (when the vertical and horizontal passes are finished)
		if(params.useSeparatedBilateral) {
			if(params.currBlurIteration % 2 != 0)
				finalColor = vec4(sumColor, varSum);
			else
				finalColor = vec4(sumColor, originVar);
		}
		else
			finalColor = vec4(sumColor, varSum);
	}
	else {
		finalColor = originColor;
	}

	outFragColor = finalColor;
}