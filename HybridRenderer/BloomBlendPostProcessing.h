#pragma once
#pragma once
#include "vulkan/vulkan.h"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_vulkan.h"
#include "utils.hpp"
#include "UniformBuffer.h"
#include "SceneGraph.h"
#include "VertexFragPipeline.h"
#include "FrameGraph.h"

class BloomBlendPostProcessing
{
public:

	utils::QueueFamilyIndices queueFamilyIndices;
	VkDescriptorPool descriptorPool;
	VkCommandPool commandPool;
	VkPhysicalDevice physicalDevice;
	VkDevice device;
	Pass renderpass;
	VkQueue graphicsQueue;
	SwapChain swapChain;
	VkDescriptorSetLayout descriptorLayout;
	std::vector<VkDescriptorSet> descriptorSets;
	TextureImage sceneImage;
	TextureImage bloomImage;
	VertexFragPipeline* pipeline;

	BloomBlendPostProcessing();
	BloomBlendPostProcessing(VkPhysicalDevice physicalDevice,
							VkDevice device,
							VkQueue graphicsQueue,
							utils::QueueFamilyIndices& queueFamilyIndices,
							SwapChain& swapChain,
							Pass& renderpass,
							TextureImage& sceneImages,
							TextureImage& bloomImages,
							VertexFragPipeline* pipeline);
	void createCommandPool();
	void createDescriptorPool();
	void initUniformBuffers();
	void createDescriptorSetLayout();
	void createDescriptorSet();
	void update(uint32_t imageIndex);
	void draw(VkCommandBuffer commandBuffer, uint32_t imageIndex);
	void cleanup();
};