#pragma once
#include <GLFW/glfw3.h>
#include <string>
#include "Mouse.hpp"
#include "Keyboard.hpp"
#include "Camera.h"

//Takes care of inputs and camera
class Window
{
private:
	GLFWwindow* window;
	std::string appName;
	int width;
	int height;

public:
	Window();
	Window(std::string appName, int width, int height);
	void createWindow();
	void cleanup();
	void initInputs();
	Keyboard& getKeyboard();
	Mouse& getMouse();
	GLFWwindow* getWindowHandle();
	int getWidth();
	int getHeight();
};