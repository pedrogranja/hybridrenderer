#version 460
#extension GL_EXT_ray_tracing : require
#extension GL_EXT_nonuniform_qualifier : enable

struct RayPayload {
	vec3 color;
	float dist;
	int pathLength;
};

layout(location = 0) rayPayloadInEXT RayPayload rayPayload;

void main()
{	
	rayPayload.color = vec3(0.0, 0.0f, 1.0f);
	rayPayload.dist = 9999.0f;
}