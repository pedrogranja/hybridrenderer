#pragma once
#include "VulkanDevice.h"
#include "RaytracingUtils.hpp"

// All BLAS need scratch memory in order to build, 
// this class provides a way for creating such memory
class RayTracingScratchBuffer
{
private:
	VulkanDevice device;
	VulkanInstance instance;

public:
	VkDeviceSize size;
	uint64_t deviceAddress = 0;
	VkBuffer buffer = VK_NULL_HANDLE;
	VkDeviceMemory memory = VK_NULL_HANDLE;

	RayTracingScratchBuffer();
	RayTracingScratchBuffer(VkDeviceSize size, VulkanDevice& device, VulkanInstance& instance);
	void cleanup();
};

