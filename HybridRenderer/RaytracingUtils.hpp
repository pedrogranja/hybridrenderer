#pragma once
#include <vulkan/vulkan.h>
#include "utils.hpp"

namespace utils {
	inline PFN_vkGetBufferDeviceAddressKHR vkGetBufferDeviceAddressKHR;
	inline PFN_vkCreateAccelerationStructureKHR vkCreateAccelerationStructureKHR;
	inline PFN_vkDestroyAccelerationStructureKHR vkDestroyAccelerationStructureKHR;
	inline PFN_vkGetAccelerationStructureBuildSizesKHR vkGetAccelerationStructureBuildSizesKHR;
	inline PFN_vkCmdBuildAccelerationStructuresKHR vkCmdBuildAccelerationStructuresKHR;
	inline PFN_vkBuildAccelerationStructuresKHR vkBuildAccelerationStructuresKHR;
	inline PFN_vkCmdCopyAccelerationStructureKHR vkCmdCopyAccelerationStructureKHR;
	inline PFN_vkGetAccelerationStructureDeviceAddressKHR vkGetAccelerationStructureDeviceAddressKHR;
	inline PFN_vkCmdTraceRaysKHR vkCmdTraceRaysKHR;
	inline PFN_vkGetRayTracingShaderGroupHandlesKHR vkGetRayTracingShaderGroupHandlesKHR;
	inline PFN_vkCreateRayTracingPipelinesKHR vkCreateRayTracingPipelinesKHR;

	inline void loadRaytracingFunctions(VkDevice device) {
		vkGetBufferDeviceAddressKHR = reinterpret_cast<PFN_vkGetBufferDeviceAddressKHR>(vkGetDeviceProcAddr(device, "vkGetBufferDeviceAddressKHR"));
		vkCreateAccelerationStructureKHR = reinterpret_cast<PFN_vkCreateAccelerationStructureKHR>(vkGetDeviceProcAddr(device, "vkCreateAccelerationStructureKHR"));
		vkDestroyAccelerationStructureKHR = reinterpret_cast<PFN_vkDestroyAccelerationStructureKHR>(vkGetDeviceProcAddr(device, "vkDestroyAccelerationStructureKHR"));
		vkGetAccelerationStructureBuildSizesKHR = reinterpret_cast<PFN_vkGetAccelerationStructureBuildSizesKHR>(vkGetDeviceProcAddr(device, "vkGetAccelerationStructureBuildSizesKHR"));
		vkCmdBuildAccelerationStructuresKHR = reinterpret_cast<PFN_vkCmdBuildAccelerationStructuresKHR>(vkGetDeviceProcAddr(device, "vkCmdBuildAccelerationStructuresKHR")); //build as on device
		vkBuildAccelerationStructuresKHR = reinterpret_cast<PFN_vkBuildAccelerationStructuresKHR>(vkGetDeviceProcAddr(device, "vkBuildAccelerationStructuresKHR")); //build as on host
		vkCmdCopyAccelerationStructureKHR = reinterpret_cast<PFN_vkCmdCopyAccelerationStructureKHR>(vkGetDeviceProcAddr(device, "vkCmdCopyAccelerationStructureKHR"));
		vkGetAccelerationStructureDeviceAddressKHR = reinterpret_cast<PFN_vkGetAccelerationStructureDeviceAddressKHR>(vkGetDeviceProcAddr(device, "vkGetAccelerationStructureDeviceAddressKHR"));
		vkCmdTraceRaysKHR = reinterpret_cast<PFN_vkCmdTraceRaysKHR>(vkGetDeviceProcAddr(device, "vkCmdTraceRaysKHR"));
		vkGetRayTracingShaderGroupHandlesKHR = reinterpret_cast<PFN_vkGetRayTracingShaderGroupHandlesKHR>(vkGetDeviceProcAddr(device, "vkGetRayTracingShaderGroupHandlesKHR"));
		vkCreateRayTracingPipelinesKHR = reinterpret_cast<PFN_vkCreateRayTracingPipelinesKHR>(vkGetDeviceProcAddr(device, "vkCreateRayTracingPipelinesKHR"));
	}

	inline uint64_t getBufferDeviceAddress(VkBuffer buffer, VkDevice device)
	{
		VkBufferDeviceAddressInfo bufferDeviceAI{};
		bufferDeviceAI.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
		bufferDeviceAI.buffer = buffer;
		return vkGetBufferDeviceAddressKHR(device, &bufferDeviceAI);
	}

	//Given a Acceleration structure find the required memory to build it
	inline void createAsObjectMemory(VkAccelerationStructureKHR acceleration_structure, VkPhysicalDevice physicalDevice, VkDevice device, VkDeviceMemory& deviceMemory)
	{
		/*
		VkMemoryRequirements2 memoryRequirements2{};
		memoryRequirements2.sType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2;
		VkAccelerationStructureMemoryRequirementsInfoKHR accelerationStructureMemoryRequirements{};
		accelerationStructureMemoryRequirements.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_KHR;
		accelerationStructureMemoryRequirements.type = VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_OBJECT_KHR;
		accelerationStructureMemoryRequirements.buildType = VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR;
		accelerationStructureMemoryRequirements.accelerationStructure = acceleration_structure;
		vkGetAccelerationStructureMemoryRequirementsKHR(device, &accelerationStructureMemoryRequirements, &memoryRequirements2);
		VkMemoryRequirements memoryRequirements = memoryRequirements2.memoryRequirements;
		VkMemoryAllocateInfo memoryAI{};
		memoryAI.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		memoryAI.allocationSize = memoryRequirements.size;
		memoryAI.memoryTypeIndex = utils::findMemoryType(physicalDevice, memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		VK_CHECK_RESULT(vkAllocateMemory(device, &memoryAI, nullptr, &deviceMemory));
		*/
	}

	inline VkAccelerationStructureBuildSizesInfoKHR getBuildSizes(uint32_t* pMaxPrimitiveCounts, VkAccelerationStructureBuildGeometryInfoKHR* buildInfo, VkDevice device)
	{
		// Query both the size of the finished acceleration structure and the amount of scratch memory needed.
		VkAccelerationStructureBuildSizesInfoKHR sizeInfo{};
		sizeInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
		sizeInfo.pNext = nullptr;
		
		utils::vkGetAccelerationStructureBuildSizesKHR(device,
													   VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
													   buildInfo,
													   pMaxPrimitiveCounts,
													   &sizeInfo);
		return sizeInfo;
	}
}