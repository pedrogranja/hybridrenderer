#version 450

#define PI 3.1415926535897932384626433832795

float SIGMA_N = 128.0f;
float SIGMA_D = 1.0f;
const float EPSILON = 0.00001;

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;
layout (location = 1) out vec4 outAlphaDebug;
layout (location = 2) out vec4 outLuminanceColorMoments;

layout(binding = 0) uniform Params {
	float alpha;
	bool useVelocityBuffer;
	bool useNormals;
	bool useMeshIds;
	bool useDepth;
	bool useBilinearTaps;
	bool estimateSpacially;
} params;

layout(binding = 1) uniform sampler2D currImage;
layout(binding = 2) uniform sampler2D currNormals;
layout(binding = 3) uniform sampler2D currDepthImage;
layout(binding = 4) uniform sampler2D prevImage;
layout(binding = 5) uniform sampler2D prevNormals;
layout(binding = 6) uniform sampler2D prevDepthImage;
layout(binding = 7) uniform sampler2D prevColorMoments;
layout(binding = 8) uniform sampler2D velocityBuffer;

const float h[25] = float[25](1.0/256.0, 1.0/64.0, 3.0/128.0, 1.0/64.0, 1.0/256.0,
                              1.0/64.0,  1.0/16.0, 3.0/32.0,  1.0/16.0, 1.0/64.0,
                              3.0/128.0, 3.0/32.0, 9.0/64.0,  3.0/32.0, 3.0/128.0,
                              1.0/64.0,  1.0/16.0, 3.0/32.0,  1.0/16.0, 1.0/64.0,
                              1.0/256.0, 1.0/64.0, 3.0/128.0, 1.0/64.0, 1.0/256.0);

float luminance(vec3 c){
  return dot(c, vec3(0.2126, 0.7152, 0.0722));
}

bool checkNormals(vec3 prevNormals, vec3 currNormals) {
	return dot(prevNormals, currNormals) > cos(PI / 4);
}

bool checkIds(int prevId, int nextId) {
	return prevId == nextId;
}

bool checkDepth(vec2 prevUv) {
	float currDepth = texture(currDepthImage, inUV).x;
	float prevDepth = texture(prevDepthImage, prevUv).x;
	float ddxDepthCurr = abs(texture(currDepthImage, inUV).y);
	float ddyDepthCurr = abs(texture(currDepthImage, inUV).z);

	return !(abs(prevDepth - currDepth) / (max(ddxDepthCurr, ddyDepthCurr) + 1e-4) > 2.0);
}

bool getPrevColorBilinearTap(int currId, vec3 currNormals, vec2 prevUv, inout vec3 res) {
	vec2 offsets[9] = {
		vec2(0.0, 0.0),
		vec2(0.0, 1.0 / textureSize(prevImage, 0).x),
		vec2(0.0, -1.0 / textureSize(prevImage, 0).x),
		vec2(-1.0 / textureSize(prevImage, 0).x, 0.0),
		vec2(1.0 / textureSize(prevImage, 0).x, 0.0),

		vec2(1.0 / textureSize(prevImage, 0).x, 1.0 / textureSize(prevImage, 0).x),
		vec2(-1.0 / textureSize(prevImage, 0).x, -1.0 / textureSize(prevImage, 0).x),
		vec2(-1.0 / textureSize(prevImage, 0).x, 1.0 / textureSize(prevImage, 0).x),
		vec2(1.0 / textureSize(prevImage, 0).x, -1.0 / textureSize(prevImage, 0).x),
	};

	int numSuccessfullTaps = 0;
	for(int i = 0; i < 9; i++) {
		vec2 offset = offsets[i];
		
		if(i == 5 && numSuccessfullTaps > 0)
			break;

		//check normals
		if(params.useNormals) {
			vec3 prevNormals = texture(prevNormals, prevUv + offset).rgb;

			if(!checkNormals(prevNormals, currNormals))
				continue;
		}

		//check mesh ids
		if(params.useMeshIds) {
			int prevId = int(texture(prevNormals, prevUv + offset).w);

			//if normals are not consistent do not integrate temporally
			if(!checkIds(prevId, currId))
				continue;
		}

		//check depth
		if(params.useDepth && !checkDepth(prevUv))
			continue;

		res += texture(prevImage, prevUv).rgb;
		numSuccessfullTaps++;

	}

	if(numSuccessfullTaps > 0) {
		res = res / float(numSuccessfullTaps);
		return true;
	}
	else
		return false;
}

float normalWeight(vec3 originNormal, vec3 normal) {
	return pow(max(0.0f, dot(originNormal,normal)), SIGMA_N);
}

float depthWeight(float originDepth, vec2 uv) {
	vec3 depthDerivatives = texture(currDepthImage, uv).xyz;
	float depth = depthDerivatives.x;
	float depthDdx = depthDerivatives.y;
	float depthDdy = depthDerivatives.z;
	float numerator = abs(originDepth - depth);
	float denominator = SIGMA_D * length(dot(vec2(depthDdx, depthDdy),(inUV - uv))) + EPSILON;
	return exp(-numerator / denominator);
}

vec2 estimateColorMoments() {
	vec2 texelSize = 1.0 / textureSize(currNormals,0).xy;
	float originDepth = texture(currDepthImage, inUV).x;
	vec3 originNormal = texture(currNormals, inUV).rgb;

	float cum_firstMoment = 0.0;
	float cum_secondMoment = 0.0;
	float cum_w = 0.0;
	int numCycles = 0;
	float wn, wd = 1.0;

	//visit around the center of the pixel
	for(int i = 0; i < 5; i++) {
		for(int j = 0; j < 5; j++) {
			vec2 uv = inUV + vec2(texelSize.x * i, texelSize.y * j);
			float depth = texture(currDepthImage, uv).x;
			vec3 normal = texture(currNormals, uv).rgb;
			vec4 colorVar = texture(currImage, uv);
			float lum = luminance(colorVar.xyz);
			
			if(params.useNormals)
				wn = normalWeight(originNormal, normal);

			if(params.useDepth)
				wd = depthWeight(originDepth, uv);

			float weight = wd * wn * h[numCycles];

			cum_firstMoment += weight * lum;
			cum_secondMoment += weight * lum * lum;
			cum_w += weight;
			numCycles++;
		}
	}

	cum_firstMoment /= cum_w;
	cum_secondMoment /= cum_w;

	return vec2(cum_firstMoment, cum_secondMoment);
}

void main() 
{
	vec2 prevUv = inUV;
	if(params.useVelocityBuffer) {
		prevUv -= texture(velocityBuffer, inUV).xy;
	}
	vec4 prevColorMom = vec4(texture(prevColorMoments, prevUv).rgb, 0.0f);
	vec3 currColor = texture(currImage, inUV).rgb;
	vec3 prevColor = vec3(0.0);
	float alpha = params.alpha;
	vec3 currNormal = texture(currNormals, inUV).rgb;

	//if there are no normals do not integrate temporally

	//use bilinear taps in 4x4
	if(params.useVelocityBuffer && params.useBilinearTaps) {
		int currId = int(texture(currNormals, inUV).w);
		if(!getPrevColorBilinearTap(currId, currNormal, prevUv, prevColor)) {
			alpha = 1.0;
		}
	}
	//normal prev color
	else {	
		prevColor = texture(prevImage, prevUv).rgb;
		
		//check normals
		if(params.useNormals) {
			vec3 prevNormals = texture(prevNormals, prevUv).rgb;

			if(currNormal.x < 0.01 && currNormal.y < 0.01 && currNormal.z < 0.01) {
				alpha = 1.0;
			}

			//if normals are not consistent do not integrate temporally
			if(!checkNormals(prevNormals, currNormal))
				alpha = 1.0;
		}
		
		//check mesh id
		if(params.useMeshIds) {
			int prevId = int(texture(prevNormals, prevUv).w);
			int currId = int(texture(currNormals, inUV).w);

			//if normals are not consistent do not integrate temporally
			if(!checkIds(prevId, currId))
				alpha = 1.0;
		}

		//check depth
		if(params.useDepth && !checkDepth(prevUv))
			alpha = 1.0;
	}

	outAlphaDebug = vec4(alpha, 0.0f, 0.0f, 1.0f);
	vec3 result = alpha * currColor + (1.0 - alpha) * prevColor;
	float lum = luminance(currColor);

	if(prevColorMom.z < 4 && (currNormal.x > 0.01 || currNormal.y > 0.01 || currNormal.z > 0.01) && params.estimateSpacially) {
		vec2 colMoments = estimateColorMoments();
		outLuminanceColorMoments =  vec4(colMoments, 0.0f, 0.0f);
		outAlphaDebug.y = 1.0f;
	}
	else {
		outLuminanceColorMoments = alpha * vec4(lum, lum * lum, 0.0f, 0.0f) + (1.0 - alpha) * prevColorMom;
	}
	
	if(alpha >= 0.999f)
		outLuminanceColorMoments.z = 0.0f;
	else
		outLuminanceColorMoments.z = prevColorMom.z + 1;

	outFragColor = vec4(result, outLuminanceColorMoments.y - outLuminanceColorMoments.x * outLuminanceColorMoments.x);
}