#pragma once
#include "EngineBase.h"
#include "BottomLevelAS.h"
#include "TopLevelAS.h"
#include "RaytracingPipeline.h"
#include "ShaderBindingTable.h"
#include "DescriptorSetRaytracing.h"
#include "CameraMovement.h"
#include "glm/ext.hpp"
#include "metrics_gui.h"

class DeferredRendererReflections : public EngineBase
{
	RenderTarget* worldSpacePositionRenderTarget;
	RenderTarget* albedoAoRenderTarget;
	RenderTarget* textureAlbedoRenderTarget;
	RenderTarget* worldSpaceNormalsRenderTarget;
	RenderTarget* aoRenderTarget;
	RenderTarget* metalRoughnessRenderTarget;
	RenderTarget* tangentsRenderTarget;
	RenderTarget* normalsTexturelessRenderTarget;
	RenderTarget* skyboxRenderTarget;
	RenderTarget* bloomBlendRenderTarget;
	RenderTarget* velocityRenderTarget;
	RenderTarget* linearDepthDerivativesRenderTarget;
	Pass* skyboxRenderPass;
	Pass* gBufferPass;
	VertexFragPipeline* gBufferPipeline;
	DescriptorSet* gBufferDescriptorSet;

	// Shadows Temporal structures
	TextureImage* prevFrame;
	TextureImage* prevNormals;
	TextureImage* prevDepth;
	TextureImage* prevShadowsColorMoments;
	RenderTarget* shadowsTemporalAccumulationRenderTarget;
	RenderTarget* shadowColorMomentsRenderTarget;
	DescriptorSet* shadowsTemporalAccumulationDescriptorSet;
	Pass* shadowsTemporalAccumulationPass;
	UniformBuffer* shadowsTemporalAccumulationUniformBuffer;
	VertexFragPipeline* shadowsTemporalAccumulationPipeline;

	//Pass being used to store prev frame colors
	Pass* passThroughPass;
	DescriptorSet* passThroughDescriptorSet;
	VertexFragPipeline* passThroughPipeline;
	RenderTarget* passThroughRenderTarget;

	//Pass being used to render to screen
	Pass* debugPass;
	DescriptorSet* debugDescriptorSet;
	UniformBuffer* debugUniformBuffer;
	VertexFragPipeline* debugPipeline;

	//Shadows Raytracing structures
	RaytracingPipeline* shadowsRaytracingPipeline;
	UniformBuffer* shadowsParamsUniform;
	RenderTarget* shadowsRaytracingTarget;
	TextureImage* raytracingShadowsAccummulation;
	std::vector<TextureImage*> blueNoise2;
	DescriptorSetRaytracing* descriptorSetRaytracing;
	ShaderBindingTable* shadowsShaderBindingTable;

	//Raytracing reflections structures
	RaytracingPipeline* raytracingReflectionsPipeline;
	UniformBuffer* reflectionsParamsUniform;
	RenderTarget* raytracingReflectionsRenderTarget;
	TextureImage* raytracingReflectionsRenderTargetAccummulation;
	RenderTarget* raytracingReflectionsHitPointsRenderTarget;
	RenderTarget* raytracingReflectionsAlbedoRenderTarget;
	RenderTarget* raytracingReflectionsFakeHitPositionRenderTarget;
	DescriptorSetRaytracing* descriptorSetReflectionsRaytracing;
	ShaderBindingTable* shaderBindingTableReflections;

	//A trous pass
	static const int numAtrousPasses = 8;
	RenderTarget* atrousRenderTargets[numAtrousPasses];
	RenderTarget* varianceBlurredRenderTargets[numAtrousPasses];
	Pass* atrousPasses[numAtrousPasses];
	DescriptorSet* atrousDescriptorSets[numAtrousPasses];
	UniformBuffer* atrousUniformBuffers[numAtrousPasses];
	VertexFragPipeline* atrousPipelines[numAtrousPasses];

	//blur variance passes
	RenderTarget* blurVarHorRenderTarget;
	Pass* blurVarHorPass;
	Pass* blurVarVerPass[numAtrousPasses];
	DescriptorSet* blurVarHorDescriptorSet[numAtrousPasses];
	UniformBuffer* blurVarHorUniformBuffer[numAtrousPasses];
	DescriptorSet* blurVarVerDescriptorSet;
	UniformBuffer* blurVarVerUniformBuffer;
	VertexFragPipeline* blurVarHorPipeline;
	VertexFragPipeline* blurVarVerPipeline[numAtrousPasses];

	//Temporal antialiasing
	TextureImage* temporalAAPrevFrame;
	RenderTarget* temporalAARenderTarget;
	UniformBuffer* temporalAAUniformBuffer;
	DescriptorSet* temporalAADescriptorSet;
	Pass* temporalAAPass;
	VertexFragPipeline* temporalAAPipeline;

	// Reflections Temporal structures
	TextureImage* reflectionsPrevFrame;
	TextureImage* reflectionsPrevColorMoments;
	RenderTarget* reflectionsTemporalRenderTarget;
	RenderTarget* reflectionsColorMoments;
	RenderTarget* reflectionsFakeHitPositionRenderTarget;
	DescriptorSet* reflectionsTemporalAccumulationDescriptorSet;
	Pass* reflectionsTemporalAccumulationPass;
	UniformBuffer* reflectionsTemporalAccumulationUniformBuffer;
	VertexFragPipeline* reflectionsTemporalAccumulationPipeline;

	//Reflections Atrous blur
	static const int numReflectionAtrousPasses = 8;
	RenderTarget* reflectionsAtrousRenderTarget[numReflectionAtrousPasses];
	Pass* reflectionsAtrousPass[numReflectionAtrousPasses];
	DescriptorSet* reflectionsAtrousDescriptorSet[numReflectionAtrousPasses];
	UniformBuffer* reflectionsAtrousUniformBuffer[numReflectionAtrousPasses];
	VertexFragPipeline* reflectionsAtrousPipeline[numReflectionAtrousPasses];

	bool takeScreenshot = false;

	struct ReflectionsTemporalAccumulationParams : public UIRenderable {
		//we need a well defined structure to send to the graphics card using uniform buffers
		struct ToSubmit {
			alignas(4) float alpha = 0.2;
			alignas(4) float stillAlpha = 0.05;
			alignas(4) float momentsAlpha = 0.2;
			alignas(4) float momentsStillAlpha = 0.2;
			alignas(4) bool useVelocityBuffer = true;
			alignas(4) bool useNormals = true;
			alignas(4) bool useMeshIds = true;
			alignas(4) bool useDepth = true;
			alignas(4) bool checkNeighborhood = true;
			alignas(4) bool estimateSpacially = false;
			alignas(4) bool useHistoryRectification = true;
			alignas(4) bool useClipping = false;
			alignas(4) bool isStill;
			alignas(4) float colorBoxSigma = 1.0f;
			alignas(4) bool useHitPointReprojection = false;
			alignas(4) int width;
			alignas(4) int height;
			alignas(16) glm::mat4 prevFrameView;
			alignas(16) glm::mat4 prevFrameProj;
			alignas(16) glm::mat4 currFrameView;
			alignas(16) glm::mat4 currFrameProj;
			alignas(8) glm::vec2 jitter;
		} toSubmit;

		virtual void renderUI() {
			ImGui::Begin("Reflection Temporal Accumulation Parameters");
			ImGui::DragFloat("alpha", &(toSubmit.alpha), 0.05f, 0.0f, 1.0f);
			ImGui::DragFloat("still alpha", &(toSubmit.stillAlpha), 0.05f, 0.0f, 1.0f);
			ImGui::DragFloat("moments alpha", &(toSubmit.momentsAlpha), 0.05f, 0.0f, 1.0f);
			ImGui::DragFloat("moments still alpha", &(toSubmit.momentsStillAlpha), 0.05f, 0.0f, 1.0f);
			ImGui::Checkbox("Use velocity buffer", &(toSubmit.useVelocityBuffer));
			ImGui::Checkbox("Use Normals", &(toSubmit.useNormals));
			ImGui::Checkbox("Use Mesh Id", &(toSubmit.useMeshIds));
			ImGui::Checkbox("Use Depth", &(toSubmit.useDepth));
			ImGui::Checkbox("Check neighborhood for samples", &(toSubmit.checkNeighborhood));
			ImGui::Checkbox("Estimate Spatially", &(toSubmit.estimateSpacially));
			ImGui::Checkbox("Use History Rectification", &(toSubmit.useHistoryRectification));
			ImGui::Checkbox("Use Clipping", &(toSubmit.useClipping));
			ImGui::InputFloat("Color Box Sigma", &(toSubmit.colorBoxSigma));
			ImGui::Checkbox("Use Hit Point Reprojection", &(toSubmit.useHitPointReprojection));
			ImGui::End();
		};
	} reflectionsTemporalAccumulationParams;

	struct ShadowsTemporalAccumulationParams : public UIRenderable {
		//we need a well defined structure to send to the graphics card using uniform buffers
		struct ToSubmit {
			alignas(4) float alpha = 0.2;
			alignas(4) float stillAlpha = 0.2;
			alignas(4) float momentsAlpha = 0.2;
			alignas(4) float momentsStillAlpha = 0.2;
			alignas(4) bool useVelocityBuffer = true;
			alignas(4) bool useNormals = true;
			alignas(4) bool useMeshId = true;
			alignas(4) bool useDepth = true;
			alignas(4) bool checkNeighborhood = true;
			alignas(4) bool estimateSpacially = false;
			alignas(4) bool useHistoryRectification = true;
			alignas(4) bool useClipping = false;
			alignas(4) bool isStill;
			alignas(4) float clipControl = 1.0f;
			alignas(4) float shadowBoxSigma = 1.0f;
			alignas(4) int width;
			alignas(4) int height;
		} toSubmit;

		virtual void renderUI() {
			ImGui::Begin("Temporal Accumulation Parameters");
			ImGui::DragFloat("alpha", &(toSubmit.alpha), 0.05f, 0.0f, 1.0f);
			ImGui::DragFloat("still alpha", &(toSubmit.stillAlpha), 0.05f, 0.0f, 1.0f);
			ImGui::DragFloat("moments alpha", &(toSubmit.momentsAlpha), 0.05f, 0.0f, 1.0f);
			ImGui::DragFloat("moments still alpha", &(toSubmit.momentsStillAlpha), 0.05f, 0.0f, 1.0f);
			ImGui::Checkbox("Use velocity buffer", &(toSubmit.useVelocityBuffer));
			ImGui::Checkbox("Use Normals", &(toSubmit.useNormals));
			ImGui::Checkbox("Use Mesh Id", &(toSubmit.useMeshId));
			ImGui::Checkbox("Use Depth", &(toSubmit.useDepth));
			ImGui::Checkbox("Check neighborhood for samples", &(toSubmit.checkNeighborhood));
			ImGui::Checkbox("Estimate Spatially", &(toSubmit.estimateSpacially));
			ImGui::Checkbox("Use History Rectification", &(toSubmit.useHistoryRectification));
			ImGui::Checkbox("Use Clipping", &(toSubmit.useClipping));
			ImGui::DragFloat("Clip Control", &(toSubmit.clipControl), 0.05f, 0.0f, 1.0f);
			ImGui::InputFloat("Shadow Box Sigma", &(toSubmit.shadowBoxSigma));
			ImGui::End();
		};
	} shadowsTemporalAccumulationParams;

	struct DebugParams : public UIRenderable {
		struct ToSubmit {
			alignas(4) bool debugAlpha = false;
			alignas(4) bool debugLuminanceVarianceInitial = false;
			alignas(4) bool debugLuminanceVarianceFinal = false;
			alignas(4) bool debugTemporalHistory = false;
			alignas(4) bool debugSpatialVariance = false;
			alignas(4) bool debugShadows = false;
			alignas(4) int debugAtrousPasses = 0;
			alignas(4) int debugAtrousPassesVariance = 0;
			alignas(4) int debugAtrousPassesWeights = 0;
			alignas(4) bool debugReflectionsRaytracing = false;
			alignas(4) bool debugTemporalReflections = false;
			alignas(4) bool debugReflectionsVariance = false;
			alignas(4) bool debugReflectionsHistory = false;
			alignas(4) bool reflectionsSpatialVarianceEstimation = false;
			alignas(4) bool debugReflectionsHitPoints = false;
			alignas(4) bool debugReflectionsShading = false;
			alignas(4) int varianceMode = 0;
			alignas(4) float varianceWeightMultiplier = 1.0f;
			alignas(4) bool debugReflectionsDist = false;
			alignas(4) bool debugSizeKernel = false;
			alignas(4) bool debugKernelWeights = false;
			alignas(4) bool debugReflectionsNormal = false;
			alignas(4) bool debugReflectionsAtrous = false;
			alignas(4) bool debugRoughness = false;
			alignas(4) bool debugMetalness = false;
			alignas(4) bool debugNormals = false;
			alignas(4) bool debugAlbedo = false;
			alignas(4) bool debugAlbedoTexture = false;
			alignas(4) bool debugDepth = false;
			alignas(4) bool debugVelocity = false;
			alignas(4) int debugReflectionsAtrousPasses = 0;
			alignas(4) int debugReflectionVariancePasses = 0;
			alignas(4) bool debugPosition = false;
		} toSubmit;

		virtual void renderUI() {
			ImGui::Begin("Debug Params");
			ImGui::Checkbox("Debug Alpha", &(toSubmit.debugAlpha));
			ImGui::Checkbox("Debug Shadows Variance Initial", &(toSubmit.debugLuminanceVarianceInitial));
			ImGui::Checkbox("Debug Shadows Variance Final", &(toSubmit.debugLuminanceVarianceFinal));
			ImGui::Checkbox("Debug Temporal History", &(toSubmit.debugTemporalHistory));
			ImGui::Checkbox("Debug Spatial Variance", &(toSubmit.debugSpatialVariance));
			ImGui::Checkbox("Debug Shadows", &(toSubmit.debugShadows));
			ImGui::SliderInt("Debug Atrous Passes", &toSubmit.debugAtrousPasses, 0, numAtrousPasses);
			ImGui::SliderInt("Debug Atrous Passes Variance", &toSubmit.debugAtrousPassesVariance, 0, numAtrousPasses);
			ImGui::Checkbox("Debug Reflections Raytracing", &(toSubmit.debugReflectionsRaytracing));
			ImGui::Checkbox("Debug Temporal Reflections", &(toSubmit.debugTemporalReflections));
			ImGui::Checkbox("Debug Reflections Variance", &(toSubmit.debugReflectionsVariance));
			ImGui::Checkbox("Debug Reflections History", &(toSubmit.debugReflectionsHistory));
			ImGui::Checkbox("Debug Reflections Spatial Variance Estimation", &(toSubmit.reflectionsSpatialVarianceEstimation));
			ImGui::Checkbox("Debug Reflections Hit Points", &(toSubmit.debugReflectionsHitPoints));
			ImGui::Checkbox("Debug Reflections Shading", &(toSubmit.debugReflectionsShading));
			ImGui::Checkbox("Debug Reflections Dist", &(toSubmit.debugReflectionsDist));
			ImGui::Checkbox("Debug Size Kernel", &(toSubmit.debugSizeKernel));
			ImGui::Checkbox("Debug Kernel Weights", &(toSubmit.debugKernelWeights));
			ImGui::Checkbox("Debug Reflections Normals", &(toSubmit.debugReflectionsNormal));
			ImGui::Checkbox("Debug Reflections Atrous", &(toSubmit.debugReflectionsAtrous));
			ImGui::Checkbox("Debug Roughness", &(toSubmit.debugRoughness));
			ImGui::Checkbox("Debug Metalness", &(toSubmit.debugMetalness));
			ImGui::Checkbox("Debug Normals", &(toSubmit.debugNormals));
			ImGui::Checkbox("Debug Albedo", &(toSubmit.debugAlbedo));
			ImGui::Checkbox("Debug Albedo Textures", &(toSubmit.debugAlbedoTexture));
			ImGui::Checkbox("Debug Depth", &(toSubmit.debugDepth));
			ImGui::Checkbox("Debug Velocity", &(toSubmit.debugVelocity));
			ImGui::Checkbox("Debug Position", &(toSubmit.debugPosition));
			ImGui::SliderInt("Debug Reflections Atrous Passes", &toSubmit.debugReflectionsAtrousPasses, 0, numReflectionAtrousPasses);
			ImGui::SliderInt("Debug Reflection Variance", &toSubmit.debugReflectionVariancePasses, 0, numReflectionAtrousPasses);
			ImGui::End();
		};

	} debugParams;

	struct AtrousParams : public UIRenderable {

		struct ToSubmit {
			alignas(4) bool useNormalWeight = true;
			alignas(4) float normalSigma = 128.0f;
			alignas(4) bool useShadowWeight = true;
			alignas(4) float shadowSigma = 10.0f;
			alignas(4) bool useDepthWeight = true;
			alignas(4) float depthSigma = 0.5f;
			alignas(4) int currIteration;
			alignas(4) int maxIteration;
			alignas(4) bool useShadowAngleBasedBlurSize = true;
			alignas(4) bool useSeparableAtrous = true;
			alignas(4) bool useAtrous = true;
			alignas(4) int blurScale;
			alignas(4) bool blurVariance = true;
			alignas(4) float shadowAngle;
		} toSubmit;

		int numCycles = 4;

		virtual void renderUI() {
			ImGui::Begin("Atrous Params");
			ImGui::Checkbox("Use Atrous", &(toSubmit.useAtrous));
			ImGui::Checkbox("Use Normal Weight", &(toSubmit.useNormalWeight));
			ImGui::DragFloat("Normals Sigma", &(toSubmit.normalSigma), 1.0f, 0.0f, 256.0f);
			ImGui::Checkbox("Use Shadow Weight", &(toSubmit.useShadowWeight));
			ImGui::DragFloat("Shadow Sigma", &(toSubmit.shadowSigma), 1.0f, 0.0f, 50.0f);
			ImGui::Checkbox("Use Depth Weight", &(toSubmit.useDepthWeight));
			ImGui::DragFloat("Depth Sigma", &(toSubmit.depthSigma), 1.0f, 0.0f, 10.0f);
			ImGui::Checkbox("Use Shadow Angle Based Blur Size", &(toSubmit.useShadowAngleBasedBlurSize));
			ImGui::Checkbox("Use Separable Atrous", &(toSubmit.useSeparableAtrous));
			ImGui::DragInt("Max Blur Iteration", &(toSubmit.maxIteration), 1, 1, 8);
			ImGui::Checkbox("Blur Variance", &(toSubmit.blurVariance));
			ImGui::End();
		};
	} atrousParams;

	struct ShadowParams : public UIRenderable {

		struct ToSubmit {
			alignas(16) glm::vec4 lightPos;
			alignas(4) int frameIndex;
			alignas(4) int numSamples = 1;
			alignas(4) float shadowConeAngle = 1.0f;
			alignas(4) float time;
			alignas(4) bool useBlueNoise = false;
			alignas(4) int width;
			alignas(4) int height;
			alignas(4) bool accumulate;
			alignas(4) int numAccumulatedFrames;
			alignas(4) int maxNumberOfFrames = 100;
		} toSubmit;

		virtual void renderUI() {
			ImGui::Begin("Shadow Parameters");
			ImGui::DragInt("Num Samples", &(toSubmit.numSamples), 1, 0, 100);
			ImGui::DragFloat("Shadow Cone Angle", &(toSubmit.shadowConeAngle), 0.5f, 0.0f, 90.0f);
			ImGui::Checkbox("Use Blue Noise", &(toSubmit.useBlueNoise));
			ImGui::Checkbox("accumulate", &(toSubmit.accumulate));
			std::string text = "Number Accumulated Frames: " + std::to_string(toSubmit.numAccumulatedFrames);
			ImGui::Text(text.c_str());
			ImGui::InputInt("Max number of accumulated frames", &(toSubmit.maxNumberOfFrames));
			ImGui::End();
		};
	} shadowParams;

	struct BlurVarParams : public UIRenderable {

		struct PushConstant {
			alignas(4) bool blurHor = true;
		} pushConstant;

		struct ToSubmit {
			alignas(4) float blurScale = 1.0f;
			alignas(4) float blurStrength = 1.0f;
		} toSubmit;

		virtual void renderUI() {
			ImGui::Begin("Variance Blur Parameters");
			ImGui::DragFloat("Blur Scale", &toSubmit.blurScale, 1, 50);
			ImGui::DragFloat("Blur Strength", &(toSubmit.blurStrength));
			ImGui::End();
		};

	} blurVarParams;

	struct ReflectionsShadingParams : public UIRenderable {

		struct ToSubmit {
			alignas(16) glm::vec4 lightDir;
			alignas(16) glm::vec4 lightColor;
			alignas(4) float lightIntensity;
			alignas(4) bool checkRef = true;
			alignas(4) bool useAdjacentHitPoints = false;
			alignas(16) glm::vec4 camPos;
		} toSubmit;

		virtual void renderUI() {
			ImGui::Begin("Reflections Shading Parameters");
			ImGui::Checkbox("Use Check Ref", &(toSubmit.checkRef));
			ImGui::Checkbox("Use Adjacent Hit Points", &(toSubmit.useAdjacentHitPoints));
			ImGui::End();
		};

	} reflectionsShadingParams;

	struct ReflectionsAtrousParams : public UIRenderable {

		bool increaseStartBlurSize = true;

		struct ToSubmit {
			alignas(4) bool useNormalWeight = true;
			alignas(4) float reflectionNormalSigma = 128.0f;
			alignas(4) bool useLumaWeight = true;
			alignas(4) float reflectionsLuminanceSigma = 10.0f;
			alignas(4) bool useDepthWeight = true;
			alignas(4) float depthSigma = 0.5f;
			alignas(4) int currBlurIteration;
			alignas(4) int maxBlurIteration = numReflectionAtrousPasses;
			alignas(4) bool useRoughnessAdaptativeIterations = true;
			alignas(4) bool useSeparatedAtrous = true;
			alignas(4) bool useAtrous = true;
			alignas(4) int blurScale = 1;
			alignas(4) int width;
			alignas(4) int height;
			alignas(4) bool useImplicitSamplingOfEnvironmentMap;
		} toSubmit;

		virtual void renderUI() {
			ImGui::Begin("Atrous Blur Parameters");
			ImGui::Checkbox("Use Atrous", &(toSubmit.useAtrous));
			ImGui::Checkbox("Use Normal Weight", &(toSubmit.useNormalWeight));
			ImGui::DragFloat("Reflections Normals Sigma", &(toSubmit.reflectionNormalSigma), 1.0f, 0.0f, 256.0f);
			ImGui::Checkbox("Use Luminance Weight", &(toSubmit.useLumaWeight));
			ImGui::DragFloat("Luminance Sigma", &(toSubmit.reflectionsLuminanceSigma), 1.0f, 0.0f, 50.0f);
			ImGui::Checkbox("Use Depth Weight", &(toSubmit.useDepthWeight));
			ImGui::DragFloat("Depth Sigma", &(toSubmit.depthSigma), 1.0f, 0.0f, 10.0f);
			ImGui::Checkbox("Use Roughness Adaptative Iterations", &(toSubmit.useRoughnessAdaptativeIterations));
			ImGui::Checkbox("Use Separated Atrous Filter", &(toSubmit.useSeparatedAtrous));
			ImGui::End();
		};

	} reflectionsAtrousParams;

	struct TemporalAntialiasing : public UIRenderable {

		struct ToSubmit {

			alignas(4) float colorBoxSigma = 1.0f;
			alignas(4) float alpha = 0.05f;
			alignas(4) int width;
			alignas(4) int height;
			alignas(4) bool useTemporalAntialiasing = true;
			alignas(4) bool useCameraShake = true;

			/*
			alignas(4) bool useTemporalAntialiasing = false;
			alignas(4) bool useSimpleAccumulation;
			alignas(4) float clipControl = 1.0f;
			*/
		} toSubmit;

		virtual void renderUI() {
			ImGui::Begin("Temporal Antialiasing");
			ImGui::Checkbox("Use Temporal Antialiasing", &toSubmit.useTemporalAntialiasing);
			ImGui::Checkbox("Use Camera Shake", &toSubmit.useCameraShake);
			ImGui::DragFloat("Color Box Sigma", &(toSubmit.colorBoxSigma), 0.05f, 0.0f, 1.0f);
			ImGui::DragFloat("Alpha", &(toSubmit.alpha), 0.05f, 0.0f, 1.0f);
			/*
			ImGui::Checkbox("Use Simple Accumulation", &toSubmit.useSimpleAccumulation);
			ImGui::DragFloat("Clip Control", &(toSubmit.clipControl), 0.05f, 0.0f, 1.0f);
			*/
			ImGui::End();
		};

	} temporalAntialiasingParams;

	

	struct ReflectionsParams : public UIRenderable {

		struct ToSubmit {
			alignas(16) glm::vec4 camPos;
			alignas(16) glm::vec4 lightPos;
			alignas(16) glm::vec4 lightColor;
			alignas(4) float lightIntensity;
			alignas(4) int vertexSize;
			alignas(4) int frameIndex;
			alignas(4) float time;
			alignas(4) float baseRoughness;
			alignas(4) float baseMetalness;
			alignas(4) float indirectStrength = 0.05f;
			alignas(4) float minBrdfSampleBias = 0.0f;
			alignas(4) float maxBrdfSampleBias = 1.0f;
			alignas(4) bool useBlueNoise = false;
			alignas(4) bool useShadows = true;
			alignas(4) float shadowConeAngle = 0.0f;
			alignas(4) bool useReinhardToneMapping = true;
			alignas(4) float lumaMultiplier = 1.0f;
			alignas(4) float normalsInterpolation = 1.0f;
			alignas(4) int maxPathLength = 1;
			alignas(4) float skyboxIntensity = 1.0f;
			alignas(4) bool accumulate;
			alignas(4) int numAccumulatedFrames;
			alignas(4) int maxNumberOfFrames = 100;
			alignas(4) int width;
			alignas(4) int height;
			alignas(4) bool useDemodulation;
			alignas(4) bool useImplicitSamplingOfEnvironmentMap = true;
		} toSubmit;

		virtual void renderUI() {
			ImGui::Begin("Reflections Ray Tracing Parameters");
			ImGui::Checkbox("Use Blue Noise", &(toSubmit.useBlueNoise));
			ImGui::InputFloat("Indirect Strength", &(toSubmit.indirectStrength));
			ImGui::DragFloat("min Brdf Sample Bias", &(toSubmit.minBrdfSampleBias), 0.05f, 0.0f, 1.0f);
			ImGui::DragFloat("max Brdf Sample Bias", &(toSubmit.maxBrdfSampleBias), 0.05f, 0.0f, 1.0f);
			ImGui::Checkbox("Use Shadows", &(toSubmit.useShadows));
			ImGui::DragFloat("Shadow Cone Angle", &(toSubmit.shadowConeAngle), 0.5f, 0.0f, 90.0f);
			ImGui::Checkbox("Use Reinhard Tone Mapping", &(toSubmit.useReinhardToneMapping));
			ImGui::InputFloat("Luma Multiplier", &(toSubmit.lumaMultiplier));
			ImGui::DragFloat("Normals Interpolation", &(toSubmit.normalsInterpolation), 0.0f, 1.0f, 0.05f);
			ImGui::DragInt("Max Path Length", &(toSubmit.maxPathLength), 1, 1, 2);
			ImGui::Checkbox("accumulate", &(toSubmit.accumulate));
			ImGui::Checkbox("Use Implicit Sampling of Environemnt Map", &(toSubmit.useImplicitSamplingOfEnvironmentMap));
			std::string text = "Number Accumulated Frames: " + std::to_string(toSubmit.numAccumulatedFrames);
			ImGui::Text(text.c_str());
			ImGui::InputInt("Max number of accumulated frames", &(toSubmit.maxNumberOfFrames));
			ImGui::End();
		};
	} reflectionsParams;

	struct CameraParams : public UIRenderable {

		glm::vec3 pos;
		glm::vec3 target;
		bool setCameraPos;
		bool setCameraTarget;
		bool playCameraAnimation;
		float timeInCameraAnimation = 0.0f;
		bool resetCameraAnimation;
		bool changeTarget;
		glm::vec3 camCurrPos;
		bool playAnimation;
		bool takeScreenshotAtEnd;
		bool tookScreenshot = false;
		bool prepareScreenshot = false;
		float timeInPrepareScreenshot = 0.0f;
		bool resetAnimation;
		float timeInAnimation;
		float timeToStopAnimation;
		float animationSpeed = 1.0f;
		bool resetAccumulation = false;
		std::vector<CameraMovement> cameraMovements;
		int cameraMovementToUse;

		const char* scenesPaths[10] = { "../Models/DistanceTest/distanceTest.gltf",
									   "../Models/gltfModel/DamagedHelmet/gltf/DamagedHelmet.gltf",
									   "../Models/BreakfastRoom/BreakfastRoom.gltf", 
									   "../Models/gltfModel/Sponza/glTF/sponza.gltf", 
									   "../Models/Spheres/spheres2.gltf",
										"../Models/Metals/Metals.gltf",
										"../Models/Chess/scene.gltf",
										"../Models/SpheresShadingTest/SpheresShadingTest.gltf",
										"../Models/ShadowTest/ShadowTest.gltf",
										"../Models/PillarsTest/pillars.gltf"};

		const char* scenesNames[10] = { "Cubes distance scene", 
										"Damaged Helmet", 
										"Breakfast Room scene", 
										"Glossy Sponza scene", 
										"Icosphere scene", 
										"Metals scene", 
										"Chess", 
										"Spheres Shading scene", 
										"Shadow Objects scene", 
										"Pillars scene" };

		int currScene = 0;
		int prevScene = 0;

		virtual void renderUI() {
			ImGui::Begin("Camera Parameters");
			ImGui::InputFloat3("Camera Pos", &(pos[0]));
			setCameraPos = ImGui::Button("Set Camera Pos");
			ImGui::InputFloat3("Camera Target", &(target[0]));
			setCameraTarget = ImGui::Button("Set Camera Target");
			ImGui::Checkbox("Play Camera Animation", &playCameraAnimation);
			resetCameraAnimation = ImGui::Button("Reset Camera Animation");
			ImGui::Checkbox("Change Target", &changeTarget);
			ImGui::Combo("combo", &currScene, scenesNames, IM_ARRAYSIZE(scenesNames));
			std::string text = "camera Pos: " + glm::to_string(camCurrPos);
			ImGui::Text(text.c_str());
			ImGui::Checkbox("Play animations", &playAnimation);
			ImGui::Checkbox("Take Screenshot At End", &takeScreenshotAtEnd);
			resetAnimation = ImGui::Button("Reset Animation");
			ImGui::InputFloat("Time to Stop Animation", &timeToStopAnimation);
			ImGui::InputFloat("Animation Speed", &animationSpeed);
			ImGui::DragInt("Camera Animation to use", &cameraMovementToUse, 1.0f, 0, cameraMovements.size());
			resetAccumulation = ImGui::Button("Reset Accumulation");
			ImGui::End();
		}

	} cameraParams;

	struct FrameTime : public UIRenderable {

		MetricsGuiMetric* frameTimeMetric;
		MetricsGuiPlot* frameTimePlot;

		virtual void renderUI() {
			//skip weird first values
			if (ImGui::GetIO().Framerate > 200)
				return;

			frameTimeMetric->AddNewValue(ImGui::GetIO().Framerate);
			frameTimePlot->UpdateAxes();
			frameTimePlot->DrawHistory();
		}
	} frameTime;

	virtual void createDrawCommandBuffers();
	virtual void update(uint32_t imageIndex);
	virtual void finishedDrawing();
	virtual void createUI();
	virtual void defineRenderer();
	virtual void cleanupSwapChain();
};