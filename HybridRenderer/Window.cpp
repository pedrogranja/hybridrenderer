#include "Window.h"

Mouse mouse;
Keyboard keyboard;

void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos) {
    mouse.mouseMovementCallback(window, xpos, ypos);
}

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    mouse.mouseButtonCallback(window, button, action, mods);
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    keyboard.keyCallback(window, key, scancode, action, mods);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Window::Window()
{
}

Window::Window(std::string appName, int width, int height) : appName(appName), width(width), height(height)
{
    createWindow();
    initInputs();
}

void Window::createWindow()
{
    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); //tell glfw that we are not working with opengl
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    window = glfwCreateWindow(width, height, "HybridRenderer", nullptr, nullptr);
}

void Window::initInputs()
{
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetCursorPosCallback(window, cursorPositionCallback);
    glfwSetKeyCallback(window, keyboardCallback);
}

Keyboard& Window::getKeyboard()
{
    return keyboard;
}

Mouse& Window::getMouse()
{
    return mouse;
}

GLFWwindow* Window::getWindowHandle()
{
    return window;
}

int Window::getWidth()
{
    return width;
}

int Window::getHeight()
{
    return height;
}

void Window::cleanup()
{
    glfwDestroyWindow(window);

    glfwTerminate();
}