#include "BottomLevelAS.h"

static int instanceIdGenerator = 0;

BottomLevelAS::BottomLevelAS()
{
}

BottomLevelAS::BottomLevelAS(int firstVertex,
							 int vertexCount,
							 VertexBuffer& vertexBuffer,
							 int firstIndex,
							 int indexCount,
							 IndexBuffer& indexBuffer,
							 VulkanDevice& device,
							 VulkanInstance& instance,
							 VkCommandPool& commandPool)
															: firstVertex(firstVertex), vertexCount(vertexCount),vertexBuffer(vertexBuffer),
															  firstIndex(firstIndex), indexCount(indexCount), indexBuffer(indexBuffer),
															  device(device),
															  instance(instance),
															  commandPool(commandPool)
{
	instanceId = instanceIdGenerator++;
	createBottomLevelAS();
}

void BottomLevelAS::update(glm::mat4& modelMatrix, uint32_t id)
{
	blasInstance = {};
	blasInstance.instanceCustomIndex = id;
	blasInstance.mask = 0xFF;
	blasInstance.instanceShaderBindingTableRecordOffset = 0;
	blasInstance.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR;
	blasInstance.accelerationStructureReference = bottomLevelAsDeviceHandle;
	glm::mat4 transposeModelMatrix = glm::transpose(modelMatrix);
	std::memcpy(&blasInstance.transform, &transposeModelMatrix, sizeof(blasInstance.transform));

	/*
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			std::cout << getAccelerationStructureInstanceKHR().transform.matrix[i][j] << " ";
		}
		std::cout << "\n";
	}
	*/
	//std::cout << glm::to_string(modelMatrix) << "\n";

	//buffer.submitDataToMemory(&blasInstance, instanceBufferMemory);
}

void BottomLevelAS::cleanup()
{
	utils::vkDestroyAccelerationStructureKHR(device.getLogicalDeviceHandle(), accelerationStructure, nullptr);

	blasBuffer.cleanup();
	vkDestroyBuffer(device.getLogicalDeviceHandle(), instanceBuffer, nullptr);
	vkFreeMemory(device.getLogicalDeviceHandle(), instanceBufferMemory, nullptr);
}

uint64_t BottomLevelAS::getDeviceHandle()
{
	return bottomLevelAsDeviceHandle;
}

uint64_t BottomLevelAS::getInstanceBufferHandle()
{
	return instanceBufferHandle;
}

VkAccelerationStructureInstanceKHR& BottomLevelAS::getAccelerationStructureInstanceKHR()
{
	return blasInstance;
}

void BottomLevelAS::createBottomLevelAS()
{
	uint32_t numTriangles = static_cast<uint32_t>(indexCount) / 3;

	VkDeviceOrHostAddressConstKHR vertexBufferDeviceAddress = {};
	VkDeviceOrHostAddressConstKHR indexBufferDeviceAddress = {};
	
	vertexBufferDeviceAddress.deviceAddress = vertexBuffer.devicePntr;
	indexBufferDeviceAddress.deviceAddress = indexBuffer.devicePntr;

	//Define the geometris the blas will possess
	VkAccelerationStructureGeometryKHR accelerationStructureGeometry = {};
	accelerationStructureGeometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
	accelerationStructureGeometry.pNext = nullptr;
	accelerationStructureGeometry.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;
	accelerationStructureGeometry.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
	accelerationStructureGeometry.geometry.triangles.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
	accelerationStructureGeometry.geometry.triangles.pNext = nullptr;
	accelerationStructureGeometry.geometry.triangles.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
	accelerationStructureGeometry.geometry.triangles.vertexData.deviceAddress = vertexBufferDeviceAddress.deviceAddress;
	accelerationStructureGeometry.geometry.triangles.maxVertex = vertexCount;
	accelerationStructureGeometry.geometry.triangles.vertexStride = sizeof(utils::Vertex);
	accelerationStructureGeometry.geometry.triangles.indexType = VK_INDEX_TYPE_UINT32;
	accelerationStructureGeometry.geometry.triangles.transformData.deviceAddress = 0;
	accelerationStructureGeometry.geometry.triangles.transformData.hostAddress = nullptr;
	accelerationStructureGeometry.geometry.triangles.indexData.deviceAddress = indexBufferDeviceAddress.deviceAddress;
	
	//How to offset the geometry inside the vertex and index buffer
	VkAccelerationStructureBuildRangeInfoKHR accelerationBuildOffsetInfo = {};
	accelerationBuildOffsetInfo.primitiveCount = numTriangles;
	accelerationBuildOffsetInfo.primitiveOffset = firstIndex * sizeof(uint32_t);
	accelerationBuildOffsetInfo.firstVertex = 0;
	accelerationBuildOffsetInfo.transformOffset = 0;

	std::vector<VkAccelerationStructureGeometryKHR> vkAccelerationStructureGeometryKHRPntr = { accelerationStructureGeometry };
	//blas build information
	VkAccelerationStructureBuildGeometryInfoKHR buildGeometryInfo = {};
	buildGeometryInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
	buildGeometryInfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
	buildGeometryInfo.geometryCount = 1;
	buildGeometryInfo.pGeometries = &accelerationStructureGeometry;
	buildGeometryInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
	buildGeometryInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
	buildGeometryInfo.srcAccelerationStructure = nullptr;
	buildGeometryInfo.pNext = nullptr;
	buildGeometryInfo.scratchData = {};

	std::vector<uint32_t> sizes = { accelerationBuildOffsetInfo.primitiveCount };

	VkAccelerationStructureBuildSizesInfoKHR sizeInfo = {};
	sizeInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
	sizeInfo.pNext = nullptr;

	VkAccelerationStructureBuildSizesInfoKHR buildSizeInfo = utils::getBuildSizes(sizes.data(), &buildGeometryInfo, device.getLogicalDeviceHandle());

	//the buffer that will hold the acceleration structure
	blasBuffer = Buffer("Blas Buffer " + std::to_string(instanceId),
						device,
						device.getQueues().graphicsQueue,
						static_cast<int>(buildSizeInfo.accelerationStructureSize), 
						VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR| VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
						VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	//create the acceleration structure
	VkAccelerationStructureCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
	createInfo.pNext = nullptr;
	createInfo.type = buildGeometryInfo.type;
	createInfo.size = buildSizeInfo.accelerationStructureSize;
	createInfo.buffer = blasBuffer.buffer;
	createInfo.offset = 0;

	VK_CHECK_RESULT(utils::vkCreateAccelerationStructureKHR(device.getLogicalDeviceHandle(), &createInfo, nullptr, &accelerationStructure));

	VkAccelerationStructureDeviceAddressInfoKHR accelerationDeviceAddressInfo = {};
	accelerationDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
	accelerationDeviceAddressInfo.accelerationStructure = accelerationStructure;

	this->bottomLevelAsDeviceHandle = utils::vkGetAccelerationStructureDeviceAddressKHR(device.getLogicalDeviceHandle(), &accelerationDeviceAddressInfo);

	//scratch buffer for the build
	scratchBuffer = RayTracingScratchBuffer(buildSizeInfo.accelerationStructureSize, device, instance);

	//build the bottom level acceleration structure
	std::vector<VkAccelerationStructureBuildRangeInfoKHR*> accelerationBuildStructureRangeInfos = { &accelerationBuildOffsetInfo };

	buildGeometryInfo.dstAccelerationStructure = accelerationStructure;
	buildGeometryInfo.scratchData.deviceAddress = scratchBuffer.deviceAddress;

	if (device.getAccelerationStructureFeatures().accelerationStructureHostCommands) {
		utils::vkBuildAccelerationStructuresKHR(device.getLogicalDeviceHandle(),
												VK_NULL_HANDLE,
												1,
												&buildGeometryInfo,
												accelerationBuildStructureRangeInfos.data());
	}
	else {
		VkCommandBuffer cmdBuf = utils::beginSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool);
		utils::vkCmdBuildAccelerationStructuresKHR(cmdBuf, 1, &buildGeometryInfo, accelerationBuildStructureRangeInfos.data());
		utils::flushCommandBuffer(cmdBuf, commandPool, device.getQueues().graphicsQueue, device.getLogicalDeviceHandle());
	}

	scratchBuffer.cleanup();
	/*
	uint32_t numTriangles = static_cast<uint32_t>(indexCount) / 3;

	VkDeviceOrHostAddressConstKHR vertexBufferDeviceAddress{};
	VkDeviceOrHostAddressConstKHR indexBufferDeviceAddress{};

	vertexBufferDeviceAddress.deviceAddress = vertexBuffer.devicePntr;
	indexBufferDeviceAddress.deviceAddress = indexBuffer.devicePntr;

	VkAccelerationStructureCreateGeometryTypeInfoKHR accelerationCreateGeometryInfo{};
	accelerationCreateGeometryInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_GEOMETRY_TYPE_INFO_KHR;
	accelerationCreateGeometryInfo.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
	accelerationCreateGeometryInfo.maxPrimitiveCount = numTriangles;
	accelerationCreateGeometryInfo.indexType = VK_INDEX_TYPE_UINT32;
	accelerationCreateGeometryInfo.maxVertexCount = static_cast<uint32_t>(vertexCount);
	accelerationCreateGeometryInfo.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
	accelerationCreateGeometryInfo.allowsTransforms = VK_FALSE;

	VkAccelerationStructureGeometryKHR accelerationStructureGeometry{};
	accelerationStructureGeometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
	accelerationStructureGeometry.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;
	accelerationStructureGeometry.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
	accelerationStructureGeometry.geometry.triangles.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
	accelerationStructureGeometry.geometry.triangles.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
	accelerationStructureGeometry.geometry.triangles.vertexData.deviceAddress = vertexBufferDeviceAddress.deviceAddress;
	accelerationStructureGeometry.geometry.triangles.vertexStride = sizeof(utils::Vertex);
	accelerationStructureGeometry.geometry.triangles.indexType = VK_INDEX_TYPE_UINT32;
	accelerationStructureGeometry.geometry.triangles.indexData.deviceAddress = indexBufferDeviceAddress.deviceAddress;

	VkAccelerationStructureCreateInfoKHR accelerationCI{};
	accelerationCI.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
	accelerationCI.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
	accelerationCI.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR; //make tracing has fast has possible
	accelerationCI.maxGeometryCount = 1;
	accelerationCI.pGeometryInfos = &accelerationCreateGeometryInfo;
	VK_CHECK_RESULT(utils::vkCreateAccelerationStructureKHR(device.getLogicalDeviceHandle(), &accelerationCI, nullptr, &accelerationStructure));

	//find the necessary memory for creating the BLAS
	utils::createAsObjectMemory(accelerationStructure, instance.getPhysicalDevice(), device.getLogicalDeviceHandle(), blasMemory);

	//bind the memory to the BLAS
	VkBindAccelerationStructureMemoryInfoKHR bindAccelerationMemoryInfo{};
	bindAccelerationMemoryInfo.sType = VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_KHR;
	bindAccelerationMemoryInfo.accelerationStructure = accelerationStructure;
	bindAccelerationMemoryInfo.memory = blasMemory;
	VK_CHECK_RESULT(utils::vkBindAccelerationStructureMemoryKHR(device.getLogicalDeviceHandle(), 1, &bindAccelerationMemoryInfo));

	//The BLAS needs scratch memory in order to build
	scratchBuffer = RayTracingScratchBuffer(accelerationStructure, device, instance);

	//Geometry description
	VkAccelerationStructureGeometryKHR* pGeometry = &accelerationStructureGeometry;
	VkAccelerationStructureBuildGeometryInfoKHR accelerationBuildGeometryInfo{};
	accelerationBuildGeometryInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
	accelerationBuildGeometryInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
	accelerationBuildGeometryInfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
	accelerationBuildGeometryInfo.update = VK_FALSE;
	accelerationBuildGeometryInfo.dstAccelerationStructure = accelerationStructure;
	accelerationBuildGeometryInfo.geometryArrayOfPointers = VK_FALSE;
	accelerationBuildGeometryInfo.geometryCount = 1;
	accelerationBuildGeometryInfo.ppGeometries = &pGeometry;
	accelerationBuildGeometryInfo.scratchData.deviceAddress = scratchBuffer.deviceAddress;

	VkAccelerationStructureBuildOffsetInfoKHR accelerationBuildOffsetInfo{};
	accelerationBuildOffsetInfo.primitiveCount = numTriangles;
	accelerationBuildOffsetInfo.primitiveOffset = firstIndex * sizeof(uint32_t);
	accelerationBuildOffsetInfo.firstVertex = 0;
	accelerationBuildOffsetInfo.transformOffset = 0;

	VkAccelerationStructureBuildOffsetInfoKHR* buildOffset = &accelerationBuildOffsetInfo;

	VkPhysicalDeviceRayTracingFeaturesKHR* rayTracingFeatures = static_cast<VkPhysicalDeviceRayTracingFeaturesKHR*>(device.getDeviceFeatures2().pNext);
	if (rayTracingFeatures->rayTracingHostAccelerationStructureCommands)
	{
		utils::vkBuildAccelerationStructureKHR(device.getLogicalDeviceHandle(), 1, &accelerationBuildGeometryInfo, &buildOffset);
	}
	else {
		//build acceleration structure on device
		VkCommandBuffer cmdBuf = utils::beginSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool);
		utils::vkCmdBuildAccelerationStructureKHR(cmdBuf, 1, &accelerationBuildGeometryInfo, &buildOffset);
		utils::flushCommandBuffer(cmdBuf, device.getQueues().graphicsQueue, device.getLogicalDeviceHandle());
	}
	VkAccelerationStructureDeviceAddressInfoKHR accelerationDeviceAddressInfo{};
	accelerationDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
	accelerationDeviceAddressInfo.accelerationStructure = accelerationStructure;

	this->bottomLevelAsDeviceHandle = utils::vkGetAccelerationStructureDeviceAddressKHR(device.getLogicalDeviceHandle(), &accelerationDeviceAddressInfo);

	scratchBuffer.cleanup();
	*/
}