#include "DeferredRendererReflections.h"

void DeferredRendererReflections::createDrawCommandBuffers() {
    commandBuffers.resize(swapChain.getSize());

    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = (uint32_t)commandBuffers.size();

    VK_CHECK_RESULT(vkAllocateCommandBuffers(device.getLogicalDeviceHandle(), &allocInfo, commandBuffers.data()));

    for (int i = 0; i < commandBuffers.size(); i++) {
        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

        VK_CHECK_RESULT(vkBeginCommandBuffer(commandBuffers[i], &beginInfo));

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////// G-BUFFER /////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        VkRenderPassBeginInfo offscreenRenderPassInfo{};
        offscreenRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        offscreenRenderPassInfo.renderPass = gBufferPass->getRenderPassHandle();
        offscreenRenderPassInfo.framebuffer = gBufferPass->getFramebuffers()[0]; //CUIDADO CASO SEJA PARA IMPRIMIR PARA A SWAPCHAIN TEM QUE SER i CASO CONTRARIO 0 � OK
        offscreenRenderPassInfo.renderArea.offset = { 0, 0 };
        offscreenRenderPassInfo.renderArea.extent = swapChain.getSwapChainExtent();

        std::array<VkClearValue, 9> clearValues{};
        clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[1].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[2].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[3].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[4].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[5].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[6].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[7].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[8].depthStencil = { 1.0f, 0 };
        offscreenRenderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
        offscreenRenderPassInfo.pClearValues = clearValues.data();

        VkViewport viewport{};
        viewport.width = (float)swapChain.getSwapChainExtent().width;
        viewport.height = (float)swapChain.getSwapChainExtent().height;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        VkRect2D scissor{};
        scissor.extent.width = swapChain.getSwapChainExtent().width;
        scissor.extent.height = swapChain.getSwapChainExtent().height;

        vkCmdSetViewport(commandBuffers[i], 0, 1, &viewport);
        vkCmdSetScissor(commandBuffers[i], 0, 1, &scissor);

        vkCmdBeginRenderPass(commandBuffers[i], &offscreenRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, gBufferPipeline->getPipeline());

        //bind ui params
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                gBufferPipeline->getPipelineLayout(),
                                2,
                                1,
                                &imGuiUi.descriptorSets[i],
                                0,
                                nullptr);

        this->scene.drawScene(commandBuffers[i], gBufferPipeline->getPipelineLayout(), i);

        vkCmdEndRenderPass(commandBuffers[i]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////// RAY TRACING SHADOWS ////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        vkCmdBindPipeline(commandBuffers[i],
                          VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR,
                          shadowsRaytracingPipeline->getPipeline());

        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR,
                                shadowsRaytracingPipeline->getPipelineLayout(),
                                0,
                                1,
                                &descriptorSetRaytracing->getDescriptorSetsHandle(),
                                0,
                                0);

        VkPhysicalDeviceRayTracingPipelinePropertiesKHR* rayTracingProperties = static_cast<VkPhysicalDeviceRayTracingPipelinePropertiesKHR*>(device.getDeviceProperties2().pNext);

        VkStridedDeviceAddressRegionKHR raygenShaderSBTEntry{};
        VkStridedDeviceAddressRegionKHR missShaderSBTEntry{};
 
        raygenShaderSBTEntry.deviceAddress = shadowsShaderBindingTable->getSbtBufferAddress() + rayTracingProperties->shaderGroupBaseAlignment * 0;
        raygenShaderSBTEntry.stride = shadowsShaderBindingTable->getEntrySize();
        raygenShaderSBTEntry.size = shadowsShaderBindingTable->getEntrySize();

        missShaderSBTEntry.deviceAddress = shadowsShaderBindingTable->getSbtBufferAddress() + rayTracingProperties->shaderGroupBaseAlignment * 1;
        missShaderSBTEntry.stride = shadowsShaderBindingTable->getEntrySize();
        missShaderSBTEntry.size = shadowsShaderBindingTable->getEntrySize();

        VkStridedDeviceAddressRegionKHR hitShaderSBTEntry = {};

        VkStridedDeviceAddressRegionKHR callableShaderSBTEntry = {};
        
        
        utils::vkCmdTraceRaysKHR(
                                commandBuffers[i],
                                &raygenShaderSBTEntry,
                                &missShaderSBTEntry,
                                &hitShaderSBTEntry,
                                &callableShaderSBTEntry,
                                swapChain.getSwapChainExtent().width,
                                swapChain.getSwapChainExtent().height,
                                1);
                                

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////// RAY TRACING REFLECTIONS ////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        vkCmdBindPipeline(commandBuffers[i],
                          VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR,
                          raytracingReflectionsPipeline->getPipeline());

        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR,
                                raytracingReflectionsPipeline->getPipelineLayout(),
                                0,
                                1,
                                &descriptorSetReflectionsRaytracing->getDescriptorSetsHandle(),
                                0,
                                0);

        VkStridedDeviceAddressRegionKHR raygenShaderReflectionsSBTEntry{};
        VkStridedDeviceAddressRegionKHR missShaderReflectionsSBTEntry{};
        VkStridedDeviceAddressRegionKHR hitShaderReflectionsSBTEntry{};

        raygenShaderReflectionsSBTEntry.deviceAddress = shaderBindingTableReflections->getSbtBufferAddress() + rayTracingProperties->shaderGroupBaseAlignment * 0;
        raygenShaderReflectionsSBTEntry.stride = shaderBindingTableReflections->getEntrySize();
        raygenShaderReflectionsSBTEntry.size = shaderBindingTableReflections->getEntrySize();

        missShaderReflectionsSBTEntry.deviceAddress = shaderBindingTableReflections->getSbtBufferAddress() + rayTracingProperties->shaderGroupBaseAlignment * 1;
        missShaderReflectionsSBTEntry.stride = shaderBindingTableReflections->getEntrySize();
        missShaderReflectionsSBTEntry.size = shaderBindingTableReflections->getEntrySize() * 2;

        hitShaderReflectionsSBTEntry.deviceAddress = shaderBindingTableReflections->getSbtBufferAddress() + rayTracingProperties->shaderGroupBaseAlignment * 3;
        hitShaderReflectionsSBTEntry.stride = shaderBindingTableReflections->getEntrySize();
        hitShaderReflectionsSBTEntry.size = shaderBindingTableReflections->getEntrySize() * 2;

        VkStridedDeviceAddressRegionKHR callableShaderReflectionsSBTEntry{};
        
        utils::vkCmdTraceRaysKHR(
                                commandBuffers[i],
                                &raygenShaderReflectionsSBTEntry,
                                &missShaderReflectionsSBTEntry,
                                &hitShaderReflectionsSBTEntry,
                                &callableShaderReflectionsSBTEntry,
                                swapChain.getSwapChainExtent().width,
                                swapChain.getSwapChainExtent().height,
                                1);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////// SHADOWS TEMPORAL ACCUMULATION ////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        utils::transitionImageLayout(commandBuffers[i], depthRenderTarget.getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1, 1, VK_IMAGE_ASPECT_DEPTH_BIT);
        utils::transitionImageLayout(commandBuffers[i], shadowsRaytracingTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1, 1);

        VkRenderPassBeginInfo renderPassInfoTemporalAccumulation{};
        renderPassInfoTemporalAccumulation.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoTemporalAccumulation.renderPass = shadowsTemporalAccumulationPass->getRenderPassHandle();
        renderPassInfoTemporalAccumulation.framebuffer = shadowsTemporalAccumulationPass->getFramebuffers()[0];
        renderPassInfoTemporalAccumulation.renderArea.offset = { 0, 0 };
        renderPassInfoTemporalAccumulation.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoTemporalAccumulation.clearValueCount = 3;
        renderPassInfoTemporalAccumulation.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoTemporalAccumulation, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, shadowsTemporalAccumulationPipeline->getPipeline());
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                shadowsTemporalAccumulationPipeline->getPipelineLayout(),
                                0,
                                1,
                                &shadowsTemporalAccumulationDescriptorSet->getDescriptorSetsHandle()[i],
                                0,
                                nullptr);
        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        utils::transitionImageLayout(commandBuffers[i], shadowsRaytracingTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL, 1, 1);
        utils::transitionImageLayout(commandBuffers[i], depthRenderTarget.getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1, 1, VK_IMAGE_ASPECT_DEPTH_BIT);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////// PASS THROUGH /////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        VkRenderPassBeginInfo renderPassInfoPassThrough{};
        renderPassInfoPassThrough.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoPassThrough.renderPass = passThroughPass->getRenderPassHandle();
        renderPassInfoPassThrough.framebuffer = passThroughPass->getFramebuffers()[0];
        renderPassInfoPassThrough.renderArea.offset = { 0, 0 };
        renderPassInfoPassThrough.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoPassThrough.clearValueCount = 2;
        renderPassInfoPassThrough.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoPassThrough, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, passThroughPipeline->getPipeline());
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                passThroughPipeline->getPipelineLayout(),
                                0,
                                1,
                                &passThroughDescriptorSet->getDescriptorSetsHandle()[i],
                                0,
                                nullptr);
        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        //store prev color moments
        utils::transitionImageLayout(commandBuffers[i], prevShadowsColorMoments->textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], shadowColorMomentsRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

        utils::copyImageToImage(commandBuffers[i],
                                device.getQueues().graphicsQueue,
                                shadowColorMomentsRenderTarget->getTextureImage().textureImages[0],
                                prevShadowsColorMoments->textureImages[0],
                                prevShadowsColorMoments->texWidth,
                                prevShadowsColorMoments->texHeight);

        utils::transitionImageLayout(commandBuffers[i], shadowColorMomentsRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], prevShadowsColorMoments->textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////// A TROUS PASS ////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        VkRenderPassBeginInfo renderPassesInfoPassAtrous[numAtrousPasses];

        for (int j = 0; j < numAtrousPasses; j++) {

            //Blur variance horizontally
            VkRenderPassBeginInfo renderPassInfoBlurVarHor{};
            renderPassInfoBlurVarHor.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassInfoBlurVarHor.renderPass = blurVarHorPass->getRenderPassHandle();
            renderPassInfoBlurVarHor.framebuffer = blurVarHorPass->getFramebuffers()[0];
            renderPassInfoBlurVarHor.renderArea.offset = { 0, 0 };
            renderPassInfoBlurVarHor.renderArea.extent = swapChain.getSwapChainExtent();
            renderPassInfoBlurVarHor.clearValueCount = 2;
            renderPassInfoBlurVarHor.pClearValues = clearValues.data();

            vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoBlurVarHor, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, blurVarHorPipeline->getPipeline());

            blurVarParams.pushConstant.blurHor = true;

            vkCmdPushConstants(commandBuffers[i],
                                blurVarHorPipeline->getPipelineLayout(),
                                VK_SHADER_STAGE_FRAGMENT_BIT,
                                0,
                                sizeof(BlurVarParams::PushConstant),
                                &blurVarParams.pushConstant);

            vkCmdBindDescriptorSets(commandBuffers[i],
                                    VK_PIPELINE_BIND_POINT_GRAPHICS,
                                    blurVarHorPipeline->getPipelineLayout(),
                                    0,
                                    1,
                                    &blurVarHorDescriptorSet[j]->getDescriptorSetsHandle()[i],
                                    0,
                                    nullptr);

            vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

            vkCmdEndRenderPass(commandBuffers[i]);

            //Blur variance vertically
            VkRenderPassBeginInfo renderPassInfoBlurVarVer{};
            renderPassInfoBlurVarVer.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassInfoBlurVarVer.renderPass = blurVarVerPass[j]->getRenderPassHandle();
            renderPassInfoBlurVarVer.framebuffer = blurVarVerPass[j]->getFramebuffers()[0];
            renderPassInfoBlurVarVer.renderArea.offset = { 0, 0 };
            renderPassInfoBlurVarVer.renderArea.extent = swapChain.getSwapChainExtent();
            renderPassInfoBlurVarVer.clearValueCount = 2;
            renderPassInfoBlurVarVer.pClearValues = clearValues.data();

            vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoBlurVarVer, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, blurVarVerPipeline[j]->getPipeline());

            blurVarParams.pushConstant.blurHor = false;

            vkCmdPushConstants(commandBuffers[i],
                                blurVarVerPipeline[j]->getPipelineLayout(),
                                VK_SHADER_STAGE_FRAGMENT_BIT,
                                0,
                                sizeof(BlurVarParams::PushConstant),
                                &blurVarParams.pushConstant);

            vkCmdBindDescriptorSets(commandBuffers[i],
                                    VK_PIPELINE_BIND_POINT_GRAPHICS,
                                    blurVarVerPipeline[1]->getPipelineLayout(),
                                    0,
                                    1,
                                    &blurVarVerDescriptorSet->getDescriptorSetsHandle()[i],
                                    0,
                                    nullptr);

            vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

            vkCmdEndRenderPass(commandBuffers[i]);

            renderPassesInfoPassAtrous[j] = {};
            renderPassesInfoPassAtrous[j].sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassesInfoPassAtrous[j].renderPass = atrousPasses[j]->getRenderPassHandle();
            renderPassesInfoPassAtrous[j].framebuffer = atrousPasses[j]->getFramebuffers()[0];
            renderPassesInfoPassAtrous[j].renderArea.offset = { 0, 0 };
            renderPassesInfoPassAtrous[j].renderArea.extent = swapChain.getSwapChainExtent();
            renderPassesInfoPassAtrous[j].clearValueCount = 2;
            renderPassesInfoPassAtrous[j].pClearValues = clearValues.data();

            vkCmdBeginRenderPass(commandBuffers[i], &renderPassesInfoPassAtrous[j], VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, atrousPipelines[j]->getPipeline());

            vkCmdBindDescriptorSets(commandBuffers[i],
                                    VK_PIPELINE_BIND_POINT_GRAPHICS,
                                    atrousPipelines[j]->getPipelineLayout(),
                                    0,
                                    1,
                                    &atrousDescriptorSets[j]->getDescriptorSetsHandle()[i],
                                    0,
                                    nullptr);
            vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

            vkCmdEndRenderPass(commandBuffers[i]);
            if (j == 0) {
                //store prev frame color buffer
                utils::transitionImageLayout(commandBuffers[i], prevFrame->textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
                utils::transitionImageLayout(commandBuffers[i], atrousRenderTargets[j]->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

                utils::copyImageToImage(commandBuffers[i],
                                        device.getQueues().graphicsQueue,
                                        atrousRenderTargets[j]->getTextureImage().textureImages[0],
                                        prevFrame->textureImages[0],
                                        prevFrame->texWidth,
                                        prevFrame->texHeight);

                utils::transitionImageLayout(commandBuffers[i], atrousRenderTargets[j]->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
                utils::transitionImageLayout(commandBuffers[i], prevFrame->textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////// DEFERRED REFLECTIONS SHADING //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        utils::transitionImageLayout(commandBuffers[i], raytracingReflectionsRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1, 1);
        utils::transitionImageLayout(commandBuffers[i], raytracingReflectionsHitPointsRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1, 1);
        utils::transitionImageLayout(commandBuffers[i], raytracingReflectionsAlbedoRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1, 1);
        utils::transitionImageLayout(commandBuffers[i], raytracingReflectionsFakeHitPositionRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1, 1);
  

       //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       ////////////////////////////////////////////////////////////////////// TEMPORAL ACCUMULATION REFLECTIONS /////////////////////////////////////////////////////////////////
       //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        utils::transitionImageLayout(commandBuffers[i], depthRenderTarget.getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1, 1, VK_IMAGE_ASPECT_DEPTH_BIT);

        VkRenderPassBeginInfo renderPassInfoRefectionsTemporalAccumulation{};
        renderPassInfoRefectionsTemporalAccumulation.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoRefectionsTemporalAccumulation.renderPass = reflectionsTemporalAccumulationPass->getRenderPassHandle();
        renderPassInfoRefectionsTemporalAccumulation.framebuffer = reflectionsTemporalAccumulationPass->getFramebuffers()[0];
        renderPassInfoRefectionsTemporalAccumulation.renderArea.offset = { 0, 0 };
        renderPassInfoRefectionsTemporalAccumulation.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoRefectionsTemporalAccumulation.clearValueCount = 5;
        renderPassInfoRefectionsTemporalAccumulation.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoRefectionsTemporalAccumulation, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, reflectionsTemporalAccumulationPipeline->getPipeline());
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                reflectionsTemporalAccumulationPipeline->getPipelineLayout(),
                                0,
                                1,
                                &reflectionsTemporalAccumulationDescriptorSet->getDescriptorSetsHandle()[i],
                                0,
                                nullptr);
        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

       utils::transitionImageLayout(commandBuffers[i], depthRenderTarget.getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1, 1, VK_IMAGE_ASPECT_DEPTH_BIT);

        //store prev color moments
        utils::transitionImageLayout(commandBuffers[i], reflectionsPrevColorMoments->textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], reflectionsColorMoments->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

        utils::copyImageToImage(commandBuffers[i],
                                device.getQueues().graphicsQueue,
                                reflectionsColorMoments->getTextureImage().textureImages[0],
                                reflectionsPrevColorMoments->textureImages[0],
                                reflectionsPrevColorMoments->texWidth,
                                reflectionsPrevColorMoments->texHeight);

        utils::transitionImageLayout(commandBuffers[i], reflectionsColorMoments->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], reflectionsPrevColorMoments->textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        //store prev frame normals
        utils::transitionImageLayout(commandBuffers[i], prevNormals->textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], worldSpaceNormalsRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

        utils::copyImageToImage(commandBuffers[i],
                                device.getQueues().graphicsQueue,
                                worldSpaceNormalsRenderTarget->getTextureImage().textureImages[0],
                                prevNormals->textureImages[0],
                                prevNormals->texWidth,
                                prevNormals->texHeight);

        utils::transitionImageLayout(commandBuffers[i], worldSpaceNormalsRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], prevNormals->textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        //store prev depth
        utils::transitionImageLayout(commandBuffers[i], prevDepth->textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], linearDepthDerivativesRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

        utils::copyImageToImage(commandBuffers[i],
                                device.getQueues().graphicsQueue,
                                linearDepthDerivativesRenderTarget->getTextureImage().textureImages[0],
                                prevDepth->textureImages[0],
                                prevDepth->texWidth,
                                prevDepth->texHeight);

        utils::transitionImageLayout(commandBuffers[i], linearDepthDerivativesRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], prevDepth->textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
     
    
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////// Atrous PASS  /////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        for(int j = 0; j < numReflectionAtrousPasses; j++) {
            VkRenderPassBeginInfo AtrousPassInfo{};
            AtrousPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            AtrousPassInfo.renderPass = reflectionsAtrousPass[j]->getRenderPassHandle();
            AtrousPassInfo.framebuffer = reflectionsAtrousPass[j]->getFramebuffers()[0];
            AtrousPassInfo.renderArea.offset = { 0, 0 };
            AtrousPassInfo.renderArea.extent = swapChain.getSwapChainExtent();
            AtrousPassInfo.clearValueCount = 1;
            AtrousPassInfo.pClearValues = clearValues.data();

            vkCmdBeginRenderPass(commandBuffers[i], &AtrousPassInfo, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, reflectionsAtrousPipeline[j]->getPipeline());

            vkCmdBindDescriptorSets(commandBuffers[i],
                                    VK_PIPELINE_BIND_POINT_GRAPHICS,
                                    reflectionsAtrousPipeline[j]->getPipelineLayout(),
                                    0,
                                    1,
                                    &reflectionsAtrousDescriptorSet[j]->getDescriptorSetsHandle()[i],
                                    0,
                                    nullptr);

            vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

            vkCmdEndRenderPass(commandBuffers[i]);
        }

        //store prev frame color buffer
        utils::transitionImageLayout(commandBuffers[i], reflectionsPrevFrame->textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], reflectionsAtrousRenderTarget[0]->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

        utils::copyImageToImage(commandBuffers[i],
                                device.getQueues().graphicsQueue,
                                reflectionsAtrousRenderTarget[0]->getTextureImage().textureImages[0],
                                reflectionsPrevFrame->textureImages[0],
                                reflectionsPrevFrame->texWidth,
                                reflectionsPrevFrame->texHeight);

        utils::transitionImageLayout(commandBuffers[i], reflectionsAtrousRenderTarget[0]->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], reflectionsPrevFrame->textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////// SKYBOX  /////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        VkRenderPassBeginInfo renderPassInfoSkybox{};
        renderPassInfoSkybox.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoSkybox.renderPass = skyboxRenderPass->getRenderPassHandle();
        renderPassInfoSkybox.framebuffer = skyboxRenderPass->getFramebuffers()[0];
        renderPassInfoSkybox.renderArea.offset = { 0, 0 };
        renderPassInfoSkybox.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoSkybox.clearValueCount = 1;
        renderPassInfoSkybox.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoSkybox, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, skyboxPipeline.getPipeline());

        skyBox.draw(commandBuffers[i], skyboxPipeline.getPipelineLayout());

        vkCmdEndRenderPass(commandBuffers[i]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////// DEFERRED RENDERING ///////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        clearValues[2].depthStencil = { 1.0f, 0 };

        VkRenderPassBeginInfo deferredRenderPassInfo{};
        deferredRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        deferredRenderPassInfo.renderPass = renderPass.getRenderPassHandle();
        deferredRenderPassInfo.framebuffer = renderPass.getFramebuffers()[0];
        deferredRenderPassInfo.renderArea.offset = { 0, 0 };
        deferredRenderPassInfo.renderArea.extent = swapChain.getSwapChainExtent();
        deferredRenderPassInfo.clearValueCount = 3;
        deferredRenderPassInfo.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &deferredRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.getPipeline());

        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                pipeline.getPipelineLayout(),
                                0,
                                1,
                                &gBufferDescriptorSet->getDescriptorSetsHandle()[i],
                                0,
                                nullptr);

        //bind the scene descriptor set
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                pipeline.getPipelineLayout(),
                                1,
                                1,
                                &sceneDescriptorSet.getDescriptorSetsHandle()[i],
                                0,
                                nullptr);

        //bind ui params
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                pipeline.getPipelineLayout(),
                                2,
                                1,
                                &imGuiUi.descriptorSets[i],
                                0,
                                nullptr);

        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////// BLUR RENDERING /////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[1].depthStencil = { 1.0f, 0 };
        clearValues[2].color = { 0.0f, 0.0f, 0.0f, 1.0f };

        //Post processing
        VkRenderPassBeginInfo renderPassInfoBlurHor{};
        renderPassInfoBlurHor.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoBlurHor.renderPass = blurHorRenderPass.getRenderPassHandle();
        renderPassInfoBlurHor.framebuffer = blurHorRenderPass.getFramebuffers()[0];
        renderPassInfoBlurHor.renderArea.offset = { 0, 0 };
        renderPassInfoBlurHor.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoBlurHor.clearValueCount = 2;
        renderPassInfoBlurHor.pClearValues = clearValues.data();

        VkRenderPassBeginInfo renderPassInfoBlurVer{};
        renderPassInfoBlurVer.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoBlurVer.renderPass = blurVerRenderPass.getRenderPassHandle();
        renderPassInfoBlurVer.framebuffer = blurVerRenderPass.getFramebuffers()[0];
        renderPassInfoBlurVer.renderArea.offset = { 0, 0 };
        renderPassInfoBlurVer.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoBlurVer.clearValueCount = 2;
        renderPassInfoBlurVer.pClearValues = clearValues.data();

        VkRenderPassBeginInfo renderPassInfoBlendBloom{};
        renderPassInfoBlendBloom.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoBlendBloom.renderPass = blendBloomRenderPass.getRenderPassHandle();
        renderPassInfoBlendBloom.framebuffer = blendBloomRenderPass.getFramebuffers()[0];
        renderPassInfoBlendBloom.renderArea.offset = { 0, 0 };
        renderPassInfoBlendBloom.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoBlendBloom.clearValueCount = 2;
        renderPassInfoBlendBloom.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoBlurHor, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, postProcessingHorPipeline.getPipeline());
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                postProcessingHorPipeline.getPipelineLayout(),
                                0,
                                1,
                                &blurHorDescriptorSet.getDescriptorSetsHandle()[i],
                                0,
                                nullptr);
        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoBlurVer, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, postProcessingVerPipeline.getPipeline());
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                postProcessingHorPipeline.getPipelineLayout(),
                                0,
                                1,
                                &blurVerDescriptorSet.getDescriptorSetsHandle()[i],
                                0,
                                nullptr);
        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////// BLOOM BLEND ////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoBlendBloom, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, postProcessingBlendBloomPipeline.getPipeline());
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                postProcessingBlendBloomPipeline.getPipelineLayout(),
                                0,
                                1,
                                &BloomBlendDescriptorSet.getDescriptorSetsHandle()[i],
                                0,
                                nullptr);
        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////// TEMPORAL ANTIALIASING PASS //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        utils::transitionImageLayout(commandBuffers[i], depthRenderTarget.getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1, 1, VK_IMAGE_ASPECT_DEPTH_BIT);

        VkRenderPassBeginInfo temporalAAPassInfo{};
        temporalAAPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        temporalAAPassInfo.renderPass = temporalAAPass->getRenderPassHandle();
        temporalAAPassInfo.framebuffer = temporalAAPass->getFramebuffers()[0];
        temporalAAPassInfo.renderArea.offset = { 0, 0 };
        temporalAAPassInfo.renderArea.extent = swapChain.getSwapChainExtent();
        temporalAAPassInfo.clearValueCount = 1;
        temporalAAPassInfo.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &temporalAAPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, temporalAAPipeline->getPipeline());

        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                temporalAAPipeline->getPipelineLayout(),
                                0,
                                1,
                                &temporalAADescriptorSet->getDescriptorSetsHandle()[i],
                                0,
                                nullptr);

        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        utils::transitionImageLayout(commandBuffers[i], depthRenderTarget.getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1, 1, VK_IMAGE_ASPECT_DEPTH_BIT);

        utils::transitionImageLayout(commandBuffers[i], temporalAAPrevFrame->textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], temporalAARenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

        utils::copyImageToImage(commandBuffers[i],
                                device.getQueues().graphicsQueue,
                                temporalAARenderTarget->getTextureImage().textureImages[0],
                                temporalAAPrevFrame->textureImages[0],
                                temporalAAPrevFrame->texWidth,
                                temporalAAPrevFrame->texHeight);

        utils::transitionImageLayout(commandBuffers[i], temporalAARenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
        utils::transitionImageLayout(commandBuffers[i], temporalAAPrevFrame->textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////// DEBUG PASS //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        VkRenderPassBeginInfo renderPassInfoPassThrough2{};
        renderPassInfoPassThrough2.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoPassThrough2.renderPass = debugPass->getRenderPassHandle();
        renderPassInfoPassThrough2.framebuffer = debugPass->getFramebuffers()[i];
        renderPassInfoPassThrough2.renderArea.offset = { 0, 0 };
        renderPassInfoPassThrough2.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoPassThrough2.clearValueCount = 2;
        renderPassInfoPassThrough2.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoPassThrough2, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, debugPipeline->getPipeline());
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                debugPipeline->getPipelineLayout(),
                                0,
                                1,
                                &debugDescriptorSet->getDescriptorSetsHandle()[i],
                                0,
                                nullptr);
        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        //prepare reflections rendertarget for rendering
        utils::transitionImageLayout(commandBuffers[i], raytracingReflectionsRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL, 1, 1);
        utils::transitionImageLayout(commandBuffers[i], raytracingReflectionsHitPointsRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL, 1, 1);
        utils::transitionImageLayout(commandBuffers[i], raytracingReflectionsAlbedoRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL, 1, 1);
        utils::transitionImageLayout(commandBuffers[i], raytracingReflectionsFakeHitPositionRenderTarget->getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL, 1, 1);

        VK_CHECK_RESULT(vkEndCommandBuffer(commandBuffers[i]));
    }
}

void DeferredRendererReflections::update(uint32_t imageIndex)
{
    skyBox.update(imageIndex);

    if (cameraParams.resetAnimation || cameraParams.resetCameraAnimation) {
        cameraParams.tookScreenshot = false;
        cameraParams.prepareScreenshot = false;
        cameraParams.timeInAnimation = 0.0f;
        cameraParams.timeInCameraAnimation = 0.0f;
        cameraParams.timeInPrepareScreenshot = 0.0f;
    }

    if(cameraParams.prepareScreenshot) {
        cameraParams.timeInPrepareScreenshot += Clock::getInstance()->deltaTime;
        if(cameraParams.timeInPrepareScreenshot > 0.05f) {
            cameraParams.tookScreenshot = true;
            takeScreenshot = true;
            cameraParams.prepareScreenshot = false;
        }
    }

    if (cameraParams.playCameraAnimation || cameraParams.resetCameraAnimation) {

        if (!cameraParams.resetCameraAnimation && (cameraParams.timeToStopAnimation <= 0.01f || cameraParams.timeToStopAnimation > cameraParams.timeInCameraAnimation))
        {
            cameraParams.timeInCameraAnimation += Clock::getInstance()->deltaTime * cameraParams.animationSpeed;
        }

        if(cameraParams.timeToStopAnimation >= 0.01f)
            cameraParams.timeInCameraAnimation = glm::min(cameraParams.timeInCameraAnimation, cameraParams.timeToStopAnimation);

        if (cameraParams.takeScreenshotAtEnd &&
            !cameraParams.tookScreenshot &&
            cameraParams.timeToStopAnimation >= 0.01f &&
            cameraParams.timeToStopAnimation <= cameraParams.timeInCameraAnimation)
        {
            cameraParams.prepareScreenshot = true;
        }

        CameraMovement::Keyframe keyframe = cameraParams.cameraMovements[cameraParams.cameraMovementToUse].evaluate(cameraParams.timeInCameraAnimation);
        camera.setCameraPos(keyframe.pos);
        camera.setCameraTarget(keyframe.target);
    }

    if(cameraParams.playAnimation || cameraParams.resetAnimation) {

        if (cameraParams.takeScreenshotAtEnd && 
            !cameraParams.tookScreenshot &&
            cameraParams.timeToStopAnimation >= 0.01f && 
            cameraParams.timeToStopAnimation <= cameraParams.timeInAnimation) 
        {
            cameraParams.prepareScreenshot = true;
        }

        else if(!cameraParams.resetAnimation && (cameraParams.timeToStopAnimation <= 0.01f || cameraParams.timeToStopAnimation > cameraParams.timeInAnimation) )
        {
            cameraParams.timeInAnimation += Clock::getInstance()->deltaTime;
        }

        if (cameraParams.timeToStopAnimation >= 0.01f)
            cameraParams.timeInAnimation = glm::min(cameraParams.timeInAnimation, cameraParams.timeToStopAnimation);

        if (!cameraParams.resetAnimation)
            scene.suns[0].dir = glm::vec4(glm::sin(glm::radians(cameraParams.timeInAnimation * cameraParams.animationSpeed * 180.0f)),
                                            -1.0f,
                                            glm::cos(glm::radians(cameraParams.timeInAnimation * cameraParams.animationSpeed * 180.0f)),
                                            1.0f);

        if (cameraParams.currScene == 0) {
            
            scene.sceneNodes[0]->children[0]->translation = scene.sceneNodes[0]->children[0]->initialTranslation - glm::vec3(0.0f, 0.0f, (glm::sin(glm::radians(cameraParams.timeInAnimation * cameraParams.animationSpeed * 180.0f))) * 2.0f);

            scene.sceneNodes[0]->children[0]->rotation = scene.sceneNodes[0]->children[0]->initialRotation * glm::quat(glm::vec3(0.0f));

            scene.sceneNodes[0]->children[0]->scale = glm::vec3(1.0f);
            
            
            /*
            glm::vec3 initialScale = scene.sceneNodes[0]->children[0]->initialScale;
            scene.sceneNodes[0]->children[0]->translation = scene.sceneNodes[0]->children[0]->initialTranslation - glm::vec3((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed)) * 4.0, 0.0f, 0.0);

            scene.sceneNodes[0]->children[0]->rotation = scene.sceneNodes[0]->children[0]->initialRotation * glm::quat(glm::vec3(cameraParams.timeInAnimation * cameraParams.animationSpeed * 2.0f,
                                                                                                                                 cameraParams.timeInAnimation * cameraParams.animationSpeed * 2.0f, 
                                                                                                                                 cameraParams.timeInAnimation * cameraParams.animationSpeed * 2.0f));
            
            scene.sceneNodes[0]->children[0]->scale = glm::vec3(initialScale.x * (glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f),
                                                                initialScale.y * (glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f),
                                                                initialScale.z * (glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f));
                                                                */
        }
        else if (cameraParams.currScene == 2) {
            /*
            glm::vec3 initialScale = scene.sceneNodes[0]->children[2]->initialScale;
            scene.sceneNodes[0]->children[2]->translation = scene.sceneNodes[0]->children[2]->initialTranslation + glm::vec3(glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed), 0.0f, 0.0);
            scene.sceneNodes[0]->children[2]->scale = glm::vec3(initialScale.x * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f)),
                                                                initialScale.y * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f)),
                                                                initialScale.z * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f)));
                                                                */
            scene.suns[0].dir = glm::vec4(glm::sin(glm::radians((cameraParams.timeInAnimation + 1.0f) * cameraParams.animationSpeed * 180.0f)),
                                            -0.4f,
                                            glm::cos(glm::radians((cameraParams.timeInAnimation + 1.0f) * cameraParams.animationSpeed * 180.0f)),
                                            1.0f);
        }
        else if (cameraParams.currScene == 4) {
            for (int i = 2; i < scene.sceneNodes[0]->children.size(); i++) {
                glm::vec3 initialScale = scene.sceneNodes[0]->children[i]->initialScale;
                scene.sceneNodes[0]->children[i]->scale = glm::vec3(initialScale.x * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed * 5.0f) * 0.5f + 1.0f)),
                                                                    initialScale.y * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed * 5.0f) * 0.5f + 1.0f)),
                                                                    initialScale.z * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed * 5.0f) * 0.5f + 1.0f)));
            }
        }
        else if (cameraParams.currScene == 8) {

            glm::vec3 initialScale = scene.sceneNodes[0]->children[0]->initialScale;
            scene.sceneNodes[0]->children[0]->scale = glm::vec3(initialScale.x * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f)),
                                                                initialScale.y * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f)),
                                                                initialScale.z * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f)));
            scene.sceneNodes[0]->children[0]->translation = glm::vec3(scene.sceneNodes[0]->children[0]->initialTranslation.x, 
                                                                      scene.sceneNodes[0]->children[0]->initialTranslation.y + glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed),
                                                                      scene.sceneNodes[0]->children[0]->initialTranslation.z);

            initialScale = scene.sceneNodes[0]->children[4]->initialScale;
            scene.sceneNodes[0]->children[4]->scale = glm::vec3(initialScale.x * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f)),
                                                                initialScale.y * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f)),
                                                                initialScale.z * ((glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed) + 1.0f)));
            scene.sceneNodes[0]->children[4]->translation = glm::vec3(scene.sceneNodes[0]->children[4]->initialTranslation.x,
                                                                      scene.sceneNodes[0]->children[4]->initialTranslation.y + glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed),
                                                                      scene.sceneNodes[0]->children[4]->initialTranslation.z);

            scene.sceneNodes[0]->children[5]->rotation = glm::quat(glm::vec3(cameraParams.timeInAnimation * cameraParams.animationSpeed, 0.0f, 0.0f));
            
        }
        else if (cameraParams.currScene == 9) {
            scene.sceneNodes[0]->children[1]->translation = glm::vec3(scene.sceneNodes[0]->children[1]->initialTranslation.x + (glm::sin(cameraParams.timeInAnimation * cameraParams.animationSpeed)) * 15.0f,
                                                                      scene.sceneNodes[0]->children[1]->initialTranslation.y, 
                                                                      scene.sceneNodes[0]->children[1]->initialTranslation.z);

            scene.suns[0].dir = glm::vec4(glm::sin(glm::radians((cameraParams.timeInAnimation + 1.0f) * cameraParams.animationSpeed * 180.0f)),
                                            -1.0f,
                                            glm::cos(glm::radians((cameraParams.timeInAnimation + 1.0f) * cameraParams.animationSpeed * 180.0f)),
                                            1.0f);
        }
    }

    /*
    float x = 2.0 * glm::cos(Clock::getInstance()->time);
    float y = 2.0 * glm::sin(Clock::getInstance()->time);
    glm::vec3 lightPos = glm::vec3(x * 0.2f, 1.0f, y * 0.2f);
    scene.suns[0].dir = -lightPos;
    */
    scene.update(imageIndex);
    if (cameraParams.setCameraPos) {
        camera.setCameraPos(cameraParams.pos);
    }

    if (cameraParams.setCameraTarget) {
        camera.setCameraTarget(cameraParams.target);
    }

    cameraParams.camCurrPos = camera.pos;

    camera.useJitter = temporalAntialiasingParams.toSubmit.useTemporalAntialiasing && temporalAntialiasingParams.toSubmit.useCameraShake;

    //submit scene data
    sceneParams.maxLevelRoughnessMipMap = static_cast<float>(maxLevelRoughnessMipMap);
    memcpy(sceneParams.suns, scene.suns.data(), scene.suns.size() * sizeof(Sun));
    sceneParams.numSuns = static_cast<int>(scene.suns.size());
    sceneParams.cameraPos = camera.pos;
    sceneUniform.submitDataToMemory(&sceneParams);

    //blur data
    blurParamsHor.toSubmit.blurHorizontal = true;
    blurParamsVer = blurParamsHor;
    blurParamsVer.toSubmit.blurHorizontal = false;
    blurHorUniformBuffer.submitDataToMemory(&blurParamsHor.toSubmit, imageIndex);
    blurVerUniformBuffer.submitDataToMemory(&blurParamsVer.toSubmit, imageIndex);

    //shadows data
    shadowParams.toSubmit.lightPos = scene.suns[0].dir;
    shadowParams.toSubmit.frameIndex = frameIndex;
    shadowParams.toSubmit.time = Clock::getInstance()->time;
    shadowParams.toSubmit.width = swapChain.getSwapChainExtent().width;
    shadowParams.toSubmit.height = swapChain.getSwapChainExtent().height;    
    if (camera.isStill() && !cameraParams.setCameraPos) {
        if (shadowParams.toSubmit.numAccumulatedFrames <= shadowParams.toSubmit.maxNumberOfFrames)
            if (shadowParams.toSubmit.numAccumulatedFrames <= shadowParams.toSubmit.maxNumberOfFrames)
                shadowParams.toSubmit.numAccumulatedFrames++;
        }
    else
        shadowParams.toSubmit.numAccumulatedFrames = 1;

    if(cameraParams.resetAccumulation)
        shadowParams.toSubmit.numAccumulatedFrames = 1;

    shadowsParamsUniform->submitDataToMemory(&shadowParams.toSubmit);

    //reflections data
    reflectionsParams.toSubmit.vertexSize = sizeof(utils::Vertex);
    reflectionsParams.toSubmit.lightPos = scene.suns[0].dir;
    reflectionsParams.toSubmit.lightColor = scene.suns[0].lightColor;
    reflectionsParams.toSubmit.lightIntensity = scene.suns[0].lightIntensity;
    reflectionsParams.toSubmit.frameIndex = frameIndex;
    reflectionsParams.toSubmit.time = Clock::getInstance()->time;
    reflectionsParams.toSubmit.baseMetalness = imGuiUi.globalShaderInputs.baseMetalness;
    reflectionsParams.toSubmit.baseRoughness = imGuiUi.globalShaderInputs.baseRoughness;
    reflectionsParams.toSubmit.camPos = glm::vec4(camera.pos, 1.0f);
    reflectionsParams.toSubmit.skyboxIntensity = imGuiUi.globalShaderInputsDeviceSubmission.skyboxIntensity;
    reflectionsParams.toSubmit.width = swapChain.getSwapChainExtent().width;
    reflectionsParams.toSubmit.height = swapChain.getSwapChainExtent().height;
    reflectionsParams.toSubmit.maxPathLength = glm::min(reflectionsParams.toSubmit.maxPathLength, 2);
    reflectionsParams.toSubmit.useDemodulation = imGuiUi.globalShaderInputs.useDemodulation;
    if (camera.isStill() && !cameraParams.setCameraPos) {
        if(reflectionsParams.toSubmit.numAccumulatedFrames <= reflectionsParams.toSubmit.maxNumberOfFrames)
            reflectionsParams.toSubmit.numAccumulatedFrames++;
    }
    else
        reflectionsParams.toSubmit.numAccumulatedFrames = 1;

    if (cameraParams.resetAccumulation)
        reflectionsParams.toSubmit.numAccumulatedFrames = 1;

    reflectionsParamsUniform->submitDataToMemory(&reflectionsParams.toSubmit);

    //temporal accumulation data
    shadowsTemporalAccumulationParams.toSubmit.isStill = camera.isStill();
    shadowsTemporalAccumulationParams.toSubmit.width = swapChain.getSwapChainExtent().width;
    shadowsTemporalAccumulationParams.toSubmit.height = swapChain.getSwapChainExtent().height;
    shadowsTemporalAccumulationUniformBuffer->submitDataToMemory(&shadowsTemporalAccumulationParams.toSubmit, imageIndex);

    //reflections accumulation data
    reflectionsTemporalAccumulationParams.toSubmit.isStill = camera.isStill();
    reflectionsTemporalAccumulationParams.toSubmit.prevFrameView = camera.GetPrevViewMatrix();
    reflectionsTemporalAccumulationParams.toSubmit.prevFrameProj = camera.GetPrevProjectionMatrixNoJitter();
    reflectionsTemporalAccumulationParams.toSubmit.currFrameView = camera.GetViewMatrix();
    reflectionsTemporalAccumulationParams.toSubmit.currFrameProj = camera.GetProjectionMatrix();
    reflectionsTemporalAccumulationParams.toSubmit.jitter = camera.GetJitter();
    reflectionsTemporalAccumulationParams.toSubmit.width = swapChain.getSwapChainExtent().width;
    reflectionsTemporalAccumulationParams.toSubmit.height = swapChain.getSwapChainExtent().height;
    reflectionsTemporalAccumulationUniformBuffer->submitDataToMemory(&reflectionsTemporalAccumulationParams.toSubmit, imageIndex);

    //atrous data
    int blurScale = 0;
    for (int i = 0; i < numAtrousPasses; i++) {
        atrousParams.toSubmit.blurScale = blurScale;
        atrousParams.toSubmit.currIteration = i;
        atrousParams.toSubmit.shadowAngle = shadowParams.toSubmit.shadowConeAngle;
        atrousUniformBuffers[i]->submitDataToMemory(&atrousParams.toSubmit, imageIndex);
        if (atrousParams.toSubmit.useSeparableAtrous) {
            if (i % 2 != 0)
                blurScale++;
        }
        else
            blurScale++;
    }

    //blur var data
    for (int i = 0; i < numAtrousPasses; i++)
        blurVarHorUniformBuffer[i]->submitDataToMemory(&blurVarParams.toSubmit, imageIndex);
    blurVarVerUniformBuffer->submitDataToMemory(&blurVarParams.toSubmit, imageIndex);

    //Atrous data
    blurScale = 0;
    for(int i = 0; i < numReflectionAtrousPasses; i++) {
        ReflectionsAtrousParams::ToSubmit toSubmit = reflectionsAtrousParams.toSubmit;
        toSubmit.blurScale = blurScale;
        toSubmit.currBlurIteration = i;
        toSubmit.width = swapChain.getSwapChainExtent().width;
        toSubmit.height = swapChain.getSwapChainExtent().height;
        toSubmit.useImplicitSamplingOfEnvironmentMap = reflectionsParams.toSubmit.useImplicitSamplingOfEnvironmentMap;
        reflectionsAtrousUniformBuffer[i]->submitDataToMemory(&toSubmit, imageIndex);

        if(toSubmit.useSeparatedAtrous) {
            if(i % 2 != 0)
                blurScale++;
        }
        else
            blurScale++;
    }

    //temporal antialiasing
    //jitter
    temporalAntialiasingParams.toSubmit.width = swapChain.getSwapChainExtent().width;
    temporalAntialiasingParams.toSubmit.height = swapChain.getSwapChainExtent().height;
    temporalAAUniformBuffer->submitDataToMemory(&temporalAntialiasingParams.toSubmit, imageIndex);

    //debug data
    debugUniformBuffer->submitDataToMemory(&debugParams.toSubmit, imageIndex);

    if (window.getKeyboard().keyUp(GLFW_KEY_I)) {
        imGuiUi.toggleActive();
    }

    if (window.getKeyboard().keyUp(GLFW_KEY_K)) {
        std::cout << "saved screenshot" << "\n";
        saveScreenshot("result.ppm", device, swapChain, commandPool);
    }
}

void DeferredRendererReflections::finishedDrawing()
{
    if (takeScreenshot) {
        vkDeviceWaitIdle(device.getLogicalDeviceHandle());
        saveScreenshot("result.ppm", device, swapChain, commandPool);
        takeScreenshot = false;
    }

    if (cameraParams.currScene != cameraParams.prevScene) {
        vkDeviceWaitIdle(device.getLogicalDeviceHandle());
        deleteScene();

        std::vector<std::string> scenePaths;
        scenePaths.push_back(cameraParams.scenesPaths[cameraParams.currScene]);

        loadScene(scenePaths);
        recreateSwapChain();
        cameraParams.prevScene = cameraParams.currScene;
    }
}

void DeferredRendererReflections::createUI()
{
    frameTime.frameTimeMetric = new MetricsGuiMetric("Frames Per Second", "fps", MetricsGuiMetric::USE_SI_UNIT_PREFIX);
    frameTime.frameTimePlot = new MetricsGuiPlot();
    frameTime.frameTimeMetric->mSelected = true;
    frameTime.frameTimePlot->mShowAverage = true;
    frameTime.frameTimePlot->mShowLegendAverage = true;
    frameTime.frameTimePlot->mRangeDampening = 0.95;
    frameTime.frameTimePlot->AddMetric(frameTime.frameTimeMetric);

    utils::QueueFamilyIndices queueFamilyIndices = utils::findQueueFamilies(instance.getPhysicalDevice(), instance.getSurface());
    imGuiUi = ImGuiUi(device,
                        queueFamilyIndices,
                        swapChain,
                        imGuiRenderPass,
                        &scene,
                        { &blurParamsHor,
                           &shadowsTemporalAccumulationParams,
                           &debugParams,
                           &shadowParams,
                           &atrousParams,
                           &blurVarParams,
                           &reflectionsAtrousParams,
                           &temporalAntialiasingParams,
                           &reflectionsParams,
                           &reflectionsTemporalAccumulationParams,
                           &reflectionsShadingParams,
                           &cameraParams,
                           &scene,
                           &frameTime
                        });
    imGuiUi.createDescriptorPool();
    imGuiUi.createCommandPool();
    imGuiUi.createCommandBuffers();
    imGuiUi.initUniformBuffers();
    imGuiUi.createDescriptorSetLayout();
    imGuiUi.createDescriptorSet();

    //TODO THIS HERE DOESNT MAKE A LOT OF SENSE
    ImGui_ImplVulkan_InitInfo init_info = {};
    init_info.Instance = instance.getInstanceHandle();
    init_info.PhysicalDevice = instance.getPhysicalDevice();
    init_info.Device = device.getLogicalDeviceHandle();
    init_info.QueueFamily = queueFamilyIndices.graphicsFamily.value();
    init_info.Queue = device.getQueues().graphicsQueue;
    init_info.PipelineCache = nullptr;
    init_info.DescriptorPool = imGuiUi.descriptorPool;
    init_info.Allocator = nullptr;
    init_info.MinImageCount = 2;
    init_info.ImageCount = swapChain.getSize();
    init_info.CheckVkResultFn = [](VkResult err) {
        if (err == 0) return;
        printf("VkResult %d\n", err);
        if (err < 0)
            abort();
    };
    ImGui_ImplVulkan_Init(&init_info, imGuiRenderPass.renderPass);
    ImGui_ImplGlfw_InitForVulkan(window.getWindowHandle(), true);

    imGuiUi.createFontImage();
    
    cameraParams.cameraMovements.clear();
    if (cameraParams.currScene == 0) {
        CameraMovement camMov1;
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(10.0, 5.0, -5.0), glm::vec3(0.0, 3.0, -5.0), 0.0f));
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(10.0, 5.0, 5.0), glm::vec3(0.0, 3.0, 5.0), 2.0f));
        cameraParams.cameraMovements.push_back(camMov1);
    }
    else if (cameraParams.currScene == 2) {
        CameraMovement camMov1;
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(-5, 4, 10), glm::vec3(0.0f, 0.0f, 0.0f), 0.0f));
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(5, 4, 10), glm::vec3(0.0f, 0.0f, 0.0f), 1.0f));
        cameraParams.cameraMovements.push_back(camMov1);
    }
    else if (cameraParams.currScene == 3) {
        CameraMovement camMov1;
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(6, 5, 0), glm::vec3(0.0f, 16.0f, 10.0f), 0.0f));
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(-6, 5, 0), glm::vec3(0.0f, 16.0f, 10.0f), 3.0f));
        cameraParams.cameraMovements.push_back(camMov1);

        CameraMovement camMov2;
        camMov2.addKeyframe(CameraMovement::Keyframe(glm::vec3(-7, 6, -5), glm::vec3(-7, 4, -3), 0.0f));
        camMov2.addKeyframe(CameraMovement::Keyframe(glm::vec3(4, 6, -5), glm::vec3(4, 4, -3), 3.0f));
        cameraParams.cameraMovements.push_back(camMov2);

        CameraMovement camMov3;
        camMov3.addKeyframe(CameraMovement::Keyframe(glm::vec3(-8, 5, -0), glm::vec3(10, 5, 0), 0.0f));
        camMov3.addKeyframe(CameraMovement::Keyframe(glm::vec3(4, 5, -0), glm::vec3(10, 5, 0), 3.0f));
        cameraParams.cameraMovements.push_back(camMov3);
    }
    else if (cameraParams.currScene == 4) {
        CameraMovement camMov1;
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(15.0f, 10.0f, 0.0), glm::vec3(0.0f, 0.0f, 0.0f), 0.0f));
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(0.0f, 10.0f, 18.0), glm::vec3(0.0f, 0.0f, 0.0f), 2.0f));
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(-15.0f, 10.0f, 0.0), glm::vec3(0.0f, 0.0f, 0.0f), 4.0f));
        cameraParams.cameraMovements.push_back(camMov1);
    }
    else if (cameraParams.currScene == 8) {
        CameraMovement camMov1;
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(12, 20, -5.0), glm::vec3(0.0f, 0.0f, -5.0f), 0.0f));
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(12, 20, 5.0), glm::vec3(0.0f, 0.0f, 5.0f), 2.0f));
        cameraParams.cameraMovements.push_back(camMov1);
    }
    else if (cameraParams.currScene == 9) {
        CameraMovement camMov1;
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(-6.0, 12.0f, 12.0f), glm::vec3(-6.0f, 3.0f, 0.0f), 0.0f));
        camMov1.addKeyframe(CameraMovement::Keyframe(glm::vec3(6.0, 12.0f, 12.0f), glm::vec3(6.0f, 3.0f, 0.0f), 2.0f));
        cameraParams.cameraMovements.push_back(camMov1);
    }
}


void DeferredRendererReflections::defineRenderer()
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// CREATE RENDERTARGETS ////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    VkFormat depthFormat = utils::findSupportedFormat(
        instance.getPhysicalDevice(),
        { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
        VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    );

    depthRenderTarget = RenderTarget("Depth Render Target",
                                     depthFormat,
                                     swapChain.getSwapChainExtent(),
                                     1,
                                     VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                                     static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                     device,
                                     instance,
                                     swapChain,
                                     commandPool);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 depthRenderTarget.getTextureImage().textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                 1,
                                 1,
                                 VK_IMAGE_ASPECT_DEPTH_BIT);

    //Offscreen render targets (G-BUFFER)
    worldSpacePositionRenderTarget = new RenderTarget("Position Render Target",
                                            VK_FORMAT_R32G32B32A32_SFLOAT,
                                            swapChain.getSwapChainExtent(),
                                            4,
                                            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                            static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                            device,
                                            instance,
                                            swapChain,
                                            commandPool);

    albedoAoRenderTarget = new RenderTarget("Albedo Render Target",
                                              VK_FORMAT_R8G8B8A8_SRGB,
                                              swapChain.getSwapChainExtent(),
                                              4,
                                              VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                              static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                              device,
                                              instance,
                                              swapChain,
                                              commandPool);

    textureAlbedoRenderTarget = new RenderTarget("Texture Albedo Render Target",
                                                 VK_FORMAT_R8G8B8A8_SRGB,
                                                 swapChain.getSwapChainExtent(),
                                                 4,
                                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                 static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                                 device,
                                                 instance,
                                                 swapChain,
                                                 commandPool);

    worldSpaceNormalsRenderTarget = new RenderTarget("Normal Render Target",
                                                      VK_FORMAT_R16G16B16A16_SFLOAT,
                                                      swapChain.getSwapChainExtent(),
                                                      4,
                                                      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                      static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                                      device,
                                                      instance,
                                                      swapChain,
                                                      commandPool);

    tangentsRenderTarget = new RenderTarget("Tangents Render Target",
                                            VK_FORMAT_R16G16B16A16_SFLOAT,
                                            swapChain.getSwapChainExtent(),
                                            4,
                                            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                            static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                            device,
                                            instance,
                                            swapChain,
                                            commandPool);

    metalRoughnessRenderTarget = new RenderTarget("Metal Roughness Render Target",
                                                  VK_FORMAT_R8G8B8A8_UNORM,
                                                  swapChain.getSwapChainExtent(),
                                                  4,
                                                  VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                  static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                                  device,
                                                  instance,
                                                  swapChain,
                                                  commandPool);

    normalsTexturelessRenderTarget = new RenderTarget("Normals Textureless Render Target",
                                                      VK_FORMAT_R16G16B16A16_SFLOAT,
                                                      swapChain.getSwapChainExtent(),
                                                      4,
                                                      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                      static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                                      device,
                                                      instance,
                                                      swapChain,
                                                      commandPool);

    linearDepthDerivativesRenderTarget = new RenderTarget("Depths and Depths Derivatives Render Target",
                                                          VK_FORMAT_R32G32B32A32_SFLOAT,
                                                          swapChain.getSwapChainExtent(),
                                                          4,
                                                          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                          static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                                          device,
                                                          instance,
                                                          swapChain,
                                                          commandPool);

    //Deferred render targets
    colorRenderTarget = RenderTarget("Color Render Target",
                                     VK_FORMAT_R32G32B32A32_SFLOAT,
                                     swapChain.getSwapChainExtent(),
                                     4,
                                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                     static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                     device,
                                     instance,
                                     swapChain,
                                     commandPool);

    //Skybox Render target
    skyboxRenderTarget = new RenderTarget("Skybox Render Target",
                                          VK_FORMAT_R16G16B16A16_SFLOAT,
                                          swapChain.getSwapChainExtent(),
                                          4,
                                          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                          static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                          device,
                                          instance,
                                          swapChain,
                                          commandPool);

    //Post processing render target

    bloomRenderTarget = RenderTarget("Bloom Render Target",
                                     VK_FORMAT_R16G16B16A16_SFLOAT,
                                     swapChain.getSwapChainExtent(),
                                     4,
                                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                     static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                     device,
                                     instance,
                                     swapChain,
                                     commandPool);

    blurHorRenderTarget = RenderTarget("Blur Hor Render Target",
                                       VK_FORMAT_R16G16B16A16_SFLOAT,
                                       swapChain.getSwapChainExtent(),
                                       4,
                                       VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                       static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                       device,
                                       instance,
                                       swapChain,
                                       commandPool);


    blurVerRenderTarget = RenderTarget("Blur Ver Render Target",
                                       VK_FORMAT_R16G16B16A16_SFLOAT,
                                       swapChain.getSwapChainExtent(),
                                       4,
                                       VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                       static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                       device,
                                       instance,
                                       swapChain,
                                       commandPool);

    bloomBlendRenderTarget = new RenderTarget("Bloom Blend Render Target",
                                              VK_FORMAT_R16G16B16A16_SFLOAT,
                                              swapChain.getSwapChainExtent(),
                                              4,
                                              VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                              static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                              device,
                                              instance,
                                              swapChain,
                                              commandPool);

    shadowsTemporalAccumulationRenderTarget = new RenderTarget("Shadows Temporal Render Target",
                                                                VK_FORMAT_R8G8B8A8_UNORM,
                                                                swapChain.getSwapChainExtent(),
                                                                4,
                                                                VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                                static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                                                device,
                                                                instance,
                                                                swapChain,
                                                                commandPool);

    shadowColorMomentsRenderTarget = new RenderTarget("Shadow Color Moments Render Target",
                                                      VK_FORMAT_R16G16B16A16_SFLOAT,
                                                      swapChain.getSwapChainExtent(),
                                                      4,
                                                      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                      static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                                      device,
                                                      instance,
                                                      swapChain,
                                                      commandPool);

    prevShadowsColorMoments = new TextureImage("Prev Shadow Color Moments", 
                                                device,
                                                commandPool,
                                                swapChain.getSwapChainExtent().width,
                                                swapChain.getSwapChainExtent().height,
                                                4,
                                                VK_FORMAT_R16G16B16A16_SFLOAT,
                                                static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT),
                                                1,
                                                true);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 prevShadowsColorMoments->textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    reflectionsTemporalRenderTarget = new RenderTarget("Reflections Temporal Render Target",
                                                       VK_FORMAT_R16G16B16A16_SFLOAT,
                                                       swapChain.getSwapChainExtent(),
                                                       4,
                                                       VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                       static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                                       device,
                                                       instance,
                                                       swapChain,
                                                       commandPool);

    reflectionsColorMoments = new RenderTarget("Reflections Color Moments Render Target", 
                                               VK_FORMAT_R16G16B16A16_SFLOAT,
                                               swapChain.getSwapChainExtent(),
                                               4,
                                               VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                               static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                               device,
                                               instance,
                                               swapChain,
                                               commandPool);

    reflectionsFakeHitPositionRenderTarget = new RenderTarget("Reflections Normals Render Target",
                                                              VK_FORMAT_R16G16B16A16_SFLOAT,
                                                              swapChain.getSwapChainExtent(),
                                                              4,
                                                              VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                              static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                                              device,
                                                              instance,
                                                              swapChain,
                                                              commandPool);

    reflectionsPrevFrame = new TextureImage("Reflections Prev Frame", 
                                            device,
                                            commandPool,
                                            swapChain.getSwapChainExtent().width,
                                            swapChain.getSwapChainExtent().height,
                                            4,
                                            VK_FORMAT_R16G16B16A16_SFLOAT,
                                            static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT),
                                            1,
                                            true);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 reflectionsPrevFrame->textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    reflectionsPrevColorMoments = new TextureImage("Reflections Prev Color Moments",    
                                                    device,
                                                    commandPool,
                                                    swapChain.getSwapChainExtent().width,
                                                    swapChain.getSwapChainExtent().height,
                                                    4,
                                                    VK_FORMAT_R16G16B16A16_SFLOAT,
                                                    static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT),
                                                    1,
                                                    true);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 reflectionsPrevColorMoments->textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    velocityRenderTarget = new RenderTarget("Velocity Render Target", 
                                            VK_FORMAT_R16G16B16A16_SFLOAT,
                                            swapChain.getSwapChainExtent(),
                                            4,
                                            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                            static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                            device,
                                            instance,
                                            swapChain,
                                            commandPool);

    passThroughRenderTarget = new RenderTarget("Pass Through Render Target", 
                                               VK_FORMAT_R16G16B16A16_SFLOAT,
                                               swapChain.getSwapChainExtent(),
                                               4,
                                               VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                               static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                               device,
                                               instance,
                                               swapChain,
                                               commandPool);

    prevFrame = new TextureImage("Prev Frame", 
                                 device,
                                 commandPool,
                                 swapChain.getSwapChainExtent().width,
                                 swapChain.getSwapChainExtent().height,
                                 4,
                                 VK_FORMAT_R8G8B8A8_UNORM,
                                 static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT),
                                 1,
                                 true);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 prevFrame->textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    prevNormals = new TextureImage("Prev Normals", 
                                   device,
                                   commandPool,
                                   swapChain.getSwapChainExtent().width,
                                   swapChain.getSwapChainExtent().height,
                                   4,
                                   VK_FORMAT_R16G16B16A16_SFLOAT,
                                   static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT),
                                   1,
                                   true);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 prevNormals->textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    prevDepth = new TextureImage("Prev Depth", 
                                 device,
                                 commandPool,
                                 swapChain.getSwapChainExtent().width,
                                 swapChain.getSwapChainExtent().height,
                                 4,
                                 VK_FORMAT_R32G32B32A32_SFLOAT,
                                 static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT),
                                 1,
                                 true);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 prevDepth->textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    for (int i = 0; i < numAtrousPasses; i++) {
        atrousRenderTargets[i] = new RenderTarget("Shadows Atrous Render Target " + std::to_string(i), 
                                                  VK_FORMAT_R8G8B8A8_UNORM,
                                                  swapChain.getSwapChainExtent(),
                                                  4,
                                                  VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                  static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                                  device,
                                                  instance,
                                                  swapChain,
                                                  commandPool);

        varianceBlurredRenderTargets[i] = new RenderTarget("Shadows Variance Blurred Render Target " + std::to_string(i),
                                                           VK_FORMAT_R16G16B16A16_SFLOAT,
                                                           swapChain.getSwapChainExtent(),
                                                           4,
                                                           VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                           static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                                           device,
                                                           instance,
                                                           swapChain,
                                                           commandPool);
    }

    blurVarHorRenderTarget = new RenderTarget("Shadows Blur Var Hor Render Target",
                                              VK_FORMAT_R16G16B16A16_SFLOAT,
                                              swapChain.getSwapChainExtent(),
                                              4,
                                              VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                              static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                              device,
                                              instance,
                                              swapChain,
                                              commandPool);

    for(int i = 0; i < numReflectionAtrousPasses; i++) {
        reflectionsAtrousRenderTarget[i] = new RenderTarget("Reflections Atrous Render Target " + std::to_string(i),
                                                               VK_FORMAT_R16G16B16A16_SFLOAT,
                                                               swapChain.getSwapChainExtent(),
                                                               4,
                                                               VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                               static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                                               device,
                                                               instance,
                                                               swapChain,
                                                               commandPool);
    }

    
    temporalAARenderTarget = new RenderTarget("Temporal Antialiasing Render Target", 
                                              VK_FORMAT_R16G16B16A16_SFLOAT,
                                              swapChain.getSwapChainExtent(),
                                              4,
                                              VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                              static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                              device,
                                              instance,
                                              swapChain,
                                              commandPool);

    temporalAAPrevFrame = new TextureImage("Temporal Antialiasing Prev Frame", 
                                           device,
                                           commandPool,
                                           swapChain.getSwapChainExtent().width,
                                           swapChain.getSwapChainExtent().height,
                                           4,
                                           VK_FORMAT_R16G16B16A16_SFLOAT,
                                           static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT),
                                           1,
                                           true);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 temporalAAPrevFrame->textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// RAYTRACING STRUCTURES ///////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    shadowsParamsUniform = new UniformBuffer("Shadow Params", device, sizeof(ShadowParams::ToSubmit), swapChain.getSize());

    blueNoise2.resize(64);
    for (int i = 0; i < 64; i++) {
        blueNoise2[i] = new TextureImage("Blue Noise" + std::to_string(i), device, commandPool, std::string("../Textures/newnewBlueNoise/64_64/LDR_RG01_" + std::to_string(i) + ".png").data(), 1 ,VK_FORMAT_R8G8B8A8_UNORM);
        blueNoise2[i]->submitImageToStageMemory();
        blueNoise2[i]->submitImageToDeviceMemory();
    }

    shadowsRaytracingTarget = new RenderTarget("Shadows Raytracing Render Target",
                                        VK_FORMAT_R8G8B8A8_UNORM,
                                        swapChain.getSwapChainExtent(),
                                        1,
                                        VK_IMAGE_LAYOUT_GENERAL,
                                        static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                        device,
                                        instance,
                                        swapChain,
                                        commandPool);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                commandPool,
                                device.getQueues().graphicsQueue,
                                shadowsRaytracingTarget->getTextureImage().textureImages[0],
                                VK_IMAGE_LAYOUT_UNDEFINED,
                                VK_IMAGE_LAYOUT_GENERAL);

    raytracingShadowsAccummulation = new TextureImage("Shadows Raytracing Accumulation",
                                                       device,
                                                       commandPool,
                                                       swapChain.getSwapChainExtent().width,
                                                       swapChain.getSwapChainExtent().height,
                                                       4,
                                                       VK_FORMAT_R32G32B32A32_SFLOAT,
                                                       static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                                       1,
                                                       true);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 raytracingShadowsAccummulation->textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_GENERAL);

    descriptorSetRaytracing = new DescriptorSetRaytracing("Shadows Raytracing",
                                                          *shadowsParamsUniform,
                                                          {
                                                              {5, &worldSpaceNormalsRenderTarget->getTextureImage()},
                                                              {6, &worldSpacePositionRenderTarget->getTextureImage()}
                                                          },
                                                          {
                                                              {7, blueNoise2}
                                                          },
                                                          {
                                                              {8, &shadowsRaytracingTarget->getTextureImage()},
                                                              {9, raytracingShadowsAccummulation}
                                                          },
                                                          {},
                                                          scene,
                                                          *scene.topLevelAs,
                                                          device,
                                                          instance,
                                                          commandPool);

    shadowsRaytracingPipeline = new RaytracingPipeline("Shadows Raytracing",
                                                device,
                                                descriptorSetRaytracing->getDescriptorSetLayout(),
                                                { {0, "raygenGBuffer.rgen.spv"} }, 
                                                { {1, "shadow.rmiss.spv"} },
                                                {  });

    shadowsShaderBindingTable = new ShaderBindingTable(device, instance, commandPool, *shadowsRaytracingPipeline);

    //Reflections
    raytracingReflectionsRenderTarget = new RenderTarget("Reflections Raytracing Render Target", 
                                                         VK_FORMAT_R32G32B32A32_SFLOAT,
                                                         swapChain.getSwapChainExtent(),
                                                         1,
                                                         VK_IMAGE_LAYOUT_GENERAL,
                                                         static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                                         device,
                                                         instance,
                                                         swapChain,
                                                         commandPool);


    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                commandPool,
                                device.getQueues().graphicsQueue,
                                raytracingReflectionsRenderTarget->getTextureImage().textureImages[0],
                                VK_IMAGE_LAYOUT_UNDEFINED,
                                VK_IMAGE_LAYOUT_GENERAL);

    raytracingReflectionsRenderTargetAccummulation = new TextureImage("Reflections Accumulation Image",
                                                                      device,
                                                                      commandPool,
                                                                      swapChain.getSwapChainExtent().width,
                                                                      swapChain.getSwapChainExtent().height,
                                                                      4,
                                                                      VK_FORMAT_R32G32B32A32_SFLOAT,
                                                                      static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                                                      1,
                                                                      true);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 raytracingReflectionsRenderTargetAccummulation->textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_GENERAL);

    raytracingReflectionsHitPointsRenderTarget = new RenderTarget("Reflections Hit Point Render Target", 
                                                                  VK_FORMAT_R16G16B16A16_SFLOAT,
                                                                  swapChain.getSwapChainExtent(),
                                                                  1,
                                                                  VK_IMAGE_LAYOUT_GENERAL,
                                                                  static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                                                  device,
                                                                  instance,
                                                                  swapChain,
                                                                  commandPool);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 raytracingReflectionsHitPointsRenderTarget->getTextureImage().textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_GENERAL);

    raytracingReflectionsAlbedoRenderTarget = new RenderTarget("Reflections Albedo Render Target", 
                                                               VK_FORMAT_R16G16B16A16_SFLOAT,
                                                               swapChain.getSwapChainExtent(),
                                                               1,
                                                               VK_IMAGE_LAYOUT_GENERAL,
                                                               static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                                               device,
                                                               instance,
                                                               swapChain,
                                                               commandPool);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 raytracingReflectionsAlbedoRenderTarget->getTextureImage().textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_GENERAL);

    raytracingReflectionsFakeHitPositionRenderTarget = new RenderTarget("Reflections Fake Hit Point Render Target",
                                                                        VK_FORMAT_R16G16B16A16_SFLOAT,
                                                                        swapChain.getSwapChainExtent(),
                                                                        1,
                                                                        VK_IMAGE_LAYOUT_GENERAL,
                                                                        static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                                                        device,
                                                                        instance,
                                                                        swapChain,
                                                                        commandPool);

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 raytracingReflectionsFakeHitPositionRenderTarget->getTextureImage().textureImages[0],
                                 VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_GENERAL);

    reflectionsParamsUniform = new UniformBuffer("Reflections Params", device, sizeof(ReflectionsParams::ToSubmit), swapChain.getSize());

    descriptorSetReflectionsRaytracing = new DescriptorSetRaytracing("Reflections Raytracing",
                                                                     *reflectionsParamsUniform,
                                                                     {
                                                                        {6, &worldSpaceNormalsRenderTarget->getTextureImage()},
                                                                        {6, &normalsTexturelessRenderTarget->getTextureImage()},
                                                                        {7, &worldSpacePositionRenderTarget->getTextureImage()},
                                                                        {8, &metalRoughnessRenderTarget->getTextureImage()},
                                                                        {9, &albedoAoRenderTarget->getTextureImage()},
                                                                        {10, &brdfLutRenderTarget.getTextureImage()},
                                                                        {11, &textureAlbedoRenderTarget->getTextureImage()}
                                                                     },
                                                                     {
                                                                        {12, blueNoise2},
                                                                        {13, scene.textures}
                                                                     },
                                                                     { 
                                                                        {14, &raytracingReflectionsRenderTarget->getTextureImage()},
                                                                        {15, &raytracingReflectionsFakeHitPositionRenderTarget->getTextureImage()},
                                                                        {16, raytracingReflectionsRenderTargetAccummulation}
                                                                     },
                                                                     {
                                                                        {20, &scene.textureCubeMap},
                                                                        {21, &mipMapCubemap}
                                                                     },
                                                                     scene,
                                                                     *scene.topLevelAs,
                                                                     device,
                                                                     instance,
                                                                     commandPool);
    
    raytracingReflectionsPipeline = new RaytracingPipeline("Reflections Pipeline",
                                                           device,
                                                           descriptorSetReflectionsRaytracing->getDescriptorSetLayout(),
                                                           { {0, "raygenReflections.rgen.spv"} },
                                                           { {1, "missReflections.rmiss.spv"}, { 2, "shadow.rmiss.spv" } },
                                                           { {3, "closestHitReflections.rchit.spv"}, {4, "closestHitGetDistance.rchit.spv"} }
                                                           );

    shaderBindingTableReflections = new ShaderBindingTable(device, instance, commandPool, *raytracingReflectionsPipeline);
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// CREATE RENDERPASSES /////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // SPECIAL DESCRIPTIONS
    // store the depth information when finished
    VkAttachmentDescription descriptionFirstPass = depthRenderTarget.getAttachmentDescription();
    descriptionFirstPass.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

    // load the previous depth information at beginning
    VkAttachmentDescription descriptionSecondPass = depthRenderTarget.getAttachmentDescription();
    descriptionSecondPass.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
    descriptionSecondPass.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    descriptionSecondPass.initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    descriptionSecondPass.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    // Render passes
    gBufferPass = new Pass("G Buffer", 
                           { *worldSpacePositionRenderTarget,
                             * albedoAoRenderTarget,
                             * worldSpaceNormalsRenderTarget,
                             *metalRoughnessRenderTarget,
                             *normalsTexturelessRenderTarget, 
                             *velocityRenderTarget,
                             *linearDepthDerivativesRenderTarget,
                             *textureAlbedoRenderTarget 
                           }, 
                           { depthRenderTarget }, 
                           false, 
                           swapChain, 
                           device, 
                           { {depthRenderTarget, descriptionFirstPass} });

    renderPass = Pass("Render Pass", 
                      { colorRenderTarget, bloomRenderTarget }, 
                      std::vector<RenderTarget>(), 
                      false, 
                      swapChain, 
                      device);

    blurHorRenderPass = Pass("Blur Hor", 
                             { blurHorRenderTarget }, 
                             std::vector<RenderTarget>(),
                             false, 
                             swapChain, 
                             device);

    blurVerRenderPass = Pass("Blur Ver", 
                             { blurVerRenderTarget }, 
                             std::vector<RenderTarget>(), 
                             false, 
                             swapChain, 
                             device);

    skyboxRenderPass = new Pass("Skybox", 
                                { *skyboxRenderTarget }, 
                                { depthRenderTarget },
                                false, 
                                swapChain, 
                                device, 
                                { { depthRenderTarget, descriptionSecondPass } });

    blendBloomRenderPass = Pass("Blend Bloom", 
                                { *bloomBlendRenderTarget }, 
                                std::vector<RenderTarget>(),
                                false, 
                                swapChain, 
                                device);

    shadowsTemporalAccumulationPass = new Pass("Temporal Accumulation", 
                                        { *shadowsTemporalAccumulationRenderTarget,
                                          *shadowColorMomentsRenderTarget 
                                        }, 
                                        std::vector<RenderTarget>(), 
                                        false, 
                                        swapChain, 
                                        device);

    reflectionsTemporalAccumulationPass = new Pass("Reflections Temporal Accumulation", 
                                                   { *reflectionsTemporalRenderTarget, 
                                                     *reflectionsColorMoments}, 
                                                   std::vector<RenderTarget>(), 
                                                   false, 
                                                   swapChain, 
                                                   device);

    passThroughPass = new Pass("Pass Through", 
                               { *passThroughRenderTarget }, 
                               std::vector<RenderTarget>(), 
                               false, 
                               swapChain, 
                               device);

    blurVarHorPass = new Pass("Shadows Blur Var Hor", 
                              { *blurVarHorRenderTarget }, 
                              std::vector<RenderTarget>(), 
                              false, 
                              swapChain, 
                              device);

    for (int i = 0; i < numAtrousPasses; i++) {
        atrousPasses[i] = new Pass("Shadows Atrous", 
                                   { *atrousRenderTargets[i] }, 
                                   std::vector<RenderTarget>(), 
                                   false, 
                                   swapChain, 
                                   device);

        //we want to store every blur pass to debug
        blurVarVerPass[i] = new Pass("Shadows Blur Var Ver", 
                                     { *varianceBlurredRenderTargets[i] }, 
                                     std::vector<RenderTarget>(), 
                                     false, 
                                     swapChain, 
                                     device);
    }

    for(int i = 0; i < numReflectionAtrousPasses; i++) {
        reflectionsAtrousPass[i] = new Pass("Reflections Atrous", 
                                               { *reflectionsAtrousRenderTarget[i] }, 
                                               std::vector<RenderTarget>(), 
                                               false, 
                                               swapChain, 
                                               device);
    }

    temporalAAPass = new Pass("Reflections Temporal AA", 
                              { *temporalAARenderTarget }, 
                              std::vector<RenderTarget>(), 
                              false, 
                              swapChain, 
                              device);

    debugPass = new Pass("Debug Pass", 
                         std::vector<RenderTarget>(), 
                         std::vector<RenderTarget>(), 
                         true, 
                         swapChain, 
                         device);

    imGuiRenderPass = ImGuiRenderpass(device.getLogicalDeviceHandle(), swapChain);
    imGuiRenderPass.createRenderPasses(VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
    imGuiRenderPass.createFramebuffers();

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////// CREATE UI //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    createUI();

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////// CREATE DESCRIPTOR SETS, UNIFORM BUFFERS AND LAYOUTS ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    utils::QueueFamilyIndices queueFamilyIndices = utils::findQueueFamilies(instance.getPhysicalDevice(), instance.getSurface());

    //scene
    sceneUniform = UniformBuffer("Scene", device, sizeof(SceneParams));
    sceneDescriptorSet = DescriptorSet("Scene",
                                       { {0,&sceneUniform} },
                                       { {1,&brdfLutRenderTarget.getTextureImage()} },
                                       { {2,{scene.textureCubeMap}},  {3,{irradiance}}, {4,{mipMapCubemap}} },
                                       {},
                                       VK_SHADER_STAGE_FRAGMENT_BIT,
                                       device,
                                       instance,
                                       commandPool,
                                       this->swapChain.getSize());

    gBufferDescriptorSet = new DescriptorSet("G Buffer", 
                                            { },
                                            { {0, &worldSpacePositionRenderTarget->getTextureImage()},
                                              {1, &albedoAoRenderTarget->getTextureImage()},
                                              {2, &worldSpaceNormalsRenderTarget->getTextureImage()},
                                              {3, &metalRoughnessRenderTarget->getTextureImage()},
                                              {4, &atrousRenderTargets[numAtrousPasses-1]->getTextureImage()},
                                              {5, &reflectionsAtrousRenderTarget[numReflectionAtrousPasses - 1]->getTextureImage()},
                                              {6, &textureAlbedoRenderTarget->getTextureImage()},
                                              {7, &raytracingReflectionsHitPointsRenderTarget->getTextureImage()}
                                            },
                                            {},
                                            {},
                                            VK_SHADER_STAGE_FRAGMENT_BIT,
                                            device,
                                            instance,
                                            commandPool,
                                            this->swapChain.getSize());

    //scene descriptor layout
    std::vector<VkDescriptorSetLayout> sceneDescriptorLayouts = scene.descriptorLayouts;
    sceneDescriptorLayouts.push_back(imGuiUi.descriptorLayout);

    //G-Buffer descriptor layout 
    std::vector<VkDescriptorSetLayout> deferredDescriptorLayouts = std::vector<VkDescriptorSetLayout>();
    deferredDescriptorLayouts.push_back(gBufferDescriptorSet->getDescriptorLayout());
    deferredDescriptorLayouts.push_back(sceneDescriptorSet.getDescriptorLayout());
    deferredDescriptorLayouts.push_back(imGuiUi.descriptorLayout);

    //skybox descriptor layout
    std::vector<VkDescriptorSetLayout> skyboxDescriptorLayout = std::vector<VkDescriptorSetLayout>();
    skyboxDescriptorLayout.push_back(skyBox.descriptorSet.getDescriptorLayout());

    //Horizontal blur
    blurHorUniformBuffer = UniformBuffer("Blur Hor", device, sizeof(BlurParams::ToSubmit), swapChain.getSize() /* 1 buffer per swapChain */);
    blurHorDescriptorSet = DescriptorSet("Blur Hor", 
                                        { {0, &blurHorUniformBuffer} },
                                        { {1, &bloomRenderTarget.getTextureImage()} },
                                        {},
                                        {},
                                        VK_SHADER_STAGE_FRAGMENT_BIT,
                                        device,
                                        instance,
                                        commandPool,
                                        swapChain.getSize() // 1 buffer per swapChain
                                    );

    //blur vertical descriptor set
    blurVerUniformBuffer = UniformBuffer("Blur Ver", device, sizeof(BlurParams::ToSubmit), swapChain.getSize() /* 1 buffer per swapChain */);
    blurVerDescriptorSet = DescriptorSet("Blur Ver", 
                                        { {0, &blurVerUniformBuffer } },
                                        { {1, &blurHorRenderTarget.getTextureImage()} },
                                        {}, 
                                        {},
                                        VK_SHADER_STAGE_FRAGMENT_BIT,
                                        device,
                                        instance,
                                        commandPool,
                                        swapChain.getSize() // 1 buffer per swapChain
                                    );

    //blend bloom descriptor set
    BloomBlendDescriptorSet = DescriptorSet("Bloom Blend", 
                                            {},
                                            { {0, &colorRenderTarget.getTextureImage()}, {1, &blurVerRenderTarget.getTextureImage()}, {2, &skyboxRenderTarget->getTextureImage()} },
                                            {},
                                            {},
                                            VK_SHADER_STAGE_FRAGMENT_BIT,
                                            device,
                                            instance,
                                            commandPool,
                                            swapChain.getSize() // 1 buffer per swapChains
                                        );

    //blur descriptor layout
    std::vector<VkDescriptorSetLayout> postProcessingDescriptorLayouts(swapChain.getSize(), blurHorDescriptorSet.getDescriptorLayout());

    //bloom blend descriptor layout
    std::vector<VkDescriptorSetLayout> postProcessingBlendDescriptorLayouts(swapChain.getSize(), BloomBlendDescriptorSet.getDescriptorLayout());

    //temporal accumulation descriptor layout
    shadowsTemporalAccumulationUniformBuffer = new UniformBuffer("Shadows Temporal Accumulation", device, sizeof(shadowsTemporalAccumulationParams.toSubmit), swapChain.getSize());
    shadowsTemporalAccumulationDescriptorSet = new DescriptorSet("Shadows Temporal Accumulation",
                                                                 { {0, shadowsTemporalAccumulationUniformBuffer} },
                                                                 { {1, &shadowsRaytracingTarget->getTextureImage()},
                                                                   {2, &worldSpaceNormalsRenderTarget->getTextureImage()},
                                                                   {3, &linearDepthDerivativesRenderTarget->getTextureImage()},
                                                                   {4, prevFrame},
                                                                   {5, prevNormals} ,
                                                                   {6, prevDepth},
                                                                   {7, prevShadowsColorMoments},
                                                                   {8, &velocityRenderTarget->getTextureImage()}
                                                                 },
                                                                 {}, 
                                                                 {},
                                                                 VK_SHADER_STAGE_FRAGMENT_BIT,
                                                                 device,
                                                                 instance,
                                                                 commandPool,
                                                                 swapChain.getSize() // 1 buffer per swapChains
                                                             );
    std::vector<VkDescriptorSetLayout> temporalAccumulationDescriptorLayouts(swapChain.getSize(), shadowsTemporalAccumulationDescriptorSet->getDescriptorLayout());


    passThroughDescriptorSet = new DescriptorSet("Pass Through", {},
                                                { {0, &shadowsTemporalAccumulationRenderTarget->getTextureImage()} },
                                                {}, 
                                                {},
                                                VK_SHADER_STAGE_FRAGMENT_BIT,
                                                device,
                                                instance,
                                                commandPool,
                                                swapChain.getSize() // 1 buffer per swapChains
                                            );
    std::vector<VkDescriptorSetLayout> passThroughDescriptorLayouts(swapChain.getSize(), passThroughDescriptorSet->getDescriptorLayout());

    //A Trous
    for (int i = 0; i < numAtrousPasses; i++) {
        atrousUniformBuffers[i] = new UniformBuffer("Shadow Atrous", device, sizeof(atrousParams.toSubmit), swapChain.getSize());
        TextureImage prevRenderTarget;
        TextureImage prevVariance = varianceBlurredRenderTargets[i]->getTextureImage();
        if (i == 0) {
            prevRenderTarget = passThroughRenderTarget->getTextureImage();
            prevVariance = passThroughRenderTarget->getTextureImage();
        }
        else {
            prevRenderTarget = atrousRenderTargets[i - 1]->getTextureImage();
        }

        atrousDescriptorSets[i] = new DescriptorSet("Shadow Atrous", 
                                                    { {0, atrousUniformBuffers[i]} },
                                                    {
                                                        {1, &prevRenderTarget},
                                                        {2, &worldSpaceNormalsRenderTarget->getTextureImage()},
                                                        {3, &worldSpacePositionRenderTarget->getTextureImage()},
                                                        {4, &shadowColorMomentsRenderTarget->getTextureImage()},
                                                        {5, &linearDepthDerivativesRenderTarget->getTextureImage()},
                                                        {6, &prevVariance}
                                                    },
                                                    {}, 
                                                    {},
                                                    VK_SHADER_STAGE_FRAGMENT_BIT,
                                                    device,
                                                    instance,
                                                    commandPool,
                                                    swapChain.getSize() // 1 buffer per swapChains
                                                );
    }


    for (int i = 0; i < numAtrousPasses; i++) {
        blurVarHorUniformBuffer[i] = new UniformBuffer("Blur Variance Hor Shadows", device, sizeof(blurVarParams.toSubmit), swapChain.getSize());
        TextureImage tex;
        if (i == 0) {
            tex = shadowsTemporalAccumulationRenderTarget->getTextureImage();
        }
        else {
            tex = atrousRenderTargets[i - 1]->getTextureImage();
        }

        blurVarHorDescriptorSet[i] = new DescriptorSet("Blur Variance Hor Shadows", 
                                                        { {0, *blurVarHorUniformBuffer} },
                                                        {
                                                            {1, &tex}
                                                        },
                                                        {}, 
                                                        {},
                                                        VK_SHADER_STAGE_FRAGMENT_BIT,
                                                        device,
                                                        instance,
                                                        commandPool,
                                                        swapChain.getSize() // 1 buffer per swapChains
                                                    );
    }

    blurVarVerUniformBuffer = new UniformBuffer("Blur Variance Ver Shadows", device, sizeof(blurVarParams.toSubmit), swapChain.getSize());
    blurVarVerDescriptorSet = new DescriptorSet("Blur Variance Ver Shadows", 
                                                { {0, *blurVarHorUniformBuffer} },
                                                {
                                                    {1, &blurVarHorRenderTarget->getTextureImage()}
                                                },
                                                {}, 
                                                {},
                                                VK_SHADER_STAGE_FRAGMENT_BIT,
                                                device,
                                                instance,
                                                commandPool,
                                                swapChain.getSize() // 1 buffer per swapChains
                                                );
    
    for(int i = 0; i < numReflectionAtrousPasses; i++) {
        TextureImage* target;
        if(i == 0)
            target = &reflectionsTemporalRenderTarget->getTextureImage();
        else
            target = &reflectionsAtrousRenderTarget[i-1]->getTextureImage();

        reflectionsAtrousUniformBuffer[i] = new UniformBuffer("Reflections A-trous " + std::to_string(i), device, sizeof(reflectionsAtrousParams.toSubmit), swapChain.getSize());
        reflectionsAtrousDescriptorSet[i] = new DescriptorSet("Reflections A-trous " + std::to_string(i),
                                                                 { {0, reflectionsAtrousUniformBuffer[i]} },
                                                                 {
                                                                    {1, target},
                                                                    {2, &worldSpaceNormalsRenderTarget->getTextureImage()},
                                                                    {3, &metalRoughnessRenderTarget->getTextureImage()},
                                                                    {4, &linearDepthDerivativesRenderTarget->getTextureImage()},
                                                                 },
                                                                 {},
                                                                 {},
                                                                 VK_SHADER_STAGE_FRAGMENT_BIT,
                                                                 device,
                                                                 instance,
                                                                 commandPool,
                                                                 swapChain.getSize() // 1 buffer per swapChains
                                                                 );
    }

    //reflections temporal accumulation descriptor layout
    reflectionsTemporalAccumulationUniformBuffer = new UniformBuffer("Reflections Temporal Accumulation", device, sizeof(reflectionsTemporalAccumulationParams.toSubmit), swapChain.getSize());
    reflectionsTemporalAccumulationDescriptorSet = new DescriptorSet("Reflections Temporal Accumulation", 
                                                                    { {0, reflectionsTemporalAccumulationUniformBuffer} },
                                                                    { {1, &raytracingReflectionsRenderTarget->getTextureImage()},
                                                                       {2, &worldSpaceNormalsRenderTarget->getTextureImage()},
                                                                       {3, &linearDepthDerivativesRenderTarget->getTextureImage()},
                                                                       {4, reflectionsPrevFrame},
                                                                       {5, prevNormals} ,
                                                                       {6, prevDepth},
                                                                       {7, reflectionsPrevColorMoments},
                                                                       {8, &velocityRenderTarget->getTextureImage()},
                                                                       {9, &raytracingReflectionsFakeHitPositionRenderTarget->getTextureImage()},
                                                                       {10, &metalRoughnessRenderTarget->getTextureImage()},
                                                                     },
                                                                     {}, 
                                                                     {},
                                                                     VK_SHADER_STAGE_FRAGMENT_BIT,
                                                                     device,
                                                                     instance,
                                                                     commandPool,
                                                                     swapChain.getSize() // 1 buffer per swapChains
                                                                    );
    std::vector<VkDescriptorSetLayout> reflectionsTemporalAccumulationDescriptorLayouts(swapChain.getSize(), reflectionsTemporalAccumulationDescriptorSet->getDescriptorLayout());

    temporalAAUniformBuffer = new UniformBuffer("Temporal Antialiasing", device, sizeof(shadowsTemporalAccumulationParams.toSubmit), swapChain.getSize());
    temporalAADescriptorSet = new DescriptorSet("Temporal Antialiasing", 
                                                { {0, temporalAAUniformBuffer} },
                                                {
                                                    {1, &bloomBlendRenderTarget->getTextureImage()},
                                                    {2, &velocityRenderTarget->getTextureImage()},
                                                    {3, temporalAAPrevFrame}
                                                
                                                },
                                                {}, 
                                                {},
                                                VK_SHADER_STAGE_FRAGMENT_BIT,
                                                device,
                                                instance,
                                                commandPool,
                                                swapChain.getSize() // 1 buffer per swapChains
                                                );

    std::vector<TextureImage*> atrousTextureImages;
    for (int i = 0; i < numAtrousPasses; i++) {
        atrousTextureImages.push_back(&atrousRenderTargets[i]->getTextureImage());
    }

    std::vector<TextureImage*> atrousVarianceTextureImages;
    for (int i = 0; i < numAtrousPasses; i++) {
        atrousVarianceTextureImages.push_back(&varianceBlurredRenderTargets[i]->getTextureImage());
    }

    std::vector<TextureImage*> reflectionsAtrousTextureImages;
    for (int i = 0; i < numReflectionAtrousPasses; i++) {
        reflectionsAtrousTextureImages.push_back(&reflectionsAtrousRenderTarget[i]->getTextureImage());
    }

    debugUniformBuffer = new UniformBuffer("Debug", device, sizeof(debugParams.toSubmit), swapChain.getSize());
    debugDescriptorSet = new DescriptorSet("Debug", 
                                           { {0, debugUniformBuffer} },
                                           {
                                             { 1, &temporalAARenderTarget->getTextureImage() },
                                             { 3, &atrousRenderTargets[numAtrousPasses-1]->getTextureImage() },
                                             { 4, &shadowColorMomentsRenderTarget->getTextureImage() },

                                             { 20, &raytracingReflectionsRenderTarget->getTextureImage() },
                                             { 21, &reflectionsTemporalRenderTarget->getTextureImage() },
                                             { 22, &reflectionsColorMoments->getTextureImage() },
                                             { 24, &raytracingReflectionsHitPointsRenderTarget->getTextureImage() }, 
                                             { 25, &raytracingReflectionsRenderTarget->getTextureImage() },
                                             { 27, &reflectionsAtrousRenderTarget[numReflectionAtrousPasses-1]->getTextureImage() },
                                             { 29, &reflectionsFakeHitPositionRenderTarget->getTextureImage() },
                                             { 30, &reflectionsAtrousRenderTarget[numReflectionAtrousPasses-1]->getTextureImage() },
                                             { 31, &metalRoughnessRenderTarget->getTextureImage() },
                                             { 32, &worldSpaceNormalsRenderTarget->getTextureImage() },
                                             { 33, &albedoAoRenderTarget->getTextureImage() },
                                             { 34, &textureAlbedoRenderTarget->getTextureImage() },
                                             { 35, &linearDepthDerivativesRenderTarget->getTextureImage() },
                                             { 36, &velocityRenderTarget->getTextureImage() },
                                             { 37, &worldSpacePositionRenderTarget->getTextureImage() }
                                           },
                                           {},
                                           {
                                               {5, atrousTextureImages },
                                               {15, atrousVarianceTextureImages },
                                               {38, reflectionsAtrousTextureImages }
                                           },
                                           VK_SHADER_STAGE_FRAGMENT_BIT,
                                           device,
                                           instance,
                                           commandPool,
                                           swapChain.getSize() // 1 buffer per swapChains
                                        );

    std::vector<VkDescriptorSetLayout> passThroughDescriptorLayouts2(swapChain.getSize(), debugDescriptorSet->getDescriptorLayout());

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////// CREATE PIPELINES //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //definition for a color attachment
    VkPipelineColorBlendAttachmentState colorBlendAttachment = initializers::pipelineColorBlendAttachmentState();

    gBufferPipeline = new VertexFragPipeline("G Buffer",
                                             "gBufferCalculation.vert.spv",
                                             "gBufferCalculation.frag.spv",
                                             device,
                                             swapChain,
                                             *gBufferPass,
                                             { colorBlendAttachment, 
                                               colorBlendAttachment, 
                                               colorBlendAttachment, 
                                               colorBlendAttachment, 
                                               colorBlendAttachment, 
                                               colorBlendAttachment, 
                                               colorBlendAttachment, 
                                               colorBlendAttachment },
                                             sceneDescriptorLayouts,
                                             utils::Vertex::getBindingDescription(),
                                             utils::Vertex::getAttributeDescriptions(),
                                             {},
                                             VK_CULL_MODE_BACK_BIT);

    pipeline = VertexFragPipeline("Pipeline", 
                                  "blur.vert.spv",
                                  "deferredShadowsPBR.frag.spv",
                                  device,
                                  swapChain,
                                  renderPass,
                                  { colorBlendAttachment, 
                                    colorBlendAttachment },
                                  deferredDescriptorLayouts,
                                  std::vector<VkVertexInputBindingDescription>(),
                                  std::vector<VkVertexInputAttributeDescription>(),
                                  {},
                                  VK_CULL_MODE_NONE);

    skyboxPipeline = VertexFragPipeline("Skybox", 
                                        "skybox.vert.spv",
                                        "skybox.frag.spv",
                                        device,
                                        swapChain,
                                        *skyboxRenderPass,
                                        { colorBlendAttachment },
                                        skyboxDescriptorLayout,
                                        utils::Vertex::getBindingDescription(),
                                        utils::Vertex::getAttributeDescriptions(),
                                        {},
                                        VK_CULL_MODE_FRONT_BIT,
                                        VK_COMPARE_OP_LESS_OR_EQUAL);

    postProcessingHorPipeline = VertexFragPipeline("Post Processing Hor", 
                                                   "blur.vert.spv",
                                                   "blur.frag.spv",
                                                   device,
                                                   swapChain,
                                                   blurHorRenderPass,
                                                   { colorBlendAttachment },
                                                   postProcessingDescriptorLayouts,
                                                   std::vector<VkVertexInputBindingDescription>(),
                                                   std::vector<VkVertexInputAttributeDescription>(),
                                                   {},
                                                   VK_CULL_MODE_NONE);

    postProcessingVerPipeline = VertexFragPipeline("Post Processing Ver", 
                                                   "blur.vert.spv",
                                                   "blur.frag.spv",
                                                   device,
                                                   swapChain,
                                                   blurVerRenderPass,
                                                   { colorBlendAttachment },
                                                   postProcessingDescriptorLayouts,
                                                   std::vector<VkVertexInputBindingDescription>(),
                                                   std::vector<VkVertexInputAttributeDescription>(),
                                                   {},
                                                   VK_CULL_MODE_NONE);

    postProcessingBlendBloomPipeline = VertexFragPipeline("Post Processing Blend Bloom", 
                                                          "blur.vert.spv",
                                                          "blendBloomSkybox.frag.spv",
                                                          device,
                                                          swapChain,
                                                          blendBloomRenderPass,
                                                          { colorBlendAttachment },
                                                          postProcessingBlendDescriptorLayouts,
                                                          std::vector<VkVertexInputBindingDescription>(),
                                                          std::vector<VkVertexInputAttributeDescription>(),
                                                          {},
                                                          VK_CULL_MODE_NONE);

    shadowsTemporalAccumulationPipeline = new VertexFragPipeline("Temporal Accumulation",
                                                                  "blur.vert.spv",
                                                                  "temporalAccumulationShadows.frag.spv",
                                                                  device,
                                                                  swapChain,
                                                                  *shadowsTemporalAccumulationPass,
                                                                  { colorBlendAttachment, 
                                                                    colorBlendAttachment },
                                                                  temporalAccumulationDescriptorLayouts,
                                                                  std::vector<VkVertexInputBindingDescription>(),
                                                                  std::vector<VkVertexInputAttributeDescription>(),
                                                                  {},
                                                                  VK_CULL_MODE_NONE);

    passThroughPipeline = new VertexFragPipeline("Pass Through", 
                                                 "blur.vert.spv",
                                                 "passThrough.frag.spv",
                                                 device,
                                                 swapChain,
                                                 *passThroughPass,
                                                 { colorBlendAttachment },
                                                 passThroughDescriptorLayouts,
                                                 std::vector<VkVertexInputBindingDescription>(),
                                                 std::vector<VkVertexInputAttributeDescription>(),
                                                 {},
                                                 VK_CULL_MODE_NONE);

    for (int i = 0; i < numAtrousPasses; i++) {

        std::vector<VkDescriptorSetLayout> atrousDescriptorLayouts(swapChain.getSize(), atrousDescriptorSets[i]->getDescriptorLayout());

        atrousPipelines[i] = new VertexFragPipeline("Atrous " + std::to_string(i), 
                                                    "blur.vert.spv",
                                                    "atrous.frag.spv",
                                                    device,
                                                    swapChain,
                                                    *atrousPasses[i],
                                                    { colorBlendAttachment },
                                                    atrousDescriptorLayouts,
                                                    std::vector<VkVertexInputBindingDescription>(),
                                                    std::vector<VkVertexInputAttributeDescription>(),
                                                    { },
                                                    VK_CULL_MODE_NONE);
    }

    VkPushConstantRange pushConstantRangeBlurVar{};
    pushConstantRangeBlurVar.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    pushConstantRangeBlurVar.offset = 0;
    pushConstantRangeBlurVar.size = sizeof(BlurVarParams::PushConstant);

    std::vector<VkDescriptorSetLayout> blurVarHorDescriptorLayouts(swapChain.getSize(), blurVarHorDescriptorSet[0]->getDescriptorLayout());

    blurVarHorPipeline = new VertexFragPipeline("Blur Var Hor", 
                                                "blur.vert.spv",
                                                "blurVariance.frag.spv",
                                                device,
                                                swapChain,
                                                *blurVarHorPass,
                                                { colorBlendAttachment },
                                                blurVarHorDescriptorLayouts,
                                                std::vector<VkVertexInputBindingDescription>(),
                                                std::vector<VkVertexInputAttributeDescription>(),
                                                { pushConstantRangeBlurVar },
                                                VK_CULL_MODE_NONE);

    std::vector<VkDescriptorSetLayout> blurVarVerDescriptorLayouts(swapChain.getSize(), blurVarVerDescriptorSet->getDescriptorLayout());

    for (int i = 0; i < numAtrousPasses; i++) {
        blurVarVerPipeline[i] = new VertexFragPipeline("Blur Var Ver", 
                                                       "blur.vert.spv",
                                                       "blurVariance.frag.spv",
                                                       device,
                                                       swapChain,
                                                       *blurVarVerPass[i],
                                                       { colorBlendAttachment },
                                                       blurVarVerDescriptorLayouts,
                                                       std::vector<VkVertexInputBindingDescription>(),
                                                       std::vector<VkVertexInputAttributeDescription>(),
                                                       { pushConstantRangeBlurVar },
                                                       VK_CULL_MODE_NONE);
    }

    for(int i = 0; i < numReflectionAtrousPasses; i++) {
        std::vector<VkDescriptorSetLayout> AtrousDescriptorLayouts(swapChain.getSize(), reflectionsAtrousDescriptorSet[i]->getDescriptorLayout());
        reflectionsAtrousPipeline[i] = new  VertexFragPipeline("Reflections Atrous " + std::to_string(i), 
                                                                  "blur.vert.spv",
                                                                  "atrousReflections.frag.spv",
                                                                  device,
                                                                  swapChain,
                                                                  *reflectionsAtrousPass[i],
                                                                  { colorBlendAttachment },
                                                                  AtrousDescriptorLayouts,
                                                                  std::vector<VkVertexInputBindingDescription>(),
                                                                  std::vector<VkVertexInputAttributeDescription>(),
                                                                  {},
                                                                  VK_CULL_MODE_NONE);
    }

    reflectionsTemporalAccumulationPipeline = new VertexFragPipeline("Reflections Temporal Accumulation", 
                                                                     "blur.vert.spv",
                                                                     "temporalAccumulationReflections.frag.spv",
                                                                     device,
                                                                     swapChain,
                                                                     *reflectionsTemporalAccumulationPass,
                                                                     { colorBlendAttachment,
                                                                       colorBlendAttachment },
                                                                     reflectionsTemporalAccumulationDescriptorLayouts,
                                                                     std::vector<VkVertexInputBindingDescription>(),
                                                                     std::vector<VkVertexInputAttributeDescription>(),
                                                                     {},
                                                                     VK_CULL_MODE_NONE);

    std::vector<VkDescriptorSetLayout> temporalAADescriptorLayouts(swapChain.getSize(), temporalAADescriptorSet->getDescriptorLayout());
    temporalAAPipeline = new  VertexFragPipeline("Temporal Antialiasing", 
                                                 "blur.vert.spv",
                                                 "temporalAntialiasing3.frag.spv",
                                                 device,
                                                 swapChain,
                                                 *temporalAAPass,
                                                 { colorBlendAttachment },
                                                 temporalAADescriptorLayouts,
                                                 std::vector<VkVertexInputBindingDescription>(),
                                                 std::vector<VkVertexInputAttributeDescription>(),
                                                 {},
                                                 VK_CULL_MODE_NONE);

    debugPipeline = new VertexFragPipeline("Debug", 
                                           "blur.vert.spv",
                                           "debugReflections.frag.spv",
                                           device,
                                           swapChain,
                                           *debugPass,
                                           { colorBlendAttachment },
                                           passThroughDescriptorLayouts2,
                                           std::vector<VkVertexInputBindingDescription>(),
                                           std::vector<VkVertexInputAttributeDescription>(),
                                           {},
                                           VK_CULL_MODE_NONE);
}

void DeferredRendererReflections::cleanupSwapChain()
{

    for (TextureImage* tex : blueNoise2) {
        tex->cleanup();
        delete tex;
    }
    //temporal accumulation reflections
    reflectionsPrevFrame->cleanup();
    delete reflectionsPrevFrame;
    reflectionsPrevColorMoments->cleanup();
    delete reflectionsPrevColorMoments;
    reflectionsTemporalRenderTarget->cleanup();
    delete reflectionsTemporalRenderTarget;
    reflectionsColorMoments->cleanup();
    delete reflectionsColorMoments;
    reflectionsFakeHitPositionRenderTarget->cleanup();
    delete reflectionsFakeHitPositionRenderTarget;
    reflectionsTemporalAccumulationDescriptorSet->cleanup();
    delete reflectionsTemporalAccumulationDescriptorSet;
    reflectionsTemporalAccumulationPass->cleanup();
    delete reflectionsTemporalAccumulationPass;
    reflectionsTemporalAccumulationUniformBuffer->cleanup();
    delete reflectionsTemporalAccumulationUniformBuffer;
    reflectionsTemporalAccumulationPipeline->cleanup();
    delete reflectionsTemporalAccumulationPipeline;
    tangentsRenderTarget->cleanup();
    delete tangentsRenderTarget;

    velocityRenderTarget->cleanup();
    delete velocityRenderTarget;
    linearDepthDerivativesRenderTarget->cleanup();
    delete linearDepthDerivativesRenderTarget;

    for(int i = 0; i < numReflectionAtrousPasses; i++) {
        reflectionsAtrousRenderTarget[i]->cleanup();
        reflectionsAtrousPass[i]->cleanup();
        reflectionsAtrousDescriptorSet[i]->cleanup();
        reflectionsAtrousUniformBuffer[i]->cleanup();
        reflectionsAtrousPipeline[i]->cleanup();
    }

    for (int i = 0; i < numAtrousPasses; i++) {
        varianceBlurredRenderTargets[i]->cleanup();
        blurVarVerPass[i]->cleanup();
        blurVarHorDescriptorSet[i]->cleanup();
        blurVarHorUniformBuffer[i]->cleanup();
        blurVarVerPipeline[i]->cleanup();

        delete blurVarVerPass[i];
        delete blurVarHorDescriptorSet[i];
        delete blurVarHorUniformBuffer[i];
        delete blurVarVerPipeline[i];
    }

    blurVarHorRenderTarget->cleanup();
    blurVarHorPass->cleanup();
    blurVarVerDescriptorSet->cleanup();
    blurVarVerUniformBuffer->cleanup();
    blurVarHorPipeline->cleanup();

    delete blurVarHorRenderTarget;
    delete blurVarHorPass;
    delete blurVarVerDescriptorSet;
    delete blurVarVerUniformBuffer;
    delete blurVarHorPipeline;

    for (int i = 0; i < numAtrousPasses; i++) {
        atrousRenderTargets[i]->cleanup();
        delete atrousRenderTargets[i];
        atrousPasses[i]->cleanup();
        delete atrousPasses[i];
        atrousDescriptorSets[i]->cleanup();
        delete atrousDescriptorSets[i];
        atrousUniformBuffers[i]->cleanup();
        delete atrousUniformBuffers[i];
        atrousPipelines[i]->cleanup();
        delete atrousPipelines[i];
    }

    temporalAAPrevFrame->cleanup();
    delete temporalAAPrevFrame;
    temporalAARenderTarget->cleanup();
    delete temporalAARenderTarget;
    temporalAAUniformBuffer->cleanup();
    delete temporalAAUniformBuffer;
    temporalAADescriptorSet->cleanup();
    delete temporalAADescriptorSet;
    temporalAAPass->cleanup();
    delete temporalAAPass;
    temporalAAPipeline->cleanup();
    delete temporalAAPipeline;

    //cleanup raytracing
    shadowsRaytracingPipeline->cleanup();
    delete shadowsRaytracingPipeline;
    shadowsParamsUniform->cleanup();
    delete shadowsParamsUniform;
    shadowsRaytracingTarget->cleanup();
    delete shadowsRaytracingTarget;
    raytracingShadowsAccummulation->cleanup();
    delete raytracingShadowsAccummulation;
    descriptorSetRaytracing->cleanup();
    delete descriptorSetRaytracing;
    shadowsShaderBindingTable->cleanup();
    delete shadowsShaderBindingTable;

    raytracingReflectionsPipeline->cleanup();
    delete raytracingReflectionsPipeline;
    reflectionsParamsUniform->cleanup();
    delete reflectionsParamsUniform;
    raytracingReflectionsRenderTarget->cleanup();
    delete raytracingReflectionsRenderTarget;
    raytracingReflectionsRenderTargetAccummulation->cleanup();
    delete raytracingReflectionsRenderTargetAccummulation;
    descriptorSetReflectionsRaytracing->cleanup();
    delete descriptorSetReflectionsRaytracing;
    shaderBindingTableReflections->cleanup();
    delete shaderBindingTableReflections;
    raytracingReflectionsHitPointsRenderTarget->cleanup();
    delete raytracingReflectionsHitPointsRenderTarget;
    raytracingReflectionsAlbedoRenderTarget->cleanup();
    delete raytracingReflectionsAlbedoRenderTarget;
    raytracingReflectionsFakeHitPositionRenderTarget->cleanup();
    delete raytracingReflectionsFakeHitPositionRenderTarget;

    passThroughPass->cleanup();
    delete passThroughPass;
    passThroughDescriptorSet->cleanup();
    delete passThroughDescriptorSet;
    passThroughPipeline->cleanup();
    delete passThroughPipeline;
    passThroughRenderTarget->cleanup();
    delete passThroughRenderTarget;
    debugPass->cleanup();
    delete debugPass;
    debugUniformBuffer->cleanup();
    delete debugUniformBuffer;
    debugDescriptorSet->cleanup();
    delete debugDescriptorSet;
    debugPipeline->cleanup();
    delete debugPipeline;

    gBufferDescriptorSet->cleanup();
    delete gBufferDescriptorSet;
    shadowsTemporalAccumulationRenderTarget->cleanup();
    delete shadowsTemporalAccumulationRenderTarget;
    shadowsTemporalAccumulationDescriptorSet->cleanup();
    delete shadowsTemporalAccumulationDescriptorSet;
    shadowsTemporalAccumulationPipeline->cleanup();
    delete shadowsTemporalAccumulationPipeline;
    shadowsTemporalAccumulationUniformBuffer->cleanup();
    delete shadowsTemporalAccumulationUniformBuffer;
    gBufferPipeline->cleanup();
    delete gBufferPipeline;
    gBufferPass->cleanup();
    delete gBufferPass;
    skyboxRenderPass->cleanup();
    delete skyboxRenderPass;
    skyboxRenderTarget->cleanup();
    delete skyboxRenderTarget;
    shadowsTemporalAccumulationPass->cleanup();
    delete shadowsTemporalAccumulationPass;
    worldSpacePositionRenderTarget->cleanup();
    delete worldSpacePositionRenderTarget;
    albedoAoRenderTarget->cleanup();
    delete albedoAoRenderTarget;
    textureAlbedoRenderTarget->cleanup();
    delete textureAlbedoRenderTarget;
    worldSpaceNormalsRenderTarget->cleanup();
    delete worldSpaceNormalsRenderTarget;
    shadowColorMomentsRenderTarget->cleanup();
    delete shadowColorMomentsRenderTarget;
    /*
    aoRenderTarget->cleanup();
    delete aoRenderTarget;
    */
    bloomBlendRenderTarget->cleanup();
    delete bloomBlendRenderTarget;
    metalRoughnessRenderTarget->cleanup();
    delete metalRoughnessRenderTarget;
    normalsTexturelessRenderTarget->cleanup();
    delete normalsTexturelessRenderTarget;
    prevShadowsColorMoments->cleanup();
    delete prevShadowsColorMoments;
    prevDepth->cleanup();
    delete prevDepth;
    prevFrame->cleanup();
    delete prevFrame;
    prevNormals->cleanup();
    delete prevNormals;
    EngineBase::cleanupSwapChain();
}