#pragma once
#include <vector>
#include "SwapChain.h"
#include "config.hpp"
#include "TextureImage.h"
#include "VulkanDevice.h"
#include "VulkanInstance.h"
#include "RenderTarget.h"
#include <unordered_map>

class Pass {
private:
	std::vector<RenderTarget> resourcesColor;
	std::vector<RenderTarget> resourcesDepth;

	std::unordered_map<RenderTarget, VkAttachmentDescription> specialDescriptions;
	VulkanDevice device;
	SwapChain swapChain;
	bool renderToSwapChain;

	std::vector<VkFramebuffer> framebuffers;
	VkRenderPass renderPassHandle;
	std::string name;

public:
	Pass();
	//create the final pass to the swapChain
	Pass(std::string name, 
		 std::vector<RenderTarget>& resourcesColor, 
		 std::vector<RenderTarget>& resourcesDepth,
		 bool renderToSwapChain, 
		 SwapChain& swapChain,
		 VulkanDevice& device, 
		 std::unordered_map<RenderTarget, VkAttachmentDescription>&& specialDescriptions = std::unordered_map<RenderTarget, VkAttachmentDescription>());

	Pass(std::string name, 
		 std::vector<RenderTarget>&& resourcesColor,
		 std::vector<RenderTarget>&& resourcesDepth, 
		 bool renderToSwapChain, 
		 SwapChain& swapChain,
		 VulkanDevice& device,
		 std::unordered_map<RenderTarget, VkAttachmentDescription>&& specialDescriptions = std::unordered_map<RenderTarget, VkAttachmentDescription>());

	VkRenderPass& getRenderPassHandle();
	std::vector<VkFramebuffer>& getFramebuffers();
	void cleanup();

private:
	void createRenderPass();
	void createFrameBuffer();
};