#pragma once
#include "GLFW/glfw3.h"

class Keyboard
{
public:
	bool keys[1024];
	bool keysUp[1024];

	Keyboard() {
		for (int i = 0; i < 1024; i++) {
			keys[i] = false;
			keysUp[i] = false;
		}
	}

	void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
		if (keys[key] == GLFW_PRESS && action == GLFW_RELEASE) {
			keysUp[key] = true;
		}
		
		keys[key] = action;
	}

	bool keyPressed(int key) {
		return keys[key];
	}

	bool keyUp(int key) {
		bool res = keysUp[key];
		keysUp[key] = false;
		return res;
	}
};