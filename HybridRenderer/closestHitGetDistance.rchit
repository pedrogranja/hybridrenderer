#version 460
#extension GL_EXT_ray_tracing : require
#extension GL_EXT_nonuniform_qualifier : enable

struct RayPayload {
	vec3 color;
	float dist;
	int pathLength;
};

layout(location = 0) rayPayloadInEXT RayPayload rayPayload;
layout(location = 1) rayPayloadEXT float prevRoughness;
layout(location = 2) rayPayloadEXT bool shadowed;

void main()
{	
	rayPayload.color = vec3(1.0f);
	rayPayload.dist = gl_RayTmaxEXT;
}