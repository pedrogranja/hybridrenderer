#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
    vec3 cameraPos;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inUv;

layout(location = 0) out vec4 fragPos;
layout(location = 1) out vec3 fragColor;
layout(location = 2) out vec3 fragNormal;
layout(location = 3) out vec2 fragTexCoord;

void main() {

    fragPos = ubo.model * vec4(inPosition.xyz, 1.0);
    fragPos /= fragPos.w;
    gl_Position = ubo.proj * ubo.view * fragPos;
    fragColor = inColor;
    fragTexCoord = inUv;
    fragNormal = mat3(transpose(inverse(ubo.model))) * inNormal;
}