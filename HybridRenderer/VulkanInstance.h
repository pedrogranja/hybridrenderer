#pragma once
#include <vulkan/vulkan_core.h>
#include "Window.h"
#include <string>
#include <algorithm>
#include "config.hpp"
#include "utils.hpp"


class VulkanInstance
{
private:
	Window window;
	VkInstance instanceHandle;
	VkSurfaceKHR surface;
	VkPhysicalDevice physicalDevice;
	VkDebugUtilsMessengerEXT debugMessenger;
	std::string appName;

public:
	VulkanInstance();
	VulkanInstance(std::string appName, Window& window);
	void cleanup();
	VkInstance& getInstanceHandle();
	VkSurfaceKHR& getSurface();
	VkPhysicalDevice& getPhysicalDevice();
	PFN_vkSetDebugUtilsObjectNameEXT setDebugUtilsObjectNameEXT;

private:
	void createInstance();
	void createSurface();
	void createPhysicalDevice();
	void setupDebugMessenger();

	bool isDeviceSuitable(VkPhysicalDevice physicalDevice);
	bool checkValidationLayerSupport();
	bool checkDeviceExtensionSupport(VkPhysicalDevice physicalDevice);
	std::vector<const char*> getRequiredExtensions();
	void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
														VkDebugUtilsMessageTypeFlagsEXT messageType,
														const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
														void* pUserData);
	VkResult CreateDebugUtilsMessengerEXT(VkInstance instance,
										  const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
										  const VkAllocationCallbacks* pAllocator,
										  VkDebugUtilsMessengerEXT* pDebugMessenger);
	void DestroyDebugUtilsMessengerEXT(VkInstance instance,
									   VkDebugUtilsMessengerEXT debugMessenger,
									   const VkAllocationCallbacks* pAllocator);
};