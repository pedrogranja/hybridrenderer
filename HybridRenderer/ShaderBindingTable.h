#pragma once
#include "RaytracingPipeline.h"
#include "Buffer.h"

class ShaderBindingTable
{
	VulkanDevice device;
	VulkanInstance instance;
	VkCommandPool commandPool;
	RaytracingPipeline rayTracingPipeline;
	
	Buffer sbtBuffer;
	uint32_t sbtSize;
	size_t entrySize;

public:
	ShaderBindingTable();
	ShaderBindingTable(VulkanDevice& device, VulkanInstance& instance, VkCommandPool commandPool, RaytracingPipeline& rayTracingPipeline);
	VkBuffer getSbtBuffer();
	VkDeviceAddress getSbtBufferAddress();
	uint32_t getSbtSize();
	size_t getEntrySize();


	//size_t GetEntrySize(const RayTracingProperties& rayTracingProperties, const std::vector<ShaderBindingTable::Entry>& entries);

	void cleanup();

private:
	size_t roundUp(size_t size, size_t powerOf2Alignment);
	void createShaderBindingTable();
};