#include "RenderTarget.h"

static int idGenerator = 0;

RenderTarget::RenderTarget()
{
}

RenderTarget::RenderTarget(std::string name, 
                           VkFormat format,
                           VkExtent2D extent,
                           int channelNumber,
                           VkImageLayout finalLayout,
                           VkImageUsageFlagBits usage,
                           VulkanDevice& device,
                           VulkanInstance& instance,
                           SwapChain& swapChain,
                           VkCommandPool& commandPool,
                           int numImages) : name(name),
                                            format(format),
                                            extent(extent),
                                            channelNumber(channelNumber),
                                            usage(usage),
                                            finalLayout(finalLayout),
                                            device(device),
                                            instance(instance),
                                            swapChain(swapChain),
                                            commandPool(commandPool),
                                            numImages(numImages)
{
    id = idGenerator;
    idGenerator++;
    createAttachmentDescription();
    createTextureImage();
}

VkAttachmentDescription& RenderTarget::getAttachmentDescription()
{
    return attachment;
}

VkFormat& RenderTarget::getFormat()
{
    return format;
}

VkImageLayout& RenderTarget::getFinalImageLayout()
{
    return finalLayout;
}

TextureImage& RenderTarget::getTextureImage()
{
    return textureImage;
}

VkExtent2D& RenderTarget::getExtent()
{
    return extent;
}

void RenderTarget::cleanup()
{
    textureImage.cleanup();
}

bool RenderTarget::operator==(const RenderTarget& other) const
{
    return id == other.id;
}

void RenderTarget::createAttachmentDescription()
{
    attachment.format = format;
    attachment.samples = VK_SAMPLE_COUNT_1_BIT;
    attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;

    //if its depth resource...
    if (format == VK_FORMAT_D32_SFLOAT
        || format == VK_FORMAT_D32_SFLOAT_S8_UINT
        || format == VK_FORMAT_D24_UNORM_S8_UINT) {
        attachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        //attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    }
    else
        attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;

    attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachment.finalLayout = finalLayout;
}

void RenderTarget::createTextureImage()
{
    //if its depth resource...
    if (format == VK_FORMAT_D32_SFLOAT
        || format == VK_FORMAT_D32_SFLOAT_S8_UINT
        || format == VK_FORMAT_D24_UNORM_S8_UINT) {
        textureImage = TextureImage(name,
                                    device,
                                    commandPool,
                                    extent.width,
                                    extent.height,
                                    channelNumber,
                                    format,
                                    usage,
                                    1,
                                    true);
    }
    else {
        textureImage = TextureImage(name,
                                    device,
                                    commandPool,
                                    extent.width,
                                    extent.height,
                                    channelNumber,
                                    format,
                                    usage,
                                    1,
                                    true);
    }
}