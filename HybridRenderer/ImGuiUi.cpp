#include "ImGuiUi.h"
#include <stdexcept>

ImGuiUi::ImGuiUi()
{}

ImGuiUi::ImGuiUi(VulkanDevice& device,
                 utils::QueueFamilyIndices& queueFamilyIndices,
                 SwapChain& swapChain,
                 ImGuiRenderpass& renderpass,
                 Scene* scene, 
                 std::vector<UIRenderable*>&& toRender) :
                                                        device(device),
                                                        queueFamilyIndices(queueFamilyIndices),
                                                        swapChain(swapChain),
                                                        renderpass(renderpass),
                                                        scene(scene), 
                                                        toRender(toRender)
{
}

void ImGuiUi::createCommandPool()
{
    VkCommandPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    VK_CHECK_RESULT(vkCreateCommandPool(device.getLogicalDeviceHandle(), &poolInfo, nullptr, &commandPool));
}

void ImGuiUi::createCommandBuffers()
{
    commandBuffers.resize(swapChain.getSize());

    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = (uint32_t)commandBuffers.size();

    VK_CHECK_RESULT(vkAllocateCommandBuffers(device.getLogicalDeviceHandle(), &allocInfo, commandBuffers.data()));
}

void ImGuiUi::createDescriptorPool()
{
    //ImGUI descriptor pool
    VkDescriptorPoolSize pool_sizes[] =
    {
        { VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
        { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1003 },
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
        { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
    };
    VkDescriptorPoolCreateInfo pool_info = {};
    pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    pool_info.maxSets = 1000 * IM_ARRAYSIZE(pool_sizes);
    pool_info.poolSizeCount = (uint32_t)IM_ARRAYSIZE(pool_sizes);
    pool_info.pPoolSizes = pool_sizes;
    VK_CHECK_RESULT(vkCreateDescriptorPool(device.getLogicalDeviceHandle(), &pool_info, nullptr, &descriptorPool));
}

void ImGuiUi::initUniformBuffers()
{
    uniformBuffers.resize(swapChain.getSize());
    for (int i = 0; i < swapChain.getSize(); i++) {
        uniformBuffers[i] = UniformBuffer("Im Gui UI " + std::to_string(i), device, sizeof(UiShaderInputsDeviceSubmit));
    }
}

void ImGuiUi::createDescriptorSetLayout()
{
    std::vector<VkDescriptorSetLayoutBinding> bindings;
    VkDescriptorSetLayoutBinding binding = {};

    //Define the descriptor set layout
    binding = initializers::descriptorSetLayoutBinding(0,
                                                       VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                                                       VK_SHADER_STAGE_FRAGMENT_BIT);

    bindings.push_back(binding);

    VkDescriptorSetLayoutCreateInfo layoutInfo{};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
    layoutInfo.pBindings = bindings.data();

    VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device.getLogicalDeviceHandle(), &layoutInfo, nullptr, &descriptorLayout));
}

void ImGuiUi::createDescriptorSet()
{
    int descriptorCount = swapChain.getSize();
    std::vector<VkDescriptorSetLayout> uniformLayouts(descriptorCount, descriptorLayout);

    //Create the descriptor set
    VkDescriptorSetAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = descriptorPool;
    allocInfo.descriptorSetCount = static_cast<uint32_t>(descriptorCount);
    allocInfo.pSetLayouts = uniformLayouts.data();


    descriptorSets.resize(descriptorCount);
    VK_CHECK_RESULT(vkAllocateDescriptorSets(device.getLogicalDeviceHandle(), &allocInfo, descriptorSets.data()));

    std::vector<VkWriteDescriptorSet> descriptorWrites;
    VkWriteDescriptorSet writeDescriptorSet = {};
    VkDescriptorImageInfo imageInfo{};

    for (int i = 0; i < descriptorCount; i++) {

        VkDescriptorBufferInfo bufferInfo{};
        bufferInfo.buffer = uniformBuffers[i].getBuffer();
        bufferInfo.offset = 0;
        bufferInfo.range = uniformBuffers[i].size;

        writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        writeDescriptorSet.dstSet = descriptorSets[i];
        writeDescriptorSet.dstBinding = 0;
        writeDescriptorSet.dstArrayElement = 0;
        writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        writeDescriptorSet.descriptorCount = 1;
        writeDescriptorSet.pBufferInfo = &bufferInfo;
        descriptorWrites.push_back(writeDescriptorSet);

        vkUpdateDescriptorSets(device.getLogicalDeviceHandle(), static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
    }
}

void ImGuiUi::createFontImage() {
    VkCommandBuffer cmdBuf = utils::beginSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool);
    ImGui_ImplVulkan_CreateFontsTexture(cmdBuf);
    utils::endSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool, device.getQueues().graphicsQueue, cmdBuf);
}

void ImGuiUi::update(uint32_t imageIndex)
{
    if(!active) return;
    
    ImGui::Begin("Global Parameters");

        ImGui::SliderFloat("Base Roughness", &globalShaderInputs.baseRoughness, 0.0f, 1.0f);
        ImGui::SliderFloat("Base Metalness", &globalShaderInputs.baseMetalness, 0.0f, 1.0f);
        ImGui::SliderFloat("Indirect Lighting Weight", &globalShaderInputs.indirectLightingWeight, 0.0f, 1.0f);
        ImGui::SliderFloat("Indirect Diffuse Weight", &globalShaderInputs.indirectDiffuseWeight, 0.0f, 1.0f);
        ImGui::InputInt("MSAA Samples", &globalInputs.msaaSamples, 4);
        if (globalInputs.msaaSamples == 0)
            globalInputs.msaaSamples = 1;
        else if (globalInputs.msaaSamples == 5) {
            globalInputs.msaaSamples = 4;
        }
        globalInputs.msaaSamples = glm::max(globalInputs.msaaSamples, 1);
        globalInputs.msaaSamples = glm::min(globalInputs.msaaSamples, 8);

        ImGui::Checkbox("Direct Diffuse", &globalShaderInputs.directDiffuse);
        ImGui::Checkbox("Direct Specular", &globalShaderInputs.directSpecular);
        ImGui::Checkbox("Indirect Diffuse", &globalShaderInputs.indirectDiffuse);
        ImGui::Checkbox("Indirect Specular", &globalShaderInputs.indirectSpecular);

        ImGui::Checkbox("Use Albedo Texture", &globalShaderInputs.useAlbedoTexture);
        ImGui::Checkbox("Use Normal Texture", &globalShaderInputs.useNormalTexture);
        ImGui::Checkbox("Use Ao Texture", &globalShaderInputs.useAoTexture);
        ImGui::Checkbox("Use Roughness Texture", &globalShaderInputs.useRoughnessTexture);
        ImGui::Checkbox("Use Metalness Texture", &globalShaderInputs.useMetalnessTexture);
        ImGui::Checkbox("Use Emission Texture", &globalShaderInputs.useEmissionTexture);
        ImGui::Checkbox("Use Shadows", &globalShaderInputs.useShadows);

        ImGui::SliderInt("Light to debug", &globalShaderInputs.lightToDebug,0, static_cast<int>(scene->suns.size())-1);
        ImGui::Checkbox("Uniform Distribution Function Only", &globalShaderInputs.uniformDistributionFunctionOnly);
        ImGui::Checkbox("Geometry Function Only", &globalShaderInputs.geometryFunctionOnly);
        ImGui::Checkbox("Fresnel Function Only", &globalShaderInputs.fresnelFunctionOnly);
        ImGui::DragFloat("Exposure", &globalShaderInputs.exposure, 0.2f, 0.0f, 1000.0f);
        ImGui::Checkbox("Use Reinhard Tone Mapping", &globalShaderInputs.useReinhardToneMapping);
        ImGui::InputFloat("Reinhard Tone Mapping Weight", &globalShaderInputs.reinhardToneMappingWeight);
        ImGui::SliderFloat("Skybox Intensity", &globalShaderInputs.skyboxIntensity, 0.0f, 1.0f);

        ImGui::InputFloat("Scale A2", &globalShaderInputs.scaleA2);
        ImGui::InputFloat("Scale NdotH", &globalShaderInputs.scaleNDotH);
        ImGui::InputFloat("Scale D", &globalShaderInputs.scaleD);
        ImGui::Checkbox("Use Tonemaping", &globalShaderInputs.useTonemap);
        ImGui::Checkbox("Use Svgf Demodulation", &globalShaderInputs.useDemodulation);
        ImGui::Checkbox("Use Sky Box", &globalShaderInputs.useSkybox);

        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    
    ImGui::End();

    ImGui::Begin("Light Parameters");
    for (int i = 0; i < scene->suns.size(); i++) {
        ImGui::BeginGroup();
        ImGui::PushID(i);
            ImGui::Text("Light %d", i);
            ImGui::DragFloat3("Light Direction", glm::value_ptr(scene->suns[i].dir), 0.1f);
            scene->suns[i].dir = glm::normalize(scene->suns[i].dir);
            ImGui::ColorEdit3("Light Color", glm::value_ptr(scene->suns[i].lightColor));
            ImGui::InputFloat("Light Intensity", &scene->suns[i].lightIntensity, 1.0f);
        ImGui::PopID();
        ImGui::NewLine();
        ImGui::EndGroup();
    }

    ImGui::End();

    for (int i = 0; i < toRender.size(); i++) {
        toRender[i]->renderUI();
    }
    
}

void ImGuiUi::draw(uint32_t imageIndex)
{
    ImGui_ImplVulkan_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    update(imageIndex);

    ImGui::Render();
    
    globalShaderInputsDeviceSubmission.indirectLightingWeight = globalShaderInputs.indirectLightingWeight;
    globalShaderInputsDeviceSubmission.directDiffuse = static_cast<int>(globalShaderInputs.directDiffuse);
    globalShaderInputsDeviceSubmission.directSpecular = static_cast<int>(globalShaderInputs.directSpecular);
    globalShaderInputsDeviceSubmission.indirectDiffuse = static_cast<int>(globalShaderInputs.indirectDiffuse);
    globalShaderInputsDeviceSubmission.indirectSpecular = static_cast<int>(globalShaderInputs.indirectSpecular);
    globalShaderInputsDeviceSubmission.useEmissionTexture = static_cast<int>(globalShaderInputs.useEmissionTexture);
    globalShaderInputsDeviceSubmission.useRoughnessTexture = static_cast<int>(globalShaderInputs.useRoughnessTexture);
    globalShaderInputsDeviceSubmission.useMetalnessTexture = static_cast<int>(globalShaderInputs.useMetalnessTexture);
    globalShaderInputsDeviceSubmission.useAoTexture = static_cast<int>(globalShaderInputs.useAoTexture);
    globalShaderInputsDeviceSubmission.useAlbedoTexture = static_cast<int>(globalShaderInputs.useAlbedoTexture);
    globalShaderInputsDeviceSubmission.useNormalTexture = static_cast<int>(globalShaderInputs.useNormalTexture);
    globalShaderInputsDeviceSubmission.lightToDebug = static_cast<int>(globalShaderInputs.lightToDebug);
    globalShaderInputsDeviceSubmission.fresnelFunctionOnly = static_cast<int>(globalShaderInputs.fresnelFunctionOnly);
    globalShaderInputsDeviceSubmission.uniformDistributionFunctionOnly = static_cast<int>(globalShaderInputs.uniformDistributionFunctionOnly);
    globalShaderInputsDeviceSubmission.geometryFunctionOnly = static_cast<int>(globalShaderInputs.geometryFunctionOnly);
    globalShaderInputsDeviceSubmission.baseRoughness = globalShaderInputs.baseRoughness;
    globalShaderInputsDeviceSubmission.baseMetalness = globalShaderInputs.baseMetalness;
    globalShaderInputsDeviceSubmission.exposure = globalShaderInputs.exposure;
    globalShaderInputsDeviceSubmission.useShadows = static_cast<int>(globalShaderInputs.useShadows);
    globalShaderInputsDeviceSubmission.indirectDiffuseWeight = static_cast<float>(globalShaderInputs.indirectDiffuseWeight);
    globalShaderInputsDeviceSubmission.useReinhardToneMapping = static_cast<int>(globalShaderInputs.useReinhardToneMapping);
    globalShaderInputsDeviceSubmission.reinhardToneMappingWeight = static_cast<float>(globalShaderInputs.reinhardToneMappingWeight);
    globalShaderInputsDeviceSubmission.skyboxIntensity = static_cast<float>(globalShaderInputs.skyboxIntensity);
    globalShaderInputsDeviceSubmission.scaleA2 = globalShaderInputs.scaleA2;
    globalShaderInputsDeviceSubmission.scaleNDotH = globalShaderInputs.scaleNDotH;
    globalShaderInputsDeviceSubmission.scaleD = globalShaderInputs.scaleD;
    globalShaderInputsDeviceSubmission.useTonemap = static_cast<int>(globalShaderInputs.useTonemap);
    globalShaderInputsDeviceSubmission.useDemodulation = static_cast<int>(globalShaderInputs.useDemodulation);

    uniformBuffers[imageIndex].submitDataToMemory(&globalShaderInputsDeviceSubmission);

    VkRenderPassBeginInfo info = {};
    VkClearValue clearValue = { 0.0f, 0.0f, 0.0f, 1.0f };
    info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    info.renderPass = renderpass.renderPass;
    info.framebuffer = renderpass.swapChainFramebuffers[imageIndex];
    info.renderArea.extent.width = renderpass.swapChain.getSwapChainExtent().width;
    info.renderArea.extent.height = renderpass.swapChain.getSwapChainExtent().height;
    info.clearValueCount = 1;
    info.pClearValues = &clearValue;

    //reset the ImGui command pool and begin recording commands for drawing
    {
        VK_CHECK_RESULT(vkResetCommandBuffer(commandBuffers[imageIndex], 0));

        VkCommandBufferBeginInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        VK_CHECK_RESULT(vkBeginCommandBuffer(commandBuffers[imageIndex], &info));
    }

    //register a command to start rendering the render pass
    vkCmdBeginRenderPass(commandBuffers[imageIndex], &info, VK_SUBPASS_CONTENTS_INLINE);
        
        ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), commandBuffers[imageIndex]);

    // Submit command buffer
    vkCmdEndRenderPass(commandBuffers[imageIndex]);
    VK_CHECK_RESULT(vkEndCommandBuffer(commandBuffers[imageIndex]));
}

void ImGuiUi::cleanup()
{
    vkDestroyDescriptorPool(device.getLogicalDeviceHandle(), descriptorPool, nullptr);

    for (int i = 0; i < uniformBuffers.size(); i++) {
        uniformBuffers[i].cleanup();
    }

    vkDestroyDescriptorSetLayout(device.getLogicalDeviceHandle(), descriptorLayout, nullptr);

    vkFreeCommandBuffers(device.getLogicalDeviceHandle(), commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());
    vkDestroyCommandPool(device.getLogicalDeviceHandle(), commandPool, nullptr);

}

void ImGuiUi::toggleActive()
{
    active = !active;
}
