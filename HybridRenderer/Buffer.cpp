#include "Buffer.h"

Buffer::Buffer() {};

Buffer::Buffer(std::string name, 
               VulkanDevice device,
               VkQueue graphicsQueue,
               int size, 
               VkBufferUsageFlags usage,
               VkMemoryPropertyFlags properties) : name(name),
                                                   device(device),
                                                   graphicsQueue(graphicsQueue),
                                                   size(size)
{

    VkBufferCreateInfo bufferInfo{};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = this->size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VK_CHECK_RESULT(vkCreateBuffer(device.getLogicalDeviceHandle(), &bufferInfo, nullptr, &buffer));

    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(device.getLogicalDeviceHandle(), buffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    VkMemoryAllocateFlagsInfoKHR allocFlagsInfo{};
    allocInfo.memoryTypeIndex = utils::findMemoryType(device.getPhysicalDeviceHandle(), memRequirements.memoryTypeBits, properties);
    if (usage & VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT) {
        allocFlagsInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO_KHR;
        allocFlagsInfo.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR;
        allocInfo.pNext = &allocFlagsInfo;
    }
    VK_CHECK_RESULT(vkAllocateMemory(device.getLogicalDeviceHandle(), &allocInfo, nullptr, &bufferMemory));

    vkBindBufferMemory(device.getLogicalDeviceHandle(), buffer, bufferMemory, 0);

    device.nameVulkanObject(VK_OBJECT_TYPE_BUFFER, (uint64_t)buffer, name + " Buffer");
    device.nameVulkanObject(VK_OBJECT_TYPE_DEVICE_MEMORY, (uint64_t)bufferMemory, name + " Buffer Memory");
}

void Buffer::submitDataToMemory(void* uboData)
{
    void* data;
    vkMapMemory(device.getLogicalDeviceHandle(), bufferMemory, 0, this->size, 0, &data);
    memcpy(data, uboData, (size_t)this->size);
    vkUnmapMemory(device.getLogicalDeviceHandle(), bufferMemory);
}

VkDeviceAddress Buffer::getDeviceAddress()
{
    VkBufferDeviceAddressInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    info.pNext = nullptr;
    info.buffer = buffer;

    return vkGetBufferDeviceAddress(device.getLogicalDeviceHandle(), &info);
}

void Buffer::cleanup()
{
    vkDestroyBuffer(device.getLogicalDeviceHandle(), buffer, nullptr);
    vkFreeMemory(device.getLogicalDeviceHandle(), bufferMemory, nullptr);
}