#include "DescriptorSetRaytracing.h"

DescriptorSetRaytracing::DescriptorSetRaytracing()
{
}

DescriptorSetRaytracing::DescriptorSetRaytracing(std::string name, 
												 UniformBuffer& uniformBuffer,
												 std::vector<TextureResource>&& textureResources,
												 std::vector<TextureArrayResource>&& textureArrayResources,
												 std::vector<TextureResource>&& textureTargets,
												 std::vector<TextureCubeResource>&& textureCubeResources,
												 Scene& scene,
												 TopLevelAS& tlas,
												 VulkanDevice& device,
												 VulkanInstance& instance,
												 VkCommandPool commandPool) : name(name),
																			  uniformBuffer(uniformBuffer),
																			  textureResources(textureResources),
																			  textureArrayResources(textureArrayResources),
																			  textureTargets(textureTargets),
																			  textureCubeResources(textureCubeResources),
																			  scene(scene),
							 												  tlas(tlas),
																			  device(device),
																			  instance(instance),
																			  commandPool(commandPool)
{
	createDescriptorPool();
	createDescriptorSet();
}

void DescriptorSetRaytracing::cleanup()
{
	vkDestroyDescriptorPool(device.getLogicalDeviceHandle(), descriptorPool, nullptr);
	vkDestroyDescriptorSetLayout(device.getLogicalDeviceHandle(), descriptorSetLayout, nullptr);
}

VkDescriptorSet& DescriptorSetRaytracing::getDescriptorSetsHandle()
{
	return descriptorSet;
}

VkDescriptorSetLayout& DescriptorSetRaytracing::getDescriptorSetLayout()
{
	return descriptorSetLayout;
}

void DescriptorSetRaytracing::createDescriptorPool()
{
	std::vector<VkDescriptorPoolSize> poolSizes{};

	VkDescriptorPoolSize accelerationStruct;
	accelerationStruct.type = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
	accelerationStruct.descriptorCount = 1;
	poolSizes.push_back(accelerationStruct);

	if(textureResources.size() > 0) {
		VkDescriptorPoolSize samplers;
		samplers.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		samplers.descriptorCount = static_cast<uint32_t>(textureResources.size());
		poolSizes.push_back(samplers);
	}

	if (textureArrayResources.size() > 0) {
		int numTextures = 0;
		for(int i = 0; i < textureArrayResources.size(); i++) {
			numTextures += static_cast<int>(textureArrayResources[i].texImages.size());
		}

		VkDescriptorPoolSize samplers;
		samplers.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		samplers.descriptorCount = numTextures;
		poolSizes.push_back(samplers);
	}

	if (textureTargets.size() > 0) {
		VkDescriptorPoolSize targets;
		targets.type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
		targets.descriptorCount = static_cast<uint32_t>(textureResources.size());
		poolSizes.push_back(targets);
	}

	VkDescriptorPoolSize storageImages;
	storageImages.type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
	storageImages.descriptorCount = 1;
	poolSizes.push_back(accelerationStruct);

	VkDescriptorPoolSize uniformBuffers;
	uniformBuffers.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uniformBuffers.descriptorCount = 1;
	poolSizes.push_back(storageImages);

	//index, vertex, offset, material and Scene Node Data buffers
	VkDescriptorPoolSize indexAndVertexBuffers;
	indexAndVertexBuffers.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	indexAndVertexBuffers.descriptorCount = 5;
	poolSizes.push_back(indexAndVertexBuffers);

	VkDescriptorPoolCreateInfo poolInfo{};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = 1;

	VK_CHECK_RESULT(vkCreateDescriptorPool(device.getLogicalDeviceHandle(), &poolInfo, nullptr, &descriptorPool));
	device.nameVulkanObject(VK_OBJECT_TYPE_DESCRIPTOR_POOL, (uint64_t)descriptorPool, name + " Descriptor Pool");
}

void DescriptorSetRaytracing::createDescriptorSet()
{
	int binding = 0;
	std::vector<VkDescriptorSetLayoutBinding> bindings;

	//descriptors acceleration structure
	VkDescriptorSetLayoutBinding accelerationStructureLayoutBinding{};
	accelerationStructureLayoutBinding.binding = binding;
	accelerationStructureLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
	accelerationStructureLayoutBinding.descriptorCount = 1;
	accelerationStructureLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
	bindings.push_back(accelerationStructureLayoutBinding);
	binding++;

	//descriptors targets image
	for (int i = 0; i < textureTargets.size(); i++) {
		VkDescriptorSetLayoutBinding textureTargetsBinding{};
		textureTargetsBinding.binding = binding;
		textureTargetsBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
		textureTargetsBinding.descriptorCount = 1;
		textureTargetsBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR;
		bindings.push_back(textureTargetsBinding);
		binding++;
	}

	//descriptors uniform
	VkDescriptorSetLayoutBinding uniformBufferBinding{};
	uniformBufferBinding.binding = binding;
	uniformBufferBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uniformBufferBinding.descriptorCount = 1;
	uniformBufferBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_MISS_BIT_KHR;
	bindings.push_back(uniformBufferBinding);
	binding++;

	//descriptors vertex buffer
	VkDescriptorSetLayoutBinding vertexBufferBinding{};
	vertexBufferBinding.binding = binding;
	vertexBufferBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	vertexBufferBinding.descriptorCount = 1;
	vertexBufferBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
	bindings.push_back(vertexBufferBinding);
	binding++;

	//descriptors index buffer
	VkDescriptorSetLayoutBinding indexBufferBinding{};
	indexBufferBinding.binding = binding;
	indexBufferBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	indexBufferBinding.descriptorCount = 1;
	indexBufferBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
	bindings.push_back(indexBufferBinding);
	binding++;

	//descriptors index offsets buffer
	VkDescriptorSetLayoutBinding indexOffsetBufferBinding{};
	indexOffsetBufferBinding.binding = binding;
	indexOffsetBufferBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	indexOffsetBufferBinding.descriptorCount = 1;
	indexOffsetBufferBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
	bindings.push_back(indexOffsetBufferBinding);
	binding++;

	//descriptors materials buffer
	VkDescriptorSetLayoutBinding materialsOffsetBufferBinding{};
	materialsOffsetBufferBinding.binding = binding;
	materialsOffsetBufferBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	materialsOffsetBufferBinding.descriptorCount = 1;
	materialsOffsetBufferBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
	bindings.push_back(materialsOffsetBufferBinding);
	binding++;

	//descriptors scene node data buffer
	VkDescriptorSetLayoutBinding sceneNodeBufferBinding{};
	sceneNodeBufferBinding.binding = binding;
	sceneNodeBufferBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	sceneNodeBufferBinding.descriptorCount = 1;
	sceneNodeBufferBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
	bindings.push_back(sceneNodeBufferBinding);
	binding++;

	//descriptors texture samplers
	for (int i = 0; i < textureResources.size(); i++) {
		VkDescriptorSetLayoutBinding textureSamplersBinding{};
		textureSamplersBinding.binding = binding;
		textureSamplersBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		textureSamplersBinding.descriptorCount = 1;
		textureSamplersBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR;
		bindings.push_back(textureSamplersBinding);
		binding++;
	}

	//descriptors texture array samplers
	for (int i = 0; i < textureArrayResources.size(); i++) {
		VkDescriptorSetLayoutBinding textureSamplersBinding{};
		textureSamplersBinding.binding = binding;
		textureSamplersBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		textureSamplersBinding.descriptorCount = static_cast<uint32_t>(textureArrayResources[i].texImages.size());
		textureSamplersBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_KHR | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
		bindings.push_back(textureSamplersBinding);
		binding++;
	}

	//descriptors texture cube array samplers
	for (int i = 0; i < textureCubeResources.size(); i++) {
		VkDescriptorSetLayoutBinding textureSamplersBinding{};
		textureSamplersBinding.binding = binding;
		textureSamplersBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		textureSamplersBinding.descriptorCount = 1;
		textureSamplersBinding.stageFlags = VK_SHADER_STAGE_MISS_BIT_KHR | VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
		bindings.push_back(textureSamplersBinding);
		binding++;
	}

	//allows descriptor to be incomplete ex: missing textures
	std::vector<VkDescriptorBindingFlags> descriptorFlags = std::vector<VkDescriptorBindingFlags>(bindings.size(), VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT);
	VkDescriptorSetLayoutBindingFlagsCreateInfo layoutInfoFlag{};
	layoutInfoFlag.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
	layoutInfoFlag.bindingCount = static_cast<uint32_t>(bindings.size());
	layoutInfoFlag.pBindingFlags = descriptorFlags.data();
	layoutInfoFlag.pNext = nullptr;

	VkDescriptorSetLayoutCreateInfo layoutInfo{};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
	layoutInfo.pBindings = bindings.data();
	layoutInfo.pNext = &layoutInfoFlag;
	VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device.getLogicalDeviceHandle(), &layoutInfo, nullptr, &descriptorSetLayout));
	device.nameVulkanObject(VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT, (uint64_t)descriptorSetLayout, name + " Descriptor Set Layout");

	std::vector<VkDescriptorSetLayout> descriptorSetLayouts(1, descriptorSetLayout);

	//Create the descriptor set
	VkDescriptorSetAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>(1);
	allocInfo.pSetLayouts = descriptorSetLayouts.data();
	VK_CHECK_RESULT(vkAllocateDescriptorSets(device.getLogicalDeviceHandle(), &allocInfo, &descriptorSet));
	device.nameVulkanObject(VK_OBJECT_TYPE_DESCRIPTOR_SET, (uint64_t)descriptorSet, name + " Descriptor Set");
	binding = 0;

	//acceleration structure
	std::vector<VkWriteDescriptorSet> descriptorWrites;
	VkWriteDescriptorSetAccelerationStructureKHR descriptorAccelerationStructureInfo{};
	descriptorAccelerationStructureInfo.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
	descriptorAccelerationStructureInfo.accelerationStructureCount = 1;
	descriptorAccelerationStructureInfo.pAccelerationStructures = &tlas.getAccelerationStructure();

	VkWriteDescriptorSet accelerationStructureWrite{};
	accelerationStructureWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	// The specialized acceleration structure descriptor has to be chained
	accelerationStructureWrite.pNext = &descriptorAccelerationStructureInfo;
	accelerationStructureWrite.dstSet = descriptorSet;
	accelerationStructureWrite.dstBinding = binding;
	accelerationStructureWrite.descriptorCount = 1;
	accelerationStructureWrite.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
	descriptorWrites.push_back(accelerationStructureWrite);
	binding++;

	//Storage Image
	std::vector<VkDescriptorImageInfo> targetImages;
	targetImages.resize(textureTargets.size());
	//target images
	for (int i = 0; i < textureTargets.size(); i++) {
		TextureImage texImage = *textureTargets[i].texImage;

		targetImages[i].imageLayout = VK_IMAGE_LAYOUT_GENERAL;
		targetImages[i].imageView = texImage.textureImageViews[0];
		targetImages[i].sampler = texImage.textureSamplers[0];

		VkWriteDescriptorSet targetWrite{};
		targetWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		targetWrite.dstSet = descriptorSet;
		targetWrite.dstBinding = binding;
		targetWrite.dstArrayElement = 0;
		targetWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
		targetWrite.descriptorCount = 1;
		targetWrite.pImageInfo = &targetImages[i];
		descriptorWrites.push_back(targetWrite);
		binding++;
	}

	//Uniform Buffer
	VkDescriptorBufferInfo descriptorUniformBufferInfo{};
	descriptorUniformBufferInfo.buffer = uniformBuffer.getBuffer();
	descriptorUniformBufferInfo.offset = 0;
	descriptorUniformBufferInfo.range = uniformBuffer.size;

	VkWriteDescriptorSet uniformBufferWrite{};
	uniformBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	uniformBufferWrite.dstSet = descriptorSet;
	uniformBufferWrite.dstBinding = binding;
	uniformBufferWrite.dstArrayElement = 0;
	uniformBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uniformBufferWrite.descriptorCount = 1;
	uniformBufferWrite.pBufferInfo = &descriptorUniformBufferInfo;
	descriptorWrites.push_back(uniformBufferWrite);
	binding++;

	//Vertex Buffer
	VkDescriptorBufferInfo vertexBufferDescriptor{};
	vertexBufferDescriptor.buffer = scene.vertexBuffer.vertexBuffer;
	vertexBufferDescriptor.range = VK_WHOLE_SIZE;

	VkWriteDescriptorSet vertexBufferWrite{};
	vertexBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	vertexBufferWrite.dstSet = descriptorSet;
	vertexBufferWrite.dstBinding = binding;
	vertexBufferWrite.dstArrayElement = 0;
	vertexBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	vertexBufferWrite.descriptorCount = 1;
	vertexBufferWrite.pBufferInfo = &vertexBufferDescriptor;
	descriptorWrites.push_back(vertexBufferWrite);
	binding++;

	//Index Buffer
	VkDescriptorBufferInfo indexBufferDescriptor{};
	indexBufferDescriptor.buffer = scene.indexBuffer.indexBuffer;
	indexBufferDescriptor.range = VK_WHOLE_SIZE;

	VkWriteDescriptorSet indexBufferWrite{};
	indexBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	indexBufferWrite.dstSet = descriptorSet;
	indexBufferWrite.dstBinding = binding;
	indexBufferWrite.dstArrayElement = 0;
	indexBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	indexBufferWrite.descriptorCount = 1;
	indexBufferWrite.pBufferInfo = &indexBufferDescriptor;
	descriptorWrites.push_back(indexBufferWrite);
	binding++;

	//Index Offset Buffer
	VkDescriptorBufferInfo indexOffsetBufferDescriptor{};
	indexOffsetBufferDescriptor.buffer = scene.indexOffsetBuffer.buffer;
	indexOffsetBufferDescriptor.range = VK_WHOLE_SIZE;

	VkWriteDescriptorSet indexOffsetBufferWrite{};
	indexOffsetBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	indexOffsetBufferWrite.dstSet = descriptorSet;
	indexOffsetBufferWrite.dstBinding = binding;
	indexOffsetBufferWrite.dstArrayElement = 0;
	indexOffsetBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	indexOffsetBufferWrite.descriptorCount = 1;
	indexOffsetBufferWrite.pBufferInfo = &indexOffsetBufferDescriptor;
	descriptorWrites.push_back(indexOffsetBufferWrite);
	binding++;

	//Material Buffer
	VkDescriptorBufferInfo materialBufferDescriptor{};
	materialBufferDescriptor.buffer = scene.materialBuffer.buffer;
	materialBufferDescriptor.range = VK_WHOLE_SIZE;

	VkWriteDescriptorSet materialBufferWrite{};
	materialBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	materialBufferWrite.dstSet = descriptorSet;
	materialBufferWrite.dstBinding = binding;
	materialBufferWrite.dstArrayElement = 0;
	materialBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	materialBufferWrite.descriptorCount = 1;
	materialBufferWrite.pBufferInfo = &materialBufferDescriptor;
	descriptorWrites.push_back(materialBufferWrite);
	binding++;

	//Scene Node Data Buffer
	VkDescriptorBufferInfo sceneNodeBufferDescriptor{};
	sceneNodeBufferDescriptor.buffer = scene.sceneNodeDataBuffer.buffer;
	sceneNodeBufferDescriptor.range = VK_WHOLE_SIZE;

	VkWriteDescriptorSet sceneNodeBufferWrite{};
	sceneNodeBufferWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	sceneNodeBufferWrite.dstSet = descriptorSet;
	sceneNodeBufferWrite.dstBinding = binding;
	sceneNodeBufferWrite.dstArrayElement = 0;
	sceneNodeBufferWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	sceneNodeBufferWrite.descriptorCount = 1;
	sceneNodeBufferWrite.pBufferInfo = &sceneNodeBufferDescriptor;
	descriptorWrites.push_back(sceneNodeBufferWrite);
	binding++;

	std::vector<VkDescriptorImageInfo> imageInfos;
	imageInfos.resize(textureResources.size());
	//texture images
	for (int i = 0; i < textureResources.size(); i++) {
		TextureImage texImage = *textureResources[i].texImage;

		imageInfos[i].imageLayout = textureResources[i].imageLayout;
		imageInfos[i].imageView = texImage.textureImageViews[0];
		imageInfos[i].sampler = texImage.textureSamplers[0];

		VkWriteDescriptorSet targetWrite{};
		targetWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		targetWrite.dstSet = descriptorSet;
		targetWrite.dstBinding = binding;
		targetWrite.dstArrayElement = 0;
		targetWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		targetWrite.descriptorCount = 1;
		targetWrite.pImageInfo = &imageInfos[i];
		descriptorWrites.push_back(targetWrite);
		binding++;
	}

	std::vector<std::vector<VkDescriptorImageInfo>> textureArrayInfos;
	//texture array images
	for (int i = 0; i < textureArrayResources.size(); i++) {
		std::vector<TextureImage*> texImages = textureArrayResources[i].texImages;
		std::vector<VkDescriptorImageInfo> textureInfos;
		textureInfos.resize(texImages.size());
		if (textureInfos.size() <= 0) {
			binding++;
			continue;
		}

		for(int j = 0; j < texImages.size(); j++) {
			textureInfos[j].imageLayout = textureArrayResources[i].imageLayout;
			textureInfos[j].imageView = texImages[j]->textureImageViews[0];
			textureInfos[j].sampler = texImages[j]->textureSamplers[0];
		}
		textureArrayInfos.push_back(textureInfos);

		VkWriteDescriptorSet targetWrite{};
		targetWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		targetWrite.dstSet = descriptorSet;
		targetWrite.dstBinding = binding;
		targetWrite.dstArrayElement = 0;
		targetWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		targetWrite.descriptorCount = static_cast<uint32_t>(textureInfos.size());
		targetWrite.pImageInfo = textureArrayInfos[i].data();
		descriptorWrites.push_back(targetWrite);
		binding++;
	}

	std::vector<VkDescriptorImageInfo> textureCubeInfos;
	textureCubeInfos.resize(textureCubeResources.size());
	//texture cube array images
	for (int i = 0; i < textureCubeResources.size(); i++) {
		TextureCubeMap* texImage = textureCubeResources[i].texImage;

		textureCubeInfos[i].imageLayout = textureCubeResources[i].imageLayout;
		textureCubeInfos[i].imageView = texImage->textureImageView;
		textureCubeInfos[i].sampler = texImage->textureSampler;

		VkWriteDescriptorSet targetWrite{};
		targetWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		targetWrite.dstSet = descriptorSet;
		targetWrite.dstBinding = binding;
		targetWrite.dstArrayElement = 0;
		targetWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		targetWrite.descriptorCount = 1;
		targetWrite.pImageInfo = &textureCubeInfos[i];
		descriptorWrites.push_back(targetWrite);
		binding++;
	}

	vkUpdateDescriptorSets(device.getLogicalDeviceHandle(), static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, VK_NULL_HANDLE);
}
