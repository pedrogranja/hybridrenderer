#include "UniformBuffer.h"

UniformBuffer::UniformBuffer()
{
}

UniformBuffer::UniformBuffer(std::string name,
                             VulkanDevice& device,
                             int size,
                             int numBuffers) : name(name),
                                               device(device),
                                               size(size),
                                               numBuffers(numBuffers)
{
    uniformBuffers.resize(numBuffers);
    uniformMemories.resize(numBuffers);
    for (int i = 0; i < uniformBuffers.size(); i++) {
        utils::createBuffer(device.getLogicalDeviceHandle(),
                            device.getPhysicalDeviceHandle(),
                            size,
                            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                            uniformBuffers[i],
                            uniformMemories[i]);


        device.nameVulkanObject(VK_OBJECT_TYPE_BUFFER, (uint64_t)uniformBuffers[i], name + " Uniform Buffer " + std::to_string(i));
        device.nameVulkanObject(VK_OBJECT_TYPE_DEVICE_MEMORY, (uint64_t)uniformMemories[i], name + " Uniform Buffer Memory " + std::to_string(i));
    }

}

void UniformBuffer::submitDataToMemory(void* uboData, int bufferIndex)
{
    void* data;
    vkMapMemory(device.getLogicalDeviceHandle(), uniformMemories[bufferIndex], 0, this->size, 0, &data);
    memcpy(data, uboData, (size_t)this->size);
    vkUnmapMemory(device.getLogicalDeviceHandle(), uniformMemories[bufferIndex]);
}

VkBuffer UniformBuffer::getBuffer(int bufferIndex)
{
    return uniformBuffers[bufferIndex];
}

int UniformBuffer::getSize()
{
    return  static_cast<int>(uniformBuffers.size());
}

void UniformBuffer::cleanup()
{
    for (int i = 0; i < uniformBuffers.size(); i++) {
        vkDestroyBuffer(device.getLogicalDeviceHandle(), uniformBuffers[i], nullptr);
        vkFreeMemory(device.getLogicalDeviceHandle(), uniformMemories[i], nullptr);
    }
}
