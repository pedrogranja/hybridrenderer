#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;

	mat4 prevModel;
	mat4 prevView;
	mat4 prevProj;

    int id;
    vec2 jitter;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inUv;

layout(location = 0) out vec4 fragPosWorld;
layout(location = 1) out vec3 fragColor;
layout(location = 2) out vec3 fragNormal;
layout(location = 3) out vec2 fragTexCoord;
layout(location = 4) out vec4 fragPosScreen;
layout(location = 5) out vec4 prevFragPosScreen;
layout(location = 6) out float depth;
layout(location = 7) flat out int id;

void main() {
    fragPosWorld = ubo.model * vec4(inPosition.xyz, 1.0);
    vec4 fragPosView = ubo.view * fragPosWorld;
    fragPosScreen = ubo.proj * ubo.view * fragPosWorld;
    prevFragPosScreen = ubo.prevProj * ubo.prevView * ubo.prevModel * vec4(inPosition.xyz, 1.0);
    gl_Position = fragPosScreen;
    fragColor = inColor;
    fragTexCoord = inUv;
    fragNormal = mat3(transpose(inverse(ubo.model))) * inNormal;
    depth = fragPosView.z;
    id = ubo.id;
}