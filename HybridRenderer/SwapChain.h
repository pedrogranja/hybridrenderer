#pragma once
#include <vector>
#include <algorithm>
#include "utils.hpp"
#include "Initializers2.hpp"

#include <vulkan/vulkan_core.h>
#include <GLFW/glfw3.h> //glfw gives us the window to which we render (Vulkan)
#include "VulkanDevice.h"
#include "VulkanInstance.h"

class SwapChain
{
private:
    Window window;
    VulkanInstance instance;
    VulkanDevice device;
    VkSwapchainKHR swapChainHandle;
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;
    std::vector<VkImage> swapChainImages; //The images of our swap chain
    std::vector<VkImageView> swapChainImageViews; //The interface to the images
    int size;

public:
    SwapChain();
    SwapChain(Window& window, VulkanInstance& instance, VulkanDevice& device);
    void cleanup();

    VkSwapchainKHR& getSwapChainHandle();
    VkFormat& getSwapChainImageFormat();
    VkExtent2D& getSwapChainExtent();
    std::vector<VkImage>& getSwapChainImages();
    std::vector<VkImageView>& getSwapChainImageViews();
    int getSize();

private:
    void createSwapChain();
    void createImageViews();

    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);
};

