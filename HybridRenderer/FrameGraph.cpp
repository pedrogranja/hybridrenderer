#include "FrameGraph.h"


Pass::Pass()
{
}

Pass::Pass(std::string name, 
           std::vector<RenderTarget>& resourcesColor,
           std::vector<RenderTarget>& resourcesDepth,   
           bool renderToSwapChain, 
           SwapChain& swapChain, 
           VulkanDevice& device, 
           std::unordered_map<RenderTarget, 
           VkAttachmentDescription>&& specialDescriptions) : name(name),
                                                             resourcesColor(resourcesColor),
                                                             resourcesDepth(resourcesDepth),
                                                             renderToSwapChain(renderToSwapChain),
                                                             swapChain(swapChain),
                                                             device(device),
                                                             specialDescriptions(specialDescriptions)
{
    createRenderPass();
    createFrameBuffer();
}

Pass::Pass(std::string name, 
           std::vector<RenderTarget>&& resourcesColor,
           std::vector<RenderTarget>&& resourcesDepth,
           bool renderToSwapChain,
           SwapChain& swapChain,
           VulkanDevice& device,
           std::unordered_map<RenderTarget, 
           VkAttachmentDescription>&& specialDescriptions) : name(name), 
                                                             resourcesColor(resourcesColor),
                                                             resourcesDepth(resourcesDepth),
                                                             renderToSwapChain(renderToSwapChain),
                                                             swapChain(swapChain),
                                                             device(device),
                                                             specialDescriptions(specialDescriptions)
{
    createRenderPass();
    createFrameBuffer();
}


VkRenderPass& Pass::getRenderPassHandle()
{
    return renderPassHandle;
}

void Pass::createRenderPass()
{
    std::vector<VkAttachmentDescription> attachmentDescriptions;
    std::vector<VkAttachmentReference> colorAttachmentRefs;
    int refCount = 0;

    for (int i = 0; i < resourcesColor.size(); i++) {
        //we know it was initialized using a map
        if (specialDescriptions.find(resourcesColor[i]) != specialDescriptions.end()) {
            colorAttachmentRefs.push_back(initializers::attachmentRef(refCount++, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL));
            attachmentDescriptions.push_back(specialDescriptions[resourcesColor[i]]);
        }
        else {
            colorAttachmentRefs.push_back(initializers::attachmentRef(refCount++, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL));
            attachmentDescriptions.push_back(resourcesColor[i].getAttachmentDescription());
        }
    }

    if (renderToSwapChain) {
        colorAttachmentRefs.push_back(initializers::attachmentRef(refCount, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL));
        attachmentDescriptions.push_back(initializers::colorAttachmentDescription(VK_IMAGE_LAYOUT_UNDEFINED,       //we dont care about the previous format
                                                                                  VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                                                                                  swapChain.getSwapChainImageFormat(),
                                                                                  VK_SAMPLE_COUNT_1_BIT,
                                                                                  VK_ATTACHMENT_LOAD_OP_CLEAR));
        refCount++;
    }
    
    //create normal depth attachments
    std::vector<VkAttachmentReference> depthAttachmentRefs;
    for (int i = 0; i < resourcesDepth.size(); i++) {
        //we know it was initialized using a map

        if (specialDescriptions.find(resourcesDepth[i]) != specialDescriptions.end()) {
            depthAttachmentRefs.push_back(initializers::attachmentRef(refCount++, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL));
            attachmentDescriptions.push_back(specialDescriptions[resourcesDepth[i]]);
        }
        else {
            depthAttachmentRefs.push_back(initializers::attachmentRef(refCount++, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL));
            attachmentDescriptions.push_back(resourcesDepth[i].getAttachmentDescription());
        }
    }
   
    VkSubpassDescription subpass{};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = static_cast<uint32_t>(colorAttachmentRefs.size());
    subpass.pColorAttachments = colorAttachmentRefs.data();
    
    if(depthAttachmentRefs.size() > 0)
        subpass.pDepthStencilAttachment = depthAttachmentRefs.data();
        

    VkSubpassDependency dependency = initializers::subpassDependency(VK_SUBPASS_EXTERNAL, 0);
    VkRenderPassCreateInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = static_cast<uint32_t>(attachmentDescriptions.size());
    renderPassInfo.pAttachments = attachmentDescriptions.data();
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;

    VK_CHECK_RESULT(vkCreateRenderPass(device.getLogicalDeviceHandle(), &renderPassInfo, nullptr, &renderPassHandle));
    device.nameVulkanObject(VK_OBJECT_TYPE_RENDER_PASS, (uint64_t)renderPassHandle, name + " Render Pass ");
}

void Pass::createFrameBuffer()
{
    if(renderToSwapChain) {
        framebuffers.resize(swapChain.getSize());

        for (size_t i = 0; i < swapChain.getSize(); i++) {
            std::vector<VkImageView> attachments;

            for (int j = 0; j < resourcesColor.size(); j++) {
                attachments.push_back(resourcesColor[j].getTextureImage().textureImageViews[0]);
            }
            
            attachments.push_back(swapChain.getSwapChainImageViews()[i]);

            std::vector<VkAttachmentReference> depthAttachmentRefs;
            for (int j = 0; j < resourcesDepth.size(); j++) {
                attachments.push_back(resourcesDepth[j].getTextureImage().textureImageViews[0]);
            }

            uint32_t width = 0;
            uint32_t height = 0;
            if (renderToSwapChain) {
                width = swapChain.getSwapChainExtent().width;
                height = swapChain.getSwapChainExtent().height;
            }
            else {
                width = resourcesColor[0].getExtent().width;
                height = resourcesColor[0].getExtent().height;
            }

            VkFramebufferCreateInfo framebufferInfo = initializers::framebufferCreateInfo(renderPassHandle,
                                                                                          static_cast<int>(attachments.size()),
                                                                                          attachments.data(),
                                                                                          width,
                                                                                          height);

            VK_CHECK_RESULT(vkCreateFramebuffer(device.getLogicalDeviceHandle(), &framebufferInfo, nullptr, &framebuffers[i]));
            device.nameVulkanObject(VK_OBJECT_TYPE_FRAMEBUFFER, (uint64_t)framebuffers[i], name + " FrameBuffer");
        }
    }
    else {
        framebuffers.resize(1);

        std::vector<VkImageView> attachments;

        for (int j = 0; j < resourcesColor.size(); j++) {
            attachments.push_back(resourcesColor[j].getTextureImage().textureImageViews[0]);
        }

        std::vector<VkAttachmentReference> depthAttachmentRefs;
        for (int j = 0; j < resourcesDepth.size(); j++) {
            attachments.push_back(resourcesDepth[j].getTextureImage().textureImageViews[0]);
        }

        uint32_t width = 0;
        uint32_t height = 0;
        if(resourcesColor.size() > 0) {
            width = resourcesColor[0].getExtent().width;
            height = resourcesColor[0].getExtent().height;
        }
        else if(resourcesDepth.size() > 0) {
            width = resourcesDepth[0].getExtent().width;
            height = resourcesDepth[0].getExtent().height;
        }

        VkFramebufferCreateInfo framebufferInfo = initializers::framebufferCreateInfo(renderPassHandle,
                                                                                      static_cast<int>(attachments.size()),
                                                                                      attachments.data(),
                                                                                      width,
                                                                                      height);

        VK_CHECK_RESULT(vkCreateFramebuffer(device.getLogicalDeviceHandle(), &framebufferInfo, nullptr, &framebuffers[0]));
        device.nameVulkanObject(VK_OBJECT_TYPE_FRAMEBUFFER, (uint64_t)framebuffers[0], name + " FrameBuffer");
    }
    
}

std::vector<VkFramebuffer>& Pass::getFramebuffers()
{
    return framebuffers;
}

void Pass::cleanup()
{
    for (size_t i = 0; i < framebuffers.size(); i++) {
        vkDestroyFramebuffer(device.getLogicalDeviceHandle(), framebuffers[i], nullptr);
    }

    vkDestroyRenderPass(device.getLogicalDeviceHandle(), renderPassHandle, nullptr);
}
