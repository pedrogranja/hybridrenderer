#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(set = 1, binding = 0) uniform Material {
	int albedoTextureSet;
	int metallicRoughnessTextureSet;
	int normalTextureSet;
	int occlusionTextureSet;
	int emissiveTextureSet;
	float metallicFactor;
	float roughnessFactor;
	vec3 baseColor;
} mat;
layout(set = 1, binding = 1) uniform sampler2D texSampler;
layout(set = 1, binding = 2) uniform sampler2D normalSampler;
layout(set = 1, binding = 3) uniform sampler2D aoSampler;
layout(set = 1, binding = 4) uniform sampler2D emissionSampler;
layout(set = 1, binding = 5) uniform sampler2D metalRoughnessSampler;

struct PointLight {
    vec3 pos;
    vec3 lightColor;
    float lightIntensity;
};

layout(set = 2, binding = 0) uniform SceneParams {
    float maxLevelRoughnessMipMap;
    vec3 cameraPos;
    PointLight pointLights[1024];
    int numPointLights;
} sceneParams;
layout(set = 2, binding = 1) uniform sampler2D brdfLut;
layout(set = 2, binding = 2) uniform samplerCube cubeMapSampler;
layout(set = 2, binding = 3) uniform samplerCube irradianceCubemap;
layout(set = 2, binding = 4) uniform samplerCube mipMapRoughnessCubemap;

layout(set = 3, binding = 0) uniform UiShaderInputs {
	float indirectLightingWeight;
	bool directDiffuse;
	bool directSpecular;
	bool indirectDiffuse;
	bool indirectSpecular;
	bool useEmissionTexture;
	bool useRoughnessTexture;
	bool useMetalnessTexture;
	bool useAoTexture;
	bool useAlbedoTexture;
	bool useNormalTexture;
    int lightToDebug;
	bool fresnelFunctionOnly;
	bool uniformDistributionFunctionOnly;
	bool geometryFunctionOnly;
	float baseRoughness;
	float baseMetalness;
    float exposure;
} globalShaderInputs;

layout(location = 0) in vec4 fragPos;
layout(location = 1) in vec3 fragColor;
layout(location = 2) in vec3 fragNormal;
layout(location = 3) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 bloomColor;

const float PI = 3.14159265359;

float random(vec2 co)
{
	float a = 12.9898;
	float b = 78.233;
	float c = 43758.5453;
	float dt= dot(co.xy ,vec2(a,b));
	float sn= mod(dt,3.14);
	return fract(sin(sn) * c);
}

// From http://filmicgames.com/archives/75
// and sacha willems https://github.com/SaschaWillems/Vulkan/blob/master/data/shaders/glsl/pbrtexture/pbrtexture.frag
vec3 Uncharted2Tonemap(vec3 x)
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;
	return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

vec4 tonemap(vec4 color)
{
	vec3 outcol = Uncharted2Tonemap(color.rgb);
	outcol = outcol * (1.0f / Uncharted2Tonemap(vec3(11.2f)));
	return vec4(outcol,1.0);
}

vec2 hammersley2d(uint i, uint N) 
{
	// Radical inverse based on http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
	uint bits = (i << 16u) | (i >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	float rdi = float(bits) * 2.3283064365386963e-10;
	return vec2(float(i) /float(N), rdi);
}

float DistributionGGX(vec3 n, vec3 h, float roughness) {
    float a = roughness * roughness;
    float a2 = a * a;
    float nDotH = max(dot(n,h),0.0f);
    float nDotH2 = nDotH*nDotH;

    float numerator = a2;
    float denominator = (nDotH2 * (a2 - 1.0f) + 1.0f);
    denominator = PI * denominator * denominator;

    return numerator / denominator;
}

float smithLambda(vec3 w, vec3 n, float roughness) {
    float wDotN = dot(w,n);
    if (wDotN <= 0) 
        return 0;

    float wDotN2 = wDotN * wDotN;
    float tanSqrd = max((1-wDotN2),0.0f) / wDotN2;

    return 0.5f * (-1 + sqrt(1 + roughness * tanSqrd));
}

float SmithHeightCorrelated(vec3 w0, vec3 wi, vec3 n, float roughness) {
    float a = roughness * roughness;
    return 1/(1+smithLambda(w0,n,a)+smithLambda(wi,n,a));
}

vec3 FresnelSchlick(vec3 F0, float cosTheta) {
    float exponent = (-5.5547*cosTheta-6.98316) * cosTheta;
    return F0 + (1-F0) * pow(2, exponent);

    //return F0 + (1.0 - F0) * pow(1.0 - cosTheta,5.0);
}

vec3 FresnelSchlickRoughness(vec3 F0, float cosTheta, float roughness)
{
    float exponent = (-5.5547*cosTheta-6.98316) * cosTheta;
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(2, exponent);
    //return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}   

vec3 getIBL(vec3 n, vec3 reflection) {
    return vec3(0.0f);
}

vec3 getNormalFromTex() {
    vec3 tangentNormal = texture(normalSampler,fragTexCoord).rgb * 2.0 - 1.0;

    vec3 q1 = dFdx(fragPos).xyz;
	vec3 q2 = dFdy(fragPos).xyz;
	vec2 st1 = dFdx(fragTexCoord);
	vec2 st2 = dFdy(fragTexCoord);

    vec3 N = normalize(fragNormal);
	vec3 T = normalize(q1 * st2.t - q2 * st1.t);
	vec3 B = -normalize(cross(N, T));
	mat3 TBN = mat3(T, B, N);

	return normalize(TBN * tangentNormal);
}

void main() {

    //inputs
    //sample color texture
    vec3 albedo = mat.baseColor; 
    if(mat.albedoTextureSet > -1 && globalShaderInputs.useAlbedoTexture) {
        albedo = texture(texSampler,fragTexCoord).rgb * albedo;
    }

    //sample normal texture
    vec3 normal = normalize(fragNormal);
    if(mat.normalTextureSet > -1 && globalShaderInputs.useNormalTexture)
        normal = getNormalFromTex();

    //sample roughness metal texture
    float metal = mat.metallicFactor * globalShaderInputs.baseMetalness;
    float roughness = mat.roughnessFactor * globalShaderInputs.baseRoughness;
    if(mat.metallicRoughnessTextureSet > -1) {
        vec4 metalRoughness = texture(metalRoughnessSampler,fragTexCoord);

        if(globalShaderInputs.useRoughnessTexture)
            roughness = metalRoughness.g * roughness;
        
        if(globalShaderInputs.useMetalnessTexture)
            metal = metalRoughness.b * metal;
    }

    //sample occlusion texture
    float ao = 1.0f;
    if(mat.occlusionTextureSet > -1  && globalShaderInputs.useAoTexture) {
        ao = texture(aoSampler,fragTexCoord).r;
    }

    //debug parameters
    float debugNDF;
    float debugG;
    vec3 debugF;

    //sample emissive
    vec3 emissive = vec3(0.0f);
    if(mat.emissiveTextureSet > -1 && globalShaderInputs.useEmissionTexture) {
        emissive = texture(emissionSampler, fragTexCoord).rgb;
        if(emissive.x < 0.01f && emissive.y < 0.01f && emissive.z < 0.01f)
            emissive = vec3(0.0f);
    }

    vec3 finalColor = vec3(0.0f);
    vec3 w0 = normalize(sceneParams.cameraPos - fragPos.xyz);
    float w0DotN = max(dot(normal,w0),0.0f);

    //base reflectivity
    vec3 F0 = vec3(0.04f);
    F0 = mix(F0,albedo,metal);

    for(uint i = 0; i < sceneParams.numPointLights; i++) {
        //lights
        vec3 lightPos = sceneParams.pointLights[i].pos.rgb;
        vec3 lightColor = sceneParams.pointLights[i].lightColor.rgb;
        float lightIntensity = sceneParams.pointLights[i].lightIntensity;
    
        //lighting
        vec3 wi = lightPos - fragPos.xyz;
        float dist = length(wi);
        wi = normalize(lightPos);
        vec3 h = normalize(wi + w0);
        float nDotWi = max(dot(normal, wi), 0.0f);
        vec3 radiance = lightColor * nDotWi * 1/(dist*dist) * lightIntensity; //incoming light

        //Specular
        float wiDotN = max(dot(normal,wi),0.0f);
        //Normal distribution
        float NDF = DistributionGGX(normal, h, roughness);
        //Geometry Function
        float G = SmithHeightCorrelated(w0, wi, normal, roughness);
        //Fresnel
        vec3 F = FresnelSchlick(F0,clamp(dot(w0,h),0.0,1.0));

        if(globalShaderInputs.lightToDebug == i) {
            debugG = G;
            debugF = F;
            debugNDF = NDF;
        }

        vec3 numerator = NDF * G * F;
        float denominator = 4.0 * w0DotN * wiDotN;
        vec3 specular = numerator / max(denominator, 0.001f);

        //diffuse
        vec3 diffuse = albedo/PI;
        vec3 kD = vec3(1.0) - F;
        kD *= (1.0 - metal); //metalic surfaces have no diffuse reflections and thus reduce the diffuse due to metalness
    
        //if deactivated...
        if(!globalShaderInputs.directDiffuse)
            diffuse = vec3(0.0f);

        if(!globalShaderInputs.directSpecular)
            specular = vec3(0.0f);

        finalColor += (kD*diffuse + specular) * radiance * nDotWi + emissive;
    }

    finalColor /= sceneParams.numPointLights;
    finalColor = finalColor / (finalColor + vec3(1.0));
    finalColor = pow(finalColor, vec3(1.0/2.2));  

    //IBL
    vec3 reflection = reflect(-w0, normal);
    reflection.y *= -1.0f;
    vec3 FIndirect = FresnelSchlickRoughness(F0,max(dot(normal,w0),0.0), roughness);
    vec3 kD = vec3(1.0) - FIndirect;
    kD *= 1.0 - metal;	  
  
    
    //indirect diffuse
    vec3 irradiance = texture(irradianceCubemap, -normal).rgb;
    vec3 diffuse = irradiance * albedo;

    //indirect specular
    float maxLod = sceneParams.maxLevelRoughnessMipMap;
    vec3 prefilteredColor = textureLod(mipMapRoughnessCubemap, reflection, roughness * maxLod).rgb;
    vec2 brdf = texture(brdfLut, vec2(w0DotN, roughness)).rg;
    vec3 specular = prefilteredColor * ( FIndirect * brdf.x + brdf.y);

    //if deactivated...
    if(!globalShaderInputs.indirectDiffuse)
        diffuse = vec3(0.0f);

    if(!globalShaderInputs.indirectSpecular)
        specular = vec3(0.0f);

    finalColor += (kD * diffuse + specular) * ao * globalShaderInputs.indirectLightingWeight;

    if(globalShaderInputs.fresnelFunctionOnly)
        outColor = vec4(debugF+FIndirect,1.0f);

    else if(globalShaderInputs.uniformDistributionFunctionOnly)
        outColor = vec4(debugNDF,debugNDF,debugNDF,1.0f);

    else if(globalShaderInputs.geometryFunctionOnly)
        outColor = vec4(debugG,debugG,debugG,1.0f);

    else
        outColor = tonemap(vec4(finalColor * globalShaderInputs.exposure, 1.0f));

    float brightness = dot(outColor.rgb, vec3(0.2126, 0.7152, 0.0722));
    if(brightness > 1.0)
        bloomColor = vec4(outColor.rgb, 1.0);
    else
        bloomColor = vec4(0.0, 0.0, 0.0, 1.0);
    bloomColor += vec4(emissive,1.0f);
}