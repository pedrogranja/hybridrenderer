#pragma once

#include <tiny_gltf.h>
#include "utils.hpp"
#include <vector>

class GltfImporter;

#include "GltfImporter.h"
#include "TextureImage.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>
#include "config.hpp"
#include "Initializers2.hpp"
#include "SwapChain.h"
#include "UniformBuffer.h"
#include <chrono>
#include "TextureCubeMap.h"
#include "Camera.h"
#include "DescriptorSet.h"
#include "VulkanInstance.h"
#include "BottomLevelAS.h"
#include "TopLevelAS.h"
#include <unordered_map>
#include <glm/gtx/string_cast.hpp>
#include "imgui_internal.h"

struct UniformBufferObject {

	alignas(16) glm::mat4 model;
	alignas(16) glm::mat4 view;
	alignas(16) glm::mat4 proj;

	alignas(16) glm::mat4 prevModel;
	alignas(16) glm::mat4 prevView;
	alignas(16) glm::mat4 prevProj;

	alignas(4) int id;
	alignas(8) glm::vec2 jitter;
};

struct Material {
	std::string name;
	int albedoTextureIndex;
	int normalTextureIndex;
	int aoTextureIndex;
	int emissionTextureIndex;
	int metalRoughnessTextureIndex;
	glm::vec3 baseColor;
	float metallicFactor;
	float roughnessFactor;
	bool inDeviceMemory;

	struct RaytracingMaterial {
		alignas(4) int albedoTextureIndex;
		alignas(4) int normalTextureIndex;
		alignas(4) int aoTextureIndex;
		alignas(4) int emissionTextureIndex;
		alignas(4) int metalRoughnessTextureIndex;
		alignas(16) glm::vec3 baseColor;
		alignas(4) float metallicFactor;
		alignas(4) float roughnessFactor;
	} raytracingMaterials;

	struct MaterialParams {
		alignas(4) int albedoTextureSet = -1;
		alignas(4) int metallicRoughnessTextureSet = -1;
		alignas(4) int normalTextureSet = -1;
		alignas(4) int occlusionTextureSet = -1;
		alignas(4) int emissiveTextureSet = -1;
		alignas(4) float metallicFactor;
		alignas(4) float roughnessFactor;
		alignas(16) glm::vec3 baseColor;
		alignas(16) glm::vec3 baseEmissivity;
	} matParams;

	struct ImGuiDisplay {
		bool albedoTextureSet = true;
		bool metallicRoughnessTextureSet = true;
		bool normalTextureSet = true;
		bool occlusionTextureSet = true;
		bool emissiveTextureSet = true;
		float metallicFactor = 1.0f;
		float roughnessFactor = 1.0f;
		glm::vec3 baseColor;
	} imGuiDisplay;

	std::vector<TextureImage*>* textures;
	/*
	VkDescriptorSetLayout materialDescriptorLayout;
	VkDescriptorSet materialDescriptor;
	*/
	UniformBuffer uniformBuffer;	//material buffer
	DescriptorSet descriptorSet;

	Material();
	Material(std::string name,
			 int albedoTextureIndex,
			 int normalTextureIndex,
			 int aoTextureIndex,
			 int emissionTextureIndex,
			 int metalRoughnessTextureIndex,
			 glm::vec3 baseColor,
		 	 float metallicFactor,
			 float roughnessFactor,
			 std::vector<TextureImage*>& textures);
	void cleanup();
	TextureImage* getAlbedoTexture();
	TextureImage* getNormalTexture();
	TextureImage* getAoTexture();
	TextureImage* getEmissionTexture();
	TextureImage* getMetalRoughnessTexture();

	void renderUI(int i) {
		if (ImGui::TreeNode(name.c_str())) {
			ImGui::DragFloat("Metallic Factor", &imGuiDisplay.metallicFactor, 0.05f, 0.0f, 1.0f);
			ImGui::DragFloat("Roughness Factor", &imGuiDisplay.roughnessFactor, 0.05f, 0.0f, 1.0f);
			ImGui::ColorEdit3("Base Color", &imGuiDisplay.baseColor[0]);
			if (albedoTextureIndex > -1)
				ImGui::Checkbox("Use Albedo Texture", &imGuiDisplay.albedoTextureSet);
			if (emissionTextureIndex > -1)
				ImGui::Checkbox("Use Emissive Texture", &imGuiDisplay.emissiveTextureSet);
			if (metalRoughnessTextureIndex > -1)
				ImGui::Checkbox("Use Metallic Roughness Texture", &imGuiDisplay.metallicRoughnessTextureSet);
			if (normalTextureIndex > -1)
				ImGui::Checkbox("Use Normal Texture", &imGuiDisplay.normalTextureSet);
			if (aoTextureIndex > -1)
				ImGui::Checkbox("Use Occlusion Texture", &imGuiDisplay.occlusionTextureSet);

			ImGui::TreePop();
		}

		//metalic factor
		matParams.metallicFactor = imGuiDisplay.metallicFactor;
		raytracingMaterials.metallicFactor = imGuiDisplay.metallicFactor;

		//roughness
		matParams.roughnessFactor = imGuiDisplay.roughnessFactor;
		raytracingMaterials.roughnessFactor = imGuiDisplay.roughnessFactor;
		
		//albedo texture
		matParams.albedoTextureSet = imGuiDisplay.albedoTextureSet ? albedoTextureIndex : -1;
		raytracingMaterials.albedoTextureIndex = imGuiDisplay.albedoTextureSet ? albedoTextureIndex : -1;
		
		//normal texture
		matParams.normalTextureSet = imGuiDisplay.normalTextureSet ? normalTextureIndex : -1;
		raytracingMaterials.normalTextureIndex = imGuiDisplay.normalTextureSet ? normalTextureIndex : -1;
		
		//occlusion texture
		matParams.occlusionTextureSet = imGuiDisplay.occlusionTextureSet ? aoTextureIndex : -1;
		raytracingMaterials.aoTextureIndex = imGuiDisplay.occlusionTextureSet ? aoTextureIndex : -1;
		
		//metallic roughness texture
		matParams.metallicRoughnessTextureSet = imGuiDisplay.metallicRoughnessTextureSet ? metalRoughnessTextureIndex : -1;
		raytracingMaterials.metalRoughnessTextureIndex = imGuiDisplay.metallicRoughnessTextureSet ? metalRoughnessTextureIndex : -1;
		
		//emissive texture
		matParams.emissiveTextureSet = imGuiDisplay.emissiveTextureSet ? emissionTextureIndex : -1;
		raytracingMaterials.emissionTextureIndex = imGuiDisplay.emissiveTextureSet ? emissionTextureIndex : -1;

		matParams.baseColor = imGuiDisplay.baseColor;
		raytracingMaterials.baseColor = imGuiDisplay.baseColor;
		/*
		ImGui::Checkbox("Use Simple Accumulation", &toSubmit.useSimpleAccumulation);
		ImGui::DragFloat("Clip Control", &(toSubmit.clipControl), 0.05f, 0.0f, 1.0f);
		*/
	}
};

struct Primitive {
	uint32_t firstIndex;
	uint32_t indexCount;
	uint32_t firstVertex;
	uint32_t vertexCount;
	int materialIndex;
	uint32_t id;
	std::vector<Material*>* materials;
	BottomLevelAS* bottomLevelAs;

	Primitive(uint32_t firstIndex,
			  uint32_t indexCount, 
		      uint32_t firstVertex,
			  uint32_t vertexCount,
			  int materialIndex,
		      int customId,
			  std::vector<Material*>* materials);
	Material* getMaterial();
	void cleanup();
};

struct Mesh {
	std::vector<Primitive*> primitives;
	void cleanup();
};


struct SceneNode {
	struct SceneNodeRaytracing {
		alignas(16) glm::mat4 model;
		alignas(16) glm::mat4 modelInverted;
	};

	std::string name;
	SceneNode* parent;
	uint32_t nodeIndex;
	std::vector<SceneNode*> children;
	Mesh* mesh;
	int id;
	bool active = true;

	glm::mat4 prevModel;
	glm::mat4 prevView;
	glm::mat4 prevProj;

	glm::vec3 translation{};
	glm::quat rotation{};
	glm::vec3 scale = glm::vec3(1.0f);

	glm::vec3 initialTranslation{};
	glm::quat initialRotation{};
	glm::vec3 initialScale = glm::vec3(1.0f);
	
	UniformBufferObject mvp;
	UniformBuffer uniformBuffer;	//multiple buffers to avoid reading when updating data
	DescriptorSet descriptorSet;

	SceneNode(std::string name, SceneNode* parent, uint32_t nodeIndex);
	glm::mat4 getLocalMatrix();
	glm::mat4 getWorldMatrix();
	void addChild(SceneNode* child);
	void cleanup();
};

struct Sun {
	alignas(16) glm::vec4 dir;
	alignas(16) glm::vec4 lightColor;
	alignas(4) float lightIntensity = 1.0f;

	Sun() {};
	Sun(glm::vec3 dir, glm::vec3 color)
	{
		this->dir = glm::vec4(glm::normalize(dir), 0.0f);
		this->lightColor = glm::vec4(color, 0.0f);
	}
};

struct Scene : public UIRenderable {
	VulkanDevice device;
	VulkanInstance instance;
	VkCommandPool commandPool;
	VkQueue graphicsQueue;
	SwapChain* swapChain;
	TextureCubeMap textureCubeMap;

	std::vector<SceneNode*> sceneNodes;
	std::vector<SceneNode*> sceneNodesList; // List of all scene Nodes
	std::vector<uint32_t> indices;
	std::vector<uint32_t> indicesOffsets; //Offsets between each primitive index start
	std::vector<utils::Vertex> vertices;
	std::vector<TextureImage*> textures;
	std::vector<Material*> materials;
	std::vector<SceneNode::SceneNodeRaytracing> sceneNodesRaytracingData;

	//Statistics
	int numNodes = 0;
	int numMaterials = 0;
	int numTexturesUsed = 0;
	int numMeshes = 0;
	int numPrimitives = 0;

	VertexBuffer vertexBuffer;
	IndexBuffer indexBuffer;

	Buffer sceneNodeDataBuffer, sceneNodeDataStagingBuffer;
	Buffer indexOffsetBuffer, indexOffsetStagingBuffer;
	Buffer materialBuffer, materialStagingBuffer;

	std::vector<Sun> suns;
	TopLevelAS* topLevelAs;

	//descriptor sets
	std::vector<VkDescriptorSetLayout> descriptorLayouts;
	//VkDescriptorSetLayout sceneLayout;
	//VkDescriptorSet sceneDescriptor;
	Camera* camera;
	uint32_t maxLevelRoughnessMipMap;
	bool storePrevMVP;

	virtual void renderUI() {
		ImGui::Begin("Materials");
		for (int i = 0; i < materials.size(); i++) {
			materials[i]->renderUI(i);
		}
		ImGui::End();
	};

	Scene();
	Scene(VulkanDevice& device,
		  VulkanInstance& instance,
		  VkCommandPool commandPool,
		  VkQueue graphicsQueue, 
		  SwapChain* swapChain,
		  Camera* camera,
		  uint32_t maxLevelRoughnessMipMap,
		  bool storePrevMVP = false);

	void loadScene(std::string filename, GltfImporter* gltfLoader);
	void loadCubeMap(std::string filename);
	void ready();
	void submitSceneData();
	void processScene();
	void processScene(SceneNode* node);
	void createRaytracingStructures();
	void createRaytracingStructures(SceneNode* node);
	void createDescriptorSets();
	void createNodeDescriptorSets(SceneNode* node);
	//update the scene
	void update(int currentImage);
	void update(SceneNode* sceneNode, int currentImage);
	//draws the scene
	void drawScene(VkCommandBuffer drawCommandbuffer, VkPipelineLayout pipelineLayout, int currentImage);
	void drawSceneNode(SceneNode& sceneNode, 
					   VkCommandBuffer drawCommandbuffer, 
					   VkPipelineLayout pipelineLayout, 
					   int currentImage);
	//cleanup
	void cleanup();
};