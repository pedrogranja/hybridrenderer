#include "VertexBuffer.h"

VertexBuffer::VertexBuffer()
{
}

VertexBuffer::VertexBuffer(VulkanDevice& device, 
                           VkCommandPool commandPool, 
                           int size) : device(device),
                                       commandPool(commandPool),
                                       size(size)
{

    utils::createBuffer(device.getLogicalDeviceHandle(),
                        device.getPhysicalDeviceHandle(),
                        size,
                        VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
                        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
                        stagingBuffer,
                        stagingMemory);

    utils::createBuffer(device.getLogicalDeviceHandle(),
                        device.getPhysicalDeviceHandle(),
                        size,
                        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                        vertexBuffer,
                        vertexMemory);

    devicePntr = utils::getBufferDeviceAddress(vertexBuffer, device.getLogicalDeviceHandle());
}

void VertexBuffer::submitDataToStageBuffer(std::vector<utils::Vertex> vertices)
{
    void* data;
    vkMapMemory(device.getLogicalDeviceHandle(), stagingMemory, 0, this->size, 0, &data);
        memcpy(data, vertices.data(), (size_t)this->size);
    vkUnmapMemory(device.getLogicalDeviceHandle(), stagingMemory);
}

void VertexBuffer::submitDataToDevice()
{
    utils::copyBuffer(device.getLogicalDeviceHandle(), device.getPhysicalDeviceHandle(), device.getQueues().graphicsQueue, commandPool, stagingBuffer, vertexBuffer, size);
    vkDestroyBuffer(device.getLogicalDeviceHandle(), stagingBuffer, nullptr);
    vkFreeMemory(device.getLogicalDeviceHandle(), stagingMemory, nullptr);
}

void VertexBuffer::cleanup()
{
    vkDestroyBuffer(device.getLogicalDeviceHandle(), vertexBuffer, nullptr);
    vkFreeMemory(device.getLogicalDeviceHandle(), vertexMemory, nullptr);
}
