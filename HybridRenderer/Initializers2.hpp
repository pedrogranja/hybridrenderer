#pragma once
#include "vulkan/vulkan.h"

namespace initializers {
    inline VkApplicationInfo applicationInfo() {

    }

    inline VkImageViewCreateInfo imageViewCreateInfo(VkImage image, VkFormat format)
    {
        VkImageViewCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = image;
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = format;
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;
        return createInfo;
    }

    inline VkRayTracingShaderGroupCreateInfoKHR createShaderGroup(uint32_t shaderIndex) {
        VkRayTracingShaderGroupCreateInfoKHR groupCI = {};
        groupCI.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
        groupCI.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
        groupCI.generalShader = shaderIndex;
        groupCI.closestHitShader = VK_SHADER_UNUSED_KHR;
        groupCI.anyHitShader = VK_SHADER_UNUSED_KHR;
        groupCI.intersectionShader = VK_SHADER_UNUSED_KHR;

        return groupCI;
    }

    inline VkPipelineShaderStageCreateInfo shaderCreateInfo(VkShaderModule vertShaderModule, VkShaderStageFlagBits stage) {
        VkPipelineShaderStageCreateInfo vertShaderStageInfo{};
        vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        vertShaderStageInfo.stage = stage;
        vertShaderStageInfo.module = vertShaderModule;
        vertShaderStageInfo.pName = "main";
        return vertShaderStageInfo;
    }

    inline VkPipelineShaderStageCreateInfo vertexShaderCreateInfo(VkShaderModule vertShaderModule) {
        VkPipelineShaderStageCreateInfo vertShaderStageInfo{};
        vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
        vertShaderStageInfo.module = vertShaderModule;
        vertShaderStageInfo.pName = "main";
        return vertShaderStageInfo;
    }

    inline VkPipelineShaderStageCreateInfo fragShaderCreateInfo(VkShaderModule fragShaderModule) {
        VkPipelineShaderStageCreateInfo fragShaderStageInfo{};
        fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        fragShaderStageInfo.module = fragShaderModule;
        fragShaderStageInfo.pName = "main";
        return fragShaderStageInfo;
    }

    inline VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo() {
        VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
        inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        inputAssembly.primitiveRestartEnable = VK_FALSE;
        return inputAssembly;
    }

    inline VkViewport viewport(float width, float height) {
        VkViewport viewport{};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = width;
        viewport.height = height;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;
        return viewport;
    }

    inline VkRect2D scissor(VkExtent2D extent) {
        VkRect2D scissor{};
        scissor.offset = { 0, 0 };
        scissor.extent = extent;
        return scissor;
    }

    inline VkPipelineViewportStateCreateInfo pipelineViewportStateCreateInfo(VkViewport viewport, VkRect2D scissor) {
        VkPipelineViewportStateCreateInfo viewportState{};
        viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportState.viewportCount = 1;
        viewportState.pViewports = &viewport;
        viewportState.scissorCount = 1;
        viewportState.pScissors = &scissor;
        return viewportState;
    }

    inline VkPipelineRasterizationStateCreateInfo pipelineRasterizationStateCreateInfo(VkPolygonMode polygonMode = VK_POLYGON_MODE_FILL,
                                                                                        VkCullModeFlags cullMode = VK_CULL_MODE_BACK_BIT,
                                                                                        VkFrontFace frontFace = VK_FRONT_FACE_CLOCKWISE) {
        VkPipelineRasterizationStateCreateInfo rasterizer{};
        rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizer.depthClampEnable = VK_FALSE;
        rasterizer.rasterizerDiscardEnable = VK_FALSE;
        rasterizer.polygonMode = polygonMode;
        rasterizer.lineWidth = 1.0f;
        rasterizer.cullMode = cullMode;
        rasterizer.frontFace = frontFace;
        rasterizer.depthBiasEnable = VK_FALSE;
        return rasterizer;
    }

    inline VkPipelineMultisampleStateCreateInfo pipelineMultisampleStateCreateInfo(VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_1_BIT) {
        VkPipelineMultisampleStateCreateInfo multisampling{};
        multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampling.sampleShadingEnable = VK_FALSE;
        multisampling.rasterizationSamples = sampleCount;
        return multisampling;
    }

    inline VkPipelineColorBlendAttachmentState pipelineColorBlendAttachmentState(VkColorComponentFlags writeMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT) {
        VkPipelineColorBlendAttachmentState colorBlendAttachment{};
        colorBlendAttachment.colorWriteMask = writeMask;
        colorBlendAttachment.blendEnable = VK_FALSE;
        return colorBlendAttachment;
    }

    inline VkPipelineColorBlendStateCreateInfo pipelineColorBlendStateCreateInfo(VkPipelineColorBlendAttachmentState* colorBlendAttachments, uint32_t attachmentCount = 1) {
        VkPipelineColorBlendStateCreateInfo colorBlending{};
        colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlending.logicOpEnable = VK_FALSE;
        colorBlending.logicOp = VK_LOGIC_OP_COPY;
        colorBlending.attachmentCount = attachmentCount;
        colorBlending.pAttachments = colorBlendAttachments;
        colorBlending.blendConstants[0] = 0.0f;
        colorBlending.blendConstants[1] = 0.0f;
        colorBlending.blendConstants[2] = 0.0f;
        colorBlending.blendConstants[3] = 0.0f;
        return colorBlending;
    }

    inline VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo(uint32_t layoutCount = 0, 
                                                               VkDescriptorSetLayout* descriptorSetLayout = nullptr,  
                                                               uint32_t pushConstantCount = 0) {
        VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
        pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutInfo.setLayoutCount = layoutCount;
        pipelineLayoutInfo.pushConstantRangeCount = pushConstantCount;
        pipelineLayoutInfo.pSetLayouts = descriptorSetLayout;
        return pipelineLayoutInfo;
    }

    inline VkAttachmentDescription colorAttachmentDescription(VkImageLayout initialLayout, 
                                                              VkImageLayout finalLayout, 
                                                              VkFormat format,
                                                              VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_1_BIT,
                                                              VkAttachmentLoadOp loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR) {
        VkAttachmentDescription colorAttachment{};
        colorAttachment.format = format;
        colorAttachment.samples = sampleCount;
        colorAttachment.loadOp = loadOp;
        colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachment.initialLayout = initialLayout;
        colorAttachment.finalLayout = finalLayout;
        return colorAttachment;
    }


    inline VkAttachmentReference attachmentRef(int attachment, VkImageLayout layout) {
        VkAttachmentReference colorAttachmentRef{};
        colorAttachmentRef.attachment = attachment;
        colorAttachmentRef.layout = layout;
        return colorAttachmentRef;
    }

    inline VkAttachmentReference colorAttachmentRef(int attachment) {
        VkAttachmentReference colorAttachmentRef{};
        colorAttachmentRef.attachment = attachment;
        colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        return colorAttachmentRef;
    }

    inline VkAttachmentDescription depthAttachmentDescription(VkImageLayout initialLayout,
                                                            VkImageLayout finalLayout,
                                                            VkFormat format,
                                                            VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_1_BIT) {
        VkAttachmentDescription colorAttachment{};
        colorAttachment.format = format;
        colorAttachment.samples = sampleCount;
        colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachment.initialLayout = initialLayout;
        colorAttachment.finalLayout = finalLayout;
        return colorAttachment;
    }

    inline VkAttachmentReference depthAttachmentRef(int attachment) {
        VkAttachmentReference depthAttachmentRef{};
        depthAttachmentRef.attachment = attachment;
        depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        return depthAttachmentRef;
    }

    inline VkSubpassDependency subpassDependency(uint32_t srcSubpass,
                                                 uint32_t dstSubpass,
                                                 VkPipelineStageFlags srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                                 VkAccessFlags srcAccessMask = 0,
                                                 VkPipelineStageFlags dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                                 VkPipelineStageFlags dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT)
    {
        VkSubpassDependency dependency{};
        dependency.srcSubpass = srcSubpass;
        dependency.dstSubpass = dstSubpass;
        dependency.srcStageMask = srcStageMask;
        dependency.srcAccessMask = srcAccessMask;
        dependency.dstStageMask = dstStageMask;
        dependency.dstAccessMask = dstAccessMask;
        return dependency;
    }

    inline VkFramebufferCreateInfo framebufferCreateInfo(VkRenderPass renderPass, int attachmentCount, VkImageView* attachments, uint32_t width, uint32_t height) {
        VkFramebufferCreateInfo framebufferInfo{};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = renderPass;
        framebufferInfo.attachmentCount = attachmentCount;
        framebufferInfo.pAttachments = attachments;
        framebufferInfo.width = width;
        framebufferInfo.height = height;
        framebufferInfo.layers = 1;
        return framebufferInfo;
    }

    inline VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo(std::vector<VkVertexInputBindingDescription>& bindingDescription, 
                                                                                   std::vector<VkVertexInputAttributeDescription>& attributeDescriptions) {
        VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
        vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindingDescription.size());
        vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
        vertexInputInfo.pVertexBindingDescriptions = bindingDescription.data();
        vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
        return vertexInputInfo;
    }

    inline VkPipelineDepthStencilStateCreateInfo pipelineDepthStencilStateCreateInfo(VkCompareOp depthCompareOp = VK_COMPARE_OP_LESS) {
        VkPipelineDepthStencilStateCreateInfo depthStencil{};
        depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthStencil.depthTestEnable = VK_TRUE;
        depthStencil.depthWriteEnable = VK_TRUE;
        depthStencil.depthCompareOp = depthCompareOp;
        depthStencil.depthBoundsTestEnable = VK_FALSE;
        depthStencil.minDepthBounds = 0.0f; // Optional
        depthStencil.maxDepthBounds = 1.0f; // Optional
        depthStencil.stencilTestEnable = VK_FALSE;
        return depthStencil;
    }

    inline VkDescriptorSetLayoutBinding descriptorSetLayoutBinding(int binding, 
                                                                   VkDescriptorType descriptorType, 
                                                                   VkShaderStageFlags shaderStage, 
                                                                   int descriptorCount = 1) {
        VkDescriptorSetLayoutBinding layoutBinding{};
        layoutBinding.binding = binding;
        layoutBinding.descriptorType = descriptorType;
        layoutBinding.descriptorCount = descriptorCount;
        layoutBinding.stageFlags = shaderStage;
        layoutBinding.pImmutableSamplers = nullptr;
        return layoutBinding;
    }

    inline VkImageMemoryBarrier imageMemoryBarrier()
    {
        VkImageMemoryBarrier imageMemoryBarrier{};
        imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        return imageMemoryBarrier;
    }

    inline VkImageCreateInfo imageCreateInfo()
    {
        VkImageCreateInfo imageCreateInfo{};
        imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        return imageCreateInfo;
    }

    inline VkMemoryAllocateInfo memoryAllocateInfo()
    {
        VkMemoryAllocateInfo memAllocInfo{};
        memAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        return memAllocInfo;
    }
};