#pragma once
#include "VulkanDevice.h"
#include "utils.hpp"
#include "RaytracingUtils.hpp"

class VertexBuffer
{
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingMemory;

public:
	
	VulkanDevice device;
	VkCommandPool commandPool;
	int size;
	VkBuffer vertexBuffer;
	VkDeviceMemory vertexMemory;
	uint64_t devicePntr;

	VertexBuffer();
	VertexBuffer(VulkanDevice& device, VkCommandPool commandPool, int size);
	void submitDataToStageBuffer(std::vector<utils::Vertex> vertices);
	void submitDataToDevice();
	void cleanup();
};