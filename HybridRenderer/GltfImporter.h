#pragma once

#include <tiny_gltf.h>
#include "utils.hpp"

//forward declaration to avoid circular dependency
struct Material;
struct Primitive;
struct Mesh;
struct SceneNode;
struct Scene;

#include "SceneGraph.h"
#include "TextureImage.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>
#include "VulkanDevice.h"
#include <algorithm>

static int test = 0;

class GltfImporter
{
public:
	tinygltf::Model gltfModel;
	tinygltf::TinyGLTF gltfContext;
	int currentSceneMaterialOffset = 0;
	int currentTexOffset = 0;
	int primitiveId = 0;

	SceneNode* loadNode(SceneNode* parentNode,
						const tinygltf::Node& node,
						uint32_t nodeIndex,
						std::vector<utils::Vertex>& vertices,
						std::vector<uint32_t>& indices,
						std::vector<uint32_t>& indicesOffsets,
						std::vector<Material*>* materials);

	TextureImage* importTexture(VulkanDevice& device,
								VkCommandPool commandPool,
								tinygltf::Image& gltfimage,
								bool gammaCorrect = false);

	void loadTextures(VulkanDevice& device,
					  VkCommandPool commandPool,
					  std::vector<TextureImage*>& textures);

	void loadMaterials(VulkanDevice& device,
					   VkCommandPool commandPool,
					   std::vector<TextureImage*>& textures,
					   std::vector<Material*>& materials);

	SceneNode* loadFromFile(VulkanDevice& device,
							VkCommandPool commandPool,
							std::string filename,
							std::vector<utils::Vertex>& vertices,
							std::vector<uint32_t>& indices,
							std::vector<uint32_t>& indicesOffsets);

	SceneNode* loadFromFile(VulkanDevice& device,
							VkCommandPool commandPool,
							std::string filename,
							std::vector<utils::Vertex>& vertices,
							std::vector<uint32_t>& indices,
							std::vector<uint32_t>& indicesOffsets,
							std::vector<TextureImage*>& textures,
							std::vector<Material*>& materials);
};