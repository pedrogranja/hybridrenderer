#include "BlurPostProcessing.h"

BlurPostProcessing::BlurPostProcessing()
{
}

BlurPostProcessing::BlurPostProcessing(VkPhysicalDevice physicalDevice,
										VkDevice device,
										VkQueue graphicsQueue,
										utils::QueueFamilyIndices& queueFamilyIndices,
										SwapChain& swapChain,
										Pass& renderpass,
										TextureImage& previousImage,
										VertexFragPipeline* pipeline,
										bool blurHor) : physicalDevice(physicalDevice),
														device(device),
														graphicsQueue(graphicsQueue),
														queueFamilyIndices(queueFamilyIndices),
														swapChain(swapChain),
														renderpass(renderpass),
														previousImage(previousImage),
														pipeline(pipeline)
{
	blurParams.blurHorizontal = static_cast<int>(blurHor);
}

void BlurPostProcessing::createCommandPool()
{
	VkCommandPoolCreateInfo poolInfo{};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	VK_CHECK_RESULT(vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool));
}

void BlurPostProcessing::createDescriptorPool()
{
	std::array<VkDescriptorPoolSize, 2> poolSizes{};
	poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[0].descriptorCount = swapChain.getSize();
	poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSizes[1].descriptorCount = swapChain.getSize();

	VkDescriptorPoolCreateInfo poolInfo{};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = static_cast<uint32_t>(swapChain.getSize());

	VK_CHECK_RESULT(vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptorPool));
}

void BlurPostProcessing::initUniformBuffers()
{
	uniformBuffers.resize(swapChain.getSize());
	for (int i = 0; i < swapChain.getSize(); i++) {
		uniformBuffers[i].init(physicalDevice, device, commandPool, graphicsQueue, sizeof(BlurParams));
	}
}

void BlurPostProcessing::createDescriptorSetLayout()
{
	std::vector<VkDescriptorSetLayoutBinding> bindings;
	VkDescriptorSetLayoutBinding binding = {};

	//Define the descriptor set layout
	binding = initializers::descriptorSetLayoutBinding(0,
													   VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
													   VK_SHADER_STAGE_FRAGMENT_BIT);

	bindings.push_back(binding);

	binding = initializers::descriptorSetLayoutBinding(1,
													   VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
													   VK_SHADER_STAGE_FRAGMENT_BIT);

	bindings.push_back(binding);

	VkDescriptorSetLayoutCreateInfo layoutInfo{};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = bindings.size();
	layoutInfo.pBindings = bindings.data();

	VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &descriptorLayout));
}

void BlurPostProcessing::createDescriptorSet()
{
	int descriptorCount = swapChain.getSize();
	std::vector<VkDescriptorSetLayout> uniformLayouts(descriptorCount, descriptorLayout);

	//Create the descriptor set
	VkDescriptorSetAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>(descriptorCount);
	allocInfo.pSetLayouts = uniformLayouts.data();


	descriptorSets.resize(descriptorCount);
	VK_CHECK_RESULT(vkAllocateDescriptorSets(device, &allocInfo, descriptorSets.data()));

	std::vector<VkWriteDescriptorSet> descriptorWrites;
	VkWriteDescriptorSet writeDescriptorSet = {};
	VkDescriptorImageInfo imageInfo{};

	for (int i = 0; i < descriptorCount; i++) {

		VkDescriptorBufferInfo bufferInfo{};
		bufferInfo.buffer = uniformBuffers[i].getBuffer();
		bufferInfo.offset = 0;
		bufferInfo.range = uniformBuffers[i].size;

		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.dstSet = descriptorSets[i];
		writeDescriptorSet.dstBinding = 0;
		writeDescriptorSet.dstArrayElement = 0;
		writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		writeDescriptorSet.descriptorCount = 1;
		writeDescriptorSet.pBufferInfo = &bufferInfo;
		descriptorWrites.push_back(writeDescriptorSet);

		VkDescriptorImageInfo imageInfo{};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = previousImage.textureImageView;
		imageInfo.sampler = previousImage.textureSampler;

		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.dstSet = descriptorSets[i];
		writeDescriptorSet.dstBinding = 1;
		writeDescriptorSet.dstArrayElement = 0;
		writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		writeDescriptorSet.descriptorCount = 1;
		writeDescriptorSet.pImageInfo = &imageInfo;
		descriptorWrites.push_back(writeDescriptorSet);

		vkUpdateDescriptorSets(device, descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);
	}
}

void BlurPostProcessing::update(uint32_t imageIndex)
{
	uniformBuffers[imageIndex].submitDataToMemory(&blurParams);
}

void BlurPostProcessing::draw(VkCommandBuffer commandBuffer, uint32_t imageIndex)
{
	vkCmdBindDescriptorSets(commandBuffer,
							VK_PIPELINE_BIND_POINT_GRAPHICS,
							pipeline->getPipelineLayout(),
							0,
							1,
							&descriptorSets[imageIndex],
							0,
							nullptr);

	vkCmdDraw(commandBuffer, 3, 1, 0, 0);
}

void BlurPostProcessing::cleanup()
{
	vkDestroyDescriptorPool(device, descriptorPool, nullptr);

	for (int i = 0; i < uniformBuffers.size(); i++) {
		uniformBuffers[i].cleanup();
	}

	vkDestroyDescriptorSetLayout(device, descriptorLayout, nullptr);
	vkDestroyCommandPool(device, commandPool, nullptr);
}