#pragma once
#include "VulkanDevice.h"
#include "RaytracingUtils.hpp"
#include "RayTracingScratchBuffer.h"
#include "BottomLevelAS.h"
#include "Buffer.h"

class TopLevelAS
{
private:
	VulkanDevice device;
	VulkanInstance instance;
	VkCommandPool commandPool;

	std::vector<BottomLevelAS*> bottomLevelASs;

	VkAccelerationStructureKHR accelerationStructure;
	uint64_t topLevelAsDeviceHandle;
	Buffer tlasBuffer;
	RayTracingScratchBuffer scratchBuffer;

	Buffer instancesBuffer;
	uint64_t instancesBufferHandle;

public:
	TopLevelAS();
	TopLevelAS(VulkanDevice& device, VulkanInstance& instance, VkCommandPool& commandPool);
	void addBlas(BottomLevelAS& bottomLevelAS);
	void createTopLevelAS();
	void update();
	void cleanup();
	VkAccelerationStructureKHR& getAccelerationStructure();
};