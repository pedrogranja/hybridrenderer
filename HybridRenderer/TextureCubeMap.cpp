#include "TextureCubeMap.h"

TextureCubeMap::TextureCubeMap() : stbiDelete(false),
                                   stageBuffersDeleted(false),
                                   inDeviceMemory(false)
{
}

TextureCubeMap::TextureCubeMap(VulkanDevice& device,
                               VkCommandPool commandPool,
                               VkFormat format,
                               int texWidth,
                               int texHeight,
                               int texChannels,
                               int mipLevels) : device(device), 
                                                commandPool(commandPool),
                                                texWidth(texWidth),
                                                texHeight(texHeight), 
                                                texChannels(texChannels), 
                                                format(format), 
                                                mipLevels(mipLevels),
                                                stbiDelete(false),
                                                stageBuffersDeleted(false),
                                                inDeviceMemory(false)
{
    utils::createImage(device.getLogicalDeviceHandle(),
                       device.getPhysicalDeviceHandle(),
                       texWidth,
                       texHeight,
                       this->format,
                       VK_IMAGE_TILING_OPTIMAL,
                       VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                       VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                       textureImage,
                       textureImageMemory,
                       6,
                       VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT,
                       mipLevels);

    utils::createImageView(device.getLogicalDeviceHandle(),
                           textureImage,
                           this->format,
                           VK_IMAGE_ASPECT_COLOR_BIT,
                           textureImageView,
                           VK_IMAGE_VIEW_TYPE_CUBE,
                           6,
                           mipLevels);

    utils::createTextureSampler(device.getLogicalDeviceHandle(), VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE, textureSampler, mipLevels);
}

TextureCubeMap::TextureCubeMap(VulkanDevice& device,
                               VkCommandPool commandPool,
                               const char* imageFile) : device(device), 
                                                        commandPool(commandPool),
                                                        format(VK_FORMAT_R16G16B16A16_SFLOAT),
                                                        stbiDelete(false), 
                                                        stageBuffersDeleted(false),
                                                        inDeviceMemory(false)
{
    importCubeMap(imageFile);

    utils::createBuffer(device.getLogicalDeviceHandle(), 
                        device.getPhysicalDeviceHandle(), 
                        imageSize,
                        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                        stagingBuffer,
                        stagingMemory);

    utils::createImage(device.getLogicalDeviceHandle(),
                        device.getPhysicalDeviceHandle(),
                        texWidth,
                        texHeight,
                        this->format,
                        VK_IMAGE_TILING_OPTIMAL,
                        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                        textureImage,
                        textureImageMemory,
                        6,
                        VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT,
                        mipLevels);

     utils::createImageView(device.getLogicalDeviceHandle(),
                            textureImage,
                            this->format,
                            VK_IMAGE_ASPECT_COLOR_BIT,
                            textureImageView,
                            VK_IMAGE_VIEW_TYPE_CUBE, 
                            6,
                            mipLevels);

    utils::createTextureSampler(device.getLogicalDeviceHandle(), VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER, textureSampler, mipLevels);
}

void TextureCubeMap::importCubeMap(const char* imageFile)
{
    this->texCube = gli::texture_cube(gli::load(imageFile));
    assert(!texCube.empty());

    texWidth = static_cast<uint32_t>(texCube.extent().x);
    texHeight = static_cast<uint32_t>(texCube.extent().y);
    imageSize = texCube.size();
    mipLevels = static_cast<int>(texCube.levels());
}

VkDescriptorSetLayout TextureCubeMap::createDescriptorLayout()
{
    VkDescriptorSetLayout descriptorLayout;
    std::vector<VkDescriptorSetLayoutBinding> bindings;
    VkDescriptorSetLayoutBinding binding = {};

    binding = initializers::descriptorSetLayoutBinding(0,
                                                    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                                                    VK_SHADER_STAGE_FRAGMENT_BIT);

    bindings.push_back(binding);

    VkDescriptorSetLayoutCreateInfo layoutInfo{};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
    layoutInfo.pBindings = bindings.data();

    VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device.getLogicalDeviceHandle(), &layoutInfo, nullptr, &descriptorLayout));
    return descriptorLayout;
}

void TextureCubeMap::submitImageToStageMemory()
{
    void* data;
    vkMapMemory(device.getLogicalDeviceHandle(), stagingMemory, 0, imageSize, 0, &data);
    memcpy(data, this->texCube.data(), static_cast<size_t>(this->texCube.size()));
    vkUnmapMemory(device.getLogicalDeviceHandle(), stagingMemory);
}

void TextureCubeMap::submitImageToDeviceMemory()
{

    utils::transitionImageLayout(device.getLogicalDeviceHandle(), 
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 textureImage, 
                                 VK_IMAGE_LAYOUT_UNDEFINED, 
                                 VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 
                                 6,
                                 mipLevels);

        copyBufferToImage(stagingBuffer, textureImage, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight));
    
    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 textureImage, 
                                 VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 
                                 6,
                                 mipLevels);

    vkDestroyBuffer(device.getLogicalDeviceHandle(), stagingBuffer, nullptr);
    vkFreeMemory(device.getLogicalDeviceHandle(), stagingMemory, nullptr);
    this->stageBuffersDeleted = true;
    this->inDeviceMemory = true;
}

void TextureCubeMap::cleanup()
{
    if (textureSampler != VK_NULL_HANDLE)
        vkDestroySampler(device.getLogicalDeviceHandle(), textureSampler, nullptr);
    if (textureImageView != VK_NULL_HANDLE)
        vkDestroyImageView(device.getLogicalDeviceHandle(), textureImageView, nullptr);
    if (textureImage != VK_NULL_HANDLE)
        vkDestroyImage(device.getLogicalDeviceHandle(), textureImage, nullptr);
    if (textureImageMemory != VK_NULL_HANDLE)
        vkFreeMemory(device.getLogicalDeviceHandle(), textureImageMemory, nullptr);

    if (!this->stageBuffersDeleted && this->inDeviceMemory) {
        vkDestroyBuffer(device.getLogicalDeviceHandle(), stagingBuffer, nullptr);
        vkFreeMemory(device.getLogicalDeviceHandle(), stagingMemory, nullptr);
    }
}

void TextureCubeMap::copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height)
{
    std::vector<VkBufferImageCopy> bufferCopyRegions;
    size_t offset = 0;

    for (uint32_t face = 0; face < 6; face++) {
        for (int mipLevel = 0; mipLevel < mipLevels; mipLevel++) {
            VkBufferImageCopy bufferCopyRegion = {};
            bufferCopyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            bufferCopyRegion.imageSubresource.mipLevel = mipLevel;
            bufferCopyRegion.imageSubresource.baseArrayLayer = face;
            bufferCopyRegion.imageSubresource.layerCount = 1;
            bufferCopyRegion.imageExtent.width = static_cast<uint32_t>(texCube[face][mipLevel].extent().x);
            bufferCopyRegion.imageExtent.height = static_cast<uint32_t>(texCube[face][mipLevel].extent().y);
            bufferCopyRegion.imageExtent.depth = 1;
            bufferCopyRegion.bufferOffset = offset;

            bufferCopyRegions.push_back(bufferCopyRegion);

            // Increase offset into staging buffer for next level / face
            offset += texCube[face][mipLevel].size();
        }
    }

    VkCommandBuffer commandBuffer = utils::beginSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool);

    vkCmdCopyBufferToImage(commandBuffer, 
                           buffer,
                           image,
                           VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 
                           static_cast<uint32_t>(bufferCopyRegions.size()), bufferCopyRegions.data());

    utils::endSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool, device.getQueues().graphicsQueue, commandBuffer);
}
