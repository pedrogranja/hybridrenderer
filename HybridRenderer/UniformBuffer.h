#pragma once
#include <vector>
#include "VulkanDevice.h"
#include "utils.hpp"

class UniformBuffer
{
	VulkanDevice device;

public:
	int size;
	int numBuffers;

	std::vector<VkBuffer> uniformBuffers;
	std::vector<VkDeviceMemory> uniformMemories;
	std::string name;

	//num buffers because we might want one buffer per frame
	UniformBuffer();
	UniformBuffer(std::string name, VulkanDevice& device, int size, int numBuffers = 1);
	void submitDataToMemory(void* uboData, int bufferIndex = 0);
	VkBuffer getBuffer(int bufferIndex = 0);
	int getSize();
	void cleanup();
};

