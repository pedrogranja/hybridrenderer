#include "DeferredRenderer.h"

void DeferredRenderer::createDrawCommandBuffers() {
    commandBuffers.resize(swapChain.getSize());

    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = (uint32_t)commandBuffers.size();

    VK_CHECK_RESULT(vkAllocateCommandBuffers(device.getLogicalDeviceHandle(), &allocInfo, commandBuffers.data()));

    for (int i = 0; i < commandBuffers.size(); i++) {
        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

        VK_CHECK_RESULT(vkBeginCommandBuffer(commandBuffers[i], &beginInfo));

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////// G-BUFFER /////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        VkRenderPassBeginInfo offscreenRenderPassInfo{};
        offscreenRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        offscreenRenderPassInfo.renderPass = gBufferPass.getRenderPassHandle();
        offscreenRenderPassInfo.framebuffer = gBufferPass.getFramebuffers()[0]; //CUIDADO CASO SEJA PARA IMPRIMIR PARA A SWAPCHAIN TEM QUE SER i CASO CONTRARIO 0 � OK
        offscreenRenderPassInfo.renderArea.offset = { 0, 0 };
        offscreenRenderPassInfo.renderArea.extent = swapChain.getSwapChainExtent();

        std::array<VkClearValue, 6> clearValues{};
        clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[1].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[2].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[3].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[4].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[5].depthStencil = { 1.0f, 0 };
        offscreenRenderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
        offscreenRenderPassInfo.pClearValues = clearValues.data();

        VkViewport viewport{};
        viewport.width = (float)swapChain.getSwapChainExtent().width;
        viewport.height = (float)swapChain.getSwapChainExtent().height;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        VkRect2D scissor{};
        scissor.extent.width = swapChain.getSwapChainExtent().width;
        scissor.extent.height = swapChain.getSwapChainExtent().height;

        vkCmdSetViewport(commandBuffers[i], 0, 1, &viewport);
        vkCmdSetScissor(commandBuffers[i], 0, 1, &scissor);

        vkCmdBeginRenderPass(commandBuffers[i], &offscreenRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, gBufferPipeline.getPipeline());

        //bind ui params
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                gBufferPipeline.getPipelineLayout(),
                                2,
                                1,
                                &imGuiUi.descriptorSets[i],
                                0,
                                nullptr);


        this->scene.drawScene(commandBuffers[i], gBufferPipeline.getPipelineLayout(), i);

        vkCmdEndRenderPass(commandBuffers[i]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////// SKYBOX  /////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        VkRenderPassBeginInfo renderPassInfoSkybox{};
        renderPassInfoSkybox.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoSkybox.renderPass = skyboxRenderPass.getRenderPassHandle();
        renderPassInfoSkybox.framebuffer = skyboxRenderPass.getFramebuffers()[0];
        renderPassInfoSkybox.renderArea.offset = { 0, 0 };
        renderPassInfoSkybox.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoSkybox.clearValueCount = 1;
        renderPassInfoSkybox.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoSkybox, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, skyboxPipeline.getPipeline());

        skyBox.draw(commandBuffers[i], skyboxPipeline.getPipelineLayout());

        vkCmdEndRenderPass(commandBuffers[i]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////// DEFERRED RENDERING ///////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        clearValues[2].depthStencil = { 1.0f, 0 };

        VkRenderPassBeginInfo deferredRenderPassInfo{};
        deferredRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        deferredRenderPassInfo.renderPass = renderPass.getRenderPassHandle();
        deferredRenderPassInfo.framebuffer = renderPass.getFramebuffers()[0];
        deferredRenderPassInfo.renderArea.offset = { 0, 0 };
        deferredRenderPassInfo.renderArea.extent = swapChain.getSwapChainExtent();
        deferredRenderPassInfo.clearValueCount = 3;
        deferredRenderPassInfo.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &deferredRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.getPipeline());

        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                pipeline.getPipelineLayout(),
                                0,
                                1,
                                &gBufferDescriptorSet.getDescriptorSetsHandle()[i],
                                0,
                                nullptr);
        
        //bind the scene descriptor set
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                pipeline.getPipelineLayout(),
                                1,
                                1,
                                &sceneDescriptorSet.getDescriptorSetsHandle()[i],
                                0,
                                nullptr);

        //bind ui params
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                pipeline.getPipelineLayout(),
                                2,
                                1,
                                &imGuiUi.descriptorSets[i],
                                0,
                                nullptr);
        
        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////// BLUR RENDERING /////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
        clearValues[1].depthStencil = { 1.0f, 0 };
        clearValues[2].color = { 0.0f, 0.0f, 0.0f, 1.0f };

        //Post processing
        VkRenderPassBeginInfo renderPassInfoBlurHor{};
        renderPassInfoBlurHor.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoBlurHor.renderPass = blurHorRenderPass.getRenderPassHandle();
        renderPassInfoBlurHor.framebuffer = blurHorRenderPass.getFramebuffers()[0];
        renderPassInfoBlurHor.renderArea.offset = { 0, 0 };
        renderPassInfoBlurHor.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoBlurHor.clearValueCount = 2;
        renderPassInfoBlurHor.pClearValues = clearValues.data();

        VkRenderPassBeginInfo renderPassInfoBlurVer{};
        renderPassInfoBlurVer.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoBlurVer.renderPass = blurVerRenderPass.getRenderPassHandle();
        renderPassInfoBlurVer.framebuffer = blurVerRenderPass.getFramebuffers()[0];
        renderPassInfoBlurVer.renderArea.offset = { 0, 0 };
        renderPassInfoBlurVer.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoBlurVer.clearValueCount = 2;
        renderPassInfoBlurVer.pClearValues = clearValues.data();

        VkRenderPassBeginInfo renderPassInfoBlendBloom{};
        renderPassInfoBlendBloom.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfoBlendBloom.renderPass = blendBloomRenderPass.getRenderPassHandle();
        renderPassInfoBlendBloom.framebuffer = blendBloomRenderPass.getFramebuffers()[i];
        renderPassInfoBlendBloom.renderArea.offset = { 0, 0 };
        renderPassInfoBlendBloom.renderArea.extent = swapChain.getSwapChainExtent();
        renderPassInfoBlendBloom.clearValueCount = 2;
        renderPassInfoBlendBloom.pClearValues = clearValues.data();

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoBlurHor, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, postProcessingHorPipeline.getPipeline());
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                postProcessingHorPipeline.getPipelineLayout(),
                                0,
                                1,
                                &blurHorDescriptorSet.getDescriptorSetsHandle()[i],
                                0,
                                nullptr);
        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoBlurVer, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, postProcessingVerPipeline.getPipeline());
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                postProcessingHorPipeline.getPipelineLayout(),
                                0,
                                1,
                                &blurVerDescriptorSet.getDescriptorSetsHandle()[i],
                                0,
                                nullptr);
        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(commandBuffers[i]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////// BLOOM BLEND ////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoBlendBloom, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, postProcessingBlendBloomPipeline.getPipeline());
        vkCmdBindDescriptorSets(commandBuffers[i],
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                postProcessingBlendBloomPipeline.getPipelineLayout(),
                                0,
                                1,
                                &BloomBlendDescriptorSet.getDescriptorSetsHandle()[i],
                                0,
                                nullptr);
        vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);
       
        vkCmdEndRenderPass(commandBuffers[i]);

        VK_CHECK_RESULT(vkEndCommandBuffer(commandBuffers[i]));
    }
}


void DeferredRenderer::defineRenderer()
{
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////// CREATE RENDERTARGETS ////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    VkFormat depthFormat = utils::findSupportedFormat(
        instance.getPhysicalDevice(),
        { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
        VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    );

    depthRenderTarget = RenderTarget("Depth Render Target",
                                     depthFormat,
                                     swapChain.getSwapChainExtent(),
                                     1,
                                     VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                                     static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT),
                                     device,
                                     instance,
                                     swapChain,
                                     commandPool);

    //Offscreen render targets (G-BUFFER)
    positionRenderTarget = RenderTarget("Position Render Target", 
                                        VK_FORMAT_R16G16B16A16_SFLOAT,
                                        swapChain.getSwapChainExtent(),
                                        4,
                                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                        static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                        device,
                                        instance,
                                        swapChain,
                                        commandPool);

    albedoRenderTarget = RenderTarget("Albedo Render Target", 
                                      VK_FORMAT_R8G8B8A8_SRGB,
                                      swapChain.getSwapChainExtent(),
                                      4,
                                      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                      static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                      device,
                                      instance,
                                      swapChain,
                                      commandPool);

    normalRenderTarget = RenderTarget("Normal Render Target",
                                      VK_FORMAT_R16G16B16A16_SFLOAT,
                                      swapChain.getSwapChainExtent(),
                                      4,
                                      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                      static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                      device,
                                      instance,
                                      swapChain,
                                      commandPool);

    metalRoughnessRenderTarget = RenderTarget("Metal Roughness Render Target",
                                              VK_FORMAT_R8G8B8A8_UNORM,
                                              swapChain.getSwapChainExtent(),
                                              4,
                                              VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                              static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                              device,
                                              instance,
                                              swapChain,
                                              commandPool);

    emissionRenderTarget = RenderTarget("Emission Render Target", 
                                        VK_FORMAT_R8G8B8A8_SRGB,
                                        swapChain.getSwapChainExtent(),
                                        4,
                                        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                        static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                        device,
                                        instance,
                                        swapChain,
                                        commandPool);

    //Deferred render targets
    colorRenderTarget = RenderTarget("Color Render Target", 
                                     VK_FORMAT_R16G16B16A16_SFLOAT,
                                     swapChain.getSwapChainExtent(),
                                     4,
                                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                     static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                     device,
                                     instance,
                                     swapChain,
                                     commandPool);

    //Skybox Render target
    skyboxRenderTarget = RenderTarget("Skybox Render Target", 
                                     VK_FORMAT_R16G16B16A16_SFLOAT,
                                     swapChain.getSwapChainExtent(),
                                     4,
                                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                     static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                     device,
                                     instance,
                                     swapChain,
                                     commandPool);

    //Post processing render target
    bloomRenderTarget = RenderTarget("Bloom Render Target", 
                                     VK_FORMAT_R16G16B16A16_SFLOAT,
                                     swapChain.getSwapChainExtent(),
                                     4,
                                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                     static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                     device,
                                     instance,
                                     swapChain,
                                     commandPool);


    blurHorRenderTarget = RenderTarget("Blur Hor Render Target", 
                                       VK_FORMAT_R16G16B16A16_SFLOAT,
                                       swapChain.getSwapChainExtent(),
                                       4,
                                       VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                       static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                       device,
                                       instance,
                                       swapChain,
                                       commandPool);


    blurVerRenderTarget = RenderTarget("Blur Ver Render Target", 
                                       VK_FORMAT_R16G16B16A16_SFLOAT,
                                       swapChain.getSwapChainExtent(),
                                       4,
                                       VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                       static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                       device,
                                       instance,
                                       swapChain,
                                       commandPool);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// CREATE RENDERPASSES /////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // SPECIAL DESCRIPTIONS
    // store the depth information when finished
    VkAttachmentDescription descriptionFirstPass = depthRenderTarget.getAttachmentDescription();
    descriptionFirstPass.storeOp = VK_ATTACHMENT_STORE_OP_STORE; 

    //load the previous depth information at beginning
    VkAttachmentDescription descriptionSecondPass = depthRenderTarget.getAttachmentDescription();
    descriptionSecondPass.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD; 
    descriptionSecondPass.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    descriptionSecondPass.initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    descriptionSecondPass.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    //Render passes
    gBufferPass = Pass("GBuffer", { positionRenderTarget, albedoRenderTarget, normalRenderTarget, metalRoughnessRenderTarget, emissionRenderTarget }, { depthRenderTarget }, false, swapChain, device, { {depthRenderTarget, descriptionFirstPass} });
    renderPass = Pass("RenderPass", { colorRenderTarget, bloomRenderTarget }, std::vector<RenderTarget>(), false, swapChain, device);
    blurHorRenderPass = Pass("Blur Hor", { blurHorRenderTarget }, std::vector<RenderTarget>(), false, swapChain, device);
    blurVerRenderPass = Pass("Blur Ver", { blurVerRenderTarget }, std::vector<RenderTarget>(), false, swapChain, device);
    skyboxRenderPass = Pass("Skybox", { skyboxRenderTarget }, { depthRenderTarget}, false, swapChain, device, { {depthRenderTarget, descriptionSecondPass} });
    blendBloomRenderPass = Pass("Blend Bloom", {}, std::vector<RenderTarget>(), true, swapChain, device);
    imGuiRenderPass = ImGuiRenderpass(device.getLogicalDeviceHandle(), swapChain);
    imGuiRenderPass.createRenderPasses(VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
    imGuiRenderPass.createFramebuffers();

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////// CREATE UI //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    createUI();

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////// CREATE DESCRIPTOR SETS AND LAYOUTS ///////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    utils::QueueFamilyIndices queueFamilyIndices = utils::findQueueFamilies(instance.getPhysicalDevice(), instance.getSurface());
    
    //scene
    sceneUniform = UniformBuffer("Scene", device, sizeof(SceneParams));
    sceneDescriptorSet = DescriptorSet("Scene",
                                       { {0,&sceneUniform} },
                                       { {1,&brdfLutRenderTarget.getTextureImage()} },
                                       { {2,{scene.textureCubeMap}},  {3,{irradiance}}, {4,{mipMapCubemap}} }, 
                                       {},
                                       VK_SHADER_STAGE_FRAGMENT_BIT,
                                       device,
                                       instance,
                                       commandPool,
                                       this->swapChain.getSize());

    gBufferDescriptorSet = DescriptorSet("G-Buffer", 
                                        {},
                                        { {0,&positionRenderTarget.getTextureImage()}, {1,&albedoRenderTarget.getTextureImage()} , {2,&normalRenderTarget.getTextureImage()} , {3,&metalRoughnessRenderTarget.getTextureImage()} , {4,&emissionRenderTarget.getTextureImage()} },
                                        {}, 
                                        {},
                                        VK_SHADER_STAGE_FRAGMENT_BIT,
                                        device,
                                        instance,
                                        commandPool,
                                        this->swapChain.getSize());

    //scene descriptor layout
    std::vector<VkDescriptorSetLayout> sceneDescriptorLayouts = scene.descriptorLayouts;
    sceneDescriptorLayouts.push_back(imGuiUi.descriptorLayout);

    //G-Buffer descriptor layout 
    std::vector<VkDescriptorSetLayout> deferredDescriptorLayouts = std::vector<VkDescriptorSetLayout>();
    deferredDescriptorLayouts.push_back(gBufferDescriptorSet.getDescriptorLayout());
    deferredDescriptorLayouts.push_back(sceneDescriptorSet.getDescriptorLayout());
    deferredDescriptorLayouts.push_back(imGuiUi.descriptorLayout);

    //skybox descriptor layout
    std::vector<VkDescriptorSetLayout> skyboxDescriptorLayout = std::vector<VkDescriptorSetLayout>();
    skyboxDescriptorLayout.push_back(skyBox.descriptorSet.getDescriptorLayout());

    //Horizontal blur
    blurHorUniformBuffer = UniformBuffer("Blur Hor", device, sizeof(BlurParams::ToSubmit), swapChain.getSize() /* 1 buffer per swapChain */);
    blurHorDescriptorSet = DescriptorSet("Blur Hor", 
                                        { {0, &blurHorUniformBuffer} },
                                        { {1, &bloomRenderTarget.getTextureImage()} },
                                        {},
                                        {},
                                        VK_SHADER_STAGE_FRAGMENT_BIT,
                                        device,
                                        instance,
                                        commandPool,
                                        swapChain.getSize() // 1 buffer per swapChain
                                        );

    //blur vertical descriptor set
    blurVerUniformBuffer = UniformBuffer("Blur Ver", device, sizeof(BlurParams::ToSubmit), swapChain.getSize() /* 1 buffer per swapChain */);
    blurVerDescriptorSet = DescriptorSet("Blur Ver", 
                                         { {0, &blurVerUniformBuffer } },
                                         { {1, &blurHorRenderTarget.getTextureImage()} },
                                         {},
                                         {},
                                         VK_SHADER_STAGE_FRAGMENT_BIT,
                                         device,
                                         instance,
                                         commandPool,
                                         swapChain.getSize() // 1 buffer per swapChain
                                         );

    //blend bloom descriptor set
    BloomBlendDescriptorSet = DescriptorSet("Bloom Blend", 
                                            {},
                                            { {0, &colorRenderTarget.getTextureImage()}, {1, &blurVerRenderTarget.getTextureImage()}, {2, &skyboxRenderTarget.getTextureImage()} },
                                            {},
                                            {},
                                            VK_SHADER_STAGE_FRAGMENT_BIT,
                                            device,
                                            instance,
                                            commandPool,
                                            swapChain.getSize() // 1 buffer per swapChains
                                            );

    //blur descriptor layout
    std::vector<VkDescriptorSetLayout> postProcessingDescriptorLayouts(swapChain.getSize(), blurHorDescriptorSet.getDescriptorLayout());

    //bloom blend descriptor layout
    std::vector<VkDescriptorSetLayout> postProcessingBlendDescriptorLayouts(swapChain.getSize(), BloomBlendDescriptorSet.getDescriptorLayout());

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////// CREATE PIPELINES //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //definition for a color attachment
    VkPipelineColorBlendAttachmentState colorBlendAttachment = initializers::pipelineColorBlendAttachmentState();

    gBufferPipeline = VertexFragPipeline("Offscreen", 
                                         "offscreen.vert.spv",
                                         "offscreen.frag.spv",
                                         device,
                                         swapChain,
                                         gBufferPass,
                                         { colorBlendAttachment, colorBlendAttachment, colorBlendAttachment, colorBlendAttachment, colorBlendAttachment },
                                         sceneDescriptorLayouts,
                                         utils::Vertex::getBindingDescription(),
                                         utils::Vertex::getAttributeDescriptions(),
                                         {},
                                         VK_CULL_MODE_BACK_BIT);

    pipeline = VertexFragPipeline("Pipeline",
                                  "blur.vert.spv",
                                  "deferredPBR.frag.spv",
                                  device,
                                  swapChain,
                                  renderPass,
                                  { colorBlendAttachment, colorBlendAttachment },
                                  deferredDescriptorLayouts,
                                  std::vector<VkVertexInputBindingDescription>(),
                                  std::vector<VkVertexInputAttributeDescription>(),
                                  {},
                                  VK_CULL_MODE_NONE);

    skyboxPipeline = VertexFragPipeline("Skybox", 
                                        "skybox.vert.spv",
                                        "skybox.frag.spv",
                                        device,
                                        swapChain,
                                        skyboxRenderPass,
                                        { colorBlendAttachment },
                                        skyboxDescriptorLayout,
                                        utils::Vertex::getBindingDescription(),
                                        utils::Vertex::getAttributeDescriptions(),
                                        {},
                                        VK_CULL_MODE_FRONT_BIT,
                                        VK_COMPARE_OP_LESS_OR_EQUAL);

    postProcessingHorPipeline = VertexFragPipeline("Post Processing Hor", 
                                                   "blur.vert.spv",
                                                   "blur.frag.spv",
                                                   device,
                                                   swapChain,
                                                   blurHorRenderPass,
                                                   { colorBlendAttachment },
                                                   postProcessingDescriptorLayouts,
                                                   std::vector<VkVertexInputBindingDescription>(),
                                                   std::vector<VkVertexInputAttributeDescription>(),
                                                   {},
                                                   VK_CULL_MODE_NONE);

    postProcessingVerPipeline = VertexFragPipeline("Post Processing Ver", 
                                                   "blur.vert.spv",
                                                   "blur.frag.spv",
                                                   device,
                                                   swapChain,
                                                   blurVerRenderPass,
                                                   { colorBlendAttachment },
                                                   postProcessingDescriptorLayouts,
                                                   std::vector<VkVertexInputBindingDescription>(),
                                                   std::vector<VkVertexInputAttributeDescription>(),                      
                                                   {}, 
                                                   VK_CULL_MODE_NONE);

    postProcessingBlendBloomPipeline = VertexFragPipeline("Post Processing Blend Bloom", 
                                                          "blur.vert.spv",
                                                          "blendBloomSkybox.frag.spv",
                                                          device,
                                                          swapChain,
                                                          blendBloomRenderPass,
                                                          { colorBlendAttachment },
                                                          postProcessingBlendDescriptorLayouts,
                                                          std::vector<VkVertexInputBindingDescription>(),
                                                          std::vector<VkVertexInputAttributeDescription>(),
                                                          {},
                                                          VK_CULL_MODE_NONE);
}

void DeferredRenderer::cleanupSwapChain()
{
    gBufferDescriptorSet.cleanup();
    gBufferPipeline.cleanup();
    gBufferPass.cleanup();
    skyboxRenderPass.cleanup();
    skyboxRenderTarget.cleanup();
    positionRenderTarget.cleanup();
    albedoRenderTarget.cleanup();
    normalRenderTarget.cleanup();
    aoRenderTarget.cleanup();
    metalRoughnessRenderTarget.cleanup();
    emissionRenderTarget.cleanup();
    EngineBase::cleanupSwapChain();
}