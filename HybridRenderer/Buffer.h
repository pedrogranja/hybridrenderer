#pragma once
#include <vulkan/vulkan.h>
#include "utils.hpp"
#include "VulkanDevice.h"

class Buffer
{
protected:
	VulkanDevice device;
	VkQueue graphicsQueue;
	std::string name;

public:
	int size;
	VkBuffer buffer;
	VkDeviceMemory bufferMemory;

	Buffer();
	Buffer(std::string name,
		   VulkanDevice device,
		   VkQueue graphicsQueue,
		   int size,
		   VkBufferUsageFlags usage,
		   VkMemoryPropertyFlags properties);
	
	void submitDataToMemory(void* uboData);
	VkDeviceAddress getDeviceAddress();
	void cleanup();
};