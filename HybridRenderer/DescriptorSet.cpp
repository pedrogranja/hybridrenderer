#include "DescriptorSet.h"

static int descriptorSetIdGenerator = 0;

DescriptorSet::DescriptorSet()
{
}

DescriptorSet::DescriptorSet(std::string name,
							 std::vector<UniformBufferResource>&& uniformBufferResources,
							 std::vector<TextureResource>&& textureResources,
							 std::vector<TextureCubeResource>&& texCubeMapResources,
							 std::vector<TextureArrayResource>&& texArrayResources,
							 VkShaderStageFlagBits shaderStage,
							 VulkanDevice& device,
							 VulkanInstance& instance,
							 VkCommandPool commandPool,
							 int numDescriptors) : name(name), 
												   uniformBufferResources(uniformBufferResources),
												   textureResources(textureResources),
												   texCubeMapResources(texCubeMapResources),
												   texArrayResources(texArrayResources),
												   shaderStage(shaderStage),
												   device(device),
												   instance(instance),
												   commandPool(commandPool),
												   numDescriptors(numDescriptors)
{
	createDescriptorPool();
	createDescriptorSetLayout();
	createDescriptorSet();
}

DescriptorSet::DescriptorSet(std::string name,
							 std::vector<UniformBufferResource>& uniformBufferResources,
							 std::vector<TextureResource>& textureResources,
							 std::vector<TextureCubeResource>& texCubeMapResources,
							 std::vector<TextureArrayResource>& texArrayResources,
							 VkShaderStageFlagBits shaderStage,
							 VulkanDevice& device,
							 VulkanInstance& instance,
							 VkCommandPool commandPool,
							 int numDescriptors) : name(name),
												   uniformBufferResources(uniformBufferResources),
												   textureResources(textureResources),
												   texCubeMapResources(texCubeMapResources),
												   texArrayResources(texArrayResources),
												   shaderStage(shaderStage),
												   device(device),
												   instance(instance),
												   commandPool(commandPool),
												   numDescriptors(numDescriptors)
{
	createDescriptorPool();
	createDescriptorSetLayout();
	createDescriptorSet();
}

void DescriptorSet::cleanup()
{
	vkDestroyDescriptorPool(device.getLogicalDeviceHandle(), descriptorPool, nullptr);
	vkDestroyDescriptorSetLayout(device.getLogicalDeviceHandle(), descriptorSetLayout, nullptr);
}

std::vector<VkDescriptorSet>& DescriptorSet::getDescriptorSetsHandle()
{
	return descriptorSetsHandle;
}

VkDescriptorSetLayout& DescriptorSet::getDescriptorLayout()
{
	return descriptorSetLayout;
}

void DescriptorSet::createDescriptorPool()
{
	std::vector<VkDescriptorPoolSize> poolSizes{};
	int poolSize = 0;
	if (uniformBufferResources.size() > 0)
		poolSize++;
	if (textureResources.size() > 0)
		poolSize++;
	if (texCubeMapResources.size() > 0)
		poolSize++;
	if (texArrayResources.size() > 0)
		poolSize++;

	poolSizes.resize(poolSize);
	int index = 0;
	if(uniformBufferResources.size() > 0) {
		poolSizes[index].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		poolSizes[index].descriptorCount = numDescriptors * static_cast<uint32_t>(uniformBufferResources.size());
		index++;
	}
	if(textureResources.size() > 0) {
		poolSizes[index].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		poolSizes[index].descriptorCount = numDescriptors * static_cast<uint32_t>(textureResources.size());
		index++;
	}
	if (texCubeMapResources.size() > 0) {
		poolSizes[index].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		poolSizes[index].descriptorCount = numDescriptors * static_cast<uint32_t>(texCubeMapResources.size());
		index++;
	}

	if (texArrayResources.size() > 0) {
		int numTextures = 0;
		for (int i = 0; i < texArrayResources.size(); i++) {
			numTextures += static_cast<int>(texArrayResources[i].texImages.size());
		}

		poolSizes[index].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		poolSizes[index].descriptorCount = numDescriptors * numTextures;
		index++;
	}

	VkDescriptorPoolCreateInfo poolInfo{};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = static_cast<uint32_t>(numDescriptors);

	VK_CHECK_RESULT(vkCreateDescriptorPool(device.getLogicalDeviceHandle(), &poolInfo, nullptr, &descriptorPool));
	device.nameVulkanObject(VK_OBJECT_TYPE_DESCRIPTOR_POOL, (uint64_t) descriptorPool, name + " Descriptor Pool");
	descriptorSetIdGenerator++;
}

void DescriptorSet::createDescriptorSetLayout()
{
	std::vector<VkDescriptorSetLayoutBinding> bindings;
	VkDescriptorSetLayoutBinding binding = {};

	for (auto& it : uniformBufferResources) {
		int bindingNumber = it.binding;
		binding = initializers::descriptorSetLayoutBinding(bindingNumber,
														   VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
														   shaderStage);
		bindings.push_back(binding);
	}

	for (auto& it : textureResources) {
		int bindingNumber = it.binding;
		binding = initializers::descriptorSetLayoutBinding(bindingNumber,
														   VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
														   shaderStage);
		bindings.push_back(binding);
	}

	for (auto& it : texCubeMapResources) {
		int bindingNumber = it.binding;
		binding = initializers::descriptorSetLayoutBinding(bindingNumber,
														   VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
														   shaderStage);
		bindings.push_back(binding);
	}

	//descriptors texture array samplers
	for (auto& it : texArrayResources) {
		int bindingNumber = it.binding;
		binding = initializers::descriptorSetLayoutBinding(bindingNumber,
															VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
															shaderStage);
		binding.descriptorCount = it.texImages.size();
		bindings.push_back(binding);
	}


	//allows descriptor to be incomplete ex: missing textures
	std::vector<VkDescriptorBindingFlags> descriptorFlags = std::vector<VkDescriptorBindingFlags>(bindings.size(), VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT); 
	VkDescriptorSetLayoutBindingFlagsCreateInfo layoutInfoFlag{};
	layoutInfoFlag.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
	layoutInfoFlag.bindingCount = static_cast<uint32_t>(bindings.size());
	layoutInfoFlag.pBindingFlags = descriptorFlags.data();
	layoutInfoFlag.pNext = nullptr;

	//Define the descriptor set layout
	VkDescriptorSetLayoutCreateInfo layoutInfo{};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
	layoutInfo.pBindings = bindings.data();
	layoutInfo.pNext = &layoutInfoFlag;

	VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device.getLogicalDeviceHandle(), &layoutInfo, nullptr, &descriptorSetLayout));
	device.nameVulkanObject(VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT, (uint64_t)descriptorSetLayout, name + " Descriptor Set Layout");
	descriptorSetIdGenerator++;
}

void DescriptorSet::createDescriptorSet()
{
	int descriptorCount = numDescriptors;
	std::vector<VkDescriptorSetLayout> descriptorSetLayouts(descriptorCount, descriptorSetLayout);

	//Create the descriptor set
	VkDescriptorSetAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>(descriptorCount);
	allocInfo.pSetLayouts = descriptorSetLayouts.data();
	descriptorSetsHandle.resize(descriptorCount);
	VK_CHECK_RESULT(vkAllocateDescriptorSets(device.getLogicalDeviceHandle(), &allocInfo, descriptorSetsHandle.data()));

	for (int i = 0; i < descriptorSetsHandle.size(); i++) {
		device.nameVulkanObject(VK_OBJECT_TYPE_DESCRIPTOR_SET, (uint64_t)descriptorSetsHandle[i], name + "Descriptor Set" + std::to_string(i));
		descriptorSetIdGenerator++;
	}

	std::vector<VkWriteDescriptorSet> descriptorWrites;
	VkWriteDescriptorSet writeDescriptorSet = {};
	std::vector<VkDescriptorImageInfo> imageInfos;
	imageInfos.resize(textureResources.size() * descriptorCount);
	int imageCount = 0;
	std::vector<VkDescriptorBufferInfo> bufferInfos;
	bufferInfos.resize(uniformBufferResources.size() * descriptorCount);
	int bufferCount = 0;
	std::vector<VkDescriptorImageInfo> cubeImageInfos;
	cubeImageInfos.resize(texCubeMapResources.size() * descriptorCount);
	int cubeImageCount = 0;
	std::vector<std::vector<VkDescriptorImageInfo>> texArrayImageInfos;
	int texArrayImageCount = 0;

	for (int i = 0; i < descriptorCount; i++) {

		descriptorWrites.clear();

		for (auto& it : uniformBufferResources) {
			int binding = it.binding;
			if (it.uniformBuffer != nullptr) {
				UniformBuffer uniformBuffer = *it.uniformBuffer;
				int index = std::min(i, static_cast<int>(uniformBuffer.getSize() - 1));
				bufferInfos[bufferCount].buffer = uniformBuffer.getBuffer(index);
				bufferInfos[bufferCount].offset = 0;
				bufferInfos[bufferCount].range = uniformBuffer.size;

				writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writeDescriptorSet.dstSet = descriptorSetsHandle[i];
				writeDescriptorSet.dstBinding = binding;
				writeDescriptorSet.dstArrayElement = 0;
				writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
				writeDescriptorSet.descriptorCount = 1;
				writeDescriptorSet.pBufferInfo = &bufferInfos[bufferCount];
				descriptorWrites.push_back(writeDescriptorSet);
				bufferCount++;
			}
		}

		for (auto& it : textureResources) {
			int binding = it.binding;
			if(it.texImage != nullptr) {
				TextureImage texImage = *it.texImage;
				//We can be using 1 image or multiple
				int index = 0;
				index = std::min(i, static_cast<int>(texImage.textureImages.size()-1));
				imageInfos[imageCount].imageLayout = it.imageLayout;
				imageInfos[imageCount].imageView = texImage.textureImageViews[index];
				imageInfos[imageCount].sampler = texImage.textureSamplers[index];

				writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				writeDescriptorSet.dstSet = descriptorSetsHandle[i];
				writeDescriptorSet.dstBinding = binding;
				writeDescriptorSet.dstArrayElement = 0;
				writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				writeDescriptorSet.descriptorCount = 1;
				writeDescriptorSet.pImageInfo = &imageInfos[imageCount];
				descriptorWrites.push_back(writeDescriptorSet);
				imageCount++;
			}
		}

		for (auto& it : texCubeMapResources) {
			int binding = it.binding;
			std::vector<TextureCubeMap> texCubeMap = it.texImageVector;
			//We can be using 1 image or multiple
			int index = 0;
			index = std::min(i, static_cast<int>(texCubeMap.size() - 1));
			cubeImageInfos[cubeImageCount].imageLayout = it.imageLayout;
			cubeImageInfos[cubeImageCount].imageView = texCubeMap[index].textureImageView;
			cubeImageInfos[cubeImageCount].sampler = texCubeMap[index].textureSampler;

			writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			writeDescriptorSet.dstSet = descriptorSetsHandle[i];
			writeDescriptorSet.dstBinding = binding;
			writeDescriptorSet.dstArrayElement = 0;
			writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			writeDescriptorSet.descriptorCount = 1;
			writeDescriptorSet.pImageInfo = &cubeImageInfos[cubeImageCount];
			descriptorWrites.push_back(writeDescriptorSet);
			cubeImageCount++;
		}

		for (auto& it : texArrayResources) {
			int binding = it.binding;
			std::vector<TextureImage*> texImages = it.texImages;
			std::vector<VkDescriptorImageInfo> textureInfos;
			textureInfos.resize(texImages.size());
			if (textureInfos.size() <= 0) {
				continue;
			}

			for (int j = 0; j < texImages.size(); j++) {
				textureInfos[j].imageLayout = it.imageLayout;
				textureInfos[j].imageView = texImages[j]->textureImageViews[0];
				textureInfos[j].sampler = texImages[j]->textureSamplers[0];
			}
			texArrayImageInfos.push_back(textureInfos);

			VkWriteDescriptorSet targetWrite{};
			targetWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			targetWrite.dstSet = descriptorSetsHandle[i];
			targetWrite.dstBinding = binding;
			targetWrite.dstArrayElement = 0;
			targetWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			targetWrite.descriptorCount = static_cast<uint32_t>(textureInfos.size());
			targetWrite.pImageInfo = texArrayImageInfos[texArrayImageCount].data();
			descriptorWrites.push_back(targetWrite);
			texArrayImageCount++;
		}

		vkUpdateDescriptorSets(device.getLogicalDeviceHandle(), static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
	}
}