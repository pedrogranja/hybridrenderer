#include "CameraMovement.h"

CameraMovement::CameraMovement()
{
	keyframes = std::vector<Keyframe>();
}

void CameraMovement::addKeyframe(CameraMovement::Keyframe keyframe)
{
	this->keyframes.push_back(keyframe);
}

CameraMovement::Keyframe CameraMovement::evaluate(float time)
{
	Keyframe res; 

	for (int i = 0; i < keyframes.size()-1; i++) {
		if (keyframes[i].time < time && keyframes[i+1].time > time) {
			res.pos = glm::mix(keyframes[i].pos, keyframes[i+1].pos, (time - keyframes[i].time) / (keyframes[i + 1].time - keyframes[i].time));
			res.target = glm::mix(keyframes[i].target, keyframes[i+1].target, (time - keyframes[i].time) / (keyframes[i + 1].time - keyframes[i].time));
		}
		else if (keyframes[i+1].time <= time) {
			res.pos = keyframes[i + 1].pos;
			res.target = keyframes[i + 1].target;
		}
	}

	return res;
}
