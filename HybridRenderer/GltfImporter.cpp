#include "GltfImporter.h"

SceneNode* GltfImporter::loadNode(SceneNode* parentNode,
								  const tinygltf::Node& node,
								  uint32_t nodeIndex,
								  std::vector<utils::Vertex>& vertices,
								  std::vector<uint32_t>& indices,
								  std::vector<uint32_t>& indicesOffsets,
								  std::vector<Material*>* materials)
{
	SceneNode* newNode = new SceneNode(node.name, parentNode, nodeIndex);

	if (node.translation.size() == 3) {
		glm::vec3 translation = glm::make_vec3(node.translation.data());
		newNode->translation = translation;
		newNode->initialTranslation = translation;
	}
	if (node.rotation.size() == 4) {
		glm::quat q = glm::make_quat(node.rotation.data());
		newNode->rotation = glm::mat4(q);
		newNode->initialRotation = glm::mat4(q);
	}

	if (node.scale.size() == 3) {
		glm::vec3 scale = glm::make_vec3(node.scale.data());
		newNode->scale = scale;
		newNode->initialScale = scale;
	}

	if (node.children.size() > 0) {
		for (int i = 0; i < node.children.size(); i++) {
			loadNode(newNode, gltfModel.nodes[node.children[i]], node.children[i], vertices, indices, indicesOffsets, materials);
		}
	}

	if (node.mesh > -1) {
		const tinygltf::Mesh mesh = gltfModel.meshes[node.mesh];
		Mesh* newMesh = new Mesh();
		newNode->mesh = newMesh;

		for (int i = 0; i < mesh.primitives.size(); i++) {
			const tinygltf::Primitive primitive = mesh.primitives[i];
			uint32_t indexStart = static_cast<uint32_t>(indices.size());
			uint32_t vertexStart = static_cast<uint32_t>(vertices.size());
			uint32_t indexCount = 0;
			uint32_t vertexCount = 0;
			const float* bufferPos = nullptr;
			const float* bufferNormals = nullptr;
			const float* bufferTexCoordSet0 = nullptr;

			{ //VERTICES
				//Make sure the vertices contain positions
				assert(primitive.attributes.find("POSITION") != primitive.attributes.end());

				//Accessor describes the way the data is suppoused to be interpreted (if it is a vec3, a float, etc)
				const tinygltf::Accessor& posAccessor = gltfModel.accessors[primitive.attributes.find("POSITION")->second];
				//Buffer view describe a chunk of data, like the vertexes or the indexes, in this case we want the vertex positions
				const tinygltf::BufferView& posView = gltfModel.bufferViews[posAccessor.bufferView];
				bufferPos = reinterpret_cast<const float*>(&(gltfModel.buffers[posView.buffer].data[posAccessor.byteOffset + posView.byteOffset]));
				vertexCount = static_cast<uint32_t>(posAccessor.count);

				if (primitive.attributes.find("NORMAL") != primitive.attributes.end()) {
					const tinygltf::Accessor& normAccessor = gltfModel.accessors[primitive.attributes.find("NORMAL")->second];
					const tinygltf::BufferView& normView = gltfModel.bufferViews[normAccessor.bufferView];
					bufferNormals = reinterpret_cast<const float*>(&(gltfModel.buffers[normView.buffer].data[normAccessor.byteOffset + normView.byteOffset]));
				}

				if (primitive.attributes.find("TEXCOORD_0") != primitive.attributes.end()) {
					const tinygltf::Accessor& uvAccessor = gltfModel.accessors[primitive.attributes.find("TEXCOORD_0")->second];
					const tinygltf::BufferView& uvView = gltfModel.bufferViews[uvAccessor.bufferView];
					bufferTexCoordSet0 = reinterpret_cast<const float*>(&(gltfModel.buffers[uvView.buffer].data[uvAccessor.byteOffset + uvView.byteOffset]));
				}

				for (size_t v = 0; v < posAccessor.count; v++) {
					utils::Vertex vert;
					if (test == 0) {
						vert.pos = glm::vec3(glm::make_vec3(&bufferPos[v * 3]));
					}
					else {
						vert.pos = glm::vec3(glm::make_vec3(&bufferPos[v * 3]));
					}
					vert.color = glm::vec3(1.0f, 1.0f, 1.0f);
					vert.normal = glm::normalize(glm::vec3(bufferNormals ? glm::make_vec3(&bufferNormals[v * 3]) : glm::vec3(0.0f)));
					glm::vec2 texCoords = bufferTexCoordSet0 ? glm::make_vec2(&bufferTexCoordSet0[v * 2]) : glm::vec2(0.0f);
					vert.texCoord = glm::vec3(texCoords.x, texCoords.y, primitive.material + currentSceneMaterialOffset);

					vertices.push_back(vert);
				}
			}

			{ //INDICES
				const tinygltf::Accessor& accessor = gltfModel.accessors[primitive.indices > -1 ? primitive.indices : 0];
				const tinygltf::BufferView& bufferView = gltfModel.bufferViews[accessor.bufferView];
				const tinygltf::Buffer& buffer = gltfModel.buffers[bufferView.buffer];

				indexCount = static_cast<uint32_t>(accessor.count);
				const void* dataPtr = &(buffer.data[accessor.byteOffset + bufferView.byteOffset]);

				switch (accessor.componentType) {
				case TINYGLTF_PARAMETER_TYPE_UNSIGNED_INT: {
					const uint32_t* buf = static_cast<const uint32_t*>(dataPtr);
					for (size_t index = 0; index < accessor.count; index++) {
						indices.push_back(buf[index] + vertexStart);
					}
					break;
				}
				case TINYGLTF_PARAMETER_TYPE_UNSIGNED_SHORT: {
					const uint16_t* buf = static_cast<const uint16_t*>(dataPtr);
					for (size_t index = 0; index < accessor.count; index++) {
						indices.push_back(buf[index] + vertexStart);
					}
					break;
				}
				case TINYGLTF_PARAMETER_TYPE_UNSIGNED_BYTE: {
					const uint8_t* buf = static_cast<const uint8_t*>(dataPtr);
					for (size_t index = 0; index < accessor.count; index++) {
						indices.push_back(buf[index] + vertexStart);
					}
					break;
				}
				default:
					throw "Index component type not supported!";
				}
			}

			Primitive* newPrimitive = new Primitive(indexStart,
													indexCount,
													vertexStart,
													vertexCount,
													primitive.material + currentSceneMaterialOffset,
													primitiveId,
													materials);
			primitiveId++;

			indicesOffsets.push_back(indexStart);

			newMesh->primitives.push_back(newPrimitive);
		}
	}

	test++;

	return newNode;
}

TextureImage* GltfImporter::importTexture(VulkanDevice& device,
										  VkCommandPool commandPool,
										  tinygltf::Image& gltfimage,
										  bool gammaCorrect)
{
	unsigned char* buffer = nullptr;
	VkDeviceSize bufferSize = 0;
	bool deleteBuffer = false;

	//Check if its rgb and covert to rgba
	if (gltfimage.component == 3) {
		bufferSize = gltfimage.width * gltfimage.height * 4;
		buffer = new unsigned char[bufferSize];
		unsigned char* rgba = buffer;
		unsigned char* rgb = &gltfimage.image[0];
		for (int32_t i = 0; i < gltfimage.width * gltfimage.height; ++i) {
			for (int32_t j = 0; j < 3; ++j) {
				rgba[j] = rgb[j];
			}
			rgba += 4;
			rgb += 3;
		}
		deleteBuffer = true;
	}
	else {
		buffer = &gltfimage.image[0];
		bufferSize = gltfimage.image.size();
	}
	VkFormat format;
	if (gltfimage.bits == 8)
		format = gammaCorrect ? VK_FORMAT_R8G8B8A8_SRGB : VK_FORMAT_R8G8B8A8_UNORM;
	else if (gltfimage.bits == 16)
		format = VK_FORMAT_R16G16B16A16_UNORM;
	else
		throw "unrecognized number of bits per channel";

	TextureImage* textureImage = new TextureImage(gltfimage.name,
												  device,
												  commandPool,
												  static_cast<stbi_uc*>(buffer),
												  gltfimage.image.size(),
												  gltfimage.width,
												  gltfimage.height,
												  gltfimage.component,
												  1,
												  format);

	textureImage->submitImageToStageMemory();

	if (deleteBuffer)
		delete[] buffer;

	return textureImage;
}

void GltfImporter::loadTextures(VulkanDevice& device,
								VkCommandPool commandPool,
								std::vector<TextureImage*>& textures)
{
	currentTexOffset = static_cast<int>(textures.size());
	textures.resize(gltfModel.textures.size() + currentTexOffset);
	for (int i = 0; i < gltfModel.textures.size(); i++) {
		tinygltf::Image image = gltfModel.images[gltfModel.textures[i].source];
		textures[i + currentTexOffset] = importTexture(device, commandPool, image);
	}
}

void GltfImporter::loadMaterials(VulkanDevice& device,
								 VkCommandPool commandPool,
								 std::vector<TextureImage*>& textures,
								 std::vector<Material*>& materials)
{
	currentSceneMaterialOffset = static_cast<int>(materials.size());
	materials.resize(gltfModel.materials.size() + currentSceneMaterialOffset);

	for (int i = 0; i < gltfModel.materials.size(); i++) {
		Material::MaterialParams matParams;
		int albedoTextureIndex = -1;
		int normalTextureIndex = -1;
		int aoTextureIndex = -1;
		int emissiveTextureIndex = -1;
		int metalicRoughnessTextureIndex = -1;

		tinygltf::Material mat = gltfModel.materials[i];

		if (mat.values.find("baseColorTexture") != mat.values.end()) {
			matParams.albedoTextureSet = 1;
			albedoTextureIndex = gltfModel.textures[mat.values["baseColorTexture"].TextureIndex()].source;
			if (textures[albedoTextureIndex + currentTexOffset] == nullptr) {
				tinygltf::Image image = gltfModel.images[albedoTextureIndex];
				textures[albedoTextureIndex + currentTexOffset] = importTexture(device, commandPool, image, true);
			}
		}
		if (mat.additionalValues.find("normalTexture") != mat.additionalValues.end()) {
			matParams.normalTextureSet = 1;
			normalTextureIndex = gltfModel.textures[mat.additionalValues["normalTexture"].TextureIndex()].source;
			if (textures[normalTextureIndex + currentTexOffset] == nullptr) {
				tinygltf::Image image = gltfModel.images[normalTextureIndex];
				textures[normalTextureIndex + currentTexOffset] = importTexture(device, commandPool, image);
			}
		}
		if (mat.additionalValues.find("occlusionTexture") != mat.additionalValues.end()) {
			matParams.occlusionTextureSet = 1;
			aoTextureIndex = gltfModel.textures[mat.additionalValues["occlusionTexture"].TextureIndex()].source;
			if (textures[aoTextureIndex + currentTexOffset] == nullptr) {
				tinygltf::Image image = gltfModel.images[aoTextureIndex];
				textures[aoTextureIndex + currentTexOffset] = importTexture(device, commandPool, image);
			}
		}
		if (mat.additionalValues.find("emissiveTexture") != mat.additionalValues.end()) {
			matParams.emissiveTextureSet = 1;
			emissiveTextureIndex = gltfModel.textures[mat.additionalValues["emissiveTexture"].TextureIndex()].source;
			if (textures[emissiveTextureIndex + currentTexOffset] == nullptr) {
				tinygltf::Image image = gltfModel.images[emissiveTextureIndex];
				textures[emissiveTextureIndex + currentTexOffset] = importTexture(device, commandPool, image, true);
			}
		}
		if (mat.values.find("metallicRoughnessTexture") != mat.values.end()) {
			matParams.metallicRoughnessTextureSet = 1;
			metalicRoughnessTextureIndex = gltfModel.textures[mat.values["metallicRoughnessTexture"].TextureIndex()].source;
			if (textures[metalicRoughnessTextureIndex + currentTexOffset] == nullptr) {
				tinygltf::Image image = gltfModel.images[metalicRoughnessTextureIndex];
				textures[metalicRoughnessTextureIndex + currentTexOffset] = importTexture(device, commandPool, image);
			}
		}
		
		auto baseColorEntry = mat.values.find("baseColorFactor");
		glm::vec3 baseColor = glm::vec3();
		if (baseColorEntry == mat.values.end()) {
			baseColor = glm::vec3(1.0f);
		}
		else {
			baseColor = glm::make_vec3(baseColorEntry->second.number_array.data());
		}

		materials[i + currentSceneMaterialOffset] = new Material(mat.name,
																 albedoTextureIndex + currentTexOffset,
																 normalTextureIndex + currentTexOffset,
																 aoTextureIndex + currentTexOffset,
																 emissiveTextureIndex + currentTexOffset,
																 metalicRoughnessTextureIndex + currentTexOffset,
																 baseColor,
															 	 static_cast<float>(mat.pbrMetallicRoughness.metallicFactor),
																 static_cast<float>(mat.pbrMetallicRoughness.roughnessFactor),
																 textures);
	}
}

SceneNode* GltfImporter::loadFromFile(VulkanDevice& device,
									  VkCommandPool commandPool,
									  std::string filename,
									  std::vector<utils::Vertex>& vertices,
									  std::vector<uint32_t>& indices,
									  std::vector<uint32_t>& indicesOffsets)
{
	std::string error;
	std::string warning;
	bool binary = false;
	bool fileLoaded = false;
	gltfContext = tinygltf::TinyGLTF();
	gltfModel = tinygltf::Model();

	//check format of file
	size_t extpos = filename.rfind('.', filename.length());
	if (extpos != std::string::npos) {
		binary = (filename.substr(extpos + 1, filename.length() - extpos) == "glb");
	}

	//load file
	fileLoaded = binary ? gltfContext.LoadBinaryFromFile(&gltfModel, &error, &warning, filename.c_str()) : gltfContext.LoadASCIIFromFile(&gltfModel, &error, &warning, filename.c_str());

	if (!fileLoaded) {
		throw "unable to load file";
	}

	SceneNode* sceneRoot = nullptr;

	//load scenes
	for (int i = 0; i < gltfModel.scenes.size(); i++) {
		tinygltf::Scene& scene = gltfModel.scenes[i];

		for (size_t j = 0; j < scene.nodes.size(); j++) {
			const tinygltf::Node node = gltfModel.nodes[scene.nodes[j]];
			sceneRoot = loadNode(nullptr, node, scene.nodes[j], vertices, indices, indicesOffsets, nullptr);
		}
	}

	return sceneRoot;
}

SceneNode* GltfImporter::loadFromFile(VulkanDevice& device,
									  VkCommandPool commandPool,
									  std::string filename,
									  std::vector<utils::Vertex>& vertices,
								      std::vector<uint32_t>& indices,
									  std::vector<uint32_t>& indicesOffsets,
								      std::vector<TextureImage*>& textures,
									  std::vector<Material*>& materials)
{
	std::string error;
	std::string warning;
	bool binary = false;
	bool fileLoaded = false;
	gltfContext = tinygltf::TinyGLTF();
	gltfModel = tinygltf::Model();

	//check format of file
	size_t extpos = filename.rfind('.', filename.length());
	if (extpos != std::string::npos) {
		binary = (filename.substr(extpos + 1, filename.length() - extpos) == "glb");
	}

	//load file
	fileLoaded = binary ? gltfContext.LoadBinaryFromFile(&gltfModel, &error, &warning, filename.c_str()) : gltfContext.LoadASCIIFromFile(&gltfModel, &error, &warning, filename.c_str());

	if (!fileLoaded) {
		throw "unable to load file";
	}

	//load textures and materials to buffers
	currentTexOffset = static_cast<int>(textures.size());
	textures.resize(gltfModel.images.size() + currentTexOffset);
	loadMaterials(device, commandPool, textures, materials);

	SceneNode* sceneRoot = new SceneNode(filename, nullptr, 100000);

	//load scenes
	for (int i = 0; i < gltfModel.scenes.size(); i++) {
		tinygltf::Scene& scene = gltfModel.scenes[i];

		for (size_t j = 0; j < scene.nodes.size(); j++) {
			const tinygltf::Node node = gltfModel.nodes[scene.nodes[j]];
			sceneRoot->addChild(loadNode(nullptr, node, scene.nodes[j], vertices, indices, indicesOffsets, &materials));
		}
	}

	return sceneRoot;
}