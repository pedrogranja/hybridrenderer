#pragma once
#include "vulkan/vulkan.h"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_vulkan.h"
#include "utils.hpp"
#include "ImGuiRenderpass.h"
#include "UniformBuffer.h"
#include "SceneGraph.h"
#include "BlurPostProcessing.h"
#include "DescriptorSet.h"
#include "VulkanDevice.h"

class ImGuiUi
{
private:
	bool active = true;

public:
	struct UiShaderInputs {
		float indirectLightingWeight = 1.0f;
		bool directDiffuse = true;
		bool directSpecular = true;
		bool indirectDiffuse = true;
		bool indirectSpecular = true;
		bool useEmissionTexture = true;
		bool useRoughnessTexture = true;
		bool useMetalnessTexture = true;
		bool useAoTexture = true;
		bool useAlbedoTexture = true;
		bool useNormalTexture = true;
		int lightToDebug = 0;
		bool fresnelFunctionOnly = false;
		bool uniformDistributionFunctionOnly = false;
		bool geometryFunctionOnly = false;
		float baseRoughness = 1.0f;
		float baseMetalness = 1.0f;
		float exposure = 2.2f;
		bool useShadows = true;
		float indirectDiffuseWeight = 0.05f;
		bool useReinhardToneMapping = true;
		float reinhardToneMappingWeight = 1.3f;
		float skyboxIntensity = 1.0f;
		float scaleA2 = 1.0f;
		float scaleNDotH = 1.0f;
		float scaleD = 1.0f;
		bool useTonemap = true;
		bool useDemodulation = true;
		bool useSkybox = true;
	} globalShaderInputs;

	struct UiShaderInputsDeviceSubmit {
		float indirectLightingWeight = 1.0f;
		int directDiffuse;
		int directSpecular;
		int indirectDiffuse;
		int indirectSpecular;
		int useEmissionTexture;
		int useRoughnessTexture;
		int useMetalnessTexture;
		int useAoTexture;
		int useAlbedoTexture;
		int useNormalTexture;
		int lightToDebug;
		int fresnelFunctionOnly;
		int uniformDistributionFunctionOnly;
		int geometryFunctionOnly;
		float baseRoughness;
		float baseMetalness;
		float exposure;
		int useShadows;
		float indirectDiffuseWeight;
		int useReinhardToneMapping = false;
		float reinhardToneMappingWeight;
		float skyboxIntensity = 1.0f;
		float scaleA2 = 1.0f;
		float scaleNDotH = 1.0f;
		float scaleD = 1.0f;
		int useTonemap = true;
		int useDemodulation = true;
	} globalShaderInputsDeviceSubmission;

	struct UiInputs {
		int msaaSamples = 1;
	} globalInputs;

	utils::QueueFamilyIndices queueFamilyIndices;
	std::vector<VkCommandBuffer> commandBuffers;
	VkDescriptorPool descriptorPool;
	VulkanDevice device;
	VkCommandPool commandPool;
	ImGuiRenderpass renderpass;
	SwapChain swapChain;
	VkDescriptorSetLayout descriptorLayout;
	std::vector<VkDescriptorSet> descriptorSets;
	std::vector<UniformBuffer> uniformBuffers;
	Scene* scene;
	std::vector<UIRenderable*> toRender;

	ImGuiUi();
	ImGuiUi(VulkanDevice& device,
			utils::QueueFamilyIndices& queueFamilyIndices, 
			SwapChain& swapChain,
			ImGuiRenderpass& renderpass,
			Scene* scene,
			std::vector<UIRenderable*>&& toRender);
	void createCommandPool();
	void createCommandBuffers();
	void createDescriptorPool();
	void initUniformBuffers();
	void createDescriptorSetLayout();
	void createDescriptorSet();
	void update(uint32_t imageIndex);
	void draw(uint32_t imageIndex);
	void createFontImage();
	void toggleActive();
	void cleanup();
};

