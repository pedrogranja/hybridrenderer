#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 1, binding = 0) uniform Material {
	int albedoTextureSet;
	int metallicRoughnessTextureSet;
	int normalTextureSet;
	int occlusionTextureSet;
	int emissiveTextureSet;
	float metallicFactor;
	float roughnessFactor;
	vec3 baseColor;
} mat;
layout(set = 1, binding = 1) uniform sampler2D texSampler;
layout(set = 1, binding = 2) uniform sampler2D normalSampler;
layout(set = 1, binding = 3) uniform sampler2D aoSampler;
layout(set = 1, binding = 4) uniform sampler2D emissionSampler;
layout(set = 1, binding = 5) uniform sampler2D metalRoughnessSampler;

layout(set = 2, binding = 0) uniform UiShaderInputs {
	float indirectLightingWeight;
	bool directDiffuse;
	bool directSpecular;
	bool indirectDiffuse;
	bool indirectSpecular;
	bool useEmissionTexture;
	bool useRoughnessTexture;
	bool useMetalnessTexture;
	bool useAoTexture;
	bool useAlbedoTexture;
	bool useNormalTexture;
    int lightToDebug;
	bool fresnelFunctionOnly;
	bool uniformDistributionFunctionOnly;
	bool geometryFunctionOnly;
	float baseRoughness;
	float baseMetalness;
    float exposure;
} globalShaderInputs;

layout(location = 0) in vec4 fragPos;
layout(location = 1) in vec3 fragColor;
layout(location = 2) in vec3 fragNormal;
layout(location = 3) in vec2 fragTexCoord;

layout (location = 0) out vec4 outPosition;
layout (location = 1) out vec4 outAlbedoAo;
layout (location = 2) out vec4 outNormal;
layout(location = 3) out vec4 outMetalRoughness;
layout(location = 4) out vec4 outEmission;

vec3 getNormalFromTex() {
    vec3 tangentNormal = texture(normalSampler,fragTexCoord).rgb * 2.0 - 1.0;

    vec3 q1 = dFdx(fragPos).xyz;
	vec3 q2 = dFdy(fragPos).xyz;
	vec2 st1 = dFdx(fragTexCoord);
	vec2 st2 = dFdy(fragTexCoord);

    vec3 N = normalize(fragNormal);
	vec3 T = normalize(q1 * st2.t - q2 * st1.t);
	vec3 B = -normalize(cross(N, T));
	mat3 TBN = mat3(T, B, N);

	return normalize(TBN * tangentNormal);
}

void main()
{
	outPosition = fragPos;

	//sample albedo
	vec4 albedo = vec4(mat.baseColor, 1.0); 
    if(mat.albedoTextureSet > -1 && globalShaderInputs.useAlbedoTexture) {
        albedo *= texture(texSampler,fragTexCoord);
	
    }
	if(mat.occlusionTextureSet > -1 && globalShaderInputs.useAoTexture) {
		albedo.w = texture(aoSampler,fragTexCoord).r;
	}

	outAlbedoAo = albedo;

	// Calculate normal
	vec3 N = normalize(fragNormal);
	if(mat.normalTextureSet > -1 && globalShaderInputs.useNormalTexture)
		N = getNormalFromTex();
	outNormal = vec4(N,1.0f);

	//sample roughness metal texture
	float metal = globalShaderInputs.baseMetalness;
	float roughness =  globalShaderInputs.baseRoughness;
    if(mat.metallicRoughnessTextureSet > -1) {
		vec4 metalRoughness = texture(metalRoughnessSampler,fragTexCoord);

		if(globalShaderInputs.useMetalnessTexture)
			metal *= texture(metalRoughnessSampler,fragTexCoord).b;

		if(globalShaderInputs.useRoughnessTexture)
			roughness *= texture(metalRoughnessSampler,fragTexCoord).g;
    }
	outMetalRoughness = vec4(0.0f, roughness ,metal, 0.0f);

	//sample emissive
	vec3 emissive = vec3(0.0f);
    if(mat.emissiveTextureSet > -1 && globalShaderInputs.useEmissionTexture) {
        emissive = texture(emissionSampler, fragTexCoord).rgb;
        if(emissive.x < 0.01f && emissive.y < 0.01f && emissive.z < 0.01f)
            emissive = vec3(0.0f);
    }
	outEmission = vec4(emissive,1.0);
}