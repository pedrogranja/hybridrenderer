#ifndef RAND_H
#define RAND_H
uint wang_hash(uint seed)
{
    seed = (seed ^ 61) ^ (seed >> 16);
    seed *= 9;
    seed = seed ^ (seed >> 4);
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> 15);
    return seed;
}

float wang_hash_float(uint seed) { return float(wang_hash(seed)) / 4294967296.0f; }

float rand(float co) { return fract(sin(co*(91.3458)) * 47453.5453); }
float rand(vec2 co) { return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453); }

uvec2 blockCipherTEA(uint v0, uint v1)
{
    uint sum = 0;
	uint iterations = 16;
    const uint delta = 0x9e3779b9;
    const uint k[4] = { 0xa341316c, 0xc8013ea4, 0xad90777d, 0x7e95761e }; // 128-bit key.
    for (uint i = 0; i < iterations; i++)
    {
        sum += delta;
        v0 += ((v1 << 4) + k[0]) ^ (v1 + sum) ^ ((v1 >> 5) + k[1]);
        v1 += ((v0 << 4) + k[2]) ^ (v0 + sum) ^ ((v0 >> 5) + k[3]);
    }
    return uvec2(v0, v1);
}

uint halton2Inverse(uint index, uint digits)
{
    index = (index << 16) | (index >> 16);
    index = ((index & 0x00ff00ff) << 8) | ((index & 0xff00ff00) >> 8);
    index = ((index & 0x0f0f0f0f) << 4) | ((index & 0xf0f0f0f0) >> 4);
    index = ((index & 0x33333333) << 2) | ((index & 0xcccccccc) >> 2);
    index = ((index & 0x55555555) << 1) | ((index & 0xaaaaaaaa) >> 1);
    return index >> (32 - digits);
}

uint halton3Inverse(uint index, uint digits)
{
    uint r = 0;
    for (uint d = 0; d < digits; ++d)
    {
        r = r * 3 + index % 3;
        index /= 3;
    }
    return r;
}

float Halton(uint b, uint i)
{
    float r = 0.0;
    float f = 1.0;
    while (i > 0) {
        f = f / float(b);
        r = r + f * float(i % b);
        i = int(floor(float(i) / float(b)));
    }
    return r;
}

vec2 getHaltonPseudoRandom(int offset, 
						   vec2 inUV,
						   uint xindex,
						   uint yindex,
						   uint frameIndex) 
{
	uint haltonIndex = ((halton2Inverse(xindex % 256, 8) * 76545 + halton3Inverse(yindex % 256, 6) * 110080) % (76545+110080)) + frameIndex * 186624;
	float r1 = Halton(7, haltonIndex);
	float r2 = Halton(13, haltonIndex);

	return vec2(r1, r2);
}

vec2 getBlueNoisePseudoRandom(int offset, 
							  vec2 inUV,
							  int width,
							  int height,
							  int frameIndex,
							  float time,
							  sampler2D[64] blueNoise) 
{
	ivec2 uv = ivec2(int(inUV.x * width) % 64, int(inUV.y * height) % 64);

	float r1 = fract(texelFetch(blueNoise[frameIndex % 64], uv, 0).x 
								+ wang_hash_float(frameIndex)); //cranley patterson rotation

	float r2 = fract(texelFetch(blueNoise[frameIndex % 64], uv, 0).y 
								+ wang_hash_float(frameIndex)); //cranley patterson rotation

	return vec2(r1, r2);
}

vec2 roughnessBias(vec2 xi, float roughness, float minBrdfBias, float maxBrdfBias, out float weight) {

	weight = clamp(roughness * 2.0f, 0.0f, 1.0f);
	xi.y = mix(0.0f, clamp(weight, minBrdfBias, maxBrdfBias), xi.y);
	return xi;
}

vec2 roughnessDistanceBias(vec2 xi, float roughness, float dist, float distanceWeight, float minBrdfBias, float maxBrdfBias, out float weight) {

	// bias the random generation based on roughness and distance to target
	weight = clamp(roughness * 6.0f, 0.0f, 1.0f);
	weight *= clamp(dist / distanceWeight, 0.0f, 1.0f);

	xi.y = mix(0.0f, clamp(weight, minBrdfBias, maxBrdfBias), xi.y);
	return xi;
}

#endif