#include "ShaderBindingTable.h"

ShaderBindingTable::ShaderBindingTable()
{
}

ShaderBindingTable::ShaderBindingTable(VulkanDevice& device,
									   VulkanInstance& instance,
									   VkCommandPool commandPool,
									   RaytracingPipeline& rayTracingPipeline) :
									   device(device),
									   instance(instance),
									   commandPool(commandPool),
									   rayTracingPipeline(rayTracingPipeline)
{
	createShaderBindingTable();
}

VkBuffer ShaderBindingTable::getSbtBuffer()
{
	return sbtBuffer.buffer;
}

VkDeviceAddress ShaderBindingTable::getSbtBufferAddress()
{
	return sbtBuffer.getDeviceAddress();
}

uint32_t ShaderBindingTable::getSbtSize()
{
	return sbtSize;
}

size_t ShaderBindingTable::getEntrySize()
{
	return entrySize;
}

void ShaderBindingTable::cleanup()
{
	sbtBuffer.cleanup();
}

size_t ShaderBindingTable::roundUp(size_t size, size_t powerOf2Alignment)
{
	return (size + powerOf2Alignment - 1) & ~(powerOf2Alignment - 1);
}


void ShaderBindingTable::createShaderBindingTable()
{
	VkPhysicalDeviceRayTracingPipelinePropertiesKHR* rayTracingProperties = static_cast<VkPhysicalDeviceRayTracingPipelinePropertiesKHR*>(device.getDeviceProperties2().pNext);
	entrySize = roundUp(rayTracingProperties->shaderGroupHandleSize + (uint32_t) 4, rayTracingProperties->shaderGroupBaseAlignment);
	std::vector<RaytracingPipeline::ShaderDescription> shaderDescriptions = rayTracingPipeline.getShaderDescriptions();
	std::vector<VkRayTracingShaderGroupCreateInfoKHR> shaderGroups = rayTracingPipeline.getShaderGroups();
	//allocate memory for the sbt
	sbtSize = static_cast<uint32_t>(entrySize) * static_cast<uint32_t>(shaderGroups.size());
	sbtBuffer = Buffer("Shader Binding Table",
					   device,
					   device.getQueues().graphicsQueue,
					   sbtSize,
					   VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
					   VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

	void* mapped = nullptr;
	//map memory so host can write to it
	std::vector<uint8_t> shaderHandleStorage(shaderGroups.size() * rayTracingProperties->shaderGroupHandleSize);
	//get the shader group handles
	VK_CHECK_RESULT(utils::vkGetRayTracingShaderGroupHandlesKHR(device.getLogicalDeviceHandle(),
																rayTracingPipeline.getPipeline(),
																0,
																static_cast<uint32_t>(shaderGroups.size()),
																sbtSize,
																shaderHandleStorage.data()));
	//write the shader handles to mapped memory
	vkMapMemory(device.getLogicalDeviceHandle(), sbtBuffer.bufferMemory, 0, sbtSize, 0, &mapped);
	auto* data = static_cast<uint8_t*>(mapped);
	for (uint32_t i = 0; i < shaderGroups.size(); i++)
	{
		memcpy(data, shaderHandleStorage.data() + shaderDescriptions[i].index * rayTracingProperties->shaderGroupHandleSize, rayTracingProperties->shaderGroupHandleSize);
		data += entrySize;
	}
	vkUnmapMemory(device.getLogicalDeviceHandle(), sbtBuffer.bufferMemory);
}
