#include "BloomBlendPostProcessing.h"

BloomBlendPostProcessing::BloomBlendPostProcessing()
{
}

BloomBlendPostProcessing::BloomBlendPostProcessing(VkPhysicalDevice physicalDevice,
												   VkDevice device,
												   VkQueue graphicsQueue,
												   utils::QueueFamilyIndices& queueFamilyIndices,
												   SwapChain& swapChain,
												   Pass& renderpass,
												   TextureImage& sceneImage,
												   TextureImage& bloomImage,
												   VertexFragPipeline* pipeline) :
																				   physicalDevice(physicalDevice),
																				   device(device),
																				   graphicsQueue(graphicsQueue),
																				   queueFamilyIndices(queueFamilyIndices),
																				   swapChain(swapChain),
																				   renderpass(renderpass),
																				   sceneImage(sceneImage),
																				   bloomImage(bloomImage),
																				   pipeline(pipeline)
{
}

void BloomBlendPostProcessing::createCommandPool()
{
	VkCommandPoolCreateInfo poolInfo{};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	VK_CHECK_RESULT(vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool));
}

void BloomBlendPostProcessing::createDescriptorPool()
{
	std::array<VkDescriptorPoolSize, 1> poolSizes{};
	poolSizes[0].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSizes[0].descriptorCount = swapChain.getSwapChainImages().size() * 2;

	VkDescriptorPoolCreateInfo poolInfo{};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = static_cast<uint32_t>(swapChain.getSize());

	VK_CHECK_RESULT(vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptorPool));
}

void BloomBlendPostProcessing::initUniformBuffers()
{
}

void BloomBlendPostProcessing::createDescriptorSetLayout()
{
	std::vector<VkDescriptorSetLayoutBinding> bindings;
	VkDescriptorSetLayoutBinding binding = {};

	//Define the descriptor set layout
	binding = initializers::descriptorSetLayoutBinding(0,
														VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
														VK_SHADER_STAGE_FRAGMENT_BIT);

	bindings.push_back(binding);

	binding = initializers::descriptorSetLayoutBinding(1,
														VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
														VK_SHADER_STAGE_FRAGMENT_BIT);

	bindings.push_back(binding);

	VkDescriptorSetLayoutCreateInfo layoutInfo{};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = bindings.size();
	layoutInfo.pBindings = bindings.data();

	VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &descriptorLayout));
}

void BloomBlendPostProcessing::createDescriptorSet()
{
	int descriptorCount = swapChain.getSize();
	std::vector<VkDescriptorSetLayout> layouts(descriptorCount, descriptorLayout);

	//Create the descriptor set
	VkDescriptorSetAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>(descriptorCount);
	allocInfo.pSetLayouts = layouts.data();

	descriptorSets.resize(descriptorCount);
	VK_CHECK_RESULT(vkAllocateDescriptorSets(device, &allocInfo, descriptorSets.data()));

	std::vector<VkWriteDescriptorSet> descriptorWrites;
	VkWriteDescriptorSet writeDescriptorSet = {};
	VkDescriptorImageInfo imageInfo{};

	for (int i = 0; i < descriptorCount; i++) {
		VkDescriptorImageInfo sceneImageInfo{};
		sceneImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		sceneImageInfo.imageView = sceneImage.textureImageView;
		sceneImageInfo.sampler = sceneImage.textureSampler;

		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.dstSet = descriptorSets[i];
		writeDescriptorSet.dstBinding = 0;
		writeDescriptorSet.dstArrayElement = 0;
		writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		writeDescriptorSet.descriptorCount = 1;
		writeDescriptorSet.pImageInfo = &sceneImageInfo;
		descriptorWrites.push_back(writeDescriptorSet);

		VkDescriptorImageInfo bloomBlendImageInfo{};
		bloomBlendImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		bloomBlendImageInfo.imageView = bloomImage.textureImageView;
		bloomBlendImageInfo.sampler = bloomImage.textureSampler;

		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.dstSet = descriptorSets[i];
		writeDescriptorSet.dstBinding = 1;
		writeDescriptorSet.dstArrayElement = 0;
		writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		writeDescriptorSet.descriptorCount = 1;
		writeDescriptorSet.pImageInfo = &bloomBlendImageInfo;
		descriptorWrites.push_back(writeDescriptorSet);

		vkUpdateDescriptorSets(device, descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);
	}
}

void BloomBlendPostProcessing::update(uint32_t imageIndex)
{
}

void BloomBlendPostProcessing::draw(VkCommandBuffer commandBuffer, uint32_t imageIndex)
{
	vkCmdBindDescriptorSets(commandBuffer,
							VK_PIPELINE_BIND_POINT_GRAPHICS,
							pipeline->getPipelineLayout(),
							0,
							1,
							&descriptorSets[imageIndex],
							0,
							nullptr);

	vkCmdDraw(commandBuffer, 3, 1, 0, 0);
}

void BloomBlendPostProcessing::cleanup()
{
	vkDestroyDescriptorPool(device, descriptorPool, nullptr);
	vkDestroyDescriptorSetLayout(device, descriptorLayout, nullptr);
	vkDestroyCommandPool(device, commandPool, nullptr);
}