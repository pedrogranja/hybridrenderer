#version 450

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;

layout(binding = 0) uniform BlurParams {
	float blurScale;
	float blurStrength;
	bool blurHorizontal;
} blurParams;

layout(binding = 1) uniform sampler2D sceneImage;

void main() 
{
	float weight[5];
	weight[0] = 0.227027;
	weight[1] = 0.1945946;
	weight[2] = 0.1216216;
	weight[3] = 0.054054;
	weight[4] = 0.016216;

	vec2 texOffset = 1.0 / textureSize(sceneImage, 0) * blurParams.blurScale;
	vec3 result = texture(sceneImage, inUV).rgb * weight[0];

	if(blurParams.blurHorizontal) {
		for(int i = 0; i < 5; i++) {
			result += texture(sceneImage,inUV + vec2(texOffset.x * i, 0.0f)).rgb * weight[i] * blurParams.blurStrength;
			result += texture(sceneImage,inUV - vec2(texOffset.x * i, 0.0f)).rgb * weight[i] * blurParams.blurStrength;
		}
	}
	else {
		for(int i = 0; i < 5; i++) {
			result += texture(sceneImage,inUV + vec2(0.0f, texOffset.x * i)).rgb * weight[i] * blurParams.blurStrength;
			result += texture(sceneImage,inUV - vec2(0.0f, texOffset.x * i)).rgb * weight[i] * blurParams.blurStrength;
		}
	}

	outFragColor = vec4(result,1.0f);
}