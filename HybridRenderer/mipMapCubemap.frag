#version 450

#define PI 3.1415926535897932384626433832795

layout (binding = 0) uniform UBO 
{
	mat4 view;
	mat4 projection;
	float roughness;
	uint numSamples;
} ubo;

layout (binding = 1) uniform samplerCube samplerEnv;

layout (location = 0) in vec3 inUVW;
layout (location = 0) out vec4 outColor;

// Based on http://byteblacksmith.com/improvements-to-the-canonical-one-liner-glsl-rand-for-opengl-es-2-0/
float random(vec2 co)
{
	float a = 12.9898;
	float b = 78.233;
	float c = 43758.5453;
	float dt= dot(co.xy ,vec2(a,b));
	float sn= mod(dt,3.14);
	return fract(sin(sn) * c);
}

vec2 hammersley2d(uint i, uint N) 
{
	// Radical inverse based on http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
	uint bits = (i << 16u) | (i >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	float rdi = float(bits) * 2.3283064365386963e-10;
	return vec2(float(i) /float(N), rdi);
}

float DistributionGGX(vec3 n, vec3 h, float roughness) {
    float a = roughness * roughness;
    float a2 = a * a;
    float nDotH = max(dot(n,h),0.0f);
    float nDotH2 = nDotH*nDotH;

    float numerator = a2;
    float denominator = (nDotH2 * (a2 - 1.0f) + 1.0f);
    denominator = PI * denominator * denominator;

    return numerator / denominator;
}

vec3 importanceSample_GGX(vec2 Xi, float roughness, vec3 normal) 
{
	// Maps a 2D point to a hemisphere with spread based on roughness
	float alpha = roughness * roughness;
	float phi = 2.0 * PI * Xi.x + random(normal.xz) * 0.1;
	float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (alpha*alpha - 1.0) * Xi.y));
	float sinTheta = sqrt(1.0 - cosTheta * cosTheta);

	//from spherical coordinates to cartesian coordinates
	vec3 H;
	H.x = cos(phi) * sinTheta;
	H.y = sin(phi) * sinTheta;
	H.z = cosTheta;

	// Tangent space
	vec3 up = abs(normal.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
	vec3 tangentX = normalize(cross(up, normal));
	vec3 tangentY = normalize(cross(normal, tangentX));

	// Convert to world Space
	return normalize(tangentX * H.x + tangentY * H.y + normal * H.z);
}

void main()
{	
	float envMapDim = float(textureSize(samplerEnv, 0).s);
	const vec3 n = normalize(inUVW);
	vec3 w0 = n;

	vec3 color = vec3(0.0);
	vec3 totalWeight = vec3(0.0);
	for(uint i = 0u; i < ubo.numSamples; i++) {
		vec2 Xi = hammersley2d(i, ubo.numSamples);
		vec3 H = importanceSample_GGX(Xi, ubo.roughness, n);
		vec3 wi = normalize(2.0 * dot(w0,H) * H - w0);

		float NDotWi = max(dot(n,wi),0.0f);
		if(NDotWi > 0.0) {
			color += texture(samplerEnv, wi).rgb * NDotWi;	
			totalWeight += NDotWi;
		}
	}

	outColor = vec4(color /= totalWeight, 1.0f);
}