#version 450

float SIGMA_N = 128.0f;
float SIGMA_L = 10.0f;
float SIGMA_D = 0.5f;
const float EPSILON = 0.00001;

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;

layout(push_constant) uniform Offset {
	int offset;
} offset;

layout(set=0, binding = 0) uniform Params {
	bool useNormalWeight;
	float normalSigma;
	bool useShadowWeight;
	float shadowSigma;
	bool useDepthWeight;
	float depthSigma;
	int currIteration;
	int maxIteration;
	bool useShadowAngleBasedBlurSize;
	bool useSeparableAtrous;
	bool useAtrous;
	int blurScale;
	bool blurVariance;
	float shadowAngle;
} params;

layout(set=0, binding = 1) uniform sampler2D currentImage;
layout(set=0, binding = 2) uniform sampler2D normalsSampler;
layout(set=0, binding = 3) uniform sampler2D positionSampler;
layout(set=0, binding = 4) uniform sampler2D colorMoments;
layout(set=0, binding = 5) uniform sampler2D depthDerivativesSampler;
layout(set=0, binding = 6) uniform sampler2D currentImageVarBlurred;

struct Plane 
{
	vec3 normal;
	vec3 pointOnPlane;
};

const float h[3] = { 1.0, 2.0 / 3.0, 1.0 / 6.0 };

const float offsets[5] = float[5]( -2, -1, 0, 1, 2 );
						
const float gaussKernel[9] = float[9](1.0/16.0, 1.0/8.0, 1.0/16.0,
									  1.0/8.0, 1.0/4.0, 1.0/8.0, 
									  1.0/16.0, 1.0/8.0, 1.0/16.0);

float luma(vec3 c){
    return dot(c, vec3(0.2126, 0.7152, 0.0722));
}

float normalWeight(vec3 originNormal, vec3 normal) {
	return pow(max(0.0f, dot(originNormal,normal)), params.normalSigma);
}

float varGauss(vec2 uv) {
	int c = 0;
	float res = 0.0;
	vec2 texelSize = 1.0 / textureSize(normalsSampler,0).xy;
	for(int i=-1; i <= 1; i++) {
		for(int j=-1; j <= 1; j++) {
			float var = texture(currentImage, uv + texelSize).w;
			res += gaussKernel[c] * var;
			c++;
		}
	}
	return res;
}

float shadowWeight(vec3 originShadow, float var, vec3 shadow) {
	float numerator = abs(originShadow.x - shadow.x);
	float varEps = 1e-3;
	float denominator = params.shadowSigma * sqrt(max(var + varEps, 0.0f));
	return exp(-numerator / denominator);
}

float depthWeight(float originDepth, float phiDepth, vec2 uv) {
	float depth = texture(depthDerivativesSampler, uv).x;
	float weightZ = (phiDepth == 0.0) ? 0.0f : abs(originDepth - depth) / phiDepth;
	return exp(-max(weightZ * params.depthSigma, 0.0f));
}

float rand(float co) { return fract(sin(co*(91.3458)) * 47453.5453); }
float rand(vec2 co){ return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453); }

void main() 
{
	if(!params.useAtrous) {
		 outFragColor = texture(currentImage, inUV);
		 return;
	}

	//Use shadow angle to define blur size
	int blurScale = 0;
	if(params.useShadowAngleBasedBlurSize) {
		if(params.shadowAngle > 6.0f)
			blurScale =  1 << min((params.blurScale + 1), 3);
		else
			blurScale =  1 << params.blurScale;
	}
	else
		blurScale =  1 << params.blurScale;

	//Separable bilateral filter has 2x the number of iterations
	int maxBlurIteration = 0;
	if(params.useSeparableAtrous) {
		maxBlurIteration = 8;
	}
	else {
		maxBlurIteration = 4;
	}

	//If we have reached the maximum number of iterations, pass through
	if(params.currIteration >= maxBlurIteration) {
		outFragColor = texture(currentImage, inUV);
		return;
	}

	//Load the necessary data
	vec4 originShadow = texture(currentImage, inUV);
	vec3 originNormal = texture(normalsSampler, inUV).rgb;		
	float originDepth = texture(depthDerivativesSampler, inUV).x;
	float phiDepth = max(max(texture(depthDerivativesSampler, inUV).y, texture(depthDerivativesSampler, inUV).z), 1e-8) * blurScale;
	float originVar = texture(currentImage, inUV).w;
	float originVarBlurred = 0.0f;
	if(params.blurVariance)
		originVarBlurred = varGauss(inUV);
	else
		originVarBlurred = texture(currentImage, inUV).w;

	//Define kernel size
	int maxKernelSize = 2;
	int kernelSizeX = maxKernelSize;
	int kernelSizeY = maxKernelSize;
	if(params.useSeparableAtrous) {
		if(params.currIteration % 2 == 0)
			kernelSizeX = 0;
		else
			kernelSizeY = 0;
	}

	vec4 finalShadow = vec4(0.0f);
	vec3 sumShadow = vec3(0.0);
	float varSum = 0.0;
	float weightSum = 0.0;

	vec2 texelSize = 1.0 / textureSize(currentImage, 0).xy;

	//visit around the center of the pixel
	for (int i = -kernelSizeX; i <= kernelSizeX; i++) {
        for (int j = -kernelSizeY; j <= kernelSizeY; j++) {
			//texelSize.x * params.offsets[j] * params.offset * offset.offset;
			vec2 uv = inUV + vec2(texelSize.x * float(i) * blurScale, texelSize.x * float(j) * blurScale);

			bool outside = uv.x < 0.0f || uv.y < 0.0f || uv.x > 1.0f || uv.y > 1.0f;

			if(!outside) {
				float weight = h[abs(i)] * h[abs(j)];
				vec4 shadow = texture(currentImage, uv);
				vec3 normal = texture(normalsSampler, uv).rgb;

				if(params.useShadowWeight) {
					float w = shadowWeight(originShadow.xyz, originVarBlurred, shadow.xyz);
					weight *= w;
				}

				if(params.useNormalWeight) {
					float w = normalWeight(originNormal, normal);
					weight *= w;
				}

				if(params.useDepthWeight) {
					float w = depthWeight(originDepth, phiDepth * length(vec2(i, j)), uv);
					weight *= w;
				}

				sumShadow += shadow.xyz * weight;
				varSum += shadow.w * weight * weight;
				weightSum += weight;
			}
		}
	}

	if(weightSum >= EPSILON) {
		sumShadow /= weightSum;
		varSum /= (weightSum * weightSum);

		//If its a separable bilateral filter, change variance only at the second iteration (when the vertical and horizontal passes are finished)
		if(params.useSeparableAtrous) {
			if(params.currIteration % 2 != 0)
				finalShadow = vec4(sumShadow, varSum);
			else
				finalShadow = vec4(sumShadow, originVar);
				
		}
		else
			finalShadow = vec4(sumShadow, varSum);
	}
	else {
		finalShadow = originShadow;
	}

	outFragColor = finalShadow;
}