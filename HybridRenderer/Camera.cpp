#include "Camera.h"

static const glm::vec2 hilton_seq_16[16] = {
    glm::vec2(0.00398f, 0.99203f),
    glm::vec2(0.49402f, 0.66002f),
    glm::vec2(0.24501f, 0.32802f),
    glm::vec2(0.74302f, 0.88136f),
    glm::vec2(0.12051f, 0.54935f),
    glm::vec2(0.61852f, 0.21735f),
    glm::vec2(0.36952f, 0.77069f),
    glm::vec2(0.86752f, 0.43868f),
    glm::vec2(0.05826f, 0.10668f),
    glm::vec2(0.55627f, 0.95514f),
    glm::vec2(0.30727f, 0.62313f),
    glm::vec2(0.80527f, 0.29113f),
    glm::vec2(0.18276f, 0.84447f),
    glm::vec2(0.68077f, 0.51246f),
    glm::vec2(0.43177f, 0.18046f),
    glm::vec2(0.92978f, 0.73380f),
};

Camera::Camera() {
}

Camera::Camera(float width,
                float height,
                glm::vec3 pos,
                glm::vec3 up,
                float fovy,
                float nearPlane,
                float farPlane,
                float yaw,
                float pitch,
                float sensitivity,
                float movSpeed) :
                                width(width),
                                height(height),
                                up(up),
                                fovy(fovy),
                                nearPlane(nearPlane),
                                farPlane(farPlane),
                                yaw(yaw),
                                pitch(pitch),
                                sensitivity(sensitivity),
                                movSpeed(movSpeed)
{
    this->pos = pos;
    useJitter = false;
    updateCameraVectors();
}


Camera::Camera(float width, 
               float height,
               glm::vec3 pos,
               glm::vec3 target,
               glm::vec3 up, 
               float fovy,
               float nearPlane,
               float farPlane,
               float sensitivity, 
               float movSpeed) :
                                width(width),
                                height(height),
                                up(up),
                                fovy(fovy),
                                nearPlane(nearPlane),
                                farPlane(farPlane),
                                pitch(pitch),
                                sensitivity(sensitivity),
                                movSpeed(movSpeed)
{
    this->pos = pos;
    setCameraTarget(target);
    useJitter = false;
    updateCameraVectors();
}

void Camera::ProcessMouseMovement(Mouse* mouse)
{
    ImGuiIO& io = ImGui::GetIO();
    if(!mouse->mouseLeftPressed || io.WantCaptureMouse || !mouse->movementUpdated) {
        return;
    }

    yaw += static_cast<float>(mouse->deltaX) * sensitivity;
    pitch += static_cast<float>(mouse->deltaY) * sensitivity;

    still = false;

    // update Front, Right and Up Vectors using the updated Euler angles
    updateCameraVectors();
}

void Camera::ProcessKeyboard(Keyboard* keyboard)
{
    still = true;
    Clock* clock = Clock::getInstance();

    if (keyboard->keyPressed(GLFW_KEY_W)) {
        still = false;
        glm::vec3 mov = front * movSpeed * clock->deltaTime;       
        this->pos += mov;
    }
    if (keyboard->keyPressed(GLFW_KEY_S)) {
        still = false;
        glm::vec3 mov = front * movSpeed * clock->deltaTime;
        this->pos -= mov;
    }
    if (keyboard->keyPressed(GLFW_KEY_A)) {
        still = false;
        glm::vec3 mov = right * movSpeed * clock->deltaTime;
        this->pos -= mov;
    }
    if (keyboard->keyPressed(GLFW_KEY_D)) {
        still = false;
        glm::vec3 mov = right * movSpeed * clock->deltaTime;
        this->pos += mov;
    }
}

//https://github.com/Haeri/PhotonBox/blob/cf20c2625b96eb3b3b1863146413796d2d2b99e3/src/math/Matrix4f.cpp
glm::mat4 Camera::createPerspective(float left, float right, float bottom, float top, float near, float far) {
    glm::mat4 ret;
    ret[0][0] = (2.0f * nearPlane) / (right - left);	ret[0][1] = 0;								ret[0][2] = (right + left) / (right - left);  ret[0][3] = 0;
    ret[1][0] = 0;								ret[1][1] = (2.0f * nearPlane) / (top - bottom);	ret[1][2] = (top + bottom) / (top - bottom);  ret[1][3] = 0;
    ret[2][0] = 0;								ret[2][1] = 0;								ret[2][2] = -(farPlane + nearPlane) / (farPlane - nearPlane);	  ret[2][3] = -(2.0f * farPlane * nearPlane) / (farPlane - nearPlane);
    ret[3][0] = 0;								ret[3][1] = 0;								ret[3][2] = -1.0f;							  ret[3][3] = 0;
    return ret;
}

//Projection extents at distance 1 from camera
//https://github.com/Haeri/PhotonBox/blob/cf20c2625b96eb3b3b1863146413796d2d2b99e3/src/component/Camera.cpp
glm::vec4 Camera::getProjectionExtents(float xOffset, float yOffset)
{
    float oneExtentY = glm::tan(0.5f * glm::radians(fovy));
    float oneExtentX = oneExtentY * width / height;
    float texelSizeX = oneExtentX / (0.5f * width);
    float texelSizeY = oneExtentY / (0.5f * height);
    float oneJitterX = texelSizeX * xOffset;
    float oneJitterY = texelSizeY * yOffset;

    return glm::vec4(oneExtentX, oneExtentY, oneJitterX, oneJitterY);// xy = frustum extents at distance 1, zw = jitter at distance 1
}

void Camera::update(int frameIndex)
{
    prevView = view;
    prevProjection = projection;
    prevProjectionNoJitter = projectionNoJitter;

    view = glm::lookAt(this->pos, this->pos + this->front, up);

    if (useJitter) {
        glm::vec2 offset = hilton_seq_16[frameIndex % 16];
        jitter = glm::vec2((glm::fract(offset.x + 0.5f) - 0.5f) / width, //Map value to [-0.5f, 0.5f]
                           (glm::fract(offset.y + 0.5f) - 0.5f) / height);                          //Map value to [-0.5f, 0.5f]

        glm::mat4 jitterMat(1.0f, 0.0f, 0.0f, 0.0f,
                            0.0f, 1.0f, 0.0f, 0.0f,
                            0.0f, 0.0f, 1.0f, 0.0f,
                            2.0f * jitter.x , 2.0f * jitter.y , 0.0f, 1.0f);

        projectionNoJitter = glm::perspective(glm::radians(fovy), width / height, nearPlane, farPlane);
        projection = jitterMat * projectionNoJitter;
        projectionNoJitter[1][1] *= -1;
        projection[1][1] *= -1;
    }
    else {
        projection = glm::perspective(glm::radians(fovy), width / height, nearPlane, farPlane);
        projection[1][1] *= -1;
        jitter = glm::vec2(0.0f, 0.0f);
        projectionNoJitter = projection;
    }
}

void Camera::updateWidthAndHeight(float width, float height)
{
    this->width = width;
    this->height = height;
}

glm::mat4 Camera::GetViewMatrix()
{
    return view;
}

glm::mat4 Camera::GetProjectionMatrix()
{
    return projection;
}

glm::mat4 Camera::GetProjectionMatrixNoJitter()
{
    return projectionNoJitter;
}

glm::mat4 Camera::GetPrevViewMatrix()
{
    return prevView;
}

glm::mat4 Camera::GetPrevProjectionMatrix()
{
    return prevProjection;
}

glm::mat4 Camera::GetPrevProjectionMatrixNoJitter()
{
    return prevProjectionNoJitter;
}

glm::vec2 Camera::GetJitter()
{
    return jitter;
}

void Camera::setCameraTarget(glm::vec3 target)
{
    glm::vec3 dir = glm::normalize(this->pos - target);
    pitch = -glm::asin(dir.y) * 180.0f / glm::pi<float>();
    yaw = atan2(-dir.x, dir.z) * 180.0f / glm::pi<float>() - 90.0f;
    updateCameraVectors();
}

void Camera::setCameraPos(glm::vec3 pos)
{
    this->still = false;
    this->pos = pos;
}

bool Camera::isStill()
{
    return still;
}

void Camera::updateCameraVectors()
{
    // calculate the new Front vector
    glm::vec3 front;
    front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    this->front = glm::normalize(front);
    // also re-calculate the Right and Up vector
    right = glm::normalize(glm::cross(front, glm::vec3(0.0f, 1.0f, 0.0f)));
    up = glm::normalize(glm::cross(right, front));
}
