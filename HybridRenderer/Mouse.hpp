#pragma once
#include "GLFW/glfw3.h"

class Mouse {
public:
	double prevX = 0.0f;
	double prevY = 0.0f;
	double deltaX = 0.0f;
	double deltaY = 0.0f;
	bool mouseLeftPressed = false;
	bool mouseRightPressed = false;
	//marks if there was an update or not
	bool movementUpdated = true;

	void consumeMovement() {
		movementUpdated = false;
	}

	void mouseMovementCallback(GLFWwindow* window, double xpos, double ypos) {
		deltaX = xpos - prevX;
		deltaY = prevY - ypos;

		prevX = xpos;
		prevY = ypos;

		movementUpdated = true;
	}

	void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
		if (button == GLFW_MOUSE_BUTTON_LEFT) {
			mouseLeftPressed = action;
		}
		
		if (button == GLFW_MOUSE_BUTTON_RIGHT) {
			mouseRightPressed = action;
		}
	}
};