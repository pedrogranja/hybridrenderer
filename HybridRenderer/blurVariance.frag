#version 450

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;

layout(push_constant) uniform BlurDir {
	bool hor;
} blurDir;

layout(binding = 0) uniform BlurParams {
	float blurScale;
	float blurStrength;
	int flags;
} blurParams;

layout(binding = 1) uniform sampler2D image;

void main() 
{
	//float weight[3] = { 0.27901, 0.44198, 0.27901};
	float weight[5] = { 0.153388,0.221461,0.250301,0.221461,0.153388 };

	vec2 texOffset = 1.0 / textureSize(image, 0) * blurParams.blurScale;
	float result = 0.0f;
	int j = 0;


	if(blurDir.hor) {
		for(int i = -2; i <= 2; i++) {
			result += texture(image,inUV + vec2(texOffset.x * i, 0.0f)).w * weight[j] * blurParams.blurStrength;
			j++;
		}
	}
	else {
		for(int i = -2; i <= 2; i++) {
			result += texture(image,inUV + vec2(0.0f, texOffset.y * i)).w * weight[j] * blurParams.blurStrength;
			j++;
		}
	}

	outFragColor = vec4(texture(image,inUV).rgb,result);
}