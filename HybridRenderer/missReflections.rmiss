#version 460
#extension GL_EXT_ray_tracing : require
#extension GL_EXT_nonuniform_qualifier : enable

#define PI 3.1415926535897932384626433832795

layout(binding = 4, set = 0) uniform UBO {
	vec4 camPos;
	vec4 lightPos;
	vec4 lightColor;
	float lightIntensity;
	int vertexSize;
	int frameIndex;
    float time;
	float baseRoughness;
	float baseMetalness;
	float indirectStrength;
	float minBrdfSampleBias;
	float maxBrdfSampleBias;
	bool useBlueNoise;
	bool useShadows;
	float shadowConeAngle;
	bool useReinhardToneMapping;
	float lumaMultiplier;
	float normalsInterpolation;
	int maxPathLength;
	float skyboxIntensity;	
	bool accumulate;
	int numAccumulatedFrames;
	int maxNumberAccumulatedFrames;
	int width;
	int height;
	bool useSvgfDemodulation;
	bool useImplicitSamplingOfEnvironmentMap;
} ubo;

layout(binding = 19, set = 0) uniform samplerCube environmentMap;

struct RayPayload {
	vec3 color;
	float dist;
	int pathLength;
};

layout(location = 0) rayPayloadInEXT RayPayload rayPayload;

void main()
{
	vec3 dir = gl_WorldRayDirectionEXT;
	dir.y = -dir.y; 
	rayPayload.color = ubo.skyboxIntensity * texture(environmentMap, dir).rgb;
	rayPayload.dist = 9999.0f;
}