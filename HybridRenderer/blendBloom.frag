#version 450

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;

layout(binding = 0) uniform sampler2D sceneImage;
layout(binding = 1) uniform sampler2D blendImage;

void main() 
{
	vec3 result = texture(sceneImage, inUV).rgb + texture(blendImage, inUV).rgb;
	outFragColor = vec4(result,1.0f);
}