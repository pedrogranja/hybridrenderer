#include "Clock.h"

Clock* Clock::instance = nullptr;

Clock::Clock() {
    prevFrameTimestamp = std::chrono::high_resolution_clock::now();
    startTimestamp = prevFrameTimestamp;
};

void Clock::update() {
    auto currentTime = std::chrono::high_resolution_clock::now();
    this->deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - prevFrameTimestamp).count();
    this->time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTimestamp).count();
    prevFrameTimestamp = currentTime;
}