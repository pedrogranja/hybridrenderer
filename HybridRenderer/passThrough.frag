#version 450

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;

layout(binding = 0) uniform sampler2D currentImage;

void main() 
{
	vec4 result = texture(currentImage, inUV);
	outFragColor = result;
}