#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Keyboard.hpp"
#include "Mouse.hpp"
#include "imgui.h"
#include "Clock.h"
#include <iostream>
#include "UIRenderable.h"

class Camera {

    float width;
    float height;
    float fovy;
    float nearPlane;
    float farPlane;
    glm::vec3 front;
    glm::vec3 right;
    glm::vec3 up;
    float yaw;
    float pitch;
    float sensitivity;
    float movSpeed;
    bool still;
    glm::mat4 view;
    glm::mat4 prevView;
    glm::mat4 prevProjection;
    glm::mat4 projection;
    glm::mat4 projectionNoJitter;
    glm::mat4 prevProjectionNoJitter;
    glm::vec2 jitter;

public:
    glm::vec3 pos;
    bool useJitter;

    Camera();
    Camera(float width,
           float height,
           glm::vec3 pos = glm::vec3(0.0f, 0.0f, 2.0f),
           glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f),
           float fovy = 60.0f,
           float nearPlane = 0.1f,
           float farPlane = 100000.0f,
           float yaw = -90.0f,
           float pitch = 0.0f,
           float sensitivity = 0.2f,
           float movSpeed = 3.5f);

    Camera(float width,
            float height,
            glm::vec3 pos = glm::vec3(0.0f, 0.0f, 2.0f),
            glm::vec3 target = glm::vec3(0.0f, 0.0f, 0.0f),
            glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f),
            float fovy = 60.0f,
            float near = 0.1f,
            float far = 100000.0f,
            float sensitivity = 0.2f,
            float movSpeed = 3.5f);

    void ProcessMouseMovement(Mouse* mouse);
    void ProcessKeyboard(Keyboard* keyboard);
    glm::mat4 createPerspective(float left, float right, float bottom, float top, float near, float far);
    glm::vec4 getProjectionExtents(float xOffset, float yOffset);
    void update(int frameIndex);
    void updateWidthAndHeight(float width, float height);
    glm::mat4 GetViewMatrix();
    glm::mat4 GetProjectionMatrix();
    glm::mat4 GetProjectionMatrixNoJitter();
    glm::mat4 GetPrevViewMatrix();
    glm::mat4 GetPrevProjectionMatrix();
    glm::mat4 GetPrevProjectionMatrixNoJitter();
    glm::vec2 GetJitter();
    void setCameraTarget(glm::vec3 target);
    void setCameraPos(glm::vec3 pos);
    bool isStill();
    void updateCameraVectors();

};