#include "RaytracingPipeline.h"

RaytracingPipeline::RaytracingPipeline()
{
}

RaytracingPipeline::RaytracingPipeline(std::string name,
									   VulkanDevice& device,
									   VkDescriptorSetLayout descriptorLayout,
									   std::vector<ShaderDescription>&& raygenShaderDescription,
									   std::vector<ShaderDescription>&& missShaderDescription,
									   std::vector<ShaderDescription>&& closestHitShaderDescription) : name(name),
																									   device(device),
																									   descriptorLayout(descriptorLayout),
																									   raygenShaderDescription(raygenShaderDescription), 
																									   missShaderDescription(missShaderDescription),
																									   closestHitShaderDescription(closestHitShaderDescription)
{
	createPipeline();
}

std::vector<VkRayTracingShaderGroupCreateInfoKHR> RaytracingPipeline::getShaderGroups()
{
	return shaderGroups;
}

VkPipeline RaytracingPipeline::getPipeline()
{
	return pipeline;
}

VkPipelineLayout RaytracingPipeline::getPipelineLayout()
{
	return  pipelineLayout;
}

void RaytracingPipeline::cleanup()
{
	vkDestroyPipeline(device.getLogicalDeviceHandle(), pipeline, nullptr);
	vkDestroyPipelineLayout(device.getLogicalDeviceHandle(), pipelineLayout, nullptr);
}

std::vector<RaytracingPipeline::ShaderDescription>& RaytracingPipeline::getShaderDescriptions()
{
	return shaderDescriptions;
}

std::vector<RaytracingPipeline::ShaderDescription>& RaytracingPipeline::getRaygenShaderDescription()
{
	return raygenShaderDescription;
}

std::vector<RaytracingPipeline::ShaderDescription>& RaytracingPipeline::getMissShaderDescription()
{
	return missShaderDescription;
}

std::vector<RaytracingPipeline::ShaderDescription>& RaytracingPipeline::getClosestHitShaderDescription()
{
	return closestHitShaderDescription;
}

void RaytracingPipeline::createPipeline()
{
	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo{};
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.setLayoutCount = 1;
	pipelineLayoutCreateInfo.pSetLayouts = &descriptorLayout;

	VK_CHECK_RESULT(vkCreatePipelineLayout(device.getLogicalDeviceHandle(), &pipelineLayoutCreateInfo, nullptr, &pipelineLayout));

	std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
	//RAYGEN SHADERS
	for (ShaderDescription shader : raygenShaderDescription) {
		VkRayTracingShaderGroupCreateInfoKHR groupCI = {};
		VkPipelineShaderStageCreateInfo shaderStage;

		shaderDescriptions.push_back(shader);
		shaderStage = initializers::shaderCreateInfo(utils::loadShader(shader.filename.data(),
													 device.getLogicalDeviceHandle()),
													 VK_SHADER_STAGE_RAYGEN_BIT_KHR);
		shaderStages.push_back(shaderStage);

		groupCI = initializers::createShaderGroup(shader.index);
		shaderGroups.push_back(groupCI);
	}

	//MISS SHADERS
	for (ShaderDescription shader : missShaderDescription) {
		VkRayTracingShaderGroupCreateInfoKHR groupCI = {};
		VkPipelineShaderStageCreateInfo shaderStage;

		shaderDescriptions.push_back(shader);
		shaderStage = initializers::shaderCreateInfo(utils::loadShader(shader.filename.data(),
													 device.getLogicalDeviceHandle()),
													 VK_SHADER_STAGE_MISS_BIT_KHR);
		shaderStages.push_back(shaderStage);

		groupCI = initializers::createShaderGroup(shader.index);
		shaderGroups.push_back(groupCI);
	}

	//CLOSEST HIT SHADERS
	for (ShaderDescription shader : closestHitShaderDescription) {
		VkRayTracingShaderGroupCreateInfoKHR groupCI = {};
		VkPipelineShaderStageCreateInfo shaderStage;

		shaderDescriptions.push_back(shader);
		shaderStage = initializers::shaderCreateInfo(utils::loadShader(shader.filename.data(),
													device.getLogicalDeviceHandle()),
													VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR);
		shaderStages.push_back(shaderStage);

		groupCI = initializers::createShaderGroup(shader.index);
		groupCI.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR;
		groupCI.generalShader = VK_SHADER_UNUSED_KHR;
		groupCI.closestHitShader = shader.index;
		shaderGroups.push_back(groupCI);
	}

	VkRayTracingPipelineCreateInfoKHR pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR;
	pipelineInfo.pNext = nullptr;
	pipelineInfo.flags = 0;
	pipelineInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
	pipelineInfo.pStages = shaderStages.data();
	pipelineInfo.groupCount = static_cast<uint32_t>(shaderGroups.size());
	pipelineInfo.pGroups = shaderGroups.data();
	pipelineInfo.maxPipelineRayRecursionDepth = 1;
	pipelineInfo.layout = pipelineLayout;
	pipelineInfo.basePipelineHandle = nullptr;
	pipelineInfo.basePipelineIndex = 0;

	VK_CHECK_RESULT(utils::vkCreateRayTracingPipelinesKHR(device.getLogicalDeviceHandle(), nullptr, nullptr, 1, &pipelineInfo, nullptr, &pipeline));

	for (int i = 0; i < shaderStages.size(); i++)
		vkDestroyShaderModule(device.getLogicalDeviceHandle(), shaderStages[i].module, nullptr);

	device.nameVulkanObject(VK_OBJECT_TYPE_PIPELINE_LAYOUT, (uint64_t)pipelineLayout, name + " Pipeline Layout");
	device.nameVulkanObject(VK_OBJECT_TYPE_PIPELINE, (uint64_t)pipeline, name + " Pipeline");
}