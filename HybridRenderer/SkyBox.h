#pragma once
#include "vulkan/vulkan.h"
#include "TextureCubeMap.h"
#include "GltfImporter.h"
#include "utils.hpp"
#include "Mouse.hpp"
#include "ImGuiUi.h"
#include "DescriptorSet.h"

class SkyBox
{
public:
	struct skyboxUniform : utils::Uniform {
		alignas(16) glm::mat4 proj;
		alignas(16) glm::mat4 view;
		alignas(4) float skyboxIntensity;
		alignas(4) float exposure;
		alignas(4) bool useTonemap;
		alignas(4) bool useSkybox;
	};

	VulkanInstance instance;
	VulkanDevice device;
	VkCommandPool commandPool;
	VkQueue graphicsQueue;
	SwapChain* swapChain;

	//cube map texture
	TextureCubeMap textureCubeMap;
	//cube geometry
	SceneNode* cube;
	Camera* camera;
	ImGuiUi* imGuiUi;
	std::vector<uint32_t> indices;
	std::vector<utils::Vertex> vertices;
	VertexBuffer vertexBuffer;
	IndexBuffer indexBuffer;
	
	bool updatable;

	UniformBuffer uniformBuffer;
	DescriptorSet descriptorSet;

	SkyBox();
	SkyBox(VulkanInstance instance,
		   VulkanDevice device,
		   VkCommandPool commandPool,
		   VkQueue graphicsQueue,
		   SwapChain* swapChain,
		   Camera* camera,
		   ImGuiUi* imGuiUi);

	SkyBox(VulkanInstance instance,
		   VulkanDevice device,
		   VkCommandPool commandPool,
		   VkQueue graphicsQueue,
		   SwapChain* swapChain);
	void cleanup();
	void loadSkyBox(std::string filename, GltfImporter* gltfLoader, uint32_t uboSize = sizeof(skyboxUniform));
	void submitSkyBoxData();
	void ready();
	void update(int currentImage);
	void draw(VkCommandBuffer drawCommandbuffer, VkPipelineLayout pipelineLayout, int currentImage);
	//update the first image
	void update(utils::Uniform* uboData);
	//draws the first image
	void draw(VkCommandBuffer drawCommandbuffer, VkPipelineLayout pipelineLayout);
	void createDescriptorLayout();
	void createDescriptorPool();
	void createDescriptorSets();
};

