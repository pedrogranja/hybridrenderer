#include "VulkanDevice.h"

VulkanDevice::VulkanDevice()
{
}

VulkanDevice::VulkanDevice(VulkanInstance instance) : instance(instance)
{
    deviceFeatures2Initialized = false;
    deviceProperties2Initialized = false;

    setEnabledFeatures();
    createLogicalDevice();
}

void VulkanDevice::createLogicalDevice()
{
    //information about the queues
    utils::QueueFamilyIndices queueFamilyIndices = utils::findQueueFamilies(instance.getPhysicalDevice(), instance.getSurface());
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    std::set<uint32_t> uniqueQueueFamilies = { queueFamilyIndices.graphicsFamily.value(), queueFamilyIndices.presentFamily.value() };
    float queuePriority = 1.0f;
    for (uint32_t queueFamily : uniqueQueueFamilies) {
        VkDeviceQueueCreateInfo queueCreateInfo = {};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queueFamily;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        queueCreateInfos.push_back(queueCreateInfo);
    }

    VkPhysicalDeviceFeatures deviceFeatures = {};
    deviceFeatures.samplerAnisotropy = VK_TRUE;
    deviceFeatures.sampleRateShading = VK_TRUE; // enable sample shading feature for the device

    VkDeviceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pQueueCreateInfos = queueCreateInfos.data();
    createInfo.pEnabledFeatures = &deviceFeatures;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(DEVICEEXTENSIONS.size());
    createInfo.ppEnabledExtensionNames = DEVICEEXTENSIONS.data();
    createInfo.pNext = &rayTracingFeatures;

    //activate device validation layer features (legacy but is a nice to have for old VK operations)
    if (enableValidationLayers) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(VALIDATIONLAYERS.size());
        createInfo.ppEnabledLayerNames = VALIDATIONLAYERS.data();
    }
    else {
        createInfo.enabledLayerCount = 0;
    }

    VK_CHECK_RESULT(vkCreateDevice(instance.getPhysicalDevice(), &createInfo, nullptr, &logicalDeviceHandle))

    //get the handle for the graphics and presentation queue of the device
    vkGetDeviceQueue(logicalDeviceHandle, queueFamilyIndices.graphicsFamily.value(), 0, &queues.graphicsQueue);
    vkGetDeviceQueue(logicalDeviceHandle, queueFamilyIndices.presentFamily.value(), 0, &queues.presentationQueue);
}

void VulkanDevice::setEnabledFeatures()
{
    enabledBufferDeviceAddresFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES;
    enabledBufferDeviceAddresFeatures.bufferDeviceAddress = true;

    indexingFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES;
    indexingFeatures.pNext = &enabledBufferDeviceAddresFeatures;
    indexingFeatures.runtimeDescriptorArray = true;
    indexingFeatures.descriptorBindingPartiallyBound = true;

    accelerationStructureFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
    accelerationStructureFeatures.pNext = &indexingFeatures;
    accelerationStructureFeatures.accelerationStructure = true;

    rayTracingFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR;
    rayTracingFeatures.pNext = &accelerationStructureFeatures;
    rayTracingFeatures.rayTracingPipeline = true;
}

void VulkanDevice::cleanup()
{
    vkDestroyDevice(logicalDeviceHandle, nullptr);
}

VkPhysicalDeviceAccelerationStructureFeaturesKHR& VulkanDevice::getAccelerationStructureFeatures()
{
    return accelerationStructureFeatures;
}

VkPhysicalDeviceProperties2& VulkanDevice::getDeviceProperties2() 
{
    if (!deviceProperties2Initialized) {
        accelProps.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_PROPERTIES_KHR;
        pipelineProps.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;
        pipelineProps.pNext = &accelProps;
     
        deviceProperties2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
        deviceProperties2.pNext = &pipelineProps;
        vkGetPhysicalDeviceProperties2(instance.getPhysicalDevice(), &deviceProperties2);
        deviceProperties2Initialized = true;
    }

    return deviceProperties2;
}

VkDevice& VulkanDevice::getLogicalDeviceHandle()
{
    return logicalDeviceHandle;
}

VkPhysicalDevice& VulkanDevice::getPhysicalDeviceHandle()
{
    return instance.getPhysicalDevice();
}

void VulkanDevice::nameVulkanObject(VkObjectType objectType, uint64_t objectHandle, std::string name) {
    if(enableValidationLayers) {
        VkDebugUtilsObjectNameInfoEXT objName = {};

        objName.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT;
        objName.pNext = NULL;
        objName.objectType = objectType;
        objName.objectHandle = (uint64_t)objectHandle;
        objName.pObjectName = name.c_str();

        instance.setDebugUtilsObjectNameEXT(this->logicalDeviceHandle, &objName);
    }
}

VulkanDevice::Queues& VulkanDevice::getQueues()
{
    return queues;
}