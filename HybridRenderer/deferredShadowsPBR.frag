#version 450

#include "constants.glsl"
#include "bsdf.glsl"

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outColor;
layout (location = 1) out vec4 bloomColor;

layout(set = 0, binding = 0) uniform sampler2D positionSampler;
layout(set = 0, binding = 1) uniform sampler2D albedoAoSampler;
layout(set = 0, binding = 2) uniform sampler2D normalSampler;
layout(set = 0, binding = 3) uniform sampler2D metalRoughnessSampler;
layout(set = 0, binding = 4) uniform sampler2D shadowsSampler;
layout(set = 0, binding = 5) uniform sampler2D reflectionsSampler;
layout(set = 0, binding = 6) uniform sampler2D textureAlbedoSampler;
layout(set = 0, binding = 7) uniform sampler2D reflectionsRayHitPoints;

struct Sun {
    vec4 dir;
    vec4 lightColor;
    float lightIntensity;
};

layout(set = 1, binding = 0) uniform SceneParams {
    float maxLevelRoughnessMipMap;
    vec3 cameraPos;
    Sun suns[1024];
    int numSuns;
} sceneParams;

vec3 demodulate(vec3 c, vec3 albedo)
{
    return c / max(albedo, vec3(0.001, 0.001, 0.001));
}

layout(set = 1, binding = 1) uniform sampler2D brdfLut;
layout(set = 1, binding = 2) uniform samplerCube cubeMapSampler;
layout(set = 1, binding = 3) uniform samplerCube irradianceCubemap;
layout(set = 1, binding = 4) uniform samplerCube mipMapRoughnessCubemap;

layout(set = 2, binding = 0) uniform UiShaderInputs {
	float indirectLightingWeight;
	bool directDiffuse;
	bool directSpecular;
	bool indirectDiffuse;
	bool indirectSpecular;
	bool useEmissionTexture;
	bool useRoughnessTexture;
	bool useMetalnessTexture;
	bool useAoTexture;
	bool useAlbedoTexture;
	bool useNormalTexture;
    int lightToDebug;
	bool fresnelFunctionOnly;
	bool uniformDistributionFunctionOnly;
	bool geometryFunctionOnly;
	float baseRoughness;
	float baseMetalness;
    float exposure;
    int useShadows;
    float indirectDiffuseWeight;
	bool useReinhardToneMapping;
	float reinhardToneMappingWeight;
	float skyboxIntensity;
	float scaleA2;
	float scaleNDotH;
	float scaleD;
    int useTonemap;
    bool useDemodulation;
} globalShaderInputs;

float random(vec2 co)
{
	float a = 12.9898;
	float b = 78.233;
	float c = 43758.5453;
	float dt= dot(co.xy ,vec2(a,b));
	float sn= mod(dt,3.14);
	return fract(sin(sn) * c);
}

// From http://filmicgames.com/archives/75
// and sacha willems https://github.com/SaschaWillems/Vulkan/blob/master/data/shaders/glsl/pbrtexture/pbrtexture.frag
vec3 Uncharted2Tonemap(vec3 x)
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;
	return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

vec4 tonemap(vec4 color)
{
	vec3 outcol = Uncharted2Tonemap(color.rgb);
	outcol = outcol * (1.0f / Uncharted2Tonemap(vec3(11.2f)));
	return vec4(outcol,1.0);
}

vec2 hammersley2d(uint i, uint N) 
{
	// Radical inverse based on http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
	uint bits = (i << 16u) | (i >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	float rdi = float(bits) * 2.3283064365386963e-10;
	return vec2(float(i) /float(N), rdi);
}

vec3 getIBL(vec3 n, vec3 reflection) {
    return vec3(0.0f);
}

void main() 
{   vec2 normalTexSize = textureSize(normalSampler, 0).xy;
    vec4 fragPos = texture(positionSampler, inUV);
    vec3 textureAlbedo = texture(textureAlbedoSampler, inUV).rgb;
    vec3 albedo = texture(albedoAoSampler,inUV).rgb * textureAlbedo;
    float ao = texture(albedoAoSampler,inUV).w;
    vec3 normal = texture(normalSampler,inUV).rgb;
    if(normal == vec3(0.0f,0.0f,0.0f))
        discard;
    vec4 metalRoughness = texture(metalRoughnessSampler,inUV);
    float roughness = metalRoughness.g;
    float metal = metalRoughness.b;
    float shadow = texture(shadowsSampler, inUV).r;

    //debug parameters
    float debugNDF;
    float debugG;
    vec3 debugF;

    //sample emissive
    vec3 emissive = vec3(0.0f);

    vec3 finalColor = vec3(0.0f);
    vec3 w0 = normalize(sceneParams.cameraPos - fragPos.xyz);
    float w0DotN = max(dot(normal,w0), 0.01f);

    //base reflectivity
    vec3 F0 = vec3(0.04f);
    F0 = mix(F0,albedo,metal);
    vec3 radiance;

    for(uint i = 0; i < sceneParams.numSuns; i++) {
        //lights
        vec3 lightDir = -sceneParams.suns[i].dir.rgb;
        vec3 lightColor = sceneParams.suns[i].lightColor.rgb;
        float lightIntensity = sceneParams.suns[i].lightIntensity;
    
        //lighting
        radiance = lightColor * lightIntensity; //incoming light
        vec3 wi = lightDir;
        vec3 h = normalize(wi + w0);
        float nDotWi = max(dot(normal, wi), 0.01f);

        ShadingData sd = getShadingData(albedo, metal, roughness, w0+0.000001f, wi, normal);
        SpecularData specData = getSpecularData(sd);
	    DiffuseData dd = getDiffuseData(sd, specData);

        vec3 specular = vec3(0.0f);
        if(sd.roughness > 0.05f)
            specular = specData.ggx;
        vec3 diffuse = clamp(dd.diffuse, vec3(0.0f), vec3(1.0f));

        if(globalShaderInputs.lightToDebug == i) {
            debugG = specData.G;
            debugF =  specData.F;
            debugNDF = specData.NDF;
        }

        //if deactivated...
        if(!globalShaderInputs.directDiffuse)
            diffuse = vec3(0.0f);

        if(!globalShaderInputs.directSpecular)
            specular = vec3(0.0f);

        if(radiance.x <= 0.01 && radiance.y <= 0.01 && radiance.z <= 0.01)
            finalColor += vec3(0.0,0.0,0.0);
        else
            finalColor += (diffuse * sd.wiDotN + specular) * radiance;
    }
    finalColor = max(finalColor, vec3(0.0f));
    finalColor /= sceneParams.numSuns;
    finalColor += emissive;
    if(globalShaderInputs.useShadows > 0)
        finalColor *= shadow;
    //Gamma Correct
    //finalColor = finalColor / (luma(finalColor) + 1.0f);
    //finalColor = pow(finalColor, vec3(1.0/2.2));

    //IBL
    
    //indirect diffuse
    vec3 irradiance = texture(irradianceCubemap, -normal).rgb;
	vec3 diffuse = mix(albedo, vec3(0.0f), metal) * globalShaderInputs.indirectDiffuseWeight;

    float f = (1.5f - 1.0f) / (1.5f + 1.0f);
    F0 = vec3(f * f);
    vec3 specular;
    if(globalShaderInputs.useDemodulation)
        specular = texture(reflectionsSampler, inUV).xyz * (mix(textureAlbedo, vec3(0.0f), metal) + mix(vec3(F0), textureAlbedo, metal));
    else
        specular = texture(reflectionsSampler, inUV).xyz;

    if(globalShaderInputs.useReinhardToneMapping) {
        specular = specular * (1.0f + luma(specular)) * globalShaderInputs.reinhardToneMappingWeight;
    }

    if(isnan(specular.x) || isnan(specular.y) || isnan(specular.z))
        specular = vec3(0.0f, 0.0f, 0.0f);

    //if deactivated...
    if(!globalShaderInputs.indirectDiffuse)
        diffuse = vec3(0.0f);

    if(!globalShaderInputs.indirectSpecular)
        specular = vec3(0.0f);

    finalColor += (diffuse + specular) * ao * globalShaderInputs.indirectLightingWeight;
    //finalColor = min(finalColor, vec3(1.0f, 1.0f, 1.0f));

    if(globalShaderInputs.fresnelFunctionOnly)
        outColor = vec4(debugF,1.0f);//vec4(debugF+FIndirect,1.0f);

    else if(globalShaderInputs.uniformDistributionFunctionOnly)
        outColor = vec4(debugNDF,debugNDF,debugNDF,1.0f);

    else if(globalShaderInputs.geometryFunctionOnly)
        outColor = vec4(debugG,debugG,debugG,1.0f);

    else {
        if(globalShaderInputs.useTonemap > 0)
            outColor = tonemap(vec4(finalColor * globalShaderInputs.exposure, 1.0f));
        else
            outColor = vec4(finalColor, 1.0f);
    }

    //outColor = vec4(tmp, 1.0f);
    float brightness = dot(outColor.rgb, vec3(0.2126, 0.7152, 0.0722));
    if(brightness > 1.0)
        bloomColor = vec4(outColor.rgb, 1.0);
    else
        bloomColor = vec4(0.0, 0.0, 0.0, 1.0);
    bloomColor += vec4(emissive,1.0f);
}