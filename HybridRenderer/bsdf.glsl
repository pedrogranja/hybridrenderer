#ifndef BSDF_H
#define BSDF_H

#include "constants.glsl"
#include "rand.glsl"

float luma(vec3 c){
    return dot(c, vec3(0.2126, 0.7152, 0.0722));
}

vec3 evalFresnelSchlick(vec3 f0, vec3 f90, float cosTheta)
{
    return f0 + (f90 - f0) * pow(max(1.0f - cosTheta, 0.0f), 5.0f); // Clamp to avoid NaN if cosTheta = 1+epsilon
}

float DistributionGGX(float NdotH, float alpha )
{
	float a2 =  alpha * alpha;
	//float d = max(((NdotH * a2 - NdotH) * NdotH + 1.0f), EPSILON);
	float d = (NdotH * NdotH) * (a2-1.0f) + 1.0f;
    return a2 / (d * d * PI); //a2 /(d * d * PI);
}

float smithLambda(float wDotN, float alpha) 
{
    float wDotN2 = wDotN * wDotN;
    float tanSqrd = max((1-wDotN2),0.0f) / wDotN2;

    return 0.5f * (-1 + sqrt(1 + alpha * tanSqrd));
}

float SmithHeightCorrelated(float w0DotN, float wiDotN, float alpha) 
{
    if (w0DotN <= 0 || wiDotN <= 0) 
        return 0;

    return 1.0/(1.0+smithLambda(w0DotN,alpha)+smithLambda(wiDotN,alpha));
}

struct ShadingData {
    vec3 diffuseAlbedo;
    vec3 specularAlbedo;
    float metal;
    float roughness;
    float alpha;
    vec3 w0;
    vec3 wi;
    vec3 normal;
    vec3 h;

    float w0DotH;
    float w0DotN;
    float wiDotN;
    float hDotN;
    float hDotWi;
};

struct SpecularData {
    float NDF;
    float G;
    vec3 F;
    vec3 ggx;
    float pdf;
};

struct DiffuseData {
    vec3 diffuse;
};

//calculates the necessary components for the shading for this specific w0, wi and point
ShadingData getShadingData(vec3 albedo, float metal, float roughness, vec3 w0, vec3 wi, vec3 normal) 
{
    ShadingData sd;
    sd.metal = metal;
    sd.roughness =  max(roughness, EPSILON);
    sd.alpha = max(sd.roughness * sd.roughness, 0.0064f);
    sd.w0 = w0;
    sd.wi = wi;
    sd.normal = normal;
	float f = (1.5f - 1.0f) / (1.5f + 1.0f);
    float F0 = f * f;
    sd.diffuseAlbedo = mix(albedo, vec3(0.0f), metal);
    sd.specularAlbedo = mix(vec3(F0), albedo, sd.metal);

    sd.h = normalize(w0 + wi);
	sd.wiDotN =  max(dot(wi, normal), 0.0f);
	sd.w0DotN = max(dot(w0, normal), 0.0f);
    sd.hDotN = max(dot(sd.h, normal), 0.0f);
	sd.hDotWi = max(dot(sd.h, wi), EPSILON);
	sd.w0DotH = max(dot(w0, sd.h), 0.0f);
    return sd;
}

//calculates the specular shading
SpecularData getSpecularData(ShadingData sd) 
{
    SpecularData specData;
    //Normal distribution function
    specData.NDF = DistributionGGX(sd.hDotN, sd.alpha);

    //Geometry Function
    specData.G = SmithHeightCorrelated(sd.w0DotN, sd.wiDotN, sd.alpha);
    //Fresnel
    specData.F = evalFresnelSchlick(sd.specularAlbedo, vec3(1.0f, 1.0f, 1.0f), sd.w0DotH);

    if (min(sd.w0DotN, sd.wiDotN) < 1e-6) 
        specData.ggx = vec3(0.0f);
    else {
        specData.ggx = specData.NDF * specData.F * specData.G / (4.0f * sd.w0DotN);
    }
    
    specData.pdf = max(specData.NDF * sd.hDotN / (4.0 * sd.w0DotN), EPSILON);
    return specData;
}

//calculates the diffuse shading
DiffuseData getDiffuseData(ShadingData sd, SpecularData specData)
{
    DiffuseData dd;
    dd.diffuse = sd.diffuseAlbedo / PI;
    return dd;
}

vec3 importanceSampleGGX(vec2 Xi, float alpha, vec3 normal, vec3 w0) 
{
	float phi = 2.0 * PI * Xi.x;
	float cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (alpha*alpha - 1.0) * Xi.y));
	float sinTheta = sqrt(1.0 - cosTheta * cosTheta);

	//from spherical coordinates to cartesian coordinates
	vec3 H;
	H.x = cos(phi) * sinTheta;
	H.y = sin(phi) * sinTheta;
	H.z = cosTheta;
	
	// Tangent space
	vec3 up = abs(normal.z) < 0.999 ? vec3(0.0, 0.0, 1.0) : vec3(1.0, 0.0, 0.0);
	vec3 tangentX = normalize(cross(up, normal));
	vec3 tangentY = normalize(cross(normal, tangentX));

	// Convert to world Space
	vec3 h = normalize(tangentX * H.x + tangentY * H.y + normal * H.z);
	
	vec3 wi = reflect(w0, h);

	if(dot(wi, normal) > 0.0f) {
        return wi;
    }
	else {
		return vec3(0.0f);
	}
}

#endif