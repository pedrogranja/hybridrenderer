#pragma once
#include <vector>
#include "SwapChain.h"
#include "config.hpp"
#include "TextureImage.h"
#include "VulkanDevice.h"
#include "VulkanInstance.h"
#include "DepthImage.h"

class RenderTarget
{
private:
	VulkanInstance instance;
	VulkanDevice device;
	SwapChain swapChain;
	VkCommandPool commandPool;

	VkFormat format;
	VkExtent2D extent;
	int channelNumber;
	VkImageLayout finalLayout;
	VkImageUsageFlagBits usage;

	VkAttachmentDescription attachment{};
	TextureImage textureImage;
	int numImages;
	std::string name;

public:
	int id;
	RenderTarget();
	RenderTarget(std::string name,
				 VkFormat format,
				 VkExtent2D extent,
				 int channelNumber,
				 VkImageLayout finalLayout,
				 VkImageUsageFlagBits usage,
				 VulkanDevice& device,
				 VulkanInstance& instance,
				 SwapChain& swapChain,
				 VkCommandPool& commandPool,
				 int numImages = 1);

	VkAttachmentDescription& getAttachmentDescription();
	VkFormat& getFormat();
	VkImageLayout& getFinalImageLayout();
	TextureImage& getTextureImage();
	VkExtent2D& getExtent();
	void cleanup();
	bool operator==(const RenderTarget& other) const;
private:
	void createAttachmentDescription();
	//Memory that will hold the resource
	void createTextureImage();
};

namespace std
{
	template <>
	struct hash<RenderTarget>
	{
		std::size_t operator()(const RenderTarget& k) const
		{
			return k.id;
		}
	};
}