#pragma once
#include "glm/glm.hpp"
#include <vector>
#include <iostream>

class CameraMovement
{
public:
	struct Keyframe {
		glm::vec3 pos;
		glm::vec3 target;
		float time;

		Keyframe() {};
		Keyframe(glm::vec3 pos, glm::vec3 target, float time) : pos(pos), target(target), time(time) {}
	};

	std::vector<Keyframe> keyframes;

	CameraMovement();
	void addKeyframe(CameraMovement::Keyframe keyframe);
	Keyframe evaluate(float time);
};