#pragma once
#include <vector>
#include <vulkan/vulkan.h>
#include "VulkanDevice.h"
#include "SwapChain.h"
#include "utils.hpp"
#include "FrameGraph.h"
#include "config.hpp"

class VertexFragPipeline
{
private:
	const char* vertexShaderFile;
	const char* fragShaderFile;
	VulkanDevice vulkanDevice;
	SwapChain swapChain;
	Pass renderPass;
	VkPipelineLayout pipelineLayout;
	VkPipeline pipeline;
	std::string name;

public:
	VertexFragPipeline();
	VertexFragPipeline(std::string name, 
					   const char* vertexShaderFile,
					   const char* fragShaderFile,
					   VulkanDevice& vulkanDevice,
					   SwapChain& swapChain,
					   Pass& renderPass,
					   std::vector<VkPipelineColorBlendAttachmentState>& colorBlendAttachments,
					   std::vector<VkDescriptorSetLayout>& descriptorLayouts,
					   std::vector<VkVertexInputBindingDescription>& vertexBindingDescription,
					   std::vector<VkVertexInputAttributeDescription>& vertexAttributeDescription,
					   std::vector<VkPushConstantRange>& pushConstantRanges,
					   VkCullModeFlags cullMode = VK_CULL_MODE_BACK_BIT,
					   VkCompareOp depthCompareOp = VK_COMPARE_OP_LESS);

	//initializer list constructor
	VertexFragPipeline(std::string name, 
					   const char* vertexShaderFile,
					   const char* fragShaderFile,
					   VulkanDevice& vulkanDevice,
					   SwapChain& swapChain,
					   Pass& renderPass,
					   std::vector<VkPipelineColorBlendAttachmentState>&& colorBlendAttachments,
					   std::vector<VkDescriptorSetLayout>&& descriptorLayouts,
					   std::vector<VkVertexInputBindingDescription>&& vertexBindingDescription,
					   std::vector<VkVertexInputAttributeDescription>&& vertexAttributeDescription,
					   std::vector<VkPushConstantRange>&& pushConstantRanges = {},
					   VkCullModeFlags cullMode = VK_CULL_MODE_BACK_BIT,
					   VkCompareOp depthCompareOp = VK_COMPARE_OP_LESS);

	//initializer list constructor (with descriptor layouts has exception)
	VertexFragPipeline(std::string name, 
					   const char* vertexShaderFile,
					   const char* fragShaderFile,
				       VulkanDevice& vulkanDevice,
					   SwapChain& swapChain,
					   Pass& renderPass,
					   std::vector<VkPipelineColorBlendAttachmentState>&& colorBlendAttachments,
					   std::vector<VkDescriptorSetLayout>& descriptorLayouts,
					   std::vector<VkVertexInputBindingDescription>&& vertexBindingDescription,
					   std::vector<VkVertexInputAttributeDescription>&& vertexAttributeDescription,
					   std::vector<VkPushConstantRange>&& pushConstantRanges = {},
					   VkCullModeFlags cullMode = VK_CULL_MODE_BACK_BIT,
					   VkCompareOp depthCompareOp = VK_COMPARE_OP_LESS);

	void cleanup();
	VkPipelineLayout& getPipelineLayout();
	VkPipeline& getPipeline();

private:
	void createPipeline(std::vector<VkPipelineColorBlendAttachmentState>& colorBlendAttachments,
		std::vector<VkDescriptorSetLayout>& descriptorLayouts,
		std::vector<VkVertexInputBindingDescription>& vertexBindingDescription,
		std::vector<VkVertexInputAttributeDescription>& vertexAttributeDescription,
		std::vector<VkPushConstantRange>& pushConstantRanges,
		VkCullModeFlags cullMode,
		VkCompareOp depthCompareOp);

};

