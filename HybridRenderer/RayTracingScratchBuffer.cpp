#include "RayTracingScratchBuffer.h"

RayTracingScratchBuffer::RayTracingScratchBuffer()
{
}

//https://github.com/SaschaWillems/Vulkan/blob/3cf8fb81e3edf7dc49019632aabe9165b55e5ce9/base/VulkanRaytracingSample.cpp
RayTracingScratchBuffer::RayTracingScratchBuffer(VkDeviceSize size,
												 VulkanDevice& device,
												 VulkanInstance& instance) :
																			size(size),
																			device(device),
																			instance(instance)
{
	// Buffer and memory
	VkBufferCreateInfo bufferCreateInfo{};
	bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferCreateInfo.size = size;
	bufferCreateInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
	VK_CHECK_RESULT(vkCreateBuffer(device.getLogicalDeviceHandle(), &bufferCreateInfo, nullptr, &buffer));
	VkMemoryRequirements memoryRequirements{};
	vkGetBufferMemoryRequirements(device.getLogicalDeviceHandle(), buffer, &memoryRequirements);
	VkMemoryAllocateFlagsInfo memoryAllocateFlagsInfo{};
	memoryAllocateFlagsInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
	memoryAllocateFlagsInfo.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR;
	VkMemoryAllocateInfo memoryAllocateInfo = {};
	memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memoryAllocateInfo.pNext = &memoryAllocateFlagsInfo;
	memoryAllocateInfo.allocationSize = memoryRequirements.size;
	memoryAllocateInfo.memoryTypeIndex = utils::findMemoryType(device.getPhysicalDeviceHandle(), memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	VK_CHECK_RESULT(vkAllocateMemory(device.getLogicalDeviceHandle(), &memoryAllocateInfo, nullptr, &memory));
	VK_CHECK_RESULT(vkBindBufferMemory(device.getLogicalDeviceHandle(), buffer, memory, 0));
	// Buffer device address
	VkBufferDeviceAddressInfoKHR bufferDeviceAddresInfo{};
	bufferDeviceAddresInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
	bufferDeviceAddresInfo.buffer = buffer;
	deviceAddress = utils::vkGetBufferDeviceAddressKHR(device.getLogicalDeviceHandle(), &bufferDeviceAddresInfo);
}

void RayTracingScratchBuffer::cleanup()
{
	vkFreeMemory(device.getLogicalDeviceHandle(), memory, nullptr);
	vkDestroyBuffer(device.getLogicalDeviceHandle(), buffer, nullptr);
}
