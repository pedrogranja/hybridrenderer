#pragma once
#include <vector>
#include "utils.hpp"

//the validation layers we want to use, the VK_LAYER_KHRONOS_validation includes all layers
const std::vector<const char*> VALIDATIONLAYERS = {
    "VK_LAYER_KHRONOS_validation"
};

//the device extensions we want to use
const std::vector<const char*> DEVICEEXTENSIONS = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME,
    VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME,
    VK_KHR_MAINTENANCE3_EXTENSION_NAME,
    VK_KHR_PIPELINE_LIBRARY_EXTENSION_NAME,
    VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME,
    VK_KHR_BUFFER_DEVICE_ADDRESS_EXTENSION_NAME,
    VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME
};

const VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_1_BIT;

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

const int WIDTH = 1980;
const int HEIGHT = 1080;

const int MAX_FRAMES_IN_FLIGHT = 2;