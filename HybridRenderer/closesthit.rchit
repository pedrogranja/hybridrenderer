#version 460
#extension GL_EXT_ray_tracing : require
#extension GL_EXT_nonuniform_qualifier : enable

#define PI 3.1415926535897932384626433832795

struct Material {
	int albedoTextureIndex;
	int normalTextureIndex;
	int aoTextureIndex;
	int emissionTextureIndex;
	int metalRoughnessTextureIndex;
};

struct SceneNodeData {
	mat4 model;
	mat4 invertedModel;
};

layout(location = 0) rayPayloadInEXT vec3 hitValue;
layout(location = 2) rayPayloadEXT bool shadowed;
hitAttributeEXT vec3 attribs;

layout(binding = 0, set = 0) uniform accelerationStructureEXT topLevelAS;
layout(binding = 3, set = 0) uniform UBO 
{
	mat4 viewInverse;
	mat4 projInverse;
	vec4 lightPos;
	int vertexSize;
	int frameIndex;
    int numSamples;
    float shadowConeAngle;
    float time;
	bool useHalton;
} ubo;
layout(binding = 4, set = 0) buffer Vertices { vec4 v[]; } vertices;
layout(binding = 5, set = 0) buffer Indices { uint i[]; } indices;
layout(binding = 6, set = 0) buffer IndicesOffsets { uint i[]; } indicesOffset;
layout(binding = 7, set = 0) readonly buffer Materials { Material materials[]; };
layout(binding = 8, set = 0) readonly buffer SceneNodesData { SceneNodeData sceneNodeData[]; };

struct Vertex
{
  vec3 pos;
  vec3 color;
  vec3 normal;
  vec2 uv;
 };

float Halton(int b, int i)
{
    float r = 0.0;
    float f = 1.0;
    while (i > 0) {
        f = f / float(b);
        r = r + f * float(i % b);
        i = int(floor(float(i) / float(b)));
    }
    return r;
}

Vertex unpack(uint index)
{
	// Unpack the vertices from the SSBO using the glTF vertex structure
	// The multiplier is the size of the vertex divided by four float components (=16 bytes)
	const int m = ubo.vertexSize / 16;

	vec4 d0 = vertices.v[m * index + 0];
	vec4 d1 = vertices.v[m * index + 1];
	vec4 d2 = vertices.v[m * index + 2];

	Vertex v;
	v.pos = d0.xyz;
	v.color = vec3(d0.w, d1.x, d1.y);
	v.normal = vec3(d1.z, d1.w, d2.x);

	return v;
}

vec3 uniformSampleCone(float thetaMax, float r1, float r2) {
	float cosThetaMax = cos(thetaMax * PI / 180.0);
	float cosTheta = 1 - r1 + r1 * cosThetaMax;
	float senTheta = sqrt(max(0.0,1.0 - cosTheta * cosTheta));
	float phi = r2 * PI * 2.0;
	
	return vec3(cos(phi) * senTheta, sin(phi) * senTheta, cosTheta);
}

// create coordinate frame with z vector pointing towards N
mat3 createCoordinateFrame(vec3 N) {	
	vec3 dx0 = vec3(0.0f,N.z,-N.y);
	vec3 dx1 = vec3(-N.z,0.0f,N.x);
	vec3 dx = normalize(dot(dx0,dx0) > dot(dx1,dx1) ? dx0 : dx1);
	vec3 dy = normalize(cross(N,dx));
	return mat3(dx,dy,N);
}

float rand(float co) { return fract(sin(co*(91.3458)) * 47453.5453); }
float rand(vec2 co){ return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453); }

void main()
{
	ivec3 index = ivec3(indices.i[3 * gl_PrimitiveID], indices.i[3 * gl_PrimitiveID + 1], indices.i[3 * gl_PrimitiveID + 2]);
	uvec3 size = gl_LaunchSizeEXT;
	uvec3 launchIndex = gl_LaunchIDEXT;
	int pixelIndex = int(launchIndex.z * size.y * size.x + launchIndex.y * size.x + launchIndex.x);

	Vertex v0 = unpack(index.x);
	Vertex v1 = unpack(index.y);
	Vertex v2 = unpack(index.z);

	const vec3 barycentricCoords = vec3(1.0f - attribs.x - attribs.y, attribs.x, attribs.y);
	vec3 normal = normalize(v0.normal * barycentricCoords.x + v1.normal * barycentricCoords.y + v2.normal * barycentricCoords.z);

	hitValue = vec3(1.0,1.0,1.0);

	for(int i = 0; i < ubo.numSamples; i++) {
		// Shadow casting
		vec3 lightVector = normalize(-ubo.lightPos.xyz);
		float r1, r2 = 0.0;
		if(ubo.useHalton) {
			//r1 = Halton(2, pixelIndex * ubo.numSamples + pixelIndex + i + int((gl_WorldRayOriginEXT.x + gl_WorldRayOriginEXT.y + gl_WorldRayOriginEXT.z) * 10000.0));
			//r2 = Halton(31, pixelIndex * ubo.numSamples + pixelIndex + i + int((gl_WorldRayOriginEXT.x + gl_WorldRayOriginEXT.y + gl_WorldRayOriginEXT.z) * 10000.0) + 10000);

			//r1 = Halton(2, pixelIndex * 20 + ubo.frameIndex  + i);
			//r2 = Halton(3, pixelIndex * 20 + ubo.frameIndex + i + 10000);

			int haltonIndex = int(Halton(8, pixelIndex * 20 + i) * 100000) + int(Halton(6, ubo.frameIndex) * 123212);
			r1 = Halton(2, haltonIndex);
			r2 = Halton(3, haltonIndex);
		}
		else {
			r1 = rand(float(pixelIndex) * 10.0 + ubo.frameIndex + float(i));
			r2 = rand(float(pixelIndex) * 10.0 + ubo.frameIndex + float(i) + 13.2f); // random offset
		}
		vec3 lightDir = createCoordinateFrame(lightVector) * uniformSampleCone(ubo.shadowConeAngle, r1, r2);
		float tmin = 0.001;
		float tmax = 10000.0;
		vec3 origin = gl_WorldRayOriginEXT + gl_WorldRayDirectionEXT * gl_HitTEXT;
		shadowed = true;
		// Trace shadow ray and offset indices to match shadow hit/miss shader group indices
		traceRayEXT(topLevelAS, gl_RayFlagsTerminateOnFirstHitEXT | gl_RayFlagsOpaqueEXT | gl_RayFlagsSkipClosestHitShaderEXT, 0xFF, 1, 0, 1, origin, tmin, lightDir, tmax, 2);
		if (shadowed) {
			hitValue -= vec3(1.0/ubo.numSamples, 1.0/ubo.numSamples, 1.0/ubo.numSamples);
		}
	}
}