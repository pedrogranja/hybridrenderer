#version 450

#define PI 3.1415926535897932384626433832795

layout (binding = 0) uniform UBO 
{
	mat4 view;
	mat4 projection;
	float deltaPhi;
	float deltaTheta;
} ubo;

layout (binding = 1) uniform samplerCube samplerEnv;

layout (location = 0) in vec3 inUVW;
layout (location = 0) out vec4 outColor;

void main()
{	
	vec3 N = normalize(inUVW);
	vec3 up = vec3(0.0, 1.0, 0.0);
	vec3 right = normalize(cross(up, N));
	up = cross(N, right);

	const float TWO_PI = PI * 2.0;
	const float HALF_PI = PI * 0.5;

	uint sampleCount = 0u;
	vec3 irradiance = vec3(0.0);  
	float sampleDelta = 0.025;
	uint maxSamples = uint(TWO_PI / ubo.deltaPhi + HALF_PI / ubo.deltaTheta);
	for (float phi = 0.0; phi < TWO_PI; phi += ubo.deltaPhi) {
		for (float theta = 0.0; theta < HALF_PI; theta += ubo.deltaTheta) {
			// spherical to cartesian (in tangent space)
			vec3 tangentSample = vec3(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
			// tangent space to world
			vec3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * N;
			irradiance += texture(samplerEnv, sampleVec).rgb * cos(theta) * sin(theta);
			sampleCount++;
		}
	}

	outColor = vec4(PI * (irradiance / float(sampleCount)),1.0);
}