#version 450

#define PI 3.1415926535897932384626433832795

float SIGMA_N = 128.0f;
float SIGMA_D = 1.0f;
const float EPSILON = 0.00001;

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;
layout (location = 1) out vec4 outMoments;

layout(binding = 0) uniform Params {
	float alpha;
	float stillAlpha;
	float momentsAlpha;
	float momentsStillAlpha;
	bool useVelocityBuffer;
	bool useNormals;
	bool useMeshIds;
	bool useDepth;
	bool checkNeighborhood;
	bool estimateSpacially;
	bool useHistoryRectification;
	bool useClipping;
	bool isStill;
	float clipControl;
	float shadowBoxSigma;
	int width;
	int height;
} params;

layout(binding = 1) uniform sampler2D currImage;
layout(binding = 2) uniform sampler2D currNormals;
layout(binding = 3) uniform sampler2D currDepthImage;
layout(binding = 4) uniform sampler2D prevImage;
layout(binding = 5) uniform sampler2D prevNormals;
layout(binding = 6) uniform sampler2D prevDepthImage;
layout(binding = 7) uniform sampler2D prevMoments;
layout(binding = 8) uniform sampler2D velocityBuffer;

//sigma 3.0
const float h[4] = float[4](0.2146, 0.189, 0.13159, 0.0713);

bool checkNormals(vec3 prevNormals, vec3 currNormals) {
	return dot(prevNormals, currNormals) > cos(PI / 4);
}

bool checkIds(int prevId, int nextId) {
	return prevId == nextId;
}

bool checkDepth(float currDepth, float prevDepth, float maxDerivative) {
	return !(abs(prevDepth - currDepth) / (maxDerivative + 1e-2) > 30.0);
}

bool getPrevColor(int currId, 
				  vec3 currNormals, 
				  float currDepth, 
				  float currMaxDerivative, 
				  vec2 prevUv, 
				  inout vec3 res, 
				  inout vec4 colorMom) {

	vec2 texelSize =  1.0 / textureSize(prevImage, 0).xy;

	ivec2 offsets[4] = {
		//2x2 positions
		ivec2(0, 0),
		ivec2(1, 0),
		ivec2(0, 1),
		ivec2(1, 1)
	};

    vec2 prevPos = floor(inUV * vec2(params.width, params.height)) - texture(velocityBuffer, inUV).xy * vec2(params.width, params.height);
	ivec2 iprevPos = ivec2(prevPos);

	vec2 currPos = inUV * vec2(params.width, params.height) + vec2(0.5, 0.5);
	ivec2 icurrPos = ivec2(currPos);

	bool v[4];
	bool valid = false;
	
	
	res = vec3(0.0f);
	colorMom.xy = vec2(0.0f);

	//Bilinear Taps
	for(int i = 0; i < 4; i++) {
		ivec2 offset = offsets[i];
		ivec2 coords =  iprevPos + offset;

		if(coords.x < 0.0f || coords.y < 0.0f || coords.x > params.width || coords.y > params.height)
			continue;

		//check normals
		if(params.useNormals) {
			vec3 prevNormals = texelFetch(prevNormals, coords, 0).rgb;

			if(!checkNormals(prevNormals, currNormals))
				continue;
		}

		//check mesh ids
		if(params.useMeshIds) {
			int prevId = int(texelFetch(prevNormals,  coords, 0).w);

			//if normals are not consistent do not integrate temporally
			if(!checkIds(prevId, currId))
				continue;
		}

		//check depth
		if(params.useDepth) {
			float prevDepth = texelFetch(prevDepthImage, coords, 0).x;

			if(!checkDepth(currDepth, prevDepth, currMaxDerivative))
				continue;
		}

		v[i] = true;
		valid = true;
	}

	
	if(valid) {
		// bilinear weights
		float x = fract(prevPos.x);
		float y = fract(prevPos.y);
		const float w[4] = { (1 - x) * (1 - y),
								x  * (1 - y),
							(1 - x) *      y,
								x  *      y };

		float sumw = 0.0f;
		for(int i = 0; i < 4; i++) {	
			ivec2 offset = offsets[i];
			ivec2 uv =  iprevPos + offset;
			if (v[i])
			{
				res += w[i] * texelFetch(prevImage, uv, 0).rgb;
				colorMom.xy += w[i] * texelFetch(prevMoments, uv, 0).xy;
				sumw += w[i];
			}
		}

		if(sumw >= 0.01) {
			res   = res / sumw;
			colorMom.xy =  colorMom.xy / sumw;
			return true;
		}
	}
	else {
		int numValidSamples = 0;
		for(int i = -1; i <= 1; i++) {
			for(int j = -1; j <= 1; j++) {
				vec2 uv =  prevUv + vec2(float(i) * texelSize.x, float(j) * texelSize.y);

				if(uv.x < 0.0f || uv.y < 0.0f || uv.x > 1.0f || uv.y > 1.0f)
					continue;

				//check normals
				if(params.useNormals) {
					vec3 prevNormals = texture(prevNormals, uv).rgb;

					if(!checkNormals(prevNormals, currNormals))
						continue;
				}

				//check mesh ids
				if(params.useMeshIds) {
					int prevId = int(texture(prevNormals, uv).w);

					//if normals are not consistent do not integrate temporally
					if(!checkIds(prevId, currId))
						continue;
				}

				//check depth
				if(params.useDepth) {
					float prevDepth = texture(prevDepthImage, uv).x;

					if(!checkDepth(currDepth, prevDepth, currMaxDerivative))
						continue;
				}

				res += texture(prevImage, uv).rgb;
				colorMom.xy += texture(prevMoments, uv).xy;
				numValidSamples++;
			}
		}

		if(numValidSamples > 0) {
			res /= float(numValidSamples);
			colorMom.xy /= float(numValidSamples);
			return true;
		}
	}

	return false;
}



float normalWeight(vec3 originNormal, vec3 normal) {
	return pow(max(0.0f, dot(originNormal,normal)), SIGMA_N);
}

float depthWeight(float originDepth, vec2 uv) {
	vec3 depthDerivatives = texture(currDepthImage, uv).xyz;
	float depth = depthDerivatives.x;
	float depthDdx = depthDerivatives.y;
	float depthDdy = depthDerivatives.z;
	float numerator = abs(originDepth - depth);
	float denominator = SIGMA_D * length(dot(vec2(depthDdx, depthDdy),(inUV - uv))) + EPSILON;
	return exp(-numerator / denominator);
}

vec2 estimateShadowMoments() {
	vec2 texelSize = 1.0 / textureSize(currNormals,0).xy;
	float originDepth = texture(currDepthImage, inUV).x;
	vec3 originNormal = texture(currNormals, inUV).rgb;

	float cum_firstMoment = 0.0;
	float cum_secondMoment = 0.0;
	float cum_w = 0.0;
	int numCycles = 0;
	float wn, wd = 1.0;

	//visit around the center of the pixel
	for(int i = -3; i <= 3; i++) {
		for(int j = -3; j <= 3; j++) {
			vec2 uv = inUV + vec2(texelSize.x * i, texelSize.y * j);
			float depth = texture(currDepthImage, uv).x;
			vec3 normal = texture(currNormals, uv).rgb;
			float shadow = texture(currImage, uv).r;
			
			if(params.useNormals)
				wn = normalWeight(originNormal, normal);

			if(params.useDepth)
				wd = depthWeight(originDepth, uv);

			float weight = wd * wn * h[abs(i)] * h[abs(j)];
			float inv = (1.0 - shadow);

			cum_firstMoment += weight * inv;
			cum_secondMoment += weight * inv * inv;
			cum_w += weight;
			numCycles++;
		}
	}

	cum_firstMoment /= cum_w;
	cum_secondMoment /= cum_w;

	return vec2(cum_firstMoment, cum_secondMoment);
}

// https://github.com/playdeadgames/temporal/blob/master/Assets/Shaders/TemporalReprojection.shader
vec3 clipAABB(vec3 cmin, vec3 cmax, vec3 p, vec3 q) {
	vec3 r = q - p;
	vec3 rmax = cmax - p.xyz;
	vec3 rmin = cmin - p.xyz;

	if (r.x > rmax.x + EPSILON)
		r *= (rmax.x / r.x);
	if (r.y > rmax.y + EPSILON)
		r *= (rmax.y / r.y);
	if (r.z > rmax.z + EPSILON)
		r *= (rmax.z / r.z);

	if (r.x < rmin.x - EPSILON)
		r *= (rmin.x / r.x);
	if (r.y < rmin.y - EPSILON)
		r *= (rmin.y / r.y);
	if (r.z < rmin.z - EPSILON)
		r *= (rmin.z / r.z);

	return p + r;
}

vec3 clipPrevShadow(vec3 currShadow, vec3 prevShadow) {
	vec2 texelSize = 1.0 / textureSize(currImage,0).xy;

	//calculate color min (cmin) and color max (cmax) in pixel neighborhood
	vec3 shadowAvg = vec3(0.0);
	vec3 shadowVar = vec3(0.0);
	for(int i = -1; i <= 1; i++) {
		for(int j = -1; j <= 1; j++) {
			vec2 uv = inUV + vec2(texelSize.x * i, texelSize.y * j);
			vec3 shadow = texture(currImage, uv).xyz;
			shadowAvg += shadow;
			shadowVar += shadow * shadow;
		}
	}
	shadowAvg /= 9.0f;
	shadowVar /= 9.0f;
    vec3 sigma = sqrt(max(vec3(0.0f), shadowVar - shadowAvg * shadowAvg));
    vec3 shadowMin = shadowAvg - params.shadowBoxSigma * sigma;
    vec3 shadowMax = shadowAvg + params.shadowBoxSigma * sigma;   

	if(params.useClipping)
		return clipAABB(shadowMin, shadowMax, clamp(shadowAvg, shadowMin, shadowMax), prevShadow);
	else {
		return clamp(prevShadow, shadowMin, shadowMax);
	}
}

void main() 
{
	//calculate the alpha for the temporal accumulation
	float alpha = params.alpha;
	float momentsAlpha = params.momentsAlpha;
	if(params.isStill) {
		alpha = params.stillAlpha;
		momentsAlpha = params.momentsStillAlpha;
	}

	//calculate previous frame UVs
	vec2 prevUv = inUV;
	if(params.useVelocityBuffer) {
		prevUv -= texture(velocityBuffer, inUV).xy;
	}

	//get frame information	
	vec4 prevMom = vec4(texture(prevMoments, prevUv).rgb, 0.0f);
	vec3 prevShadow = texture(prevImage, prevUv).rgb;
	vec3 currShadow = texture(currImage, inUV).rgb;
	vec3 currNormal = texture(currNormals, inUV).rgb;	
	float currDepth = texture(currDepthImage, inUV, 0).x;
	float currMaxDerivative = max(texture(currDepthImage, inUV, 0).y, texture(currDepthImage, inUV, 0).z);

	//Is background, don't temporally accumulate
	if(abs(currNormal.x) < 0.01 && abs(currNormal.y) < 0.01 && abs(currNormal.z) < 0.01) {
		outFragColor = vec4(0.0f, 0.0f, 0.0f, 0.0f);
		outMoments =  vec4(0.0f, 0.0f, 0.0f, 0.0f);
		return;
	}

	//check reprojection validity
	if(params.useVelocityBuffer && params.checkNeighborhood) {
		int currId = int(texture(currNormals, inUV).w);
		if(!getPrevColor(currId, currNormal, currDepth, currMaxDerivative, prevUv, prevShadow, prevMom)) {
			alpha = 1.0;
		}
	}
	//normal prev color
	else {	
		//check if outside of image
		if(prevUv.x < 0.0f || prevUv.y < 0.0f || prevUv.x > 1.0f || prevUv.y > 1.0f)
			alpha = 1.0f;

		//check normals
		if(params.useNormals) {
			vec3 prevNormals = texture(prevNormals, prevUv).rgb;

			if(currNormal.x < 0.01 && currNormal.y < 0.01 && currNormal.z < 0.01) {
				alpha = 1.0;
			}

			//if normals are not consistent do not integrate temporally
			if(!checkNormals(prevNormals, currNormal))
				alpha = 1.0;
		}
		
		//check mesh id
		if(params.useMeshIds) {
			int prevId = int(texture(prevNormals, prevUv).w);
			int currId = int(texture(currNormals, inUV).w);

			//if normals are not consistent do not integrate temporally
			if(!checkIds(prevId, currId))
				alpha = 1.0;
		}

		//check depth
		if(params.useDepth) {
			float prevDepth = texture(prevDepthImage, prevUv).x;

			if(!checkDepth(currDepth, prevDepth, currMaxDerivative))
				alpha = 1.0;
		}
	}

	//if low temporal history...
	if(prevMom.z < 4 && params.estimateSpacially) {
		vec2 colMoments = estimateShadowMoments();
		outMoments =  vec4(colMoments, 0.0f, 0.0f);
	}
	else {
		outMoments =  params.momentsAlpha * vec4(currShadow.x, currShadow.x * currShadow.x, 0.0f, 0.0f) + (1.0 - params.momentsAlpha) * prevMom;
		//outMoments = mix(vec4(currLum, currLum * currLum, 0.0f, 0.0f), prevMom, params.momentsAlpha);
	}

	if(alpha >= 0.999f) {
		outMoments.z = 0.0f; //reset history
		outMoments.y = 3.0f; //if no temporal accumulation artificially increase variance
	}
	else
		outMoments.z = prevMom.z + 1; //increment history
				
	//remove possible nans from the moment estimation
	if(isnan(outMoments.x) || isnan(outMoments.y)) {
		outMoments.x = 0.0f;
		outMoments.y = 0.0f;
	}

	//make sure previous color matches current one
	if(params.useHistoryRectification)
		prevShadow = mix(prevShadow, clipPrevShadow(currShadow, prevShadow), params.clipControl);
	
	//calculate variance and blend previous frame information
	float var = max((outMoments.y - outMoments.x * outMoments.x), 0.0f);
	vec3 result = alpha * currShadow + (1.0 - alpha) * prevShadow;
	outFragColor = vec4(result, var);

}