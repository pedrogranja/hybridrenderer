#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_ray_tracing : require
#extension GL_EXT_nonuniform_qualifier : require

#include "constants.glsl"
#include "bsdf.glsl"
#include "raycommon.glsl"
#include "rand.glsl"

hitAttributeEXT vec3 attribs;

struct SceneNodeData {
	mat4 model;
	mat4 invertedModel;
};

struct Material {
	int albedoTextureIndex;
	int normalTextureIndex;
	int aoTextureIndex;
	int emissionTextureIndex;
	int metalRoughnessTextureIndex;
	vec3 baseColor;
	float metallicFactor;
	float roughnessFactor;
};

layout(binding = 0, set = 0) uniform accelerationStructureEXT topLevelAS;
layout(binding = 4, set = 0) uniform UBO {
	vec4 camPos;
	vec4 lightPos;
	vec4 lightColor;
	float lightIntensity;
	int vertexSize;
	int frameIndex;
    float time;
	float baseRoughness;
	float baseMetalness;
	float indirectStrength;
	float minBrdfSampleBias;
	float maxBrdfSampleBias;
	bool useBlueNoise;
	bool useShadows;
	float shadowConeAngle;
	bool useReinhardToneMapping;
	float lumaMultiplier;
	float normalsInterpolation;
	int maxPathLength;
	float skyboxIntensity;	
	bool accumulate;
	int numAccumulatedFrames;
	int maxNumberAccumulatedFrames;
	int width;
	int height;
	bool useSvgfDemodulation;
	bool useImplicitSamplingOfEnvironmentMap;
} ubo;

layout(binding = 5, set = 0) buffer Vertices { vec4 v[]; } vertices;
layout(binding = 6, set = 0) buffer Indices { uint i[]; } indices;
layout(binding = 7, set = 0) readonly buffer IndicesOffsets { uint indicesOffset[]; };
layout(binding = 8, set = 0) readonly buffer Materials { Material materials[]; };
layout(binding = 9, set = 0) readonly buffer SceneNodesData { SceneNodeData sceneNodeData[]; };
layout(binding = 10, set = 0) uniform sampler2D normalSampler;
layout(binding = 11, set = 0) uniform sampler2D normalsImageTextureless;
layout(binding = 12, set = 0) uniform sampler2D positionsSampler;
layout(binding = 13, set = 0) uniform sampler2D roughnessMetalSampler;
layout(binding = 14, set = 0) uniform sampler2D albedoSampler;
layout(binding = 15, set = 0) uniform sampler2D brdfLut;
layout(binding = 16, set = 0) uniform sampler2D albedoTextureSampler;
layout(binding = 17, set = 0) uniform sampler2D[64] blueNoise;
layout(binding = 18, set = 0) uniform sampler2D[] textures;
layout(binding = 20, set = 0) uniform samplerCube mipMapRoughnessCubemap;

struct Vertex
{
  vec3 pos;
  vec3 color;
  vec3 normal;
  vec2 uv;
  int materialId;
 };

Vertex unpack(uint index)
{
	// Unpack the vertices from the SSBO using the glTF vertex structure
	// The multiplier is the size of the vertex divided by four float components (=16 bytes)
	const int m = ubo.vertexSize / 16;

	vec4 d0 = vertices.v[m * index + 0];
	vec4 d1 = vertices.v[m * index + 1];
	vec4 d2 = vertices.v[m * index + 2];

	Vertex v;
	v.pos = d0.xyz;
	v.color = vec3(d0.w, d1.x, d1.y);
	v.normal = vec3(d1.z, d1.w, d2.x);
	v.uv = vec2(d2.y, d2.z);
	v.materialId = int(d2.w);

	return v;
}

vec3 uniformSampleCone(float thetaMax, float r1, float r2) {
	float cosThetaMax = cos(thetaMax * PI / 180.0);
	float cosTheta = 1 - r1 + r1 * cosThetaMax;
	float senTheta = sqrt(max(0.0,1.0 - cosTheta * cosTheta));
	float phi = r2 * PI * 2.0;
	
	return vec3(cos(phi) * senTheta, sin(phi) * senTheta, cosTheta);
}

// create coordinate frame with z vector pointing towards N
mat3 createCoordinateFrame(vec3 N) {	
	vec3 dx0 = vec3(0.0f,N.z,-N.y);
	vec3 dx1 = vec3(-N.z,0.0f,N.x);
	vec3 dx = normalize(dot(dx0,dx0) > dot(dx1,dx1) ? dx0 : dx1);
	vec3 dy = normalize(cross(N,dx));
	return mat3(dx,dy,N);
}

vec3 calculateDirectIllumination(vec3 w0, vec3 normal, vec3 wi, vec3 lightColor, float lightIntensity, float roughness, float metal, vec3 albedo) {
    vec3 radiance = lightColor * lightIntensity; //incoming light

	ShadingData sd = getShadingData(albedo, metal, roughness, w0, wi, normal);
	SpecularData specData = getSpecularData(sd);
	DiffuseData dd = getDiffuseData(sd, specData);

    vec3 res = (dd.diffuse * sd.wiDotN + specData.ggx) * radiance;

	return max(res, vec3(0.0f));
}

struct RayPayload {
	vec3 color;
	float dist;
	int pathLength;
};

layout(location = 0) rayPayloadInEXT RayPayload rayPayload;
layout(location = 1) rayPayloadEXT float prevRoughness;
layout(location = 2) rayPayloadEXT bool shadowed;

void main()
{
	//calculate the pixel coordinates and index
	uvec3 size = gl_LaunchSizeEXT;
	uvec3 launchIndex = gl_LaunchIDEXT;
	int pixelIndex = int(launchIndex.z * size.y * size.x + launchIndex.y * size.x + launchIndex.x);
	
	//pixel center and Uvs
	const vec2 pixelCenter = vec2(launchIndex.xy) + vec2(0.5);
	const vec2 inUV = pixelCenter/vec2(size.xy);

	//Find the triangle vertices
	uint indexOffset = indicesOffset[gl_InstanceCustomIndexEXT];
	ivec3 index = ivec3(indices.i[indexOffset + 3 * gl_PrimitiveID], indices.i[indexOffset + 3 * gl_PrimitiveID + 1], indices.i[indexOffset + 3 * gl_PrimitiveID + 2]);
	Vertex v0 = unpack(index.x);
	Vertex v1 = unpack(index.y);
	Vertex v2 = unpack(index.z);

	//Find the material associated to this triangle
	Material mat = materials[v0.materialId];

	//Calculate the UVs
	const vec3 barycentricCoords = vec3(1.0f - attribs.x - attribs.y, attribs.x, attribs.y);
	vec2 uv = v0.uv * barycentricCoords.x + v1.uv * barycentricCoords.y + v2.uv * barycentricCoords.z;

	//Compute the necessary information for shading (positions, normals, etc.)
	
	//Albedo
	vec3 albedo = mat.baseColor;
	if(mat.albedoTextureIndex > -1)
		albedo *= texture(textures[mat.albedoTextureIndex], uv).xyz;

	//Ambient Occlusion
	float ao = 1.0f;
	if(mat.aoTextureIndex > -1) {
		ao *= texture(textures[mat.aoTextureIndex], uv).r;
	}

	//Normal
	vec3 normal = vec3(0.0);
	normal = normalize(v0.normal * barycentricCoords.x + v1.normal * barycentricCoords.y + v2.normal * barycentricCoords.z);
	normal = normalize(mat3(transpose(sceneNodeData[gl_InstanceCustomIndexEXT].invertedModel)) * normal);
	if(mat.normalTextureIndex > -1) {
		mat3 TBN = createCoordinateFrame(normal);
	    vec3 tangentNormal = texture(textures[mat.normalTextureIndex], uv).rgb * 2.0 - 1.0;
		vec3 textureNormal = TBN * tangentNormal;

		if(!isnan(textureNormal.x) && !isnan(textureNormal.y) && !isnan(textureNormal.z))
			normal = normalize(textureNormal);
	}

	//Position
	vec3 pos = gl_WorldRayOriginEXT + gl_RayTmaxEXT * gl_WorldRayDirectionEXT + normal * 0.01;

	//Metal + Roughness
	float metal = ubo.baseMetalness * mat.metallicFactor;
	float roughness = ubo.baseRoughness * mat.roughnessFactor;

	if(mat.metalRoughnessTextureIndex > -1) {
		vec4 metalRoughness = texture(textures[mat.metalRoughnessTextureIndex],uv);
		metal *= metalRoughness.b;
		roughness *= metalRoughness.g;
    }

	vec2 xi = vec2(0.0f);

	//Trace Shadows
	vec3 lightDir = -normalize(ubo.lightPos.rgb);
	float shadow = 1.0f;
	shadowed = false;
	if(ubo.useShadows) {
		if(ubo.useBlueNoise) {
			xi = getBlueNoisePseudoRandom(0, inUV, ubo.width, ubo.height, ubo.frameIndex, ubo.time, blueNoise);
		}
		else {
			uint index = launchIndex.x * uint(1973) + launchIndex.y * uint(9277) + uint(ubo.frameIndex) * uint(146624);
			xi = vec2(blockCipherTEA(index, 3)) / 4294967296.0f;
		}

		vec3 dir = createCoordinateFrame(lightDir) * uniformSampleCone(ubo.shadowConeAngle, xi.x, xi.y);
		shadowed = true;
		
		// Trace shadow ray and offset indices to match shadow hit/miss shader group indices
		traceRayEXT(topLevelAS, gl_RayFlagsTerminateOnFirstHitEXT | gl_RayFlagsOpaqueEXT | gl_RayFlagsSkipClosestHitShaderEXT, 0xFF, 1, 0, 1, pos, TMIN, dir, TMAX, 2);
		if (shadowed) {
			shadow -= 1.0;
		}
	}

	//Shading
    vec3 lightColor = ubo.lightColor.rgb;
    float lightIntensity = ubo.lightIntensity;
    
    //lighting
	vec3 w0 = -normalize(gl_WorldRayDirectionEXT);
	vec3 wi = lightDir;

	//calculate direct illumination
	vec3 directIllumination = calculateDirectIllumination(w0, normal, wi, lightColor, lightIntensity, roughness, metal, albedo) * shadow;
	
	rayPayload.pathLength++;
	vec3 F = vec3(0.0f);	
	vec3 specularIllumination = vec3(0.0f, 0.0f, 0.0f);
	
	if(rayPayload.pathLength < ubo.maxPathLength) {
		//get a pseudo random number
		if(ubo.useBlueNoise) {
			xi = getBlueNoisePseudoRandom(0, inUV, ubo.width, ubo.height, ubo.frameIndex, ubo.time, blueNoise);
		}
		else {
			uint index = launchIndex.x * uint(1973) + launchIndex.y * uint(9277) + uint(ubo.frameIndex) * uint(14624);
			xi = vec2(blockCipherTEA(index, 2)) / 4294967296.0f;
		}

		//Bias sampling in order to produce less noise
		xi.y = clamp(xi.y, ubo.minBrdfSampleBias, ubo.maxBrdfSampleBias);
		
		//Generate Outgoing Ray Direction
		if(roughness < 0.01f) 
			wi = reflect(-w0, normal);
		else 
			wi = importanceSampleGGX(xi, roughness*roughness, normal, -w0);

		//calculate the shading for the ray to be traced
		ShadingData sd = getShadingData(albedo, metal, roughness, w0, wi, normal);
		SpecularData specData = getSpecularData(sd);
		F = specData.F;

		//Trace the ray
		RayPayload prevRaypayload = rayPayload;
		traceRayEXT(topLevelAS, gl_RayFlagsOpaqueEXT, 0xff, 0, 0, 0, pos, TMIN, wi, TMAX, 0);

		//calculate the specular illumination
		specularIllumination = rayPayload.color * specData.ggx / specData.pdf;
		rayPayload = prevRaypayload;
	}
	//sample the environment map
	else if(ubo.useImplicitSamplingOfEnvironmentMap) { 
		const float MAX_REFLECTION_LOD = 8.0;
		wi = reflect(-w0, normal);

		ShadingData sd = getShadingData(albedo, metal, roughness, w0, wi, normal);
		SpecularData specData = getSpecularData(sd);	
		F = specData.F;

		//Sample environment map
		float roughnessSample = roughness * MAX_REFLECTION_LOD;

		//See if Environment map is occluded
		if(ubo.useShadows) {
			shadowed = true;	
			// Trace shadow ray and offset indices to match shadow hit/miss shader group indices
			traceRayEXT(topLevelAS, gl_RayFlagsTerminateOnFirstHitEXT | gl_RayFlagsOpaqueEXT | gl_RayFlagsSkipClosestHitShaderEXT, 0xFF, 1, 0, 1, pos, TMIN, wi, TMAX, 2);
			if (!shadowed) {
				wi.y = -wi.y;
				specularIllumination = ubo.skyboxIntensity * textureLod(mipMapRoughnessCubemap, wi, roughnessSample).rgb * specData.ggx / specData.pdf;
			}
		}
		else {
				wi.y = -wi.y;
				specularIllumination = ubo.skyboxIntensity * textureLod(mipMapRoughnessCubemap, wi, roughnessSample).rgb * specData.ggx / specData.pdf;
		}
	}
	
	//Calculate intensity of indirect diffuse component
	vec3 indirectDiffuse = mix(albedo, vec3(0.0f), metal) * ubo.indirectStrength;

	rayPayload.color += max(directIllumination + (specularIllumination + indirectDiffuse) * ao, vec3(0.00001f));
	rayPayload.dist = gl_RayTmaxEXT;
}