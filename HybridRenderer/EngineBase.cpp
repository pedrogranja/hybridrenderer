#include "EngineBase.h"


void EngineBase::initImGui()
{
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    io.WantCaptureMouse = true;
    //io.ConfigFlags |= ImGuiConfigFlags_Nav
    
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
}


void EngineBase::initVulkan()
{
    window = Window("Hybrid renderer", WIDTH, HEIGHT);
    instance = VulkanInstance("Hybrid Renderer", window);
    device = VulkanDevice(instance);
    swapChain = SwapChain(window, instance, device);
    utils::loadRaytracingFunctions(device.getLogicalDeviceHandle());
    createCommandPool();
    generateBRDFLut();
    generateIrradianceCubemap();
    generateMipMaps();

    std::vector<std::string> scenePaths;
    //scenePaths.push_back("../Models/Spheres/spheres2.gltf");
    scenePaths.push_back("../Models/DistanceTest/distanceTest.gltf");
    //scenePaths.push_back("../Models/waffle.gltf");
    loadScene(scenePaths);
    defineRenderer();
    
    createDrawCommandBuffers();
    createSynchronizationObjects();
}

void EngineBase::updateSamples()
{
    VkSampleCountFlagBits sampleCountTmp = static_cast<VkSampleCountFlagBits>(imGuiUi.globalInputs.msaaSamples);
    if (sampleCountTmp != sampleCount) {
        sampleCount = sampleCountTmp;
        //store the previous values
        ImGuiUi::UiShaderInputs shaderInputs = imGuiUi.globalShaderInputs;
        ImGuiUi::UiInputs globalInputs = imGuiUi.globalInputs;
        recreateSwapChain();
        //atribute the old parameters
        imGuiUi.globalShaderInputs = shaderInputs;
        imGuiUi.globalInputs = globalInputs;
    }
}

void EngineBase::update(uint32_t imageIndex)
{
    skyBox.update(imageIndex);
    //scene.sceneNodes[0]->translation = glm::vec3(0.0f, glm::sin(Clock::getInstance()->time), 0.0);
    scene.update(imageIndex);

    //submit scene data
    sceneParams.maxLevelRoughnessMipMap = static_cast<float>(maxLevelRoughnessMipMap);
    memcpy(sceneParams.suns, scene.suns.data(), scene.suns.size() * sizeof(Sun));
    sceneParams.numSuns = static_cast<int>(scene.suns.size());
    sceneParams.cameraPos = camera.pos;
    sceneUniform.submitDataToMemory(&sceneParams);

    //blur data
    blurParamsHor.toSubmit.blurHorizontal = true;
    blurParamsVer = blurParamsHor;
    blurParamsVer.toSubmit.blurHorizontal = false;
    blurHorUniformBuffer.submitDataToMemory(&blurParamsHor.toSubmit, imageIndex);
    blurVerUniformBuffer.submitDataToMemory(&blurParamsVer.toSubmit, imageIndex);
}

void EngineBase::drawFrame()
{
    updateSamples();

    vkWaitForFences(device.getLogicalDeviceHandle(), 1, &inFlightFences[currentFrame], VK_TRUE, UINT64_MAX);

    uint32_t imageIndex;
    VkResult res = vkAcquireNextImageKHR(device.getLogicalDeviceHandle(), swapChain.getSwapChainHandle(), UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);

    //recreate swapchain
    if (res == VK_ERROR_OUT_OF_DATE_KHR) {
        recreateSwapChain();
        return;
    }
    else if (res != VK_SUCCESS && res != VK_SUBOPTIMAL_KHR) {
        throw std::runtime_error("failed to acquire swap chain image!");
    }

    Clock::getInstance()->update();
    camera.updateWidthAndHeight(static_cast<float>(swapChain.getSwapChainExtent().width), static_cast<float>(swapChain.getSwapChainExtent().height));
    camera.update(imageIndex);
    camera.ProcessKeyboard(&window.getKeyboard());
    camera.ProcessMouseMovement(&window.getMouse());
    window.getMouse().consumeMovement();
    update(imageIndex);

    if (imagesInFlight[imageIndex] != VK_NULL_HANDLE) {
        vkWaitForFences(device.getLogicalDeviceHandle(), 1, &imagesInFlight[imageIndex], VK_TRUE, UINT64_MAX);
    }
    imagesInFlight[imageIndex] = inFlightFences[currentFrame];

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkSemaphore waitSemaphores[] = { imageAvailableSemaphores[currentFrame] };
    VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;

    imGuiUi.draw(imageIndex);

    std::vector<VkCommandBuffer> commandBuffersToDraw = { commandBuffers[imageIndex] , imGuiUi.commandBuffers[imageIndex] };
    submitInfo.commandBufferCount = static_cast<uint32_t>(commandBuffersToDraw.size());
    submitInfo.pCommandBuffers = commandBuffersToDraw.data();

    VkSemaphore signalSemaphores[] = { renderFinishedSemaphores[currentFrame] };
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    VK_CHECK_RESULT(vkResetFences(device.getLogicalDeviceHandle(), 1, &inFlightFences[currentFrame]));

    VK_CHECK_RESULT(vkQueueSubmit(device.getQueues().graphicsQueue, 1, &submitInfo, inFlightFences[currentFrame]));

    VkPresentInfoKHR presentInfo{};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;

    VkSwapchainKHR swapChains[] = {  swapChain.getSwapChainHandle() };
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;

    presentInfo.pImageIndices = &imageIndex;
    
    res = vkQueuePresentKHR(device.getQueues().presentationQueue, &presentInfo);

    if (res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR) {
        recreateSwapChain();
    }
    else if (res != VK_SUCCESS) {
        throw std::runtime_error("failed to present swap chain image!");
    }

    currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;	
    frameIndex++;

    finishedDrawing();
}

void EngineBase::run()
{
    initImGui();
    initVulkan();
    mainLoop();
    cleanup();
}

void EngineBase::mainLoop()
{
    while (!glfwWindowShouldClose(window.getWindowHandle())) {
        glfwPollEvents();
        drawFrame();
    }

    vkDeviceWaitIdle(device.getLogicalDeviceHandle());
}

void EngineBase::cleanupSwapChain()
{
    pipeline.cleanup();
    skyboxPipeline.cleanup();
    postProcessingHorPipeline.cleanup();
    postProcessingVerPipeline.cleanup();
    postProcessingBlendBloomPipeline.cleanup();
    colorRenderTarget.cleanup();
    bloomRenderTarget.cleanup();
    renderPass.cleanup();
    blurHorRenderTarget.cleanup();
    blurHorRenderPass.cleanup();
    blurVerRenderTarget.cleanup();
    blurVerRenderPass.cleanup();
    blendBloomRenderPass.cleanup();
    imGuiRenderPass.cleanup();
    depthRenderTarget.cleanup();
    colorResolveImage.cleanup();
    swapChain.cleanup();
    imGuiUi.cleanup();
    sceneUniform.cleanup();
    sceneDescriptorSet.cleanup();
    blurHorUniformBuffer.cleanup();
    blurHorDescriptorSet.cleanup();
    blurVerUniformBuffer.cleanup();
    blurVerDescriptorSet.cleanup();
    BloomBlendDescriptorSet.cleanup();
    ImGui_ImplVulkan_Shutdown();
    ImGui_ImplGlfw_Shutdown();
}

void EngineBase::recreateSwapChain()
{
    int width = 0, height = 0;
    glfwGetFramebufferSize(window.getWindowHandle(), &width, &height);
    while (width == 0 || height == 0) {
        glfwGetFramebufferSize(window.getWindowHandle(), &width, &height);
        glfwWaitEvents();
    }

    vkDeviceWaitIdle(device.getLogicalDeviceHandle());

    cleanupSwapChain();

    swapChain = SwapChain(window, instance, device);
    defineRenderer();
    createDrawCommandBuffers();
}

void EngineBase::cleanup()
{
    cleanupSwapChain();

    //IMGUI
    ImGui::DestroyContext();

    deleteScene();
    irradiance.cleanup();
    mipMapCubemap.cleanup();

    //BRDF LUT
    brdfLutRenderTarget.cleanup();

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(device.getLogicalDeviceHandle(), renderFinishedSemaphores[i], nullptr);
        vkDestroySemaphore(device.getLogicalDeviceHandle(), imageAvailableSemaphores[i], nullptr);
        vkDestroyFence(device.getLogicalDeviceHandle(), inFlightFences[i], nullptr);
    }

    vkDestroyCommandPool(device.getLogicalDeviceHandle(), commandPool, nullptr);

    device.cleanup();
    instance.cleanup();
    window.cleanup();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// INITIALIZATION /////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

EngineBase::EngineBase()
{}


void EngineBase::generateBRDFLut()
{
    //init texture
    uint32_t dim = 512;

    VkExtent2D dims = { dim, dim };
    brdfLutRenderTarget = RenderTarget("Brdf Lut Render Target",
                                       VK_FORMAT_R16G16_SFLOAT,
                                       dims,
                                       2,
                                       VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                       static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                       device,
                                       instance, 
                                       swapChain,
                                       commandPool);

    //init renderpass
    std::vector<RenderTarget> resourcesColor = { brdfLutRenderTarget };
    std::vector<RenderTarget> resourcesDepth = {  };
    Pass brdfLutRenderpass("BRDF Lut", resourcesColor, resourcesDepth, false, swapChain, device);

    //descriptor layout
    VkDescriptorSetLayout descriptorLayout;
    VkDescriptorSetLayoutCreateInfo layoutInfo{};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device.getLogicalDeviceHandle(), &layoutInfo, nullptr, &descriptorLayout));

    std::vector<VkVertexInputBindingDescription> bindingDescription;
    std::vector<VkVertexInputAttributeDescription> attributeDescription;
    std::vector<VkDescriptorSetLayout> descriptorSetLayouts;
    descriptorSetLayouts.push_back(descriptorLayout);
    VkPipelineColorBlendAttachmentState colorBlendAttachment = initializers::pipelineColorBlendAttachmentState();
    std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachments = { colorBlendAttachment };
    std::vector<VkPushConstantRange> pushConstants = std::vector<VkPushConstantRange>();
    VertexFragPipeline brdfLutPipeline("BRDF Lut", 
                                       "genbrdflut.vert.spv",
                                       "genbrdflut.frag.spv",
                                       device,
                                       swapChain,
                                       brdfLutRenderpass,
                                       colorBlendAttachments,
                                       descriptorSetLayouts,
                                       bindingDescription,
                                       attributeDescription,
                                       pushConstants,
                                       VK_CULL_MODE_NONE
                                       );

    VkClearValue clearValues[1];
    clearValues[0].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };

    VkRenderPassBeginInfo renderPassBeginInfo{};
    renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassBeginInfo.renderPass = brdfLutRenderpass.getRenderPassHandle();
    renderPassBeginInfo.renderArea.extent.width = dim;
    renderPassBeginInfo.renderArea.extent.height = dim;
    renderPassBeginInfo.clearValueCount = 1;
    renderPassBeginInfo.pClearValues = clearValues;
    renderPassBeginInfo.framebuffer = brdfLutRenderpass.getFramebuffers()[0];

    VkCommandBuffer cmdBuf = utils::beginSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool);
       
        vkCmdBeginRenderPass(cmdBuf, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

        VkViewport viewport{};
        viewport.width = (float)dim;
        viewport.height = (float)dim;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        VkRect2D scissor{};
        scissor.extent.width = dim;
        scissor.extent.height = dim;

        vkCmdSetViewport(cmdBuf, 0, 1, &viewport);
        vkCmdSetScissor(cmdBuf, 0, 1, &scissor);
        vkCmdBindPipeline(cmdBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, brdfLutPipeline.getPipeline());
        vkCmdDraw(cmdBuf, 3, 1, 0, 0);

        vkCmdEndRenderPass(cmdBuf);
    
    utils::flushCommandBuffer(cmdBuf, commandPool, device.getQueues().graphicsQueue, device.getLogicalDeviceHandle());

    //cleanup
    vkDestroyDescriptorSetLayout(device.getLogicalDeviceHandle(), descriptorLayout, nullptr);
    brdfLutRenderpass.cleanup();
    brdfLutPipeline.cleanup();
}

void EngineBase::generateIrradianceCubemap()
{
    uint32_t dim = 64;
    VkFormat format = VK_FORMAT_R32G32B32A32_SFLOAT;
    const uint32_t numMips = static_cast<uint32_t>(floor(log2(dim))) + 1;

    //uniform
    struct IrradianceUniform : utils::Uniform {
        alignas(16) glm::mat4 view;
        alignas(16) glm::mat4 projection;
        alignas(4) float deltaPhi = (2.0f * glm::pi<float>()) / 180.0f;
        alignas(4) float deltaTheta = (0.5f * glm::pi<float>()) / 64.0f;
    } IrradianceUniform;

    //init Skybox we want to render
    GltfImporter gltfLoader;
    SkyBox skyboxTmp = SkyBox(instance, device, commandPool, device.getQueues().graphicsQueue, &swapChain);
    skyboxTmp.loadSkyBox("../Environments/papermill.ktx", &gltfLoader, sizeof(IrradianceUniform));
    skyboxTmp.submitSkyBoxData();
    skyboxTmp.createDescriptorLayout();
    skyboxTmp.createDescriptorPool();
    skyboxTmp.createDescriptorSets();

    //Init offscreen rendertarget
    VkExtent2D dims = { dim, dim };
    RenderTarget offscreenRenderTarget = RenderTarget("Irradiance Offscreen Render Target",
                                                      format,
                                                      dims,
                                                      2,
                                                      VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                                                      static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                                      device,
                                                      instance,
                                                      swapChain,
                                                      commandPool);

    //init result texture
    irradiance = TextureCubeMap(device,
                                commandPool,
                                format,
                                dim, 
                                dim,
                                4,
                                numMips);

    //init renderPass
    std::vector<RenderTarget> resourcesColor = { offscreenRenderTarget };
    std::vector<RenderTarget> resourcesDepth = {  };
    Pass irradianceRenderpass("Irradiance", resourcesColor, resourcesDepth, false, swapChain, device);

    //create the pipeline
    std::vector<VkDescriptorSetLayout> descriptorSetLayouts;
    descriptorSetLayouts.push_back(skyboxTmp.descriptorSet.getDescriptorLayout());
    std::vector<VkVertexInputBindingDescription> bindingDescription = utils::Vertex::getBindingDescription();
    std::vector<VkVertexInputAttributeDescription> attributeDescription = utils::Vertex::getAttributeDescriptions();
    VkPipelineColorBlendAttachmentState colorBlendAttachment = initializers::pipelineColorBlendAttachmentState();
    std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachments = { colorBlendAttachment };
    std::vector<VkPushConstantRange> pushConstants = std::vector<VkPushConstantRange>();
    VertexFragPipeline irradiancePipeline("Irradiance",
                                          "irradiance.vert.spv",
                                          "irradiance.frag.spv",
                                          device, 
                                          swapChain,
                                          irradianceRenderpass,
                                          colorBlendAttachments, 
                                          descriptorSetLayouts,
                                          bindingDescription, 
                                          attributeDescription,
                                          pushConstants,
                                          VK_CULL_MODE_NONE
                                        );

    //Start drawing
    VkClearValue clearValues[1];
    clearValues[0].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };

    VkRenderPassBeginInfo renderPassBeginInfo{};
    renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassBeginInfo.renderPass = irradianceRenderpass.getRenderPassHandle();
    renderPassBeginInfo.renderArea.extent.width = dim;
    renderPassBeginInfo.renderArea.extent.height = dim;
    renderPassBeginInfo.clearValueCount = 1;
    renderPassBeginInfo.pClearValues = clearValues;
    renderPassBeginInfo.framebuffer = irradianceRenderpass.getFramebuffers()[0];

    std::vector<glm::mat4> matrices = {
        glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
    };

    //transition target image to optimal write
    utils::transitionImageLayout(device.getLogicalDeviceHandle(), 
                                 commandPool, 
                                 device.getQueues().graphicsQueue, 
                                 irradiance.textureImage, 
                                 VK_IMAGE_LAYOUT_UNDEFINED, 
                                 VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 
                                 6, 
                                 numMips);

    VkViewport viewport{};
    viewport.width = (float)dim;
    viewport.height = (float)dim;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor{};
    scissor.extent.width = dim;
    scissor.extent.height = dim;

    for (uint32_t m = 0; m < numMips; m++) {
        for (uint32_t f = 0; f < 6; f++) {
            VkCommandBuffer cmdBuf = utils::beginSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool);

            viewport.width = static_cast<float>(dim * std::pow(0.5f, m));
            viewport.height = static_cast<float>(dim * std::pow(0.5f, m));
            vkCmdSetViewport(cmdBuf, 0, 1, &viewport);
            vkCmdSetScissor(cmdBuf, 0, 1, &scissor);

            vkCmdBeginRenderPass(cmdBuf, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

                IrradianceUniform.projection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 512.0f);
                IrradianceUniform.view = matrices[f];
                skyboxTmp.update(&IrradianceUniform);

                vkCmdBindPipeline(cmdBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, irradiancePipeline.getPipeline());

                skyboxTmp.draw(cmdBuf, irradiancePipeline.getPipelineLayout());

            vkCmdEndRenderPass(cmdBuf);

            utils::transitionImageLayout(cmdBuf, offscreenRenderTarget.getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

                VkImageCopy copyRegion{};

                copyRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                copyRegion.srcSubresource.baseArrayLayer = 0;
                copyRegion.srcSubresource.mipLevel = 0;
                copyRegion.srcSubresource.layerCount = 1;
                copyRegion.srcOffset = { 0, 0, 0 };

                copyRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                copyRegion.dstSubresource.baseArrayLayer = f;
                copyRegion.dstSubresource.mipLevel = m;
                copyRegion.dstSubresource.layerCount = 1;
                copyRegion.dstOffset = { 0, 0, 0 };

                copyRegion.extent.width = static_cast<uint32_t>(viewport.width);
                copyRegion.extent.height = static_cast<uint32_t>(viewport.height);
                copyRegion.extent.depth = 1;

                vkCmdCopyImage(
                               cmdBuf,
                               offscreenRenderTarget.getTextureImage().textureImages[0],
                               VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                               irradiance.textureImage,
                               VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                               1,
                               &copyRegion);

            utils::transitionImageLayout(cmdBuf, offscreenRenderTarget.getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

            utils::flushCommandBuffer(cmdBuf, commandPool, device.getQueues().graphicsQueue, device.getLogicalDeviceHandle());
        }
    }

    utils::transitionImageLayout(device.getLogicalDeviceHandle(), commandPool, device.getQueues().graphicsQueue, irradiance.textureImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 6, numMips);

    irradianceRenderpass.cleanup();
    irradiancePipeline.cleanup();
    skyboxTmp.cleanup();
    offscreenRenderTarget.cleanup();
}

void EngineBase::generateMipMaps()
{
    uint32_t dim = 512;
    VkFormat format = VK_FORMAT_R16G16B16A16_SFLOAT;
    const uint32_t numMips = static_cast<uint32_t>(floor(log2(dim))) + 1;

    //uniform
    struct MipMapUniform : utils::Uniform {
        alignas(16) glm::mat4 view;
        alignas(16) glm::mat4 projection;
        alignas(4) float roughness;
        alignas(4) uint32_t numSamples = 512u;
    } MipMapUniform;

    //init Skybox we want to render
    GltfImporter gltfLoader;
    SkyBox skyboxTmp = SkyBox(instance, device, commandPool, device.getQueues().graphicsQueue, &swapChain);
    skyboxTmp.loadSkyBox("../Environments/papermill.ktx", &gltfLoader, sizeof(MipMapUniform));
    skyboxTmp.submitSkyBoxData();
    skyboxTmp.createDescriptorLayout();
    skyboxTmp.createDescriptorPool();

    skyboxTmp.createDescriptorSets();
    //Init offscreen rendertarget
    VkExtent2D dims = { dim, dim };
    RenderTarget offscreenRenderTarget = RenderTarget("Mip Map Offscreen Render Target",
                                                      format,
                                                      dims,
                                                      4,
                                                      VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                                                      static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
                                                      device,
                                                      instance,
                                                      swapChain,
                                                      commandPool);

    //init result texture
    mipMapCubemap = TextureCubeMap(device,
                                   commandPool,
                                   format,
                                   dim,
                                   dim,
                                   4,
                                   numMips);

    //init renderPass
    std::vector<RenderTarget> resourcesColor = { offscreenRenderTarget };
    std::vector<RenderTarget> resourcesDepth = { };
    Pass mipMapRenderpass("Mip Map", resourcesColor, resourcesDepth, false, swapChain, device);

    std::vector<VkDescriptorSetLayout> descriptorSetLayouts;
    descriptorSetLayouts.push_back(skyboxTmp.descriptorSet.getDescriptorLayout());
    std::vector<VkVertexInputBindingDescription> bindingDescription = utils::Vertex::getBindingDescription();
    std::vector<VkVertexInputAttributeDescription> attributeDescription = utils::Vertex::getAttributeDescriptions();
    VkPipelineColorBlendAttachmentState colorBlendAttachment = initializers::pipelineColorBlendAttachmentState();
    std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachments = { colorBlendAttachment };
    std::vector<VkPushConstantRange> pushConstants = std::vector<VkPushConstantRange>();
    VertexFragPipeline mipMapPipeline("Mip Map", 
                                      "mipMapCubemap.vert.spv",
                                      "mipMapCubemap.frag.spv",
                                      device,
                                      swapChain,
                                      mipMapRenderpass,
                                      colorBlendAttachments,
                                      descriptorSetLayouts,
                                      bindingDescription,
                                      attributeDescription,
                                      pushConstants,
                                      VK_CULL_MODE_NONE
                                      );
    //Start drawing
    VkClearValue clearValues[1];
    clearValues[0].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };

    VkRenderPassBeginInfo renderPassBeginInfo{};
    renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassBeginInfo.renderPass = mipMapRenderpass.getRenderPassHandle();
    renderPassBeginInfo.renderArea.extent.width = dim;
    renderPassBeginInfo.renderArea.extent.height = dim;
    renderPassBeginInfo.clearValueCount = 1;
    renderPassBeginInfo.pClearValues = clearValues;
    renderPassBeginInfo.framebuffer = mipMapRenderpass.getFramebuffers()[0];

    std::vector<glm::mat4> matrices = {
        glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
        glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
    };

    //transition target image to optimal write
    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool, 
                                 device.getQueues().graphicsQueue, 
                                 mipMapCubemap.textureImage, 
                                 VK_IMAGE_LAYOUT_UNDEFINED, 
                                 VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 
                                 6, 
                                 numMips);

    VkViewport viewport{};
    viewport.width = (float)dim;
    viewport.height = (float)dim;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor{};
    scissor.extent.width = dim;
    scissor.extent.height = dim;

    for (uint32_t m = 0; m < numMips; m++) {
        for (uint32_t f = 0; f < 6; f++) {
            VkCommandBuffer cmdBuf = utils::beginSingleTimeCommands(device.getLogicalDeviceHandle(), commandPool);

            viewport.width = static_cast<float>(dim * std::pow(0.5f, m));
            viewport.height = static_cast<float>(dim * std::pow(0.5f, m));
            vkCmdSetViewport(cmdBuf, 0, 1, &viewport);
            vkCmdSetScissor(cmdBuf, 0, 1, &scissor);

            vkCmdBeginRenderPass(cmdBuf, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

            MipMapUniform.projection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 512.0f);
            MipMapUniform.view = matrices[f];
            MipMapUniform.roughness = (float)m / (float)(numMips - 1);
            skyboxTmp.update(&MipMapUniform);

            vkCmdBindPipeline(cmdBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, mipMapPipeline.getPipeline());

                skyboxTmp.draw(cmdBuf, mipMapPipeline.getPipelineLayout());

            vkCmdEndRenderPass(cmdBuf);

            utils::transitionImageLayout(cmdBuf, offscreenRenderTarget.getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

            VkImageCopy copyRegion{};

            copyRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            copyRegion.srcSubresource.baseArrayLayer = 0;
            copyRegion.srcSubresource.mipLevel = 0;
            copyRegion.srcSubresource.layerCount = 1;
            copyRegion.srcOffset = { 0, 0, 0 };

            copyRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            copyRegion.dstSubresource.baseArrayLayer = f;
            copyRegion.dstSubresource.mipLevel = m;
            copyRegion.dstSubresource.layerCount = 1;
            copyRegion.dstOffset = { 0, 0, 0 };

            copyRegion.extent.width = static_cast<uint32_t>(viewport.width);
            copyRegion.extent.height = static_cast<uint32_t>(viewport.height);
            copyRegion.extent.depth = 1;

            vkCmdCopyImage(
                        cmdBuf,
                        offscreenRenderTarget.getTextureImage().textureImages[0],
                        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                        mipMapCubemap.textureImage,
                        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                        1,
                        &copyRegion);

            utils::transitionImageLayout(cmdBuf, offscreenRenderTarget.getTextureImage().textureImages[0], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

            utils::flushCommandBuffer(cmdBuf, commandPool, device.getQueues().graphicsQueue, device.getLogicalDeviceHandle());
        }
    }

    utils::transitionImageLayout(device.getLogicalDeviceHandle(),
                                 commandPool,
                                 device.getQueues().graphicsQueue,
                                 mipMapCubemap.textureImage, 
                                 VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 
                                 6,
                                 numMips);

    maxLevelRoughnessMipMap = numMips;

    mipMapRenderpass.cleanup();
    mipMapPipeline.cleanup();
    skyboxTmp.cleanup();
    offscreenRenderTarget.cleanup();
}

void EngineBase::createCommandPool()
{
    utils::QueueFamilyIndices queueFamilyIndices = utils::findQueueFamilies(instance.getPhysicalDevice(), instance.getSurface());

    VkCommandPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();


    VK_CHECK_RESULT(vkCreateCommandPool(device.getLogicalDeviceHandle(), &poolInfo, nullptr, &commandPool));
}

void EngineBase::createDrawCommandBuffers()
{
    commandBuffers.resize(swapChain.getSize());

    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = (uint32_t)commandBuffers.size();

    VK_CHECK_RESULT(vkAllocateCommandBuffers(device.getLogicalDeviceHandle(), &allocInfo, commandBuffers.data()));

    for (size_t i = 0; i < commandBuffers.size(); i++) {
        VkCommandBufferBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

        VK_CHECK_RESULT(vkBeginCommandBuffer(commandBuffers[i], &beginInfo));

            VkRenderPassBeginInfo renderPassInfo{};
            renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassInfo.renderPass = renderPass.getRenderPassHandle();
            renderPassInfo.framebuffer = renderPass.getFramebuffers()[0];
            renderPassInfo.renderArea.offset = { 0, 0 };
            renderPassInfo.renderArea.extent =  swapChain.getSwapChainExtent();

            std::array<VkClearValue, 3> clearValues{};
            clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
            clearValues[1].color = { 0.0f, 0.0f, 0.0f, 1.0f };
            clearValues[2].depthStencil = { 1.0f, 0 };
            renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
            renderPassInfo.pClearValues = clearValues.data();

            VkViewport viewport{};
            viewport.width = (float) swapChain.getSwapChainExtent().width;
            viewport.height = (float) swapChain.getSwapChainExtent().height;
            viewport.minDepth = 0.0f;
            viewport.maxDepth = 1.0f;

            VkRect2D scissor{};
            scissor.extent.width =  swapChain.getSwapChainExtent().width;
            scissor.extent.height =  swapChain.getSwapChainExtent().height;

            vkCmdSetViewport(commandBuffers[i], 0, 1, &viewport);
            vkCmdSetScissor(commandBuffers[i], 0, 1, &scissor);

            vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);    
                
                vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, skyboxPipeline.getPipeline());

                this->skyBox.draw(commandBuffers[i], skyboxPipeline.getPipelineLayout(), static_cast<int>(i));
            
                vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.getPipeline());

                //bind the scene descriptor set
                vkCmdBindDescriptorSets(commandBuffers[i],
                                        VK_PIPELINE_BIND_POINT_GRAPHICS,
                                        this->pipeline.getPipelineLayout(),
                                        2,
                                        1,
                                        &sceneDescriptorSet.getDescriptorSetsHandle()[i],
                                        0,
                                        nullptr);

                //bind ui params
                vkCmdBindDescriptorSets(commandBuffers[i],
                                        VK_PIPELINE_BIND_POINT_GRAPHICS,
                                        pipeline.getPipelineLayout(),
                                        3,
                                        1,
                                        &imGuiUi.descriptorSets[i],
                                        0,
                                        nullptr);

                this->scene.drawScene(commandBuffers[i], pipeline.getPipelineLayout(), static_cast<int>(i));

            vkCmdEndRenderPass(commandBuffers[i]);

            clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
            clearValues[1].depthStencil = { 1.0f, 0 };
            clearValues[2].color = { 0.0f, 0.0f, 0.0f, 1.0f };

            //Post processing
            VkRenderPassBeginInfo renderPassInfoBlurHor{};
            renderPassInfoBlurHor.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassInfoBlurHor.renderPass = blurHorRenderPass.getRenderPassHandle();
            renderPassInfoBlurHor.framebuffer = blurHorRenderPass.getFramebuffers()[0];
            renderPassInfoBlurHor.renderArea.offset = { 0, 0 };
            renderPassInfoBlurHor.renderArea.extent =  swapChain.getSwapChainExtent();
            renderPassInfoBlurHor.clearValueCount = 2;
            renderPassInfoBlurHor.pClearValues = clearValues.data();

            VkRenderPassBeginInfo renderPassInfoBlurVer{};
            renderPassInfoBlurVer.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassInfoBlurVer.renderPass = blurVerRenderPass.getRenderPassHandle();
            renderPassInfoBlurVer.framebuffer = blurVerRenderPass.getFramebuffers()[0];
            renderPassInfoBlurVer.renderArea.offset = { 0, 0 };
            renderPassInfoBlurVer.renderArea.extent =  swapChain.getSwapChainExtent();
            renderPassInfoBlurVer.clearValueCount = 2;
            renderPassInfoBlurVer.pClearValues = clearValues.data();

            VkRenderPassBeginInfo renderPassInfoBlendBloom{};
            renderPassInfoBlendBloom.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassInfoBlendBloom.renderPass = blendBloomRenderPass.getRenderPassHandle();
            renderPassInfoBlendBloom.framebuffer = blendBloomRenderPass.getFramebuffers()[i];
            renderPassInfoBlendBloom.renderArea.offset = { 0, 0 };
            renderPassInfoBlendBloom.renderArea.extent =  swapChain.getSwapChainExtent();
            renderPassInfoBlendBloom.clearValueCount = 2;
            renderPassInfoBlendBloom.pClearValues = clearValues.data();

            vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoBlurHor, VK_SUBPASS_CONTENTS_INLINE);

                vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, postProcessingHorPipeline.getPipeline());
                vkCmdBindDescriptorSets(commandBuffers[i],
                                        VK_PIPELINE_BIND_POINT_GRAPHICS,
                                        postProcessingHorPipeline.getPipelineLayout(),
                                        0,
                                        1,
                                        &blurHorDescriptorSet.getDescriptorSetsHandle()[i],
                                        0,
                                        nullptr);
                vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);
                
            vkCmdEndRenderPass(commandBuffers[i]);

            vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoBlurVer, VK_SUBPASS_CONTENTS_INLINE);

                vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, postProcessingVerPipeline.getPipeline());
                vkCmdBindDescriptorSets(commandBuffers[i],
                                        VK_PIPELINE_BIND_POINT_GRAPHICS,
                                        postProcessingHorPipeline.getPipelineLayout(),
                                        0,
                                        1,
                                        &blurVerDescriptorSet.getDescriptorSetsHandle()[i],
                                        0,
                                        nullptr);
                vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

            vkCmdEndRenderPass(commandBuffers[i]);

            vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfoBlendBloom, VK_SUBPASS_CONTENTS_INLINE);

                vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, postProcessingBlendBloomPipeline.getPipeline());
                vkCmdBindDescriptorSets(commandBuffers[i],
                                        VK_PIPELINE_BIND_POINT_GRAPHICS,
                                        postProcessingBlendBloomPipeline.getPipelineLayout(),
                                        0,
                                        1,
                                        &BloomBlendDescriptorSet.getDescriptorSetsHandle()[i],
                                        0,
                                        nullptr);
                vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);

            vkCmdEndRenderPass(commandBuffers[i]);

        VK_CHECK_RESULT(vkEndCommandBuffer(commandBuffers[i]));
    }
}

void EngineBase::createSynchronizationObjects()
{
    imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
    inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
    imagesInFlight.resize( swapChain.getSize(), VK_NULL_HANDLE);

    VkSemaphoreCreateInfo semaphoreInfo{};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        VK_CHECK_RESULT(vkCreateSemaphore(device.getLogicalDeviceHandle(), &semaphoreInfo, nullptr, &imageAvailableSemaphores[i]));
        VK_CHECK_RESULT(vkCreateSemaphore(device.getLogicalDeviceHandle(), &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]));
        VK_CHECK_RESULT(vkCreateFence(device.getLogicalDeviceHandle(), &fenceInfo, nullptr, &inFlightFences[i]));
    }
}

void EngineBase::createUI()
{
    utils::QueueFamilyIndices queueFamilyIndices = utils::findQueueFamilies(instance.getPhysicalDevice(), instance.getSurface());
    imGuiUi = ImGuiUi(device, 
                      queueFamilyIndices, 
                      swapChain, 
                      imGuiRenderPass,
                      &scene,
                      { &blurParamsHor });
    imGuiUi.createDescriptorPool();
    imGuiUi.createCommandPool();
    imGuiUi.createCommandBuffers();
    imGuiUi.initUniformBuffers();
    imGuiUi.createDescriptorSetLayout();
    imGuiUi.createDescriptorSet();

    //TODO THIS HERE DOESNT MAKE A LOT OF SENSE
    ImGui_ImplVulkan_InitInfo init_info = {};
    init_info.Instance = instance.getInstanceHandle();
    init_info.PhysicalDevice = instance.getPhysicalDevice();
    init_info.Device = device.getLogicalDeviceHandle();
    init_info.QueueFamily = queueFamilyIndices.graphicsFamily.value();
    init_info.Queue = device.getQueues().graphicsQueue;
    init_info.PipelineCache = nullptr;
    init_info.DescriptorPool = imGuiUi.descriptorPool;
    init_info.Allocator = nullptr;
    init_info.MinImageCount = 2;
    init_info.ImageCount =  swapChain.getSize();
    init_info.CheckVkResultFn = [](VkResult err) {
                                    if (err == 0) return;
                                    printf("VkResult %d\n", err);
                                    if (err < 0)
                                        abort();
                                };
    ImGui_ImplVulkan_Init(&init_info, imGuiRenderPass.renderPass);
    ImGui_ImplGlfw_InitForVulkan(window.getWindowHandle(), true);

    imGuiUi.createFontImage();
}

void EngineBase::loadScene(std::vector<std::string> sceneNames)
{
    //models
    GltfImporter gltfLoader;
    scene = Scene(device,
                  instance,
                  commandPool,
                  device.getQueues().graphicsQueue,
                  &swapChain,
                  &camera,
                  maxLevelRoughnessMipMap, 
                  true);
    for (int i = 0; i < sceneNames.size(); i++) {
        scene.loadScene(sceneNames[i], &gltfLoader);
    }
    //scene.loadScene("../Models/spheres2.gltf", &gltfLoader);
    //scene.loadScene("../Models/BreakfastRoom/BreakfastRoom.gltf", &gltfLoader);
    //scene.loadScene("C:/Users/pag/Desktop/projetoTese/HybridRenderer/Models/gltfModel/Sponza/glTF/sponza.gltf", &gltfLoader);
    //scene.loadScene("../Models/gltfModel/DamagedHelmet/gltf/DamagedHelmet.gltf", &gltfLoader);
    //scene.sceneNodes[1]->scale = glm::vec3(0.5f, 0.5f, 0.5f);
    //scene.sceneNodes[1]->translation = glm::vec3(0.0f, 1.0f, 0.0f);

    
    //scene.loadScene("../Models/gltfModel/TwoSidedPlane/gltf/TwoSidedPlane.gltf", &gltfLoader);
    //scene.sceneNodes[0]->translation = glm::vec3(0.0f, 0.0f, 0.0f);
    //scene.sceneNodes[1]->scale = glm::vec3(8.0f, 8.0f, 8.0f);
    //scene.sceneNodes[1]->rotation = glm::vec3(glm::pi<float>() / 2.0, 0.0f,0.0f);
    //scene.sceneNodes[1]->translation = glm::vec3(0.0f, 0.0f, -8.0f);
    
    //scene.sceneNodes[1]->scale = glm::vec3(2.0f, 2.0f, 2.0f);

    scene.loadCubeMap("../Environments/papermill.ktx");

    scene.suns.push_back(Sun(glm::vec3(-0.94, -0.31, -0.14), glm::vec3(1.0f, 1.0f, 1.0f)));
    scene.ready(); // finish loading and defining scene

    //sky box
    skyBox = SkyBox(instance, device, commandPool, device.getQueues().graphicsQueue, &swapChain, &camera, &imGuiUi);
    skyBox.loadSkyBox("../Environments/papermill.ktx", &gltfLoader);
    skyBox.ready(); // finish loading and defining skybox

    camera = Camera(static_cast<float>(swapChain.getSwapChainExtent().width),
                    static_cast<float>(swapChain.getSwapChainExtent().height),
                    glm::vec3(3.4f, 4.13f, 9.74f),
                    glm::vec3(3.2f, 3.9f, 8.5f),
                    glm::vec3(0.0f, 1.0f, 0.0f), 
                    59.4898f
                    );
}

void EngineBase::deleteScene()
{
    scene.cleanup();
    skyBox.cleanup();

}

void EngineBase::defineRenderer()
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// CREATE RENDERTARGETS ////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    VkFormat depthFormat = utils::findSupportedFormat(
                                                      instance.getPhysicalDevice(),
                                                      { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
                                                      VK_IMAGE_TILING_OPTIMAL,
                                                      VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
                                                     );

    depthRenderTarget = RenderTarget("Depth Render Target",
                                     depthFormat,
                                     swapChain.getSwapChainExtent(),
                                     1,
                                     VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                                     static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT),
                                     device,
                                     instance,
                                     swapChain,
                                     commandPool);

    colorRenderTarget = RenderTarget("Color Render Target",
                                     VK_FORMAT_R16G16B16A16_SFLOAT,
                                     swapChain.getSwapChainExtent(),
                                     4,
                                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                     static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                     device,
                                     instance,
                                     swapChain,
                                     commandPool);

    bloomRenderTarget = RenderTarget("Bloom Render Target",
                                     VK_FORMAT_R16G16B16A16_SFLOAT,
                                     swapChain.getSwapChainExtent(),
                                     4,
                                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                     static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                     device,
                                     instance,
                                     swapChain,
                                     commandPool);


    blurHorRenderTarget = RenderTarget("Horizontal Blur Render Target",
                                       VK_FORMAT_R16G16B16A16_SFLOAT,
                                       swapChain.getSwapChainExtent(),
                                       4,
                                       VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                       static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                       device,
                                       instance,
                                       swapChain,
                                       commandPool);

    blurVerRenderTarget = RenderTarget("Vertical Blur Render Target",
                                       VK_FORMAT_R16G16B16A16_SFLOAT,
                                       swapChain.getSwapChainExtent(),
                                       4,
                                       VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                       static_cast<VkImageUsageFlagBits>(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT),
                                       device,
                                       instance,
                                       swapChain,
                                       commandPool);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////// CREATE RENDERPASSES /////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    renderPass = Pass("Renderpass", { colorRenderTarget, bloomRenderTarget }, { depthRenderTarget }, false, swapChain, device);
    blurHorRenderPass = Pass("Blur Hor", { blurHorRenderTarget }, { depthRenderTarget }, false, swapChain, device);
    blurVerRenderPass = Pass("Blur Ver", { blurVerRenderTarget }, { depthRenderTarget }, false, swapChain, device);
    blendBloomRenderPass = Pass("Blend Bloom", {}, { depthRenderTarget }, true, swapChain, device);
    imGuiRenderPass = ImGuiRenderpass(device.getLogicalDeviceHandle(), swapChain);
    imGuiRenderPass.createRenderPasses(VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
    imGuiRenderPass.createFramebuffers();

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////// CREATE UI //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    createUI();

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////// CREATE DESCRIPTOR SETS AND LAYOUTS ///////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    utils::QueueFamilyIndices queueFamilyIndices = utils::findQueueFamilies(instance.getPhysicalDevice(), instance.getSurface());

    //scene
    sceneUniform = UniformBuffer("Scene", device, sizeof(SceneParams));
    sceneDescriptorSet = DescriptorSet("Scene Descriptor Set",
                                       { {0,&sceneUniform} },
                                       { {1,&brdfLutRenderTarget.getTextureImage()} },
                                       { {2,{scene.textureCubeMap}},  {3,{irradiance}}, {4,{mipMapCubemap}} }, 
                                       {},
                                       VK_SHADER_STAGE_FRAGMENT_BIT,
                                       device,
                                       instance,
                                       commandPool,
                                       this->swapChain.getSize());

    //scene descriptor layout
    std::vector<VkDescriptorSetLayout> sceneDescriptorLayouts = scene.descriptorLayouts;
    sceneDescriptorLayouts.push_back(sceneDescriptorSet.getDescriptorLayout());
    sceneDescriptorLayouts.push_back(imGuiUi.descriptorLayout);

    //skybox descriptor layout
    std::vector<VkDescriptorSetLayout> skyboxDescriptorLayout = std::vector<VkDescriptorSetLayout>();
    skyboxDescriptorLayout.push_back(skyBox.descriptorSet.getDescriptorLayout());

    //Horizontal blur
    blurHorUniformBuffer = UniformBuffer("Blur Hor", device, sizeof(BlurParams::ToSubmit), swapChain.getSize() /* 1 buffer per swapChain */);
    blurHorDescriptorSet = DescriptorSet("Horizontal Blur Descriptor Set",
                                         { {0, &blurHorUniformBuffer} },
                                         { {1, &bloomRenderTarget.getTextureImage()} },
                                         {}, 
                                         {},
                                         VK_SHADER_STAGE_FRAGMENT_BIT,
                                         device,
                                         instance,
                                         commandPool,
                                         swapChain.getSize() // 1 buffer per swapChain
                                        );

    //blur vertical descriptor set
    blurVerUniformBuffer = UniformBuffer("Blur Ver", device, sizeof(BlurParams::ToSubmit), swapChain.getSize() /* 1 buffer per swapChain */ );
    blurVerDescriptorSet = DescriptorSet("Vertical Blur Descriptor Set", 
                                         { {0, &blurVerUniformBuffer } },
                                         { {1, &blurHorRenderTarget.getTextureImage()} },
                                         {}, 
                                         {},
                                         VK_SHADER_STAGE_FRAGMENT_BIT,
                                         device,
                                         instance,
                                         commandPool,
                                         swapChain.getSize() // 1 buffer per swapChain
                                        );

    //blend bloom descriptor set
    BloomBlendDescriptorSet = DescriptorSet("Bloom Blend Descriptor Set", 
                                            {},
                                            { {0, &colorRenderTarget.getTextureImage()}, {1, &blurVerRenderTarget.getTextureImage()} },
                                            {}, 
                                            {},
                                            VK_SHADER_STAGE_FRAGMENT_BIT,
                                            device,
                                            instance,
                                            commandPool,
                                            swapChain.getSize() // 1 buffer per swapChains
                                            );

    //blur descriptor layout
    std::vector<VkDescriptorSetLayout> postProcessingDescriptorLayouts(swapChain.getSize(), blurHorDescriptorSet.getDescriptorLayout());

    //bloom blend descriptor layout
    std::vector<VkDescriptorSetLayout> postProcessingBlendDescriptorLayouts(swapChain.getSize(), BloomBlendDescriptorSet.getDescriptorLayout());

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////// CREATE PIPELINES //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //definition for a color attachment
    VkPipelineColorBlendAttachmentState colorBlendAttachment = initializers::pipelineColorBlendAttachmentState();

    pipeline = VertexFragPipeline("Pipeline",
                                  "triangle.vert.spv",
                                  "triangle.frag.spv",
                                  device,
                                  swapChain,
                                  renderPass,
                                  { colorBlendAttachment, colorBlendAttachment },
                                  sceneDescriptorLayouts,
                                  utils::Vertex::getBindingDescription(),
                                  utils::Vertex::getAttributeDescriptions(),
                                  {},
                                  VK_CULL_MODE_BACK_BIT);

    skyboxPipeline = VertexFragPipeline("Skybox", 
                                        "skybox.vert.spv",
                                        "skybox.frag.spv",
                                        device,
                                        swapChain,
                                        renderPass,
                                        { colorBlendAttachment },
                                        skyboxDescriptorLayout,
                                        utils::Vertex::getBindingDescription(),
                                        utils::Vertex::getAttributeDescriptions(),
                                        {},
                                        VK_CULL_MODE_FRONT_BIT,
                                        VK_COMPARE_OP_LESS_OR_EQUAL);

    postProcessingHorPipeline = VertexFragPipeline("Post Processing Hor", 
                                                   "blur.vert.spv",
                                                   "blur.frag.spv",
                                                   device,
                                                   swapChain,
                                                   blurHorRenderPass,
                                                   { colorBlendAttachment },
                                                   postProcessingDescriptorLayouts,
                                                   std::vector<VkVertexInputBindingDescription>(),
                                                   std::vector<VkVertexInputAttributeDescription>(),
                                                   {}, 
                                                   VK_CULL_MODE_NONE);

    postProcessingVerPipeline = VertexFragPipeline("Post Processing Ver", 
                                                   "blur.vert.spv",
                                                   "blur.frag.spv",
                                                   device,
                                                   swapChain,
                                                   blurVerRenderPass,
                                                   { colorBlendAttachment },
                                                   postProcessingDescriptorLayouts,
                                                   std::vector<VkVertexInputBindingDescription>(),
                                                   std::vector<VkVertexInputAttributeDescription>(),
                                                   {}, 
                                                   VK_CULL_MODE_NONE);

    postProcessingBlendBloomPipeline = VertexFragPipeline("Post Processing Blend Bloom", 
                                                          "blur.vert.spv",
                                                          "blendBloom.frag.spv",
                                                          device,
                                                          swapChain,
                                                          blendBloomRenderPass,
                                                          { colorBlendAttachment },
                                                          postProcessingBlendDescriptorLayouts,
                                                          std::vector<VkVertexInputBindingDescription>(),
                                                          std::vector<VkVertexInputAttributeDescription>(),
                                                          {}, 
                                                          VK_CULL_MODE_NONE);
}