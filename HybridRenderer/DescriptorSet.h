#pragma once
#include "vulkan/vulkan.h"
#include "UniformBuffer.h"
#include "TextureImage.h"
#include "TextureCubeMap.h"
#include <vector>
#include <unordered_map>
#include "SwapChain.h"
#include "FrameGraph.h"
#include <algorithm>
#include "VulkanInstance.h"

class DescriptorSet
{
public:
	struct UniformBufferResource {
		int binding;
		UniformBuffer* uniformBuffer = nullptr;
	};

	struct TextureResource {
		int binding;
		TextureImage* texImage = nullptr;
		VkImageLayout imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	};

	struct TextureCubeResource {
		int binding;
		std::vector<TextureCubeMap> texImageVector;
		VkImageLayout imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	};

	struct TextureArrayResource {
		int binding;
		std::vector<TextureImage*> texImages;
		VkImageLayout imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	};

private:
	VulkanDevice device;
	VulkanInstance instance;
	VkCommandPool commandPool;
	int numDescriptors;

	// int: descriptor binding
	std::vector<UniformBufferResource> uniformBufferResources;
	std::vector<TextureResource> textureResources;
	std::vector<TextureCubeResource> texCubeMapResources;
	std::vector<TextureArrayResource> texArrayResources;
	VkShaderStageFlagBits shaderStage;

	VkDescriptorPool descriptorPool;
	VkDescriptorSetLayout descriptorSetLayout;
	std::vector<VkDescriptorSet> descriptorSetsHandle;
	std::string name;

public:
	DescriptorSet();
	// && for initializer lists

	DescriptorSet(std::string name, 
				  std::vector<UniformBufferResource>&& uniformBufferResources,
				  std::vector<TextureResource>&& textureResources,
				  std::vector<TextureCubeResource>&& texCubeMapResources,
				  std::vector<TextureArrayResource>&& texArrayResources,
				  VkShaderStageFlagBits shaderStage,
				  VulkanDevice& device,
				  VulkanInstance& instance,
				  VkCommandPool commandPool,
				  int numDescriptors);

	DescriptorSet(std::string name,
				  std::vector<UniformBufferResource>& uniformBufferResources,
				  std::vector<TextureResource>& textureResources,
				  std::vector<TextureCubeResource>& texCubeMapResources,
				  std::vector<TextureArrayResource>& texArrayResources,
				  VkShaderStageFlagBits shaderStage,
				  VulkanDevice& device,
				  VulkanInstance& instance,
				  VkCommandPool commandPool,
				  int numDescriptors);

	void cleanup();
	std::vector<VkDescriptorSet>& getDescriptorSetsHandle();
	VkDescriptorSetLayout& getDescriptorLayout();

private:
	void createDescriptorPool();
	void createDescriptorSetLayout();
	void createDescriptorSet();
};

