#version 450
#extension GL_EXT_nonuniform_qualifier : require

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outFragColor;

layout(binding = 0) uniform Params {
	bool debugAlpha;
	bool debugLuminanceVarianceInitial;
	bool debugLuminanceVarianceFinal;
	bool debugTemporalHistory;
	bool spatialVarianceEstimation;
	int debugAtrousPasses;
	int debugAtrousPassesWeights;
} params;

layout(binding = 1) uniform sampler2D currentImage;
layout(binding = 2) uniform sampler2D alphaDebugImage;
layout(binding = 3) uniform sampler2D luminanceVarianceImage;

layout(binding = 4) uniform sampler2D atrousPassImage1;
layout(binding = 5) uniform sampler2D atrousPassImage2;
layout(binding = 6) uniform sampler2D atrousPassImage3;
layout(binding = 7) uniform sampler2D atrousPassImage4;
layout(binding = 8) uniform sampler2D atrousPassImage5;

layout(binding = 9) uniform sampler2D atrousPassImageWeights1;
layout(binding = 10) uniform sampler2D atrousPassImageWeights2;
layout(binding = 11) uniform sampler2D atrousPassImageWeights3;
layout(binding = 12) uniform sampler2D atrousPassImageWeights4;
layout(binding = 13) uniform sampler2D atrousPassImageWeights5;

void main() 
{
	if(params.debugAlpha) {
		float alpha = texture(alphaDebugImage, inUV).x;
		outFragColor = vec4(alpha,0.0f,0.0f,1.0f);
	}
	else if(params.debugLuminanceVarianceInitial) {
		vec2 colMoments = texture(luminanceVarianceImage, inUV).xy;
		outFragColor = vec4(colMoments.y - colMoments.x * colMoments.x, 0.0f, 0.0f, 1.0f);
	}
	else if(params.debugLuminanceVarianceFinal) {
		outFragColor = vec4(texture(currentImage, inUV).w, 0.0f, 0.0f, 1.0f);
	}
	else if(params.debugTemporalHistory) {
		float temporalHistory = texture(luminanceVarianceImage, inUV).z;
		outFragColor = vec4(temporalHistory/4.0f,temporalHistory/4.0f,temporalHistory/4.0f, 1.0f);
	}
	else if(params.spatialVarianceEstimation) {
		float spatialVar = texture(alphaDebugImage, inUV).y;
		outFragColor = vec4(spatialVar,0.0f,0.0f,1.0f);
	}
	else if(params.debugAtrousPasses > 0) {
		if(params.debugAtrousPasses == 1)
			outFragColor = vec4(texture(atrousPassImage1, inUV).xyz, 1.0f);
		else if(params.debugAtrousPasses == 2)
			outFragColor = vec4(texture(atrousPassImage2, inUV).xyz, 1.0f);
		else if(params.debugAtrousPasses == 3)
			outFragColor = vec4(texture(atrousPassImage3, inUV).xyz, 1.0f);
		else if(params.debugAtrousPasses == 4)
			outFragColor = vec4(texture(atrousPassImage4, inUV).xyz, 1.0f);
		else if(params.debugAtrousPasses == 5)
			outFragColor = vec4(texture(atrousPassImage5, inUV).xyz, 1.0f);
	}
	else if(params.debugAtrousPassesWeights > 0) {
		if(params.debugAtrousPassesWeights == 1)
			outFragColor = vec4(texture(atrousPassImageWeights1, inUV).xyz, 1.0f);
		else if(params.debugAtrousPassesWeights == 2)
			outFragColor = vec4(texture(atrousPassImageWeights2, inUV).xyz, 1.0f);
		else if(params.debugAtrousPassesWeights == 3)
			outFragColor = vec4(texture(atrousPassImageWeights3, inUV).xyz, 1.0f);
		else if(params.debugAtrousPassesWeights == 4)
			outFragColor = vec4(texture(atrousPassImageWeights4, inUV).xyz, 1.0f);
		else if(params.debugAtrousPassesWeights == 5)
			outFragColor = vec4(texture(atrousPassImageWeights5, inUV).xyz, 1.0f);
	}
	else {
		vec3 result = texture(currentImage, inUV).rgb;
		outFragColor = vec4(result, 1.0f);
	}
}