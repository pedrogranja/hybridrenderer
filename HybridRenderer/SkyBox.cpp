#include "SkyBox.h"

SkyBox::SkyBox()
{
}

SkyBox::SkyBox(VulkanInstance instance,
				VulkanDevice device,
				VkCommandPool commandPool,
				VkQueue graphicsQueue,
				SwapChain* swapChain,
				Camera* camera,
				ImGuiUi* imGuiUi) : instance(instance),
									device(device),
									commandPool(commandPool),
									graphicsQueue(graphicsQueue),
									swapChain(swapChain),
									camera(camera),
									imGuiUi(imGuiUi)
{
	this->updatable = true;
}

SkyBox::SkyBox(VulkanInstance instance,
				VulkanDevice device,
				VkCommandPool commandPool,
				VkQueue graphicsQueue,
				SwapChain* swapChain) : instance(instance),
				device(device),
				commandPool(commandPool),
				graphicsQueue(graphicsQueue),
				swapChain(swapChain)
{
	this->updatable = false;
}

void SkyBox::cleanup()
{
	textureCubeMap.cleanup();
	vertexBuffer.cleanup();
	indexBuffer.cleanup();
	uniformBuffer.cleanup();
	descriptorSet.cleanup();
}

void SkyBox::loadSkyBox(std::string filename, GltfImporter* gltfLoader, uint32_t uboSize)
{
	//load texture
	textureCubeMap = TextureCubeMap(device, commandPool, filename.data());
	textureCubeMap.submitImageToStageMemory();
	textureCubeMap.submitImageToDeviceMemory();
	std::vector<uint32_t> indicesOffsets = std::vector<uint32_t>();

	//load cube
	cube = gltfLoader->loadFromFile(device,
									commandPool,
									"../Models/cube.gltf",
									vertices,
									indices,
									indicesOffsets);

	//create uniform buffer containers
	int uniformBuffersContainerSize = 0;
	if (updatable)
		uniformBuffer = UniformBuffer("Skybox",
									  device,
									  uboSize,
									  swapChain->getSize());
	else
		uniformBuffer = UniformBuffer("Skybox", 
									  device,
									  uboSize,
									  1);
}

void SkyBox::submitSkyBoxData()
{
	vertexBuffer = VertexBuffer(device, commandPool, sizeof(vertices[0]) * static_cast<int>(vertices.size()));
	vertexBuffer.submitDataToStageBuffer(vertices);
	vertexBuffer.submitDataToDevice();

	indexBuffer = IndexBuffer(device, commandPool, sizeof(indices[0]) * static_cast<int>(indices.size()));
	indexBuffer.submitDataToStageBuffer(indices);
	indexBuffer.submitDataToDevice();
}

void SkyBox::ready()
{
	submitSkyBoxData();
	createDescriptorLayout();
	createDescriptorPool();
	createDescriptorSets();
}

void SkyBox::update(int currentImage)
{
	skyboxUniform ubo{};

	ubo.proj = glm::perspective(glm::radians(60.0f),
								swapChain->getSwapChainExtent().width / (float)swapChain->getSwapChainExtent().height,
								0.1f,
								100000.0f);
	ubo.proj[1][1] *= -1;

	ubo.view = ubo.view = camera->GetViewMatrix();
	//remove translation component
	ubo.view = glm::mat4(glm::mat3(ubo.view));
	ubo.skyboxIntensity = imGuiUi->globalShaderInputs.skyboxIntensity;
	ubo.exposure = imGuiUi->globalShaderInputs.exposure;
	ubo.useTonemap = static_cast<bool>(imGuiUi->globalShaderInputs.useTonemap);
	ubo.useSkybox = static_cast<bool>(imGuiUi->globalShaderInputs.useSkybox);

	uniformBuffer.submitDataToMemory(&ubo, currentImage);
}

void SkyBox::update(utils::Uniform* uboData)
{
	uniformBuffer.submitDataToMemory(uboData, 0);
}

void SkyBox::draw(VkCommandBuffer drawCommandbuffer, VkPipelineLayout pipelineLayout, int currentImage)
{

	VkDeviceSize offsets[] = { 0 };
	vkCmdBindVertexBuffers(drawCommandbuffer, 0, 1, &this->vertexBuffer.vertexBuffer, offsets);
	vkCmdBindIndexBuffer(drawCommandbuffer, this->indexBuffer.indexBuffer, 0, VK_INDEX_TYPE_UINT32);

	//bind the scene descriptor set
	vkCmdBindDescriptorSets(drawCommandbuffer,
		VK_PIPELINE_BIND_POINT_GRAPHICS,
		pipelineLayout,
		0,
		1,
		&descriptorSet.getDescriptorSetsHandle()[currentImage],
		0,
		nullptr);

	vkCmdDrawIndexed(drawCommandbuffer, static_cast<uint32_t>(this->indices.size()), 1, 0, 0, 0);
}

void SkyBox::draw(VkCommandBuffer drawCommandbuffer, VkPipelineLayout pipelineLayout)
{
	VkDeviceSize offsets[] = { 0 };
	vkCmdBindVertexBuffers(drawCommandbuffer, 0, 1, &this->vertexBuffer.vertexBuffer, offsets);
	vkCmdBindIndexBuffer(drawCommandbuffer, this->indexBuffer.indexBuffer, 0, VK_INDEX_TYPE_UINT32);

	//bind the scene descriptor set
	vkCmdBindDescriptorSets(drawCommandbuffer,
		VK_PIPELINE_BIND_POINT_GRAPHICS,
		pipelineLayout,
		0,
		1,
		&descriptorSet.getDescriptorSetsHandle()[0],
		0,
		nullptr);

	vkCmdDrawIndexed(drawCommandbuffer, static_cast<uint32_t>(this->indices.size()), 1, 0, 0, 0);
}

void SkyBox::createDescriptorLayout()
{

	/*
	std::vector<VkDescriptorSetLayoutBinding> bindings;
	VkDescriptorSetLayoutBinding binding = {};

	//Define the descriptor set layout
	binding = initializers::descriptorSetLayoutBinding(0,
													   VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
													   VK_SHADER_STAGE_ALL);

	bindings.push_back(binding);

	binding = initializers::descriptorSetLayoutBinding(1,
														VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
														VK_SHADER_STAGE_FRAGMENT_BIT);

	bindings.push_back(binding);

	VkDescriptorSetLayoutCreateInfo layoutInfo{};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = bindings.size();
	layoutInfo.pBindings = bindings.data();

	VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &descriptorLayout));
	*/
}

void SkyBox::createDescriptorPool()
{
	/*
	int descriptorCount = 0;
	if (updatable) {
		descriptorCount = swapChain->getSize();
	}
	else {
		descriptorCount = 1;
	}

	std::array<VkDescriptorPoolSize, 2> poolSizes{};
	poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[0].descriptorCount = descriptorCount;
	poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSizes[1].descriptorCount = descriptorCount;

	VkDescriptorPoolCreateInfo poolInfo{};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = static_cast<uint32_t>(
											descriptorCount
											);

	VK_CHECK_RESULT(vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptorPool));
	*/
}

void SkyBox::createDescriptorSets()
{
	int descriptorCount = 0;
	if (this->updatable)
		descriptorCount = swapChain->getSize();
	else
		descriptorCount = 1;

	descriptorSet = DescriptorSet("Skybox",
								  { {0,&uniformBuffer} },
								  {},
								  { {1,{textureCubeMap}} }, 
							      {},
								  VK_SHADER_STAGE_ALL,
								  device,
								  instance,
								  commandPool, // 1 buffer per swapChain
								  descriptorCount
								);

	/*
	int descriptorCount = 0;
	if (updatable) {
		descriptorCount = swapChain->getSize();
	}
	else {
		descriptorCount = 1;
	}
	std::vector<VkDescriptorSetLayout> uniformLayouts(descriptorCount, descriptorLayout);

	//Create the descriptor set
	VkDescriptorSetAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>(descriptorCount);
	allocInfo.pSetLayouts = uniformLayouts.data();


	descriptorSets.resize(descriptorCount);
	VK_CHECK_RESULT(vkAllocateDescriptorSets(device, &allocInfo, descriptorSets.data()));

	std::vector<VkWriteDescriptorSet> descriptorWrites;
	VkWriteDescriptorSet writeDescriptorSet = {};
	VkDescriptorImageInfo imageInfo{};

	for (int i = 0; i < descriptorCount; i++) {

		VkDescriptorBufferInfo bufferInfo{};
		bufferInfo.buffer = uniformBuffersContainer[i].getBuffer();
		bufferInfo.offset = 0;
		bufferInfo.range = uniformBuffersContainer[i].size;

		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.dstSet = descriptorSets[i];
		writeDescriptorSet.dstBinding = 0;
		writeDescriptorSet.dstArrayElement = 0;
		writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		writeDescriptorSet.descriptorCount = 1;
		writeDescriptorSet.pBufferInfo = &bufferInfo;
		descriptorWrites.push_back(writeDescriptorSet);

		writeDescriptorSet = {};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = textureCubeMap.textureImageView;
		imageInfo.sampler = textureCubeMap.textureSampler;

		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.dstSet = descriptorSets[i];
		writeDescriptorSet.dstBinding = 1;
		writeDescriptorSet.dstArrayElement = 0;
		writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		writeDescriptorSet.descriptorCount = 1;
		writeDescriptorSet.pImageInfo = &imageInfo;
		descriptorWrites.push_back(writeDescriptorSet);

		vkUpdateDescriptorSets(device, descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);
	}
	*/
}

