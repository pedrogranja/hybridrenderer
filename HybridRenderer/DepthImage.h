#pragma once
#include "vulkan/vulkan.h"
#include "utils.hpp"

class DepthImage
{
protected:
	VkPhysicalDevice physicalDevice;
	VkDevice device;

public:
	VkImage depthImage;
	VkDeviceMemory depthImageMemory;
	VkImageView depthImageView;
	VkFormat depthFormat;

	void init(VkPhysicalDevice physicalDevice, 
			  VkDevice device,
			  uint32_t width,
			  uint32_t height,
			  VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_1_BIT);
	void cleanup();
};

