#pragma once

#include<chrono>

class Clock
{
    static Clock* instance;
    std::chrono::steady_clock::time_point prevFrameTimestamp;
    std::chrono::steady_clock::time_point startTimestamp;
    

    // Private constructor so that no objects can be created.
    Clock();

public:
    float deltaTime;
    float time;

    static Clock* getInstance() {
        if (!instance)
            instance = new Clock();
        return instance;
    }
    void update();
};