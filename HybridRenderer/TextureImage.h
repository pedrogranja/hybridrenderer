#pragma once
#include "VulkanDevice.h"
#include "stb_image.h"
#include "utils.hpp"

class TextureImage
{
	VulkanDevice device;
	VkCommandPool commandPool;
	VkBuffer stagingBuffer = VK_NULL_HANDLE;
	VkDeviceMemory stagingMemory;

public:
	int texWidth;
	int texHeight;
	int texChannels;
	stbi_uc* pixels = nullptr;
	VkDeviceSize imageSize;
	std::vector<VkImage> textureImages;
	std::vector<VkDeviceMemory> textureImageMemories;
	std::vector<VkImageView> textureImageViews;
	std::vector<VkSampler> textureSamplers;
	VkFormat format;
	bool stbiDelete;
	bool stageBuffersDeleted;
	bool inDeviceMemory;
	int numImages;
	std::string name;

	TextureImage();

	TextureImage(std::string name,
				 VulkanDevice& device,
				 VkCommandPool commandPool,
				 int texWidth,
				 int texHeight,
				 int texChannels,
				 VkFormat format,
				 int usage,
				 int numImages = 1,
				 bool createSampler = true,
				 int mipLevels = 1,
				 VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_1_BIT,
				 VkImageLayout initialImageLayout = VK_IMAGE_LAYOUT_UNDEFINED);

	TextureImage(std::string name, 
				 VulkanDevice& device,
				 VkCommandPool commandPool,
				 stbi_uc* pixels,
				 VkDeviceSize size,
				 int texWidth,
				 int texHeight, 
				 int texChannels,
				 int numImages = 1,
				 VkFormat format = VK_FORMAT_R8G8B8A8_SRGB);

	TextureImage(std::string name, 
				 VulkanDevice& device,
				 VkCommandPool commandPool,
				 const char* imageFile,
				 int numImages = 1,
				 VkFormat format = VK_FORMAT_R8G8B8A8_SRGB);

	void submitImageToStageMemory();
	void submitImageToDeviceMemory();
	void cleanup();

private:
	void importImage(const char* imageFile);
};