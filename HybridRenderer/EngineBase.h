#pragma once
#include <iostream>
#include <stdexcept>
#include <functional>
#include <cstdlib>
#include <cstring>
#include <optional>
#include <vector>
#include <set>
#include <cstdint> 
#include <algorithm>
#include <unordered_map>
#include "Initializers2.hpp"
#include "utils.hpp"
#include "config.hpp"
#include "SwapChain.h"
#include "UniformBuffer.h"
#include "TextureImage.h"
#include "DepthImage.h"
#include "SceneGraph.h"
#include "GltfImporter.h"
#include "SkyBox.h"
#include "Mouse.hpp"
#include "Camera.h"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_vulkan.h"
#include "ImGuiRenderpass.h"
#include "ImGuiUi.h"
#include "BlurPostProcessing.h"
#include "BloomBlendPostProcessing.h"
#include "Window.h"
#include "VulkanInstance.h"
#include "VulkanDevice.h"
#include "VertexFragPipeline.h"
#include "RenderTarget.h"
#include "FrameGraph.h"
#include "DescriptorSet.h"
#include <unordered_map>
#include "Screenshot.hpp"
#include "UIRenderable.h"

//Vulkan
#include <vulkan/vulkan_core.h>

//GLFW
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h> //glfw gives us the window to which we render (Vulkan)

//GLM
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <chrono>

class EngineBase
{
protected:
    Window window; //GLFW window where content will be displayed
    VulkanInstance instance;
    VulkanDevice device;
    SwapChain swapChain;
    Camera camera;
    
    VertexFragPipeline pipeline;
    VertexFragPipeline skyboxPipeline;
    VertexFragPipeline postProcessingHorPipeline;
    VertexFragPipeline postProcessingVerPipeline;
    VertexFragPipeline postProcessingBlendBloomPipeline;

    VkCommandPool commandPool;
    std::vector<VkCommandBuffer> commandBuffers;
    
    //depth render target
    RenderTarget depthRenderTarget;

    RenderTarget colorRenderTarget;
    RenderTarget bloomRenderTarget;
    Pass renderPass;
    //SceneRenderPass renderPass;
    RenderTarget blurHorRenderTarget;
    Pass blurHorRenderPass;
    RenderTarget blurVerRenderTarget;
    Pass blurVerRenderPass;
    Pass blendBloomRenderPass;
    ImGuiRenderpass imGuiRenderPass;

    //synchronization
    std::vector<VkSemaphore> imageAvailableSemaphores; //marks if the image is available for drawing (not being used for displaying in screen) (GPU-GPU synchronization)
    std::vector<VkSemaphore> renderFinishedSemaphores; //marks if the render has finished and the image is ready to be presented (GPU-GPU synchronization)
    std::vector<VkFence> inFlightFences;               //make sure we wait for the GPU to start reusing the objects from a specific frame (CPU-GPU synchronization) 
    std::vector<VkFence> imagesInFlight;               //make sure we are not using the image already (vkAcquireNextImageKHR may return out of order 
                                                       //CPU-GPU synchronization)
    size_t currentFrame = 0;

    //number of samples (MSAA)
    VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_1_BIT;

    //models
    Scene scene;
    SkyBox skyBox;

    //BRDF LUT
    RenderTarget brdfLutRenderTarget;

    //Irradiance
    TextureCubeMap irradiance;

    //mip map cubemap
    TextureCubeMap mipMapCubemap;
    uint32_t maxLevelRoughnessMipMap;

    //color resolve
    TextureImage colorResolveImage;

    ImGuiUi imGuiUi;
    int frameIndex = 0;

    struct BlurParams : public UIRenderable {
        //we need a well defined structure to send to the graphics card using uniform buffers
        struct ToSubmit {
            alignas(4) float blurScale = 0.0f;
            alignas(4) float blurStrength = 0.8f;
            alignas(4) int blurHorizontal = 1;
        } toSubmit;

        virtual void renderUI() {
            ImGui::Begin("Bloom Parameters");
            ImGui::DragFloat("Blur Size", &(toSubmit.blurScale), 0.2f, 0.0f, 10.0f);
            ImGui::DragFloat("Blur Strength", &(toSubmit.blurStrength), 0.2f, 0.0f, 10.0f);
            ImGui::End();
        };
    } blurParamsHor, blurParamsVer;

    struct SceneParams : public utils::Uniform {
        alignas(4) float maxLevelRoughnessMipMap;
        alignas(16) glm::vec3 cameraPos;
        alignas(16) Sun suns[1024];
        alignas(4) int numSuns;
    } sceneParams;

    //Scene
    UniformBuffer sceneUniform;
    DescriptorSet sceneDescriptorSet;
    //Blur Hor
    UniformBuffer blurHorUniformBuffer;
    DescriptorSet blurHorDescriptorSet;
    //Blur Ver
    UniformBuffer blurVerUniformBuffer;
    DescriptorSet blurVerDescriptorSet;
    //Bloom Blend
    DescriptorSet BloomBlendDescriptorSet;

public:
    EngineBase();
    void initImGui();
    void initVulkan(); // Initiates and configures basic objects at a global level (Logical device, extensions, etc.)
    void updateSamples();
    virtual void update(uint32_t imageIndex);
    void drawFrame();
    void run();
    void mainLoop();
    virtual void cleanupSwapChain();
    void recreateSwapChain();
    void cleanup();

protected:
    //initialization
    void generateBRDFLut();
    void generateIrradianceCubemap();
    void generateMipMaps();
    void loadScene(std::vector<std::string> scenePaths);
    void deleteScene();
    virtual void createUI();
    void createCommandPool();
    virtual void createDrawCommandBuffers();
    void createSynchronizationObjects();
    virtual void defineRenderer();
    virtual void finishedDrawing() {};
};