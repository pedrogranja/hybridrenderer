#version 450

layout (binding = 0) uniform UBO 
{
	mat4 projection;
	mat4 view;
	float skyboxIntensity;
	float exposure;
	bool useTonemap;
	bool useSkybox;
} ubo;

// From http://filmicgames.com/archives/75
// and sacha willems https://github.com/SaschaWillems/Vulkan/blob/master/data/shaders/glsl/pbrtexture/pbrtexture.frag
vec3 Uncharted2Tonemap(vec3 x)
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;
	return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

vec4 tonemap(vec4 color)
{
	vec3 outcol = Uncharted2Tonemap(color.rgb);
	outcol = outcol * (1.0f / Uncharted2Tonemap(vec3(11.2f)));	
	return vec4(outcol,1.0);
}

layout (binding = 1) uniform samplerCube samplerEnv;

layout (location = 0) in vec3 inUVW;

layout (location = 0) out vec4 outColor;

void main() 
{
	if(!ubo.useSkybox) {
		outColor = vec4(0.0f);
		return;
	}
	vec3 color = texture(samplerEnv, inUVW).rgb;
	if(ubo.useTonemap)
		outColor = tonemap(ubo.skyboxIntensity * ubo.exposure * vec4(color, 1.0));
	else
		outColor = ubo.skyboxIntensity * vec4(color, 1.0);
	//outColor = vec4(0.0f);
}