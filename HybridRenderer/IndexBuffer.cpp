#include "IndexBuffer.h"

IndexBuffer::IndexBuffer()
{
}

IndexBuffer::IndexBuffer(VulkanDevice& device, 
                         VkCommandPool commandPool, 
                         int size) : device(device), 
                                     commandPool(commandPool), 
                                     size(size)
{
    utils::createBuffer(device.getLogicalDeviceHandle(),
                        device.getPhysicalDeviceHandle(),
                        size,
                        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                        stagingBuffer,
                        stagingMemory);

    utils::createBuffer(device.getLogicalDeviceHandle(),
                        device.getPhysicalDeviceHandle(),
                        size, 
                        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                        indexBuffer,
                        indexMemory);

    devicePntr = utils::getBufferDeviceAddress(indexBuffer, device.getLogicalDeviceHandle());
}

void IndexBuffer::submitDataToStageBuffer(std::vector<uint32_t> indices)
{
    void* data;
    vkMapMemory(device.getLogicalDeviceHandle(), stagingMemory, 0, this->size, 0, &data);
    memcpy(data, indices.data(), (size_t)this->size);
    vkUnmapMemory(device.getLogicalDeviceHandle(), stagingMemory);
}

void IndexBuffer::submitDataToDevice()
{
    utils::copyBuffer(device.getLogicalDeviceHandle(), device.getPhysicalDeviceHandle(),device.getQueues().graphicsQueue, commandPool, stagingBuffer, indexBuffer, size);
    vkDestroyBuffer(device.getLogicalDeviceHandle(), stagingBuffer, nullptr);
    vkFreeMemory(device.getLogicalDeviceHandle(), stagingMemory, nullptr);
}

void IndexBuffer::cleanup()
{
    vkDestroyBuffer(device.getLogicalDeviceHandle(), indexBuffer, nullptr);
    vkFreeMemory(device.getLogicalDeviceHandle(), indexMemory, nullptr);
}
