#pragma once
#include "VulkanDevice.h"
#include "RaytracingUtils.hpp"
#include "RayTracingScratchBuffer.h"
#include "Buffer.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include <glm/gtx/string_cast.hpp>

class BottomLevelAS
{
private:
	VulkanDevice device;
	VulkanInstance instance;
	VkCommandPool commandPool;

	VkAccelerationStructureKHR accelerationStructure;
	uint64_t bottomLevelAsDeviceHandle;
	VkAccelerationStructureInstanceKHR blasInstance{};
	RayTracingScratchBuffer scratchBuffer;
	Buffer blasBuffer;

	int firstVertex;
	int vertexCount;
	VertexBuffer vertexBuffer;
	int firstIndex;
	int indexCount;
	IndexBuffer indexBuffer;
	int instanceId;

	Buffer buffer;
	VkBuffer instanceBuffer;
	uint64_t instanceBufferHandle;
	VkDeviceMemory instanceBufferMemory;

public:
	BottomLevelAS();
	BottomLevelAS(int firstVertex, 
				  int vertexCount, 
				  VertexBuffer& vertexBuffer,
				  int firstIndex, 
				  int indexCount, 
				  IndexBuffer& indexBuffer, 
				  VulkanDevice& device,
				  VulkanInstance& instance,
				  VkCommandPool& commandPool);
	void update(glm::mat4& modelMatrix, uint32_t id);
	void cleanup();
	uint64_t getDeviceHandle();
	uint64_t getInstanceBufferHandle();
	VkAccelerationStructureInstanceKHR& getAccelerationStructureInstanceKHR();

private:
	void createBottomLevelAS();
};