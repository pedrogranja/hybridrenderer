This projects aims at creating a hybrid real time renderer, mixing rasterization and ray tracing techniques in order to archieve real time performance and photo realistic quality.

Requirements:
- An Nvidia ray tracing graphics card

How to install?
- Simply download. All the libraries and necessary files are already contained within the project
- Import the project into Visual Studio
